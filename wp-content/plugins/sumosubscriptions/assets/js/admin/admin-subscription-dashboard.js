/* global sumosubscriptions_dashboard */

jQuery( function( $ ) {

    // sumosubscriptions_dashboard is required to continue, ensure the object exists
    if ( typeof sumosubscriptions_dashboard === 'undefined' ) {
        return false ;
    }

    var is_blocked = function( $node ) {
        return $node.is( '.processing' ) || $node.parents( '.processing' ).length ;
    } ;

    /**
     * Block a node visually for processing.
     *
     * @param {JQuery Object} $node
     */
    var block = function( $node ) {
        if ( ! is_blocked( $node ) ) {
            $node.addClass( 'processing' ).block( {
                message : null ,
                overlayCSS : {
                    background : '#fff' ,
                    opacity : 0.6
                }
            } ) ;
        }
    } ;

    /**
     * Unblock a node after processing is complete.
     *
     * @param {JQuery Object} $node
     */
    var unblock = function( $node ) {
        $node.removeClass( 'processing' ).unblock() ;
    } ;

    var $notes_div = $( '#sumosubscription_log_information' ).closest( 'div' ) ;
    var $exporter_div = $( '.sumo-subscription-exporter-wrapper' ).closest( 'div' ) ;
    var $cancel_method_div = $( '#sumosubscription_cancel_methods, #sumosubscription_details' ).closest( 'div' ) ;

    var edit_subscription = {
        /**
         * Manage Subscription Editable UI events.
         */
        init : function() {
            this.triggerOnPageLoad() ;

            $( document ).on( 'click' , '.sumo_add_note' , this.addSubscriptionNote ) ;
            $( document ).on( 'click' , '.sumo_delete_note' , this.deleteSubscriptionNote ) ;
            $( document ).on( 'click' , '#sumo_view_unpaid_renewal_order' , this.viewUnpaidRenewalOrder ) ;
            $( document ).on( 'change' , '.sumo_subscription_cancel_method_via' , this.uponToggleCancelMethod ) ;
            $( document ).on( 'click' , '.sumo_submit_subscription_cancel_request' , this.submitCancelRequest ) ;
            $( document ).on( 'click' , '.sumo_revoke_subscription_cancel_request' , this.revokeCancelRequest ) ;
            $( document ).on( 'click' , 'form.subscription-exporter > div.export-actions > input' , this.subscriptionExporter.export ) ;
        } ,
        triggerOnPageLoad : function() {

            this.importDatepicker() ;
            $( '#sumo_unpaid_renewal_order' ).hide() ;
        } ,
        uponToggleCancelMethod : function( evt ) {
            var $this = $( evt.currentTarget ) ;

            $( '.sumo_submit_subscription_cancel_request' ).slideUp() ;
            $( '#sumo_subscription_cancel_scheduled_on' ).slideUp() ;

            if ( $.inArray( $this.val() , Array( 'immediate' , 'end_of_billing_cycle' , 'scheduled_date' ) ) !== - 1 ) {
                $( '.sumo_submit_subscription_cancel_request' ).slideDown() ;
            }
            if ( $this.val() === 'scheduled_date' ) {
                $( '#sumo_subscription_cancel_scheduled_on' ).slideDown() ;
                $( '#sumo_subscription_cancel_scheduled_on' ).datepicker( {
                    minDate : 0 ,
                    maxDate : $( '#sumo_subscription_data' ).data( 'next_payment_date' ) ,
                    changeMonth : true ,
                    dateFormat : 'yy-mm-dd' ,
                    numberOfMonths : 1 ,
                    showButtonPanel : true ,
                    defaultDate : '' ,
                    showOn : 'focus' ,
                    buttonImageOnly : true ,
                } ) ;
            }
        } ,
        submitCancelRequest : function( evt ) {
            var $this = $( evt.currentTarget ) ;
            var $subscription_id = $this.data( 'subscription_id' ) ;

            switch ( $( '.sumo_subscription_cancel_method_via' ).val() ) {
                case 'immediate':
                    if ( sumosubscriptions_dashboard.display_dialog_upon_cancel ) {
                        if ( window.confirm( sumosubscriptions_dashboard.warning_message_upon_immediate_cancel ) ) {
                            edit_subscription.requestCancelMethod( $subscription_id , 'immediate' ) ;
                            return true ;
                        }
                    } else {
                        edit_subscription.requestCancelMethod( $subscription_id , 'immediate' ) ;
                    }
                    break ;
                case 'end_of_billing_cycle':
                    if ( sumosubscriptions_dashboard.display_dialog_upon_cancel ) {
                        if ( window.confirm( sumosubscriptions_dashboard.warning_message_upon_at_the_end_of_billing_cancel ) ) {
                            edit_subscription.requestCancelMethod( $subscription_id , 'end_of_billing_cycle' ) ;
                            return true ;
                        }
                    } else {
                        edit_subscription.requestCancelMethod( $subscription_id , 'end_of_billing_cycle' ) ;
                    }
                    break ;
                case 'scheduled_date':
                    if ( $( '#sumo_subscription_cancel_scheduled_on' ).val() === '' ) {
                        window.alert( sumosubscriptions_dashboard.warning_message_upon_invalid_date ) ;
                        return false ;
                    }

                    if ( sumosubscriptions_dashboard.display_dialog_upon_cancel ) {
                        if ( window.confirm( sumosubscriptions_dashboard.warning_message_upon_on_the_scheduled_date_cancel.replace( '[sumo_cancel_scheduled_date]' , $( '#sumo_subscription_cancel_scheduled_on' ).val() ) ) ) {
                            edit_subscription.requestCancelMethod( $subscription_id , 'scheduled_date' ) ;
                            return true ;
                        }
                    } else {
                        edit_subscription.requestCancelMethod( $subscription_id , 'scheduled_date' ) ;
                    }
                    break ;
            }
            return false ;
        } ,
        revokeCancelRequest : function() {
            if ( ! sumosubscriptions_dashboard.display_dialog_upon_revoking_cancel ) {
                return true ;
            }

            if ( window.confirm( sumosubscriptions_dashboard.warning_message_upon_revoking_cancel ) ) {
                return true ;
            }
            return false ;
        } ,
        requestCancelMethod : function( subscription_id , requested_method ) {

            $.blockUI.defaults.overlayCSS.cursor = 'wait' ;
            block( $cancel_method_div ) ;

            $.ajax( {
                type : 'POST' ,
                url : sumosubscriptions_dashboard.wp_ajax_url ,
                dataType : 'json' ,
                data : {
                    action : 'sumosubscription_cancel_request' ,
                    security : sumosubscriptions_dashboard.cancel_request_nonce ,
                    cancel_method_requested_by : 'admin' ,
                    subscription_id : subscription_id ,
                    cancel_method_requested : requested_method ,
                    scheduled_date : $( '#sumo_subscription_cancel_scheduled_on' ).val()
                } ,
                success : function() {
                    unblock( $cancel_method_div ) ;
                    location.reload() ;
                }
            } ) ;
        } ,
        addSubscriptionNote : function( evt ) {
            evt.preventDefault() ;
            var $content = $( '#add_subscription_note' ).val() ;
            var $post_id = $( evt.currentTarget ).attr( 'data-id' ) ;

            $.blockUI.defaults.overlayCSS.cursor = 'wait' ;
            block( $notes_div ) ;

            $.ajax( {
                type : 'POST' ,
                url : sumosubscriptions_dashboard.wp_ajax_url ,
                data : {
                    action : 'sumosubscription_add_subscription_note' ,
                    security : sumosubscriptions_dashboard.add_note_nonce ,
                    content : $content ,
                    post_id : $post_id
                } ,
                success : function( data ) {
                    $( 'ul.subscription_notes' ).prepend( data ) ;
                    $( '#add_subscription_note' ).val( '' ) ;
                } ,
                complete : function() {
                    unblock( $notes_div ) ;
                }
            } ) ;
        } ,
        deleteSubscriptionNote : function( evt ) {
            var $this = $( evt.currentTarget ) ;
            var $note_to_delete = $this.parent().parent().attr( 'rel' ) ;

            $.blockUI.defaults.overlayCSS.cursor = 'wait' ;
            block( $notes_div ) ;

            $.ajax( {
                type : 'POST' ,
                url : sumosubscriptions_dashboard.wp_ajax_url ,
                data : {
                    action : 'sumosubscription_delete_subscription_note' ,
                    security : sumosubscriptions_dashboard.delete_note_nonce ,
                    delete_id : $note_to_delete
                } ,
                success : function( data ) {
                    if ( data === true ) {
                        $this.parent().parent().remove() ;
                    }
                } ,
                complete : function() {
                    unblock( $notes_div ) ;
                }
            } ) ;
            return false ;
        } ,
        subscriptionExporter : {
            export : function( evt ) {
                $( evt.currentTarget ).closest( 'form' ).find( '#exported_data' ).val( '' ) ;

                $.blockUI.defaults.overlayCSS.cursor = 'wait' ;
                block( $exporter_div ) ;

                $.ajax( {
                    type : 'POST' ,
                    url : sumosubscriptions_dashboard.wp_ajax_url ,
                    data : {
                        action : 'sumosubscription_init_data_export' ,
                        security : sumosubscriptions_dashboard.exporter_nonce ,
                        exportDataBy : $( evt.currentTarget ).closest( 'form' ).serialize() ,
                    } ,
                    success : function( response ) {
                        if ( 'done' === response.export ) {
                            window.location = response.redirect_url ;
                        } else if ( 'processing' === response.export ) {
                            var i , j = 1 , chunkedData , chunk = 10 , step = 0 ;

                            for ( i = 0 , j = response.original_data.length ; i < j ; i += chunk ) {
                                chunkedData = response.original_data.slice( i , i + chunk ) ;
                                step += chunkedData.length ;
                                edit_subscription.subscriptionExporter.processExport( response.original_data.length , chunkedData , step ) ;
                            }
                        } else {
                            window.location = response.redirect_url ;
                        }
                    } ,
                    complete : function() {
                        unblock( $exporter_div ) ;
                    }
                } ) ;
            } ,
            processExport : function( originalDataLength , chunkedData , step ) {

                $.ajax( {
                    type : 'POST' ,
                    url : sumosubscriptions_dashboard.wp_ajax_url ,
                    async : false ,
                    dataType : 'json' ,
                    data : {
                        action : 'sumosubscription_handle_exported_data' ,
                        security : sumosubscriptions_dashboard.exporter_nonce ,
                        exportDataBy : $( 'form.subscription-exporter' ).serialize() ,
                        originalDataLength : originalDataLength ,
                        chunkedData : chunkedData ,
                        step : step ,
                        generated_data : $( 'form.subscription-exporter' ).find( '#exported_data' ).val() ,
                    } ,
                    success : function( response ) {
                        if ( 'done' === response.export ) {
                            window.location = response.redirect_url ;
                        } else if ( 'processing' === response.export ) {
                            $( 'form.subscription-exporter' ).find( '#exported_data' ).val( JSON.stringify( response.generated_data ) ) ;
                        } else {
                            window.location = response.redirect_url ;
                        }
                    }
                } ) ;
            } ,
        } ,
        viewUnpaidRenewalOrder : function( evt ) {
            var $this = $( evt.currentTarget ) ;

            $this.text( 'Hide' ) ;

            if ( $( '#sumo_unpaid_renewal_order' ).is( ':visible' ) ) {
                $this.text( sumosubscriptions_dashboard.view_renewal_orders_text ) ;
            }

            $( '#sumo_unpaid_renewal_order' ).slideToggle( 'fast' ) ;
            return false ;
        } ,
        importDatepicker : function() {

            $( '.date-picker' ).datepicker( {
                minDate : 0 ,
                changeMonth : true ,
                dateFormat : 'yy-mm-dd' ,
                numberOfMonths : 1 ,
                showButtonPanel : true ,
                defaultDate : '' ,
                showOn : 'focus' ,
                buttonImageOnly : true
            } ) ;
            $( '#sumo_subscription_from_date' ).datepicker( {
                changeMonth : true ,
                dateFormat : 'yy-mm-dd' ,
                numberOfMonths : 1 ,
                showButtonPanel : true ,
                defaultDate : '' ,
                showOn : 'focus' ,
                buttonImageOnly : true ,
                onClose : function( selectedDate ) {
                    var maxDate = new Date( Date.parse( selectedDate ) ) ;
                    maxDate.setDate( maxDate.getDate() + 1 ) ;
                    $( '#sumo_subscription_to_date' ).datepicker( 'option' , 'minDate' , maxDate ) ;
                }
            } ) ;
            $( '#sumo_subscription_to_date' ).datepicker( {
                changeMonth : true ,
                dateFormat : 'yy-mm-dd' ,
                numberOfMonths : 1 ,
                showButtonPanel : true ,
                defaultDate : '' ,
                showOn : 'focus' ,
                buttonImageOnly : true ,
            } ) ;
        }
    } ;

    edit_subscription.init() ;
} ) ;