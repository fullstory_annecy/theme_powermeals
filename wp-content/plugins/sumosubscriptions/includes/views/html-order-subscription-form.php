<table class="shop_table sumo_order_subscription">
    <tr class="sumo_order_subscription_users_status">
        <td>
            <label><?php echo get_option( 'sumo_order_subsc_checkout_label_option' ) ; ?></label>
            <input type="checkbox" id="sumo_order_subscription_status" <?php checked( 'yes' === self::$subscribed_plan_props[ 'subscribed' ] || self::$get_option[ 'default_subscribed' ] , true ) ; ?> />
        </td>
    </tr>
    <?php
    if( self::$get_option[ 'can_user_select_plan' ] ) {
        ?>
        <tr class="sumo_order_subscription_users_duration">
            <td>
                <label><?php echo get_option( 'sumo_order_subsc_duration_checkout_label_option' ) ; ?></label>
            </td>
            <td>
                <select id="sumo_order_subscription_duration">
                    <?php
                    $default_period  = 'D' ;
                    $duration_period = sumosubs_get_duration_period_selector() ;

                    foreach( self::$get_option[ 'duration_period_selector' ] as $index => $period ):
                        if( 0 === $index ) {
                            $default_period = $period ;
                        }
                        ?>
                        <option value="<?php echo $period ; ?>" <?php selected( self::$subscribed_plan_props[ 'duration_period' ] , $period ) ; ?>><?php echo $duration_period[ $period ] ; ?></option>
                    <?php endforeach ; ?>
                </select>
            </td>
        </tr>
        <tr class="sumo_order_subscription_users_duration_value">
            <td>
                <label><?php echo get_option( 'sumo_order_subsc_duration_value_checkout_label_option' ) ; ?></label>
            </td>
            <td>
                <select id="sumo_order_subscription_duration_value" >
                    <?php
                    $selected_duration_period = self::$subscribed_plan_props[ 'duration_period' ] ? self::$subscribed_plan_props[ 'duration_period' ] : $default_period ;

                    foreach( sumo_get_subscription_duration_options( $selected_duration_period , true , self::$get_option[ 'min_duration_length_user_can_select' ][ $selected_duration_period ] , self::$get_option[ 'max_duration_length_user_can_select' ][ $selected_duration_period ] ) as $duration_value_key => $label ) {
                        ?>
                        <option value="<?php echo $duration_value_key ; ?>" <?php selected( $duration_value_key , self::$subscribed_plan_props[ 'duration_length' ] ) ; ?>><?php echo $label ; ?></option>
                    <?php } ?>
                </select>
            </td>
        </tr>
        <?php if( self::$get_option[ 'can_user_select_recurring_length' ] ) : ?>
            <tr class="sumo_order_subscription_users_recurring">
                <td>
                    <label><?php echo get_option( 'sumo_order_subsc_recurring_checkout_label_option' ) ; ?></label>
                </td>
                <td>
                    <select id="sumo_order_subscription_recurring" >
                        <?php
                        foreach( sumo_get_subscription_recurring_options( 'last' , self::$get_option[ 'min_recurring_length_user_can_select' ] ) as $recurring_key => $label ) {
                            ?>
                            <option value="<?php echo $recurring_key ; ?>" <?php selected( $recurring_key , self::$subscribed_plan_props[ 'recurring_length' ] ) ; ?>><?php echo $label ; ?></option>
                        <?php } ?>
                    </select>
                </td>
            </tr>
        <?php endif ; ?>
        <?php
    }
    ?>
</table>