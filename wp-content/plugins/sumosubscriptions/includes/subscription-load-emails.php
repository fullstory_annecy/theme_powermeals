<?php

if( ! defined( 'ABSPATH' ) ) {
    exit ; // Exit if accessed directly
}

/**
 * Load subscription email classes.
 * @param array $emails
 * @return array
 */
function sumo_load_subscription_email_classes( $emails ) {
    include_once 'abstracts/abstract-sumo-subscriptions-email.php' ;

    $emails[ 'SUMOSubscriptions_New_Order_Email' ]                           = include( 'emails/class-sumo-subscriptions-new-order-email.php' ) ;
    $emails[ 'SUMOSubscriptions_Pause_Order_Email' ]                         = include( 'emails/class-sumo-subscriptions-pause-order-email.php' ) ;
    $emails[ 'SUMOSubscriptions_Completed_Order_Email' ]                     = include( 'emails/class-sumo-subscriptions-completed-order-email.php' ) ;
    $emails[ 'SUMOSubscriptions_Processing_Order_Email' ]                    = include( 'emails/class-sumo-subscriptions-processing-order-email.php' ) ;
    $emails[ 'SUMOSubscriptions_Cancelled_Order_Email' ]                     = include( 'emails/class-sumo-subscriptions-cancelled-order-email.php' ) ;
    $emails[ 'SUMOSubscriptions_Expired_Order_Email' ]                       = include( 'emails/class-sumo-subscriptions-expired-order-email.php' ) ;
    $emails[ 'SUMOSubscriptions_Manual_Invoice_Order_Email' ]                = include( 'emails/class-sumo-subscriptions-manual-invoice-order-email.php' ) ;
    $emails[ 'SUMOSubscriptions_Automatic_Charging_Reminder_Email' ]         = include( 'emails/class-sumo-subscriptions-automatic-charging-reminder-email.php' ) ;
    $emails[ 'SUMOSubscriptions_Auto_to_Manual_Subscription_Renewal_Email' ] = include( 'emails/class-sumo-subscriptions-auto-to-manual-subscription-renewal-email.php' ) ;
    $emails[ 'SUMOSubscriptions_Preapproval_Access_Revoked_Email' ]          = include( 'emails/class-sumo-subscriptions-preapproval-access-revoked-email.php' ) ;
    $emails[ 'SUMOSubscriptions_Automatic_Renewed_Order_Email' ]             = include( 'emails/class-sumo-subscriptions-automatic-renewed-order-email.php' ) ;
    $emails[ 'SUMOSubscriptions_Manual_Suspended_Order_Email' ]              = include( 'emails/class-sumo-subscriptions-manual-suspended-order-email.php' ) ;
    $emails[ 'SUMOSubscriptions_Automatic_Suspended_Order_Email' ]           = include( 'emails/class-sumo-subscriptions-automatic-suspended-order-email.php' ) ;
    $emails[ 'SUMOSubscriptions_Manual_Overdue_Order_Email' ]                = include( 'emails/class-sumo-subscriptions-manual-overdue-order-email.php' ) ;
    $emails[ 'SUMOSubscriptions_Automatic_Overdue_Order_Email' ]             = include( 'emails/class-sumo-subscriptions-automatic-overdue-order-email.php' ) ;
    $emails[ 'SUMOSubscriptions_Cancel_Request_Submitted' ]                  = include( 'emails/class-sumo-subscriptions-cancel-request-submitted-email.php' ) ;
    $emails[ 'SUMOSubscriptions_Cancel_Request_Revoked' ]                    = include( 'emails/class-sumo-subscriptions-cancel-request-revoked-email.php' ) ;
    $emails[ 'SUMOSubscriptions_Turnoff_Auto_Payments_Success_Email' ]       = include( 'emails/class-sumo-subscriptions-turnoff-auto-payments-success-email.php' ) ;
    $emails[ 'SUMOSubscriptions_Pending_Authorization_Email' ]               = include( 'emails/class-sumo-subscriptions-pending-authorization.php' ) ;

    if( 'old-subscribers' === get_option( 'sumosubs_new_subscription_order_template_for_old_subscribers' , 'default' ) ) {
        $emails[ 'SUMOSubscriptions_New_Order_Old_Subscribers_Email' ] = include( 'emails/class-sumo-subscriptions-new-order-old-subscribers-email.php' ) ;
    }

    return $emails ;
}

add_filter( 'woocommerce_email_classes' , 'sumo_load_subscription_email_classes' , 10 ) ;

function sumosubs_wc_email_handler( $bool , $order ) {
    if( ! $order = wc_get_order( $order ) ) {
        return $bool ;
    }

    $disabled_wc_order_emails = get_option( 'sumosubs_disabled_wc_order_emails' , array() ) ;

    if( ! empty( $disabled_wc_order_emails ) && sumo_is_order_contains_subscriptions( sumosubs_get_order_id( $order ) ) ) {
        if( 'woocommerce_email_enabled_new_order' === current_filter() ) {
            if( in_array( 'new' , $disabled_wc_order_emails ) ) {
                return false ;
            }
        } else if( $order->has_status( $disabled_wc_order_emails ) ) {
            return false ;
        }
    }
    return $bool ;
}

add_filter( 'woocommerce_email_enabled_new_order' , 'sumosubs_wc_email_handler' , 99 , 2 ) ;
add_filter( 'woocommerce_email_enabled_customer_processing_order' , 'sumosubs_wc_email_handler' , 99 , 2 ) ;
add_filter( 'woocommerce_email_enabled_customer_processing_order' , 'sumosubs_wc_email_handler' , 99 , 2 ) ;
