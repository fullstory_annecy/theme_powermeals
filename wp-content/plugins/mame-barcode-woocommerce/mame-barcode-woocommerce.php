<?php
/**
 * Plugin Name: mame Barcode for WooCommerce
 * Plugin URI: https://www.mamedev.ch
 * Description: Swiss Post Barcode plugin for WooCommerce
 * Version: 1.4.3
 * Author: mame webdesign hüttig
 * Author URL: https://www.mamedev.ch
 * License: Purchase a license key at mamedev.ch.
 *
 * Requires at least: 4.0.1
 * Tested up to: 5.1.1
 * Requires PHP: 5.3
 *
 * WC requires at least: 3.0
 * WC tested up to: 3.5.7
 */

include_once( ABSPATH . 'wp-admin/includes/plugin.php' );

// Define global constants.
define( 'MAME_BC_PLUGIN_VERSION', '1.4.3' );
define( 'MAME_BC_UPDATE_URL', 'http://www.mamedev.ch' );
define( 'MAME_BC_PLUGIN_NAME', 'Barcode for WooCommerce' );
define( 'MAME_BC_DISPLAY_NAME', 'mame Barcode for WooCommerce' );

define( 'MAME_BC_PRODUCTION_URL', 'https://wedec.post.ch' );
define( 'MAME_BC_TEST_URL', 'https://wedecint.post.ch' );

//define( 'MAME_BC_SPEC_VERSION', '6' );

define( 'MAME_BC_PREFIX', 'mame_bc' );
define( 'MAME_BC_PLUGIN_PATH', 'mame-barcode-woocommerce' );

define( 'MAME_BC_DB_VERSION', '1.3' );

// Subscriptions
if ( !defined( 'MAME_SUBSCRIPTIONS_ACTIVE' ) )
    define( 'MAME_SUBSCRIPTIONS_ACTIVE', is_plugin_active( 'woocommerce-subscriptions/woocommerce-subscriptions.php' ) );

if ( !defined( 'MAME_WOO_TRACKING_ACTIVE' ) )
    define( 'MAME_WOO_TRACKING_ACTIVE', is_plugin_active( 'woocommerce-shipment-tracking/woocommerce-shipment-tracking.php' ) );

if ( !defined( 'MAME_WC_ACTIVE' ) )
    define( 'MAME_WC_ACTIVE', is_plugin_active( 'woocommerce/woocommerce.php' ) );

// Debug
if ( !defined( 'MAME_BC_DEBUG' ) )
    define( 'MAME_BC_DEBUG', false );
if ( !defined( 'MAME_DEBUG' ) )
    define( 'MAME_DEBUG', false );

register_activation_hook( __FILE__, MAME_BC_PREFIX . '_on_activation' );
register_deactivation_hook( __FILE__, MAME_BC_PREFIX . '_on_deactivation' );

// Include WP_List_Table
if ( !class_exists( 'WP_List_Table' ) ) {
    require_once( ABSPATH . 'wp-admin/includes/class-wp-list-table.php' );
}

// Helper functions
include_once( plugin_dir_path( __FILE__ ) . 'lib/Mame_Helper_Functions.php' );
include_once( plugin_dir_path( __FILE__ ) . 'lib/Mame_WP_Helper.php' );
if ( MAME_WC_ACTIVE ) {
    include_once( plugin_dir_path( __FILE__ ) . 'lib/Mame_WC_Helper.php' );

}
include_once( plugin_dir_path( __FILE__ ) . 'lib/Mame_Html.php' );
include_once( plugin_dir_path( __FILE__ ) . 'lib/Mame_Json_Response.php' );


// Initialize plugin.
include_once( plugin_dir_path( __FILE__ ) . 'includes/Barcode_Plugin_Init.php' );
$webstamp_init = new Barcode_Plugin_Init();

/*
 *  Include licensing/updates files.
 */
include_once( plugin_dir_path( __FILE__ ) . 'lib/updates/Mame_Licensing_Handler.php' );
$licensing = new Mame_Licensing_Handler( MAME_BC_PLUGIN_NAME, MAME_BC_DISPLAY_NAME, MAME_BC_UPDATE_URL, MAME_BC_PREFIX, 'barcode', MAME_BC_PLUGIN_PATH, false );

/*
 * Initialize updater.
 */
include( dirname( __FILE__ ) . '/lib/updates/Mame_Plugin_Updater.php' );

$license_options = $licensing->network_valid_license() ? Mame_Licensing_Handler::get_option( MAME_BC_PREFIX . '_license_options' ) : null;
if ( $license_options && isset( $license_options[ 'license_key' ] ) ) {
    $edd_updater = new Mame_Plugin_Updater( MAME_BC_UPDATE_URL, __FILE__, array(
        'version'   => MAME_BC_PLUGIN_VERSION,
        'license'   => $license_options[ 'license_key' ],
        'item_name' => MAME_BC_PLUGIN_NAME,
        'author'    => 'mame webdesign hüttig',
        'url'       => get_home_url( 1 )
    ) );
}

include_once( plugin_dir_path( __FILE__ ) . 'includes/admin/Barcode_File_Manager.php' );

// Data
include_once( plugin_dir_path( __FILE__ ) . 'includes/data/Barcode_Data.php' );
include_once( plugin_dir_path( __FILE__ ) . 'lib/db/Mame_WP_DB_Query.php' );
include_once( plugin_dir_path( __FILE__ ) . 'lib/db/Mame_WP_Active_Record.php' );
include_once( plugin_dir_path( __FILE__ ) . 'includes/data/Barcode_Database_Manager.php' );

// Objects
include_once( plugin_dir_path( __FILE__ ) . 'lib/Mame_Object.php' );
include_once( plugin_dir_path( __FILE__ ) . 'includes/objects/Barcode_Generate_Label_Object.php' );
include_once( plugin_dir_path( __FILE__ ) . 'includes/objects/Barcode_Order.php' );
include_once( plugin_dir_path( __FILE__ ) . 'includes/objects/Barcode_Template.php' );
include_once( plugin_dir_path( __FILE__ ) . 'includes/objects/Barcode_Additional_Data.php' );
include_once( plugin_dir_path( __FILE__ ) . 'includes/objects/Barcode_Communication.php' );
include_once( plugin_dir_path( __FILE__ ) . 'includes/objects/Barcode_Customer.php' );
include_once( plugin_dir_path( __FILE__ ) . 'includes/objects/Barcode_Item.php' );
include_once( plugin_dir_path( __FILE__ ) . 'includes/objects/Barcode_Label_Address.php' );
include_once( plugin_dir_path( __FILE__ ) . 'includes/objects/Barcode_Label_Definition.php' );
include_once( plugin_dir_path( __FILE__ ) . 'includes/objects/Barcode_Label.php' );
include_once( plugin_dir_path( __FILE__ ) . 'includes/objects/Barcode_Notification.php' );
include_once( plugin_dir_path( __FILE__ ) . 'includes/objects/Barcode_Recipient.php' );
include_once( plugin_dir_path( __FILE__ ) . 'includes/objects/Barcode_Service_Code_Attributes.php' );

// Order
include_once( plugin_dir_path( __FILE__ ) . 'includes/order/Barcode_Order_Handler.php' );

// Editor
include_once( plugin_dir_path( __FILE__ ) . 'includes/editor/Barcode_Json_Response.php' );
include_once( plugin_dir_path( __FILE__ ) . 'includes/editor/Barcode_Editor_Handler.php' );
include_once( plugin_dir_path( __FILE__ ) . 'includes/editor/Barcode_Template_Editor_Handler.php' );
new Barcode_Editor_Handler();
new Barcode_Template_Editor_Handler();

// Pages
include_once( plugin_dir_path( __FILE__ ) . 'includes/admin/Barcode_Order_List_Table.php' );
include_once( plugin_dir_path( __FILE__ ) . 'includes/admin/Barcode_Template_List_Table.php' );
include_once( plugin_dir_path( __FILE__ ) . 'includes/admin/Barcode_Settings_Page.php' );

add_action( 'plugins_loaded', function () {

    Barcode_Settings_Page::get_instance();

    load_plugin_textdomain( 'dhuett', false, plugin_basename( dirname( __FILE__ ) . '/localization/' ) );

} );

include_once( plugin_dir_path( __FILE__ ) . 'includes/oauth/PF_ECommerce_OpenId_Manager.php' );
include_once( plugin_dir_path( __FILE__ ) . 'includes/oauth/Barcode_OpenId_Manager.php' );

include_once( plugin_dir_path( __FILE__ ) . 'includes/admin/Barcode_Order_Admin.php' );
Barcode_Order_Admin::get_instance();

// WooCommerce
include_once( plugin_dir_path( __FILE__ ) . 'includes/woocommerce/functions.php' );
if ( MAME_WOO_TRACKING_ACTIVE )
    include_once( plugin_dir_path( __FILE__ ) . 'includes/woocommerce/shipment-tracking.php' );

/**
 * Initialize license keys and setup db on install.
 *
 * @global type $wp_rewrite
 */
function mame_bc_on_activation()
{
    // Initialize license key and status.
    $license_options = get_option( MAME_BC_PREFIX . '_license_options' );
    if ( !$license_options )
        update_option( MAME_BC_PREFIX . '_license_options', [ 'license_key' => '', 'license_status' => false ] );

    // Setup database.
    Barcode_Database_Manager::setup();

    // Create upload dir.
    $upload_dir    = wp_upload_dir();
    $ws_upload_dir = $upload_dir[ 'basedir' ] . '/mame_barcode/stamps';
    wp_mkdir_p( $ws_upload_dir );
    wp_mkdir_p( $upload_dir[ 'basedir' ] . '/mame_barcode/receipts' );
    wp_mkdir_p( $upload_dir[ 'basedir' ] . '/mame_barcode/temp' );

    // Protect files.
    file_put_contents( $upload_dir[ 'basedir' ] . '/mame_barcode/.htaccess', 'deny from all' );
}
