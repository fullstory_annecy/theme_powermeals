# README

## Changelog

### Version 1.4.3 - 02.04.2020

* Feature - Added option to add ProClima logo to labels.

### Version 1.4.2 - 13.12.2019

* Fix - Check if current page is mame_bc_menu_settings before creating new Session and Customer.

### Version 1.4.1 - 11.11.2019

* Fix - check if current user has permission 'manage_woocommerce' instead of checking for 'administrator' when printing or downloading files.
* Fix - include wc-cart-functions.php before creating new WC_Customer and calling get_checkout_fields().

### Version 1.4.0 - 09.09.2019

* Feature - Custom assignment of WooCommerce fields to Barcode address fields.

### Version 1.3.3 - 06.05.2019

* Fix - Use property instead of constant for display name.

### Version 1.3.2 - 19.04.2019

* Fix - Show preview button after save.

### Version 1.3.1 - 18.04.2019

* Fix - Bind click event on document for image upload.

### Version 1.3.0 - 12.04.2019

* Feature - WooCommerce Shipment Tracking integration.
* Fix - Open print dialog after order.
* Fix - Load overview on template load.

### Version 1.2.8 - 11.04.2019

* Fix - Correctly check, activate and deactivate licenses of the current blog.

### Version 1.2.7 - 09.04.2019

* Fix - Correctly check activated licenses in multisite installations.

### Version 1.2.6 - 08.04.2019

* Fix - Check if GET index is defined for settings page.

### Version 1.2.5 - 08.04.2019

* Fix - Renamed variable for jQuery noConflict mode.

### Version 1.2.4 - 15.03.2019

* Fix - Escape attribute on settings page.

### Version 1.2.3 - 13.03.2019

* Fix - Typo in db setup.

### Version 1.2.2 - 28.02.2019

* Fix - Check if Barcode_Customer::$domicile_post_office is set before the order.

### Version 1.2.1 - 28.02.2019

* Fix - Added missing dot before extension in filename.
* Tweak - Only log errors if MAME_DEBUG set.
* Tweak - Added get_file() function to Barcode_Label to retrieve the content of the label file.

### Version 1.2.0 - 27.01.2019

* Feature - Added tracking numbers to the order overview.
* Feature - Added option to include tracking numbers in confirmation emails.
* Fix - load WooCommerce buttons on admin screen when loading draft or starting new order.

### Version 1.1.1 - 13.01.2019

* Fix - show editor fields when no draft or template available.

### Version 1.1.0 - 13.01.2019

* Feature - Added order templates (editor, template list view).
* Feature - Added setting to directly jump to order overview screen on template load.
* Feature - Added setting to directly jump to print screen after an order has been placed.
* Feature - Added setting to add links to order confirmation email to load templates directly.
* Tweak - Fill in fields from draft order directly in PHP.
* Fix - Get correct file type when downloading files.
* Fix - Option name in Mame_Licensing.

### Version 1.0.0 - 04.11.2018

* Initial release.
