<?php

if ( !defined( 'ABSPATH' ) ) {
    exit; // Exit if accessed directly
}

if ( !class_exists( 'Mame_WP_DB_Query' ) ) {

    /**
     * Class Mame_WP_DB_Query
     *
     * A query object to communicate with the WP database and retrieve data.
     */
    class Mame_WP_DB_Query
    {
        /**
         * @var The name of the calling class
         */
        private $class_name;

        /**
         * @var string The name of of database table(s)
         */
        private $from;

        /**
         * @var array Conditions of the form ['=', ['id', '12']]
         */
        private $where;

        /**
         * @var string The query to be executed.
         */
        private $query_string;

        /**
         * Mame_WP_DB_Query constructor.
         * @param $table_name
         * @param $class_name
         */
        public function __construct( $table_name, $class_name )
        {
            $this->from       = $table_name;
            $this->class_name = $class_name;
        }

        /**
         * Adds conditions to the query.
         *
         * @param array $conditions
         * @return Mame_WP_DB_Query $this
         */
        public function where( $conditions )
        {
            /*
            $a = [
              'like', ['a', 'b'],
            ];
            */
            $this->where = $conditions;
            if ( !empty( $conditions ) ) {
                foreach ( $conditions as $cond ) {
                    $this->where[] = $cond;
                }
                //$this->query[ 'where' ][ $k ] = $v;
            }
            return $this;
        }

        public function whereEquals( $conditions )
        {

        }

        public function whereLike( $conditions )
        {

        }


        /**
         * Returns an array of objects of the calling class as results of the query.
         *
         * @return array|null
         */
        public function all()
        {
            global $wpdb;
            $this->build_query_string();
            $results = $wpdb->get_results( $this->query_string );
            if ( !empty( $results ) ) {
                return array_map( function ( $obj ) {
                    return new $this->class_name( $obj );
                }, $results );
            }
            return null;
        }

        /**
         * Returns a single object of the calling class as result of the query.
         *
         * @return mixed|null
         */
        public function one()
        {
            $results = $this->all();
            if ( !empty( $results ) )
                return $results[ 0 ];
            return null;
        }

        /**
         * Creates a query string from $this.
         */
        private function build_query_string()
        {
            $this->query_string = "
            SELECT *
            FROM " . $this->from . "
            ";

            if ( !empty( $this->where ) ) {
                $count = 0;
                foreach ( $this->where as $cond ) {
                    if ( $count > 0 )
                        $this->query_string .= " and";
                    $this->query_string .= " " . $cond[ 1 ][ 0 ] . $cond[ 0 ] . "'" . $cond[ 1 ][ 2 ] . "'";
                    $count++;
                }
            }
        }
    }

}