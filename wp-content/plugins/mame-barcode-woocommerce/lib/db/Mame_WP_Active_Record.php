<?php
if ( !defined( 'ABSPATH' ) ) {
    exit; // Exit if accessed directly
}

if ( !class_exists( 'Mame_WP_Active_Record' ) ) {

    /**
     * Class Mame_WP_Active_Record
     *
     * Query functions to communicate with the database.
     *
     * Objects should extend this class.
     */
    interface Mame_WP_Active_Record
    {
        /**
         * Returns a new query object for the calling class.
         *
         * @return Mame_WP_DB_Query
         */
        public static function find();

        /**
         * Saves/updates a record.
         *
         * @return mixed
         */
        public static function save();

        /**
         * Updates a record.
         *
         * @return mixed
         */
        public static function update();
    }

}