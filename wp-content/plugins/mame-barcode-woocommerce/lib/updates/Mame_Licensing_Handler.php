<?php

if ( !defined( 'ABSPATH' ) ) {
    exit; // Exit if accessed directly
}

if ( !class_exists( 'Mame_Licensing_Handler' ) ) :

    class Mame_Licensing_Handler
    {
        private        $prefix;
        private        $tab_name;
        private        $plugin_name;
        private        $plugin_display_name;
        private        $plugin_url;
        private        $plugin_path;
        private static $instance;

        public function __construct( $plugin_name, $plugin_display_name, $plugin_url, $prefix = '', $tab_name = '', $plugin_path = '', $settings_tab = true )
        {
            $this->prefix              = $prefix;
            $this->plugin_name         = $plugin_name;
            $this->plugin_display_name = $plugin_display_name;
            $this->tab_name            = $tab_name;
            $this->plugin_url          = $plugin_url;
            $this->plugin_path         = $plugin_path;

            add_action( 'wp_ajax_' . $prefix . '_ajax_license', array( $this, 'ajax_handler' ) );

            // Licensing notices.
            add_action( 'admin_notices', array( $this, 'license_notice' ) );
            add_action( 'network_admin_notices', array( $this, 'license_notice' ) );

            if ( $settings_tab && MAME_WC_ACTIVE ) {

                // Define custom licensing field.
                add_action( 'woocommerce_admin_field_' . $prefix . '_licensing', array( $this, 'display_licensing_field' ) );

                // Save custom licensing field.
                add_filter( 'woocommerce_admin_settings_sanitize_option', array( $this, 'update_licensing_option' ), 10, 3 );
            }
        }

        public function network_valid_license()
        {
            if ( !is_multisite() )
                return $this->valid_license();

            $network_active = false;
            $valid          = true;

            $plugins = get_site_option( 'active_sitewide_plugins' );
            foreach ( $plugins as $key => $value ) {
                $data = get_plugin_data( WP_PLUGIN_DIR . '/' . $key );
                if ( $data[ 'Name' ] === $this->plugin_display_name )
                    $network_active = true;
            }

            global $wpdb;
            $blogs = $wpdb->get_results( "
        SELECT blog_id
        FROM {$wpdb->blogs}
        WHERE site_id = '{$wpdb->siteid}'
        AND spam = '0'
        AND deleted = '0'
        AND archived = '0'
    " );

            if ( $network_active ) {
                foreach ( $blogs as $blog ) {
                    if ( !$this->valid_license( $blog->blog_id ) ) {
                        $valid = false;
                        break;
                    }
                }
            } else {

                foreach ( $blogs as $blog ) {
                    $plugins = get_blog_option( $blog->blog_id, 'active_plugins' );

                    foreach ( $plugins as $key => $value ) {
                        $data = get_plugin_data( WP_PLUGIN_DIR . '/' . $value );
                        if ( $data[ 'Name' ] === $this->plugin_display_name ) {
                            if ( !$this->valid_license( $blog->blog_id ) ) {
                                $valid = false;
                                break;
                            }
                            continue;
                        }
                    }
                }
            }
            return $valid;
        }

        /**
         * Get single instance of this class.
         *
         * @param $plugin_name
         * @param $plugin_display_name
         * @param $plugin_url
         * @param string $prefix
         * @param string $tab_name
         * @param string $plugin_path
         * @param bool $settings_tab
         * @return Mame_Licensing_Handler
         */
        public static function get_instance( $plugin_name, $plugin_display_name, $plugin_url, $prefix = '', $tab_name = '', $plugin_path = '', $settings_tab = true )
        {
            if ( empty( static::$instance ) ) {
                static::$instance = new static( $plugin_name, $plugin_display_name, $plugin_url, $prefix, $tab_name, $plugin_path, $settings_tab );
            }

            return static::$instance;
        }

        public function ajax_handler()
        {
            if ( !check_ajax_referer( $this->prefix . '_license_nonce', 'security', false, false ) ) {
                return;
            }
            $action = $_POST[ 'license_action' ];
            if ( $action == 'activate' ) {
                $this->activate_license( $_POST[ 'license' ], true, get_current_blog_id() );
            } elseif ( $action == 'deactivate' ) {
                $this->deactivate_license( true, get_current_blog_id() );
            } elseif ( $action == 'check' ) {
                $this->check_license( true, get_current_blog_id() );
            } elseif ( $action == 'notice' ) {
                $this->ignore_notice();
            }
        }

        public function license_notice()
        {
            if ( is_network_admin() ) {
                if ( !$this->network_valid_license() ) {
                    echo '<div class="error"><p>';
                    printf( __( 'The license for %1$s has not been activated for all sites that use the plugin.', 'dhuett' ), $this->plugin_name );
                    echo '</p></div>';
                }
            } else {
                if ( !$this->valid_license( get_current_blog_id() ) ) {
                    echo '<div class="error"><p>';
                    printf( __( 'License for %1$s is invalid or missing. <a href="%2$s">ACTIVATE HERE</a>.', 'dhuett' ), $this->plugin_name, 'admin.php?page=wc-settings&tab=' . $this->tab_name );
                    echo '</p></div>';
                }
            }
        }

        /**
         * Dismiss notice
         */
        public function ignore_notice()
        {
            global $current_user;
            $user_id = $current_user->ID;

            update_user_meta( $user_id, $this->prefix . '_notice_ignore', true );

            ob_clean();
            echo json_encode( array( 'status' => true ) );
            exit;
        }

        /**
         * Display custom WooCommerce admin field type 'licensing'.
         *
         * woocommerce_admin_field_{licensing}
         * @param $field
         */
        public function display_licensing_field( $field )
        {
            ?>
            <tr valign="top">
                <th scope="row" class="titledesc">
                    <label
                            for="<?php echo esc_attr( $field[ 'id' ] ); ?>"><?php echo esc_html( $field[ 'title' ] ); ?></label>
                </th>
                <td class="forminp forminp-<?php echo sanitize_title( $field[ 'type' ] ) ?>">
                    <?php $this->license_activation_field() ?>
                    <div>
						<span
                                class="description"><?php printf( __( 'You can manage your license in <a href="%1$s" target="_blank"> your account at mamedev.ch</a>.', 'dhuett' ), 'http://mamedev.ch/checkout/purchase-history/' ); ?></span>
                    </div>
                </td>
            </tr>
            <?php

        }

        /**
         * Choose action (activate/deactivate) based on POST action.
         *
         * @param $value
         * @param $option
         * @param $raw_value
         */
        public function update_licensing_option( $value, $option, $raw_value )
        {
            if ( $option[ 'type' ] == 'licensing' ) {

                if ( !check_admin_referer( $this->prefix . '_license_nonce', $this->prefix . '_license_nonce' ) ) {
                    return;
                }

                if ( isset( $_POST[ $this->prefix . '_license_activate' ] ) ) {

                    $license = sanitize_text_field( $_POST[ $this->prefix . '_license_key' ] );

                    $this->activate_license( $license, false, get_current_blog_id() );
                }
                if ( isset( $_POST[ $this->prefix . '_license_deactivate' ] ) ) {
                    $this->deactivate_license( false, get_current_blog_id() );
                }
                if ( isset( $_POST[ $this->prefix . '_license_check' ] ) ) {
                    $this->check_license( false, get_current_blog_id() );
                }
            }

            return $value;
        }

        /**
         * Display the licensing field. Activate/check the license.
         *
         * @param string $message
         */
        public function license_activation_field( $message = '' )
        {
            wp_nonce_field( $this->prefix . '_license_nonce', $this->prefix . '_license_nonce' );
            ?>

            <div id="<?= $this->prefix ?>-license-field-wrapper" class="mame-license-field-wrapper">
                <?php $license_valid = $this->valid_license( get_current_blog_id() );
                ?>
                <div id="<?= $this->prefix ?>-license-invalid"
                     class="mame-license-invalid-msg" <?php echo $license_valid ? 'style="display:none;"' : '' ?>>
                    <div>
                        <p class="description"
                           style="margin-bottom:0.5em"><?php printf( __( 'Enter your license code to receive updates and support for the %1$s plugin', 'dhuett' ), $this->plugin_name ); ?></p>

                    </div>
                    <input id="<?= $this->prefix ?>_license_key" name="<?= $this->prefix ?>_license_key"
                           class="mame-license-input" size="70"
                           type="text"
                           value="<?php echo $this->get_license_options( get_current_blog_id() )[ 'license_prev' ] ?>"/>
                    <input type="submit" name="<?= $this->prefix ?>-license-activate-btn"
                           class="<?= $this->prefix ?>-license-activate-btn"
                           value="<?php echo __( 'Activate', 'dhuett' ) ?>"/>
                </div>

                <div
                        id="<?= $this->prefix ?>-license-valid"
                        class="mame-license-valid-msg" <?php echo $license_valid ? 'class="' . $this->prefix . '-check-license"' : 'style="display:none;"' ?>>
                    <p><?php echo __( 'Valid license: ', 'dhuett' ); ?><span
                                id="<?= $this->prefix ?>-current-license-key"><?php echo $this->get_license( get_current_blog_id() ); ?></span>
                    </p>
                    <input type="submit" name="<?= $this->prefix ?>_license_deactivate"
                           class="<?= $this->prefix ?>-license-deactivate-btn"
                           value="<?php echo __( 'Deactivate', 'dhuett' ) ?>"/>
                    <input type="submit" name="<?= $this->prefix ?>-license-check-btn"
                           class="<?= $this->prefix ?>-license-check-btn"
                           value="<?php echo __( 'Check license', 'dhuett' ) ?>"/>
                </div>
                <?php
                ?>
                <p id="<?= $this->prefix ?>-license-status-message" class="mame-license-status-msg"
                   style="color: red"><?php echo $message; ?></p>
            </div>
            <div id="<?= $this->prefix ?>-loader" class="mame-loader"><img
                        src="<?= plugins_url() . '/' . $this->plugin_path . '/assets/images/loader.gif' ?>"></div>
            <?php

        }

        /**
         * License activation.
         *
         * @param $license
         * @param bool $is_ajax
         *
         * @param int $blog_id
         * @return mixed
         */
        public function activate_license( $license, $is_ajax = false, $blog_id = 1 )
        {
            $api_params = array(
                'edd_action' => 'activate_license',
                'license'    => $license,
                'item_name'  => urlencode( $this->plugin_name ),
                'url'        => get_home_url( $blog_id ),
            );

            $response = wp_remote_post( $this->plugin_url, array(
                'timeout'   => 15,
                'sslverify' => false,
                'body'      => $api_params
            ) );

            if ( is_wp_error( $response ) ) {
                return __( 'Error: Unable to activate license.', 'dhuett' );
            } else {

                $license_data = json_decode( wp_remote_retrieve_body( $response ) );
                $message      = '';
                $status       = '';

                if ( isset( $license_data->license ) ) {

                    $status = $license_data->license;

                    if ( $status == 'valid' ) {

                        $license_options                     = get_option( $this->prefix . '_license_options' );
                        $license_options[ 'license_status' ] = true;
                        $license_options[ 'license_key' ]    = sanitize_text_field( $license );
                        $license_options[ 'license_prev' ]   = sanitize_text_field( $license );
                        static::update_option( $this->prefix . '_license_options', $license_options, $blog_id );

                        $message = __( 'License activation successful.', 'dhuett' );
                    } else {

                        $message = __( 'License key invalid.', 'dhuett' );
                    }
                } else {

                    $message = __( 'Unable to activate license.', 'dhuett' );
                }

                if ( $is_ajax ) {

                    $return_array = array(
                        'status'  => $status,
                        'message' => $message
                    );
                    echo json_encode( $return_array );
                    exit;
                }
            }

        }

        public function deactivate_license( $is_ajax = false, $blog_id = 1 )
        {
            $api_params = array(
                'edd_action' => 'deactivate_license',
                'license'    => $this->get_license( get_current_blog_id() ),
                'item_name'  => $this->plugin_name,
                'url'        => get_home_url( $blog_id ),
            );

            $response = wp_remote_post( $this->plugin_url, array(
                'body'      => $api_params,
                'timeout'   => 15,
                'sslverify' => false
            ) );

            if ( is_wp_error( $response ) ) {
                return __( 'Error: Unable to deactivate license.', 'dhuett' );
            } else {
                $license_data = json_decode( wp_remote_retrieve_body( $response ) );
                $message      = '';
                $status       = '';

                if ( isset( $license_data->license ) ) {

                    $status = $license_data->license;

                    if ( $status == 'deactivated' ) {

                        $license_options                     = static::get_option( $this->prefix . '_license_options', $blog_id );
                        $license_options[ 'license_status' ] = false;
                        $license_options[ 'license_key' ]    = '';

                        static::update_option( $this->prefix . '_license_options', $license_options, $blog_id );

                        $message = __( 'License deactivation successful.', 'dhuett' );
                    } else {

                        $message = __( 'Unable to deactivate license.', 'dhuett' );
                    }
                } else {

                    $message = __( 'Unable to deactivate license.', 'dhuett' );
                }

                if ( $is_ajax ) {

                    $return_array = array(
                        'status'  => $status,
                        'message' => $message
                    );
                    echo json_encode( $return_array );
                    exit;
                }
            }

        }

        public static function get_option( $option, $blog_id = null )
        {
            if ( is_multisite() ) {
                return get_blog_option( $blog_id ?: 1, $option );

            } else {
                return get_option( $option );
            }
        }

        public static function update_option( $option, $value, $blog_id = null )
        {
            if ( is_multisite() ) {
                update_blog_option( $blog_id ?: 1, $option, $value );

            } else {
                update_option( $option, $value );
            }
        }

        /**
         * Check if license is valid.
         *
         * @param bool $is_ajax
         *
         * @param int $blog_id
         * @return bool
         */
        public function check_license( $is_ajax = false, $blog_id = 1 )
        {
            $api_params = array(
                'edd_action' => 'check_license',
                'license'    => $this->get_license( get_current_blog_id() ),
                'item_name'  => urlencode( $this->plugin_name ),
                'url'        => get_home_url( $blog_id ),
            );
            $response   = wp_remote_get( add_query_arg( $api_params, $this->plugin_url ), array(
                'timeout'   => 15,
                'sslverify' => false
            ) );

            if ( is_wp_error( $response ) ) {
                return false;
            }

            $license_data = json_decode( wp_remote_retrieve_body( $response ), true );
            if ( $license_data[ 'license' ] == 'valid' ) {
                $license_data[ 'message' ] = __( 'License key valid.', 'dhuett' );

            } else {
                $license_options                     = static::get_option( $this->prefix . '_license_options', $blog_id );
                $license_options[ 'license_status' ] = false;
                $license_options[ 'license_key' ]    = '';

                static::update_option( $this->prefix . '_license_options', $license_options, $blog_id );

                $license_data[ 'message' ] = __( 'License key invalid.', 'dhuett' );
            }

            if ( $is_ajax ) {

                echo json_encode( $license_data );
                exit;
            }

            return $license_data;
        }

        public function get_license_options( $blog_id = 1 )
        {
            return static::get_option( $this->prefix . '_license_options', $blog_id );
        }

        /**
         * Return license key.
         *
         * @param int $blog_id
         * @return bool
         */
        public function get_license( $blog_id = 1 )
        {
            $license_options = $this->get_license_options( $blog_id );
            if ( $license_options )
                return $license_options[ 'license_key' ];

            return false;
        }

        /**
         * Is the license valid?
         *
         * @param int $blog_id
         * @return bool
         */
        public function valid_license( $blog_id = 1 )
        {
            return $this->get_license_options( $blog_id )[ 'license_status' ];
        }

        /**
         * Add network options for multisite installations.
         */
        public function register_network_settings()
        {
            register_setting( $this->prefix . '-network-settings-group', $this->prefix . '_network_license_key', array( $this, 'sanitize_network_options' ) );
        }

        /**
         * Sanitize the network settings.
         *
         * @param $input
         *
         * @return mixed
         */
        public function sanitize_network_options( $input )
        {
            $input = sanitize_text_field( $input );

            return $input;

        }

        /**
         * Add the network settings submenu page.
         */
        public function network_settings()
        {
            add_submenu_page( 'settings.php', __( $this->plugin_name . ' Network Settings', 'dhuett' ), __( $this->plugin_name, 'dhuett' ), 'manage_options', $this->prefix . '-network-settings', array( $this, 'display_network_settings' ) );
        }

        public function display_network_settings()
        {
            if ( isset( $_POST[ $this->prefix . '_license_key' ] ) ) {

                $license = sanitize_text_field( $_POST[ $this->prefix . '_license_key' ] );

                $license_notice = $this->activate_license( $license, false, get_current_blog_id() );
            }
            if ( isset( $_POST[ $this->prefix . '_license_deactivate' ] ) ) {
                $license_notice = $this->deactivate_license( false, get_current_blog_id() );
            }
            ?>
            <h1><?php _e( $this->plugin_name . ' license key', 'dhuett' ); ?></h1>
            <form action="settings.php?page=<?= $this->tab_name ?>-network-settings" method="post">
                <table>
                    <tr valign="top">
                        <th scope="row" class="titledesc">

                        </th>
                        <td class="forminp forminp-licensing">

                            <?php $this->license_activation_field() ?>

                        </td>
                    </tr>
                </table>
            </form>
            <?php
        }
    }

endif;