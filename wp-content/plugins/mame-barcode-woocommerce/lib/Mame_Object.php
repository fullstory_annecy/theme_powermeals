<?php

if ( !defined( 'ABSPATH' ) ) {
    exit;
}

if ( !class_exists( 'Mame_Object' ) ) {

    /**
     * Class Mame_Object
     *
     * Base class with helper functions.
     */
    abstract class Mame_Object
    {
        /**
         * Mame_Object constructor.
         * Loads the values of $object into $this where $object can be an object or an array.
         *
         * @param null|StdClass|array $object
         */
        public function __construct( $object = null )
        {
            if ( $object ) {
                $this->populate( $object );
            }
        }

        /**
         * populate this object with properties of other object.
         *
         * @param StdClass|array $object
         */
        public function populate( $object )
        {
            if ( is_object( $object ) )
                $object = get_object_vars( $object );
            foreach ( $object as $name => $value ) {
                if ( property_exists( $this, $name ) )
                    $this->$name = $value;
            }
        }

        /**
         * Returns the class instance as json encoded string.
         *
         * @return string
         */
        public function as_json()
        {
            return json_encode( $this );
        }
    }
}
