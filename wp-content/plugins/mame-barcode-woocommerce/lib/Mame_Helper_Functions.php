<?php

/**
 * Created by PhpStorm.
 * User: Mozzarella
 * Date: 20.03.18
 * Time: 16:58
 */

if ( !defined( 'ABSPATH' ) ) {
    exit; // Exit if accessed directly
}

if ( !class_exists( 'Mame_Helper_Functions' ) ) :

    /**
     * Class Mame_Helper_Functions
     * @author Dirk Hüttig <dirk.huettig@mame-webdesign.ch>
     */
    class Mame_Helper_Functions
    {
        /**
         * Writes $log to error log if constant MAME_DEBUG is set to true.
         *
         * @param $log
         */
        public static function write_log( $log )
        {
            if ( MAME_DEBUG ) {
                if ( is_array( $log ) || is_object( $log ) ) {
                    error_log( print_r( $log, true ) );
                } else {
                    error_log( $log );
                }
            }
        }

        /**
         * Adds a tag before the $log message.
         *
         * @param $tag
         * @param $log
         */
        public static function write_log_with_tag( $tag, $log )
        {
            if (MAME_DEBUG) {
                error_log( 'TAG: ' . $tag );
                static::write_log( $log );
            }
        }

        /**
         * Maps an array of objects to an array $key => $value pairs where $key and $value are properties of the objects.
         *
         * @param $object_array
         * @param $key
         * @param $value
         * @return mixed
         */
        public static function object_array_map( $object_array, $key, $value )
        {
            return array_reduce( $object_array, function ( $c, $i ) use ( $key, $value ) {
                $c[ (string)( $i->{$key} ) ] = $i->{$value};
                return $c;
            }, [] );
        }

        /**
         * Maps an array of objects to an array $key => $value pairs where $key and $value are properties of the objects.
         *
         * Since PHP converts integer-like strings to integers we use the following structure to preserve the string keys:
         * [
         *      'key' => $key,
         *      'value' => $value,
         * ]
         *
         * @param $object_array
         * @param $key
         * @param $value
         * @return mixed
         */
        public static function object_array_map_string_keys( $object_array, $key, $value )
        {
            return array_reduce( $object_array, function ( $c, $i ) use ( $key, $value ) {
                $c[] = [ 'key' => $i->{$key}, 'value' => $i->{$value} ];
                return $c;
            }, [] );
        }

        /**
         * Recursively converts an array to an object.
         *
         * @param $array
         * @param null $class
         * @return stdClass
         */
        public static function array_to_object( $array, $class = null )
        {
            $obj = $class ? new $class() : new stdClass;
            foreach ( $array as $k => $v ) {
                if ( strlen( $k ) ) {
                    if ( is_array( $v ) ) {
                        $obj->{$k} = array_to_object( $v );
                    } else {
                        $obj->{$k} = $v;
                    }
                }
            }
            return $obj;
        }

        /**
         * Deep conversion of an object to an associative array. Removes null values.
         *
         * @param $object
         * @return array
         */
        public static function object_to_array( $object )
        {
            $array = (array)$object;
            foreach ( $array as $k => $v ) {
                if ( is_object( $v ) ) {
                    $array[ $k ] = static::object_to_array( $v );
                }
                if ( $v == null )
                    unset( $array[ $k ] );
                /*
                                if ( $v === null || $v === '' ) {
                                    unset( $array[ $k ] );
                                }
                */
            }
            return $array;
        }

        /**
         * Converts CamelCase strings to underscore strings (camel_case).
         *
         * @param $str
         * @return string
         */
        public static function camelcase_to_underscore( $str )
        {
            return strtolower( preg_replace( [ '/([a-z\d])([A-Z])/', '/([^_])([A-Z][a-z])/' ], '$1_$2', $str ) );
        }

        public static function replace_special_chars( $string )
        {
            $search  = array( "Ä", "Ö", "Ü", "ä", "ö", "ü", "ß", "´" );
            $replace = array( "Ae", "Oe", "Ue", "ae", "oe", "ue", "ss", "" );
            return str_replace( $search, $replace, $string );
        }
    }

endif;