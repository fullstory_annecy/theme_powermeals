<?php

if ( !defined( 'ABSPATH' ) ) {
    exit; // Exit if accessed directly
}

if ( !class_exists( 'Barcode_OpenId_Manager' ) ) :

    class Barcode_OpenId_Manager extends PF_ECommerce_OpenId_Manager
    {
        const ENDPOINT_BARCODE_ADDRESS_LABEL = '/api/barcode/v1/generateAddressLabel';

        public function __construct()
        {
            parent::__construct();
        }

        /**
         * Sends a request to generate a new address label.
         *
         * @param $json_data The order data
         * @return mixed
         */
        public function generate_address_label( $json_data )
        {
            $response = wp_remote_post( $this->url . static::ENDPOINT_BARCODE_ADDRESS_LABEL, [
                'headers' => [ 'Authorization' => 'Bearer ' . $this->token, 'Accept' => 'application/json', 'Content-Type' => 'application/json' ],
                'body'    => $json_data,
            ] );

            return $response;
        }

        /**
         * Adds client id, client secret and franking license.
         *
         * @return mixed|void
         */
        public function set_identification()
        {
            $options = get_option( 'mame_bc_options_group' );
            if ( $options[ 'environment' ] == 'production' ) {
                $this->client_id        = $options[ 'client_id' ];
                $this->client_secret    = $options[ 'client_secret' ];
                $this->franking_license = $options[ 'franking_license' ];
                $this->url              = MAME_BC_PRODUCTION_URL;
            } else {
                $this->client_id        = $options[ 'client_id_test' ];
                $this->client_secret    = $options[ 'client_secret_test' ];
                $this->franking_license = $options[ 'franking_license_test' ];
                $this->url              = MAME_BC_TEST_URL;
            }
        }
    }

endif;