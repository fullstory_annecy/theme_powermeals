<?php

if ( !defined( 'ABSPATH' ) ) {
    exit; // Exit if accessed directly
}

if ( !class_exists( 'PF_ECommerce_OpenId_Manager' ) ) :

    abstract class PF_ECommerce_OpenId_Manager
    {
        const ENDPOINT_TOKEN = '/WEDECOAuth/token';

        /**
         * @var string
         */
        protected $url;

        /**
         * @var string
         */
        protected $client_id;

        /**
         * @var string
         */
        protected $client_secret;

        /**
         * @var string
         */
        protected $franking_license;

        /**
         * @var string
         */
        public    $token;

        public function __construct( )
        {
            $this->set_identification();
        }

        /**
         * Requests a new token.
         *
         * @param $grant_type
         * @param $scope
         * @return string|null
         */
        public function get_token( $grant_type, $scope )
        {
            $request_url = add_query_arg( [
                'grant_type'    => $grant_type,
                'client_id'     => $this->client_id,
                'client_secret' => $this->client_secret,
                'scope'         => $scope,
            ], $this->url . static::ENDPOINT_TOKEN );

            Mame_WP_Helper::write_log_with_tag('PF_ECommerce_OpenId_Manager.get_token().$request_url', $request_url);

            $response = wp_remote_post( $request_url );
            $body     = json_decode( $response[ 'body' ] );

            Mame_WP_Helper::write_log_with_tag('PF_ECommerce_OpenId_Manager.get_token().$token', $body->access_token);

            return $this->token = $body->access_token;

        }

        /**
         * @return mixed
         */
        public abstract function set_identification();

    }

endif;