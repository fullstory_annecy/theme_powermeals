<?php
if ( !defined( 'ABSPATH' ) ) {
    exit; // Exit if accessed directly
}

if ( !class_exists( 'Barcode_Order_Handler' ) ) {

    /**
     * Class Barcode_Order_Handler
     */
    class Barcode_Order_Handler
    {
        /**
         * @var Barcode_Order
         */
        private $order;

        /**
         * @var Barcode_Generate_Label_Object
         */
        private $label_object;

        /**
         * @var Barcode_OpenId_Manager
         */
        private $openid_manager;

        /**
         * Barcode_Order_Handler constructor.
         */
        public function __construct()
        {
            $this->openid_manager = new Barcode_OpenId_Manager();
            $this->openid_manager->get_token( 'client_credentials', 'WEDEC_BARCODE_READ' );
        }

        /**
         * Places a new preview label order.
         *
         * @param Barcode_Order $order
         * @return array
         */
        public function new_order_preview( $order )
        {
            $this->order = $order;

            // Get the label object.
            $this->generate_label_object( $order );

            // Preview.
            $this->label_object->labelDefinition->printPreview = true;
            //$this->label_object->attributes->proClima = true;

            // Place order.
            return $this->handle_order( 'temp' );
        }

        /**
         * Places a new label order.
         *
         * @param Barcode_Order $order
         * @return array
         */
        public function new_order( $order )
        {
            $this->order = $order;

            // Get the label object.
            $this->generate_label_object( $order );

            // Place order.
            return $this->handle_order();
        }

        /**
         * Places an order and handles the response
         *
         * @param string $directory
         * @return array
         */
        private function handle_order( $directory = 'stamps' )
        {
            $order_array = Mame_Helper_Functions::object_to_array( $this->label_object );

            $options = get_option( 'mame_bc_options_group' );
            if ( $options[ 'proclima' ] ) {
                $order_array[ 'item' ][ 'attributes' ][ 'proClima' ] = true;
            }
            // Workaround - Swiss Post Barcode returns an internal server error if no empty notification is sent.
            $order_array[ 'item' ][ 'notification' ] = [];

            $response = $this->openid_manager->generate_address_label( json_encode( $order_array ) );
            //var_dump($response);die();
            if ( $response[ 'response' ][ 'code' ] != 200 )
                return [ 'error' => true, 'message' => $response[ 'response' ][ 'message' ], 'code' => $response[ 'response' ][ 'code' ] ];

            // Successful
            if ( !empty( $response[ 'body' ] ) ) {
                $response_body = json_decode( $response[ 'body' ] );
                if ( !empty( $response_body->item ) ) {

                    if ( isset( $response_body->item->errors ) )
                        return [ 'error' => true, 'message' => $response_body->item->errors[ 0 ]->message, 'code' => $response_body->item->errors[ 0 ]->code ];

                    if ( !empty( $response_body->item->label ) ) {
                        if ( !empty( $response_body->labelDefinition ) )
                            $this->order->labelDefinition = $response_body->labelDefinition;

                        $filenames = $this->save_labels( $response_body->item->label, $directory );

                        $notices = [];

                        if ( !empty( $response_body->labelDefinition ) ) {

                            if ( !empty( $response_body->labelDefinition->colorPrintRequired ) && $response_body->labelDefinition->colorPrintRequired )
                                $notices[] = __( 'Color print required', 'dhuett' );

                            if ( !$response_body->labelDefinition->printPreview ) {

                                $this->order->order_type = Barcode_Order::TYPE_ORDER;
                                $this->order->time       = time();
                                $this->order->ident_code = $response_body->item->identCode;
                                Barcode_Database_Manager::save_order( $this->order );

                                if ( MAME_WOO_TRACKING_ACTIVE ) {
                                    if ( function_exists( 'wc_st_add_tracking_number' ) && $this->order->wc_order_id ) {
                                        wc_st_add_tracking_number( $this->order->wc_order_id, $this->order->ident_code, 'Swiss Post' );
                                    }
                                }
                            }
                        }

                        return [ 'error' => false, 'files' => $filenames, 'notices' => $notices ];
                    }

                }
            }
            return [ 'error' => true, 'message' => __( 'Error in communication with the servers of the Swiss Post.', 'dhuett' ) ];
        }

        /**
         * Saves all labels of a response.
         *
         * @param array $labels
         * @param $directory
         * @return array
         */
        private function save_labels( $labels, $directory )
        {

            $label_file_names = [];
            foreach ( $labels as $l ) {
                $label       = base64_decode( $l );
                $upload_dir  = wp_upload_dir();
                $upload_path = str_replace( '/', DIRECTORY_SEPARATOR, $upload_dir[ 'basedir' ] ) . DIRECTORY_SEPARATOR . 'mame_barcode' . DIRECTORY_SEPARATOR . $directory . DIRECTORY_SEPARATOR;

                $label_file_names[] = $this->save_file( $label, $upload_path );

            }
            return $label_file_names;

        }

        /**
         * Saves a single file to the $upload_path
         *
         * @param string $file The file content
         * @param null $upload_path The complete upload path
         * @param null $filename
         * @return bool|string
         */
        private function save_file( $file, $upload_path = null, $filename = null )
        {
            if ( $file ) {

                if ( !$upload_path ) {
                    $upload_dir  = wp_upload_dir();
                    $upload_path = str_replace( '/', DIRECTORY_SEPARATOR, $upload_dir[ 'basedir' ] ) . DIRECTORY_SEPARATOR . 'mame_barcode' . DIRECTORY_SEPARATOR . 'stamps' . DIRECTORY_SEPARATOR;
                }

                if ( !$filename ) {
                    $options = get_option( 'mame_bc_options_group' );
                    if ( isset( $options[ 'stamp_file_type' ] ) )
                        $filename = uniqid() . '.' . $options[ 'stamp_file_type' ];
                    else
                        $filename = uniqid() . '.pdf';

                }
                $hashed_filename = md5( $filename . microtime() ) . '_' . $filename;
                $upload          = file_put_contents( $upload_path . $hashed_filename, $file );

                if ( !$this->order->labelDefinition->printPreview ) {
                    // Save in DB.
                    $label           = new Barcode_Label( $this->order->labelDefinition );
                    $label->filename = $hashed_filename;
                    $label->order_id = $this->order->id;
                    $label_id        = Barcode_Database_Manager::save_label( $label );
                    // Barcode_Database_Manager::save_order_to_label( $this->order->id, $label_id );
                }
                return $hashed_filename;
            }
            return false;
        }

        /**
         * Generates the Barcode_Generate_Label_Object object which is needed to order a label.
         *
         * @param Barcode_Order $order
         */
        private function generate_label_object( $order )
        {
            $options = get_option( 'mame_bc_options_group' );

            $customer     = $order->get_customer();
            $customer->id = null;
            if ( !$customer->domicilePostOffice )
                $customer->domicilePostOffice = $customer->domicile_post_office;
            $customer->domicile_post_office = null;
            if ( !empty( $order->image_id ) && $order->image_id != 0 ) {
                $attachment_image = wp_get_attachment_image_src( $order->image_id );

                if ( !empty( $attachment_image ) ) {
                    $mime_type = get_post_mime_type( $order->image_id );
                    $type      = strtoupper( substr( $mime_type, strpos( $mime_type, "/" ) + 1 ) );
                    if ( $type == 'JPEG' )
                        $type = 'JPG';
                    $customer->logoFormat      = strtoupper( $type );
                    $customer->logoAspectRatio = 'KEEP';
                    $customer->logo            = base64_encode( file_get_contents( $attachment_image[ 0 ] ) );
                }
            }
            //$customer->email

            $label_def                 = new Barcode_Label_Definition();
            $label_def->labelLayout    = $order->label_size;
            $label_def->printAddresses = 'RECIPIENT_AND_CUSTOMER';
            if ( $order->print_addresses )
                $label_def->printAddresses = $order->print_addresses;
            $order->print_addresses = null;

            if ( $options[ 'stamp_file_type' ] === 'png' ) {
                $label_def->imageFileType = 'PNG';
            } else {
                $label_def->imageFileType = 'PDF';
            }
            $label_def->imageResolution = 300;

            $recipient     = $order->get_recipient();
            $recipient->id = null;
            if ( !$recipient->houseNo )
                $recipient->houseNo = $recipient->house_no;
            $recipient->house_no = null;
            if ( !$recipient->floorNo )
                $recipient->floorNo = $recipient->floor_no;
            $recipient->floor_no = null;
            if ( !$recipient->addressSuffix )
                $recipient->addressSuffix = $recipient->address_suffix;
            $recipient->address_suffix = null;
            if ( !$recipient->firstName )
                $recipient->firstName = $recipient->first_name;
            $recipient->first_name = null;
            if ( !$recipient->mailboxNo )
                $recipient->mailboxNo = $recipient->mailbox_no;
            $recipient->mailbox_no = null;

            // Strangely enough Barcode doesn't accept umlauts for these fields.
            $recipient->name1 = Mame_Helper_Functions::replace_special_chars( $recipient->name1 );
            $recipient->name2 = Mame_Helper_Functions::replace_special_chars( $recipient->name2 );
            $recipient->name3 = Mame_Helper_Functions::replace_special_chars( $recipient->name3 );
            $recipient->city  = Mame_Helper_Functions::replace_special_chars( $recipient->city );

            $item = new Barcode_Item();
            // $item->itemID    = '';
            $item->recipient = $recipient;

            $attributes       = new Barcode_Service_Code_Attributes();
            $attributes->przl = json_decode( $order->dispatch_type );
            // TODO Pro clima
            $additions = json_decode( $order->additions );
            if ( !empty ( $additions ) )
                $attributes->przl = array_merge( $attributes->przl, $additions );
            $attributes->weight = intval( $order->weight );

            $item->attributes                    = $attributes;
            $this->label_object                  = new Barcode_Generate_Label_Object();
            $this->label_object->language        = defined( 'ICL_LANGUAGE_CODE' ) ? strtoupper( ICL_LANGUAGE_CODE ) : strtoupper( substr( ( Mame_WC_Helper::is_woocommerce_version_up( '2.7.0' ) ? get_user_locale() : get_locale() ), 0, 2 ) );
            $this->label_object->frankingLicense = ( $options[ 'environment' ] == 'production' ? $options[ 'franking_license' ] : $options[ 'franking_license_test' ] );
            $this->label_object->labelDefinition = $label_def;
            $this->label_object->customer        = $customer;
            $this->label_object->item            = $item;
        }
    }

}