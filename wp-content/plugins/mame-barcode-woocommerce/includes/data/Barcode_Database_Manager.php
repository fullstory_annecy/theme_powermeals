<?php

if ( !defined( 'ABSPATH' ) ) {
    exit;
}

if ( !class_exists( 'Barcode_Database_Manager' ) ) {

    /**
     * Class Barcode_Database_Manager
     */
    class Barcode_Database_Manager
    {
        // tables
        const ORDER     = '_order';
        const LABEL     = '_label';
        const CUSTOMER  = '_customer';
        const RECIPIENT = '_recipient';
        const TEMPLATE  = '_template';

        public static function table( $name )
        {
            global $wpdb;
            return $wpdb->prefix . MAME_BC_PREFIX . $name;
        }

        /**
         * Creates database tables and relations.
         */
        public static function setup()
        {
            global $wpdb;
            $charset_collate = $wpdb->get_charset_collate();

            // tables
            $table_order     = static::table( static::ORDER );
            $table_label     = static::table( static::LABEL );
            $table_customer  = static::table( static::CUSTOMER );
            $table_recipient = static::table( static::RECIPIENT );
            $table_template  = static::table( static::TEMPLATE );

            // order
            $sql = "CREATE TABLE $table_order (
              id int NOT NULL AUTO_INCREMENT,
              order_type enum('order', 'draft', 'template'),
              franking_license varchar(8),
              pp_franking tinyint(1),
              customer_id int,
              customer_system varchar(255),
              label_id int,
              recipient_id int,
              przl text,
              free_text varchar(34),
              delivery_date int,
              parcel_no tinyint,
              parcel_total tinyint,
              delivery_place varchar(35),
              proclima tinyint(1),
              label_size varchar(4),
              weight mediumint,
              print_addresses enum('RECIPIENT_AND_CUSTOMER', 'ONLY_CUSTOMER', 'ONLY_RECIPIENT', 'NONE'),
              unnumber text,
              dispatch_type text,
              additions text,
              time int,
              wc_order_id int,
              image_id int,
              ident_code varchar(255),
              PRIMARY KEY  (id)
            ) $charset_collate;";

            // label
            $sql .= "CREATE TABLE $table_label (
              id int NOT NULL AUTO_INCREMENT,
              order_id int,
              label_layout varchar(2),
              print_addresses varchar(40),
              image_file_type varchar(5),
              image_resolution mediumint,
              print_preview tinyint(1),
              color_print_required tinyint(1),
              item_id varchar(255),
              ident_code varchar(40),
              filename varchar(255),
              PRIMARY KEY  (id)
            ) $charset_collate;";


            // customer
            $sql .= "CREATE TABLE $table_customer (
              id int NOT NULL AUTO_INCREMENT,
              name1 varchar(25),
              name2 varchar(25),
              street varchar(25),
              pobox varchar(25),
              zip varchar(6),
              city varchar(25),
              country varchar(2),
              logo_id int,
              logo_format varchar(3),
              logo_rotation smallint,
              logo_aspect_ratio varchar(20),
              logo_horizontal_align varchar(20),
              logo_vertical_align varchar(20),
              domicile_post_office varchar (35),
              PRIMARY KEY  (id)
            ) $charset_collate;";

            // recipient
            $sql .= "CREATE TABLE $table_recipient (
              id int NOT NULL AUTO_INCREMENT,
              post_ident varchar(15),
              title varchar(35),
              personally_addressed tinyint(1),
              first_name varchar(35),
              name1 varchar(35),
              name2 varchar(35),
              name3 varchar(35),
              address_suffix varchar(35),
              street varchar(35),
              house_no varchar(10),
              pobox varchar(35),
              floor_no varchar(5),
              mailbox_no varchar(10),
              zip varchar (10),
              city varchar (35),
              country varchar (2),
              phone varchar (20),
              mobile varchar (20),
              email varchar (160),
              PRIMARY KEY  (id)
            ) $charset_collate;";

            $sql .= "CREATE TABLE $table_template (
              id int NOT NULL AUTO_INCREMENT,
              name varchar(255),
              franking_license varchar(8),
              pp_franking tinyint(1),
              customer_id int,
              customer_system varchar(255),
              recipient_id int,
              przl text,
              free_text varchar(34),
              delivery_date int,
              parcel_no tinyint,
              parcel_total tinyint,
              delivery_place varchar(35),
              proclima tinyint(1),
              label_size varchar(4),
              weight mediumint,
              print_addresses enum('RECIPIENT_AND_CUSTOMER', 'ONLY_CUSTOMER', 'ONLY_RECIPIENT', 'NONE'),
              unnumber text,
              dispatch_type text,
              additions text,      
              wc_order_id int,
              image_id int,
              use_address tinyint(1),
              load_wc_address enum('shipping', 'billing', 'custom') NULL default NULL,
              use_sender_address tinyint(1),
              time int,    
              PRIMARY KEY  (id)
            ) $charset_collate;";

            require_once( ABSPATH . 'wp-admin/includes/upgrade.php' );
            $result = dbDelta( $sql );

            //var_dump($result);die();

            // Save db version.
            update_option( MAME_BC_PREFIX . '_db_version', MAME_BC_DB_VERSION );
        }

        /**
         * Returns all orders (order, draft, template).
         *
         * @return mixed
         */
        public static function get_orders()
        {
            return static::get_all_results( static::table( static::ORDER ) );
        }

        /**
         * @param $template_id
         * @return null|Barcode_Template
         */
        public static function get_template( $template_id )
        {
            if ( $template_id ) {
                $templates = static::get_all_where( static::table( static::TEMPLATE ), 'id', $template_id );
                if ( !empty( $templates ) ) {
                    return new Barcode_Template( $templates[ 0 ] );
                }
            }
            return null;
        }

        /**
         * @return Barcode_Template[]|null
         */
        public static function get_templates()
        {
            $templates = static::get_all_results( static::table( static::TEMPLATE ) );
            if ( !empty( $templates ) ) {
                return array_map( function ( $item ) {
                    return new Barcode_Template( $item );
                }, $templates );
            }
            return null;
        }

        /**
         * Returns the unique draft order (for WC order id if provided).
         *
         * @param null $wc_order_id
         * @return null|Barcode_Order
         */
        public static function get_draft_order( $wc_order_id = null )
        {
            global $wpdb;
            $table = static::table( static::ORDER );

            if ( $wc_order_id ) {
                $orders = $wpdb->get_results(
                    "
	            SELECT * 
	            FROM $table
	            WHERE order_type = 'draft' and wc_order_id = '$wc_order_id'
	            "
                );

            } else {
//                $orders = static::get_all_where( $table, 'status', 'draft' );
                $orders = $wpdb->get_results(
                    "
	            SELECT * 
	            FROM $table
	            WHERE order_type = 'draft' and ( wc_order_id IS NULL or wc_order_id = '0' or wc_order_id = 0 or wc_order_id = '' ) 
	            "
                );
            }

            if ( $orders && !empty( $orders ) )
                return new Barcode_Order( $orders[ 0 ] );

            return null;
        }

        /**
         * Returns the recipient with id $id.
         *
         * @param $id
         * @return Barcode_Recipient|null
         */
        public static function get_recipient( $id )
        {

            $recipients = static::get_all_where( static::table( static::RECIPIENT ), 'id', $id );
            if ( !empty( $recipients ) ) {
                return new Barcode_Recipient( $recipients[ 0 ] );
            }
            return null;
        }

        /**
         * Returns the customer with id $id.
         *
         * @param $id
         * @return Barcode_Customer|null
         */
        public static function get_customer( $id )
        {

            $customers = static::get_all_where( static::table( static::CUSTOMER ), 'id', $id );
            if ( !empty( $customers ) ) {
                return new Barcode_Customer( $customers[ 0 ] );
            }
            return null;
        }

        /**
         * Gets all finished orders by wc_order_id
         *
         * @param $wc_order_id
         * @return Barcode_Order[]|null
         */
        public static function get_orders_by_wc_order( $wc_order_id )
        {
            global $wpdb;
            $table = static::table( static::ORDER );

            $orders = $wpdb->get_results(
                "
	            SELECT * 
	            FROM $table
	            WHERE wc_order_id = '$wc_order_id' AND order_type = 'order'
	            "
            );

            if ( !empty( $orders ) ) {
                return array_map( function ( $item ) {
                    return new Barcode_Order( $item );
                }, $orders );
            }
            return null;
        }

        /**
         * Returns all Barcode labels of order with ID $order_id.
         *
         * @param $order_id
         * @return Barcode_Label[]|null
         */
        public static function get_labels_from_order( $order_id )
        {
            $labels = static::get_all_where( static::table( static::LABEL ), 'order_id', $order_id );
            if ( !empty( $labels ) ) {
                return array_map( function ( $item ) {
                    return new Barcode_Label( $item );
                }, $labels );
            }
            return null;
        }

        /**
         * Returns all results of table $table.
         *
         * @param $table
         * @return mixed
         */
        public static function get_all_results( $table )
        {
            global $wpdb;

            return $wpdb->get_results(
                "
	            SELECT * 
	            FROM $table
	            "
            );
        }

        /**
         * Returns all results of table $table with one condition.
         *
         * @param $table
         * @param $attribute
         * @param $value
         * @return mixed
         */
        public static function get_all_where( $table, $attribute, $value )
        {
            global $wpdb;

            return $wpdb->get_results(
                "
	            SELECT * 
	            FROM $table
	            WHERE $attribute = '$value'
	            "
            );
        }

        /**
         * Deletes a row from a table.
         *
         * @param $table
         * @param $where
         * @return mixed
         */
        public static function delete( $table, $where )
        {
            global $wpdb;

            return $wpdb->delete( $table, $where );
        }

        /**
         * Updates a row of a table.
         *
         * @param $table
         * @param $data
         * @param $where
         * @return mixed
         */
        public static function update( $table, $data, $where )
        {
            global $wpdb;
            return $wpdb->update( $table, $data, $where );
        }

        /**
         * @param Barcode_Template|null $template
         * @return int
         */
        public static function update_template( $template = null )
        {
            if ( isset( $template->id ) && !empty( $template->id ) ) {
                $old_template = static::get_template( $template->id );
                $customer_id  = $old_template->customer_id;
                $old_template->populate( $template );
                $template              = $old_template;
                $template->customer_id = $customer_id;
            }

            // Update sender address.
            if ( $template->customer_id ) {
                // Update existing address
                static::update_customer( $template->customer_id, $template->customer );
            } else {
                // Create new address and assign id.
                $sender_id             = static::save_customer( $template->customer );
                $template->customer_id = $sender_id;
            }

            return static::save_order( $template );
        }

        public static function update_draft_order( $args )
        {
            global $wpdb;

            $new_order = new Barcode_Order( $args );

            //$table_order = static::table( static::ORDER );
            $wc_order_id = $new_order->wc_order_id;

            // Get draft order
            $order = static::get_draft_order( $wc_order_id );

            if ( $order ) {
                // Draft order exists.

                $order->populate( $args );
            } else {
                // No existing draft.
                $order             = new Barcode_Order( $args );
                $order->order_type = 'draft';
                if ( $wc_order_id )
                    $order->wc_order_id = $wc_order_id;
            }

            if ( !empty( $new_order->dispatch_type ) && is_array( $new_order->dispatch_type ) )
                $order->dispatch_type = json_encode( $new_order->dispatch_type );

            if ( !empty( $new_order->additions ) && is_array( $new_order->additions ) )
                $order->additions = json_encode( $new_order->additions );

            // Update receiver address.
            if ( !empty( $new_order->address ) ) {
                if ( $order->recipient_id ) {
                    // Update existing address
                    static::update_address( $order->recipient_id, $new_order->address );
                } else {
                    // Create new address and assign id.
                    $order->recipient_id = static::save_address( $new_order->address );
                }
            }

            // Update sender address.
            if ( !empty( $new_order->sender ) ) {
                if ( $order->customer_id ) {
                    // Update existing address
                    static::update_customer( $order->customer_id, $new_order->sender );
                } else {
                    // Create new address and assign id.
                    $order->customer_id = static::save_customer( $new_order->sender );
                }
            }

            static::save_order( $order );
        }

        /**
         * Updates an order in the database.
         *
         * @param Barcode_Order|Barcode_Template|array $order
         * @return int
         */
        public static function save_order( $order )
        {
            global $wpdb;

            $args    = [];
            $formats = [];

            if ( isset( $order->id ) ) {
                $args[ 'id' ] = $order->id;
                $formats[]    = '%d';
            }

            if ( isset( $order->name ) ) {
                $args[ 'name' ] = $order->name;
                $formats[]      = '%s';
            }

            if ( isset( $order->load_wc_address ) ) {
                $args[ 'load_wc_address' ] = $order->load_wc_address;
                $formats[]                 = '%s';
            }

            if ( isset( $order->frankingLicense ) ) {
                $args[ 'franking_license' ] = $order->frankingLicense;
                $formats[]                  = '%s';
            }

            if ( isset( $order->ppFranking ) ) {
                $args[ 'pp_franking' ] = $order->ppFranking;
                $formats[]             = '%d';
            }

            if ( isset( $order->customer_id ) ) {
                $args[ 'customer_id' ] = $order->customer_id;
                $formats[]             = '%d';
            }

            if ( isset( $order->customerSystem ) ) {
                $args[ 'customer_system' ] = $order->customerSystem;
                $formats[]                 = '%s';
            }

            if ( isset( $order->label_id ) ) {
                $args[ 'label_id' ] = $order->label_id;
                $formats[]          = '%d';
            }

            if ( isset( $order->recipient_id ) ) {
                $args[ 'recipient_id' ] = $order->recipient_id;
                $formats[]              = '%d';
            }

            if ( isset( $order->przl ) ) {
                $args[ 'przl' ] = $order->przl;
                $formats[]      = '%s';
            }

            if ( isset( $order->label_size ) ) {
                $args[ 'label_size' ] = $order->label_size;
                $formats[]            = '%s';
            }

            if ( isset( $order->free_text ) ) {
                $args[ 'free_text' ] = $order->free_text;
                $formats[]           = '%s';
            }

            if ( isset( $order->delivery_date ) ) {
                $args[ 'delivery_date' ] = $order->delivery_date;
                $formats[]               = '%d';
            }

            if ( isset( $order->parcel_no ) ) {
                $args[ 'parcel_no' ] = $order->parcel_no;
                $formats[]           = '%d';
            }

            if ( isset( $order->parcel_total ) ) {
                $args[ 'parcel_total' ] = $order->parcel_total;
                $formats[]              = '%d';
            }

            if ( isset( $order->delivery_place ) ) {
                $args[ 'delivery_place' ] = $order->delivery_place;
                $formats[]                = '%s';
            }

            if ( isset( $order->proclima ) ) {
                $args[ 'proclima' ] = $order->proclima;
                $formats[]          = '%d';
            }

            if ( isset( $order->weight ) ) {
                $args[ 'weight' ] = $order->weight;
                $formats[]        = '%d';
            }

            if ( isset( $order->unnumber ) ) {
                $args[ 'unnumber' ] = $order->unnumber;
                $formats[]          = '%s';
            }

            if ( isset( $order->order_type ) ) {
                $args[ 'order_type' ] = $order->order_type;
                $formats[]            = '%s';
            }

            if ( isset( $order->dispatch_type ) ) {
                $args[ 'dispatch_type' ] = $order->dispatch_type;
                $formats[]               = '%s';
            }

            if ( isset( $order->additions ) ) {
                $args[ 'additions' ] = $order->additions;
                $formats[]           = '%s';
            }

            if ( isset( $order->time ) ) {
                $args[ 'time' ] = $order->time;
                $formats[]      = '%d';
            }

            if ( isset( $order->image_id ) ) {
                $args[ 'image_id' ] = $order->image_id;
                $formats[]          = '%s';
            }

            if ( isset( $order->print_addresses ) ) {
                $args[ 'print_addresses' ] = $order->print_addresses;
                $formats[]                 = '%s';
            }

            if ( isset( $order->wc_order_id ) ) {
                $args[ 'wc_order_id' ] = $order->wc_order_id;
                $formats[]             = '%d';
            }

            if ( isset( $order->ident_code ) ) {
                $args[ 'ident_code' ] = $order->ident_code;
                $formats[]             = '%s';
            }

            $table = static::table( static::ORDER );
            if ( $order instanceof Barcode_Template )
                $table = static::table( static::TEMPLATE );

            $result = $wpdb->replace( $table, $args, $formats );

            if ( !$result ) {
                return 0;
            }

            $id = $wpdb->insert_id;

            return $id;
        }

        /**
         * @param Barcode_Label $label
         * @return int
         */
        public static function save_label( $label )
        {
            global $wpdb;

            $args = [
                'order_id'             => $label->order_id,
                'label_layout'         => $label->labelLayout,
                'print_addresses'      => $label->printAddresses,
                'image_file_type'      => $label->imageFileType,
                'image_resolution'     => $label->imageResolution,
                'print_preview'        => $label->printPreview,
                'color_print_required' => $label->colorPrintRequired,
                'item_id'              => $label->itemId,
                'ident_code'           => $label->identCode,
                'filename'             => $label->filename,
            ];

            $formats = [
                '%d', // order_id
                '%s', // label_layout
                '%s', // print_addresses
                '%s', // image_file_type
                '%d', // image_resolution
                '%d', // print_preview
                '%d', // color_print_required
                '%s', // item_id
                '%s', // ident_code
                '%s', // filename
            ];

            $result = $wpdb->insert( static::table( static::LABEL ), $args, $formats );
            if ( !$result ) {
                //  db insert failed.
                return 0;
            }
            return $wpdb->insert_id;
        }

        public static function save_order_to_label( $order_id, $label_id )
        {
            global $wpdb;

            return $wpdb->insert( static::table( static::ORDER_TO_LABEL ), [ 'order_id' => $order_id, 'label_id' => $label_id ], [ '%d', '%d' ] );
        }

        /**
         * Updates an existing recipient address.
         *
         * @param $id
         * @param $args
         * @return int
         */
        public static function update_address( $id, $args )
        {
            $address_array = static::get_all_where( static::table( static::RECIPIENT ), 'id', $id )[ 0 ];

            $address = new Barcode_Recipient();
            if ( !empty( $address_array ) )
                $address->id = $address_array->id;

            $address->populate( $args );

            return static::save_address( $address );
        }

        /**
         * @param Barcode_Recipient $address
         * @return int
         */
        public static function save_address( $address )
        {
            global $wpdb;

            if ( is_array( $address ) )
                $address = (object)$address;

            $country = !empty( $address->country ) ? $address->country : 'CH';

            $args = [
                'name1'                => $address->name1,
                'first_name'           => $address->first_name,
                'name2'                => $address->name2,
                'name3'                => $address->name3,
                'title'                => $address->title,
                'post_ident'           => $address->postIdent,
                'personally_addressed' => $address->personallyAddressed,
                'address_suffix'       => $address->address_suffix,
                'street'               => $address->street,
                'house_no'             => $address->house_no,
                'pobox'                => $address->pobox,
                'floor_no'             => $address->floor_no,
                'mailbox_no'           => $address->mailbox_no,
                'zip'                  => $address->zip,
                'city'                 => $address->city,
                'country'              => $country,
                'phone'                => $address->phone,
                'mobile'               => $address->mobile,
                'email'                => $address->email,
            ];

            $formats = [
                '%s', // name1
                '%s', // first_name
                '%s', // name2
                '%s', // name3
                '%s', // title
                '%s', // post_ident
                '%d', // personally_addressed
                '%s', // address_suffix
                '%s', // street
                '%s', // house_no
                '%s', // pobox
                '%s', // floor_no
                '%s', // mailbox_no
                '%s', // zip
                '%s', // city
                '%s', // country
                '%s', // phone
                '%s', // mobile
                '%s', // email
            ];

            if ( isset( $address->id ) ) {
                $args[ 'id' ] = $address->id;
                $formats[]    = '%d';
            }

            $result = $wpdb->replace( static::table( static::RECIPIENT ), $args, $formats );
            if ( !$result ) {
                return 0;
            }

            // The id of the inserted result.
            return $wpdb->insert_id;
        }

        /**
         * Updates an existing customer address.
         *
         * @param $id
         * @param $args
         * @return int
         */
        public static function update_customer( $id, $args )
        {
            $address_array = static::get_all_where( static::table( static::CUSTOMER ), 'id', $id )[ 0 ];

            $address = new Barcode_Customer();
            if ( !empty( $address_array ) )
                $address->id = $address_array->id;

            $address->populate( $args );

            return static::save_customer( $address );
        }

        /**
         * @param Barcode_Recipient $address
         * @return int
         */
        public static function save_customer( $address )
        {
            global $wpdb;

            if ( is_array( $address ) )
                $address = (object)$address;

            $country = !empty( $address->country ) ? $address->country : 'CH';

            $args = [
                'name1'                => $address->name1,
                'name2'                => $address->name2,
                'street'               => $address->street,
                'pobox'                => $address->pobox,
                'zip'                  => $address->zip,
                'city'                 => $address->city,
                'country'              => $country,
                'domicile_post_office' => $address->domicile_post_office
            ];

            $formats = [
                '%s', // name1
                '%s', // name2
                '%s', // street
                '%s', // pobox
                '%s', // zip
                '%s', // city
                '%s', // country
                '%s', // domicile_post_office
            ];

            if ( isset( $address->id ) ) {
                $args[ 'id' ] = $address->id;
                $formats[]    = '%d';
            }

            $result = $wpdb->replace( static::table( static::CUSTOMER ), $args, $formats );
            if ( !$result ) {
                return 0;
            }

            // The id of the inserted result.
            return $wpdb->insert_id;
        }

        public static function save_file( $file, $upload_path = null, $filename = null )
        {
            if ( $file ) {

                if ( !$upload_path ) {
                    $upload_dir  = wp_upload_dir();
                    $upload_path = str_replace( '/', DIRECTORY_SEPARATOR, $upload_dir[ 'basedir' ] ) . DIRECTORY_SEPARATOR . 'webstamp' . DIRECTORY_SEPARATOR . 'stamps' . DIRECTORY_SEPARATOR;
                }

                if ( !$filename )
                    $filename = uniqid() . '.pdf';
                $hashed_filename = md5( $filename . microtime() ) . '_' . $filename;
                $upload          = file_put_contents( $upload_path . $hashed_filename, $file );
                return $hashed_filename;
            }
            return false;

        }
    }

}