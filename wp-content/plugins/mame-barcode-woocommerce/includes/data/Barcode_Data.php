<?php
if ( !defined( 'ABSPATH' ) ) {
    exit; // Exit if accessed directly
}

if ( !class_exists( 'Barcode_Data' ) ) {

    /**
     * Class Barcode_Data
     */
    class Barcode_Data
    {

        public static function get_label_sizes()
        {
            return [
                'A6' => __( 'A6', 'dhuett' ),
                'A7' => __( 'A7', 'dhuett' ),
            ];
        }

        public static function get_printer_resolutions()
        {
            return [
                200,
                300,
                600,
            ];
        }

        public static function get_image_formats()
        {
            return [
                'EPS',
                'GIF',
                'JPG', // not recommended as barcode may not have high enough quality
                'PNG',
                'PDF',
                'sPDF', // Format sPDF is a PDF file without embedded fonts. In order to display this format correctly, the Arial font must be installed on your computer. The generation and transmission times are faster with sPDF than with PDF.
                'ZPL2',
            ];
        }

        public static function get_print_address()
        {
            return [
                'RECIPIENT_AND_CUSTOMER' => __( 'Recipient and sender', 'dhuett' ),
                'ONLY_CUSTOMER'          => __( 'Only sender', 'dhuett' ),
                'ONLY_RECIPIENT'         => __( 'Only recipient', 'dhuett' ),
                'NONE'                   => __( 'No address', 'dhuett' ),
            ];
        }

        public static function get_dispatch_types()
        {
            return [
                'ECO'    => [
                    'title' => __( 'PostPac Economy', 'dhuett' ),
                ],
                'PRI'    => [
                    'title' => __( 'PostPac Priority', 'dhuett' ),
                ],
                'SEM'    => [
                    'title' => __( 'Swiss-Express "Moon"', 'dhuett' ),
                ],
                'SKB'    => [
                    'title' => __( 'SameDay afternoon/evening', 'dhuett' ),
                ],
                'VL'     => [
                    'title' => __( 'VinoLog', 'dhuett' ),
                ],
                // https://www.post.ch/de/geschaeftlich/themen-a-z/branchenloesungen/branchenloesung-handel/direct-mit-abendzustellung?inMobileApp=false&inIframe=false&lang=de
                /*
                'DIRECT' => [
                    'title' => __( 'Direct', 'dhuett' )
                ],
                */

            ];
        }

        public static function get_weights()
        {
            return [
                '2000'  => __( 'Up to 2 kg' ),
                '5000'  => __( 'Up to 5 kg' ),
                '10000' => __( 'Up to 10 kg' ),
                '20000' => __( 'Up to 20 kg' ),
                '30000' => __( 'Up to 30 kg' ),
            ];
        }

        // TODO Conditions
        public static function get_basic_services()
        {
            return [
                'eco'        => [
                    'value' => [ 'ECO' ],
                    'name'  => __( 'PostPac Economy', 'dhuett' ),
                ],
                'pri'        => [
                    'value' => [ 'PRI' ],
                    'name'  => __( 'PostPac Priority', 'dhuett' )
                ],
                'sp_eco'     => [
                    'value' => [ 'SP', 'ECO' ],
                    'name'  => __( 'Bulky goods Economy', 'dhuett' ),
                ],
                'sp_pri'     => [
                    'value' => [ 'SP', 'PRI' ],
                    'name'  => __( 'Bulky goods Economy', 'dhuett' ),
                ],
                'ppr'        => [
                    'value' => [ 'PPR' ],
                    'name'  => __( 'PostPac Promo', 'dhuett' ),
                ],
                'sem'        => [
                    'value' => [ 'SEM' ],
                    'name'  => __( 'Swiss-Express "Moon"', 'dhuett' ),
                ],
                'sem_sp'     => [
                    'value' => [ 'SEM', 'SP' ],
                    'name'  => __( 'Bulky goods "Moon"', 'dhuett' ),
                ],
                'skb'        => [
                    'value' => [ 'SKB' ],
                    'name'  => __( 'SameDay afternoon/evening', 'dhuett' ),
                ],
                'skb_sp'     => [
                    'value' => [ 'SKB', 'SP' ],
                    'name'  => __( 'SameDay afternoon/evening bulky goods', 'dhuett' ),
                ],
                'vl'         => [
                    'value' => [ 'VL' ],
                    'name'  => __( 'VinoLog', 'dhuett' ),
                ],
                // TODO Basic service DIRECT can only be used in conjunction with AZS.
                'direct'     => [
                    'value' => [ 'DIRECT' ],
                    'name'  => __( 'Direct', 'dhuett' ),
                ],
                'gas_eco'    => [
                    'value' => [ 'GAS', 'ECO' ],
                    'name'  => __( 'PostPac Economy GAS', 'dhuett' ),
                ],
                'gas_pri'    => [
                    'value' => [ 'GAS', 'PRI' ],
                    'name'  => __( 'PostPac Priority GAS', 'dhuett' ),
                ],
                'gas_sp_eco' => [
                    'value' => [ 'GAS', 'SP', 'ECO' ],
                    'name'  => __( 'Bulky goods Economy GAS', 'dhuett' ),
                ],
                'gas_sp_pri' => [
                    'value' => [ 'GAS', 'SP', 'PRI' ],
                    'name'  => __( 'Bulky goods Priority GAS', 'dhuett' ),
                ],
                'gas_sem'    => [
                    'value' => [ 'GAS', 'SEM' ],
                    'name'  => __( 'Swiss-Express "Moon" GAS', 'dhuett' ),
                ],
                'gas_skb'    => [
                    'value' => [ 'GAS', 'SKB' ],
                    'name'  => __( 'SameDay afternoon/evening GAS', 'dhuett' ),
                ],
            ];
        }

        public static function get_delivery_instructions()
        {
            return [
                'ZAW3211' => __( 'Direct delivery to an upper floor (A)', 'dhuett' ),
                'ZAW3212' => __( 'Do not place in letterbox; deliver manually or notify (B)', 'dhuett' ),
                'ZAW3213' => __( 'Notify delivery by telephone (C)', 'dhuett' ),
                'ZAW3214' => __( 'Place in letterbox or at front door (D)', 'dhuett' ),
                'ZAW3215' => __( 'Deliver contents; take back box (K)', 'dhuett' ),
                'ZAW3216' => __( 'Failed delivery; return item as priority on the same day (E)', 'dhuett' ),
                'ZAW3217' => __( 'Specific delivery date, deliver on ... (F)', 'dhuett' ),
                'ZAW3218' => __( 'Deliver when all items have arrived (G)', 'dhuett' ),
                'ZAW3219' => __( 'Deposit item (H)', 'dhuett' ),
                'ZAW3220' => __( 'Follow delivery information in document pouch (I)', 'dhuett' ),
                'ZAW3222' => __( 'Present item; leave in cellar (L)', 'dhuett' ),
                // For the collection of empty containers or materials for recycling – please contact your customer advisor for further information.
                'ZAW3232' => __( 'You require a contract with Post CH Ltd', 'dhuett' ),
                // Only available in conjunction with notification service code 128 (“Exchange/return”).
                'ZAW3233' => __( 'Exchange/Return', 'dhuett' ),
                'ZAW3234' => __( 'Do not deliver to mailbox or neighbour: do not leave anywhere', 'dhuett' ),
            ];
        }

        public static function get_additional_services()
        {
            return array_merge( static::get_additional_services_liability(), static::get_additional_services_time(), static::get_additional_services_delicate_goods() );
        }

        public static function get_additional_services_liability()
        {
            return [
                'FRA'  => __( 'Fragile', 'dhuett' ),
                'RMP'  => __( 'Personal delivery', 'dhuett' ),
                'SI'   => __( 'Signature', 'dhuett' ),
                'AS'   => __( 'Signature (insurance)', 'dhuett' ),
                'COLD' => __( 'Disposet Cold', 'dhuett' ),
                'BLN'  => __( 'Electronic COD', 'dhuett' ),
            ];
        }

        public static function get_additional_services_time()
        {
            return [
                'SA'      => __( 'Saturday delivery', 'dhuett' ),
                // TODO When using the AZS (Evening delivery) and ZFZ (Time slot delivery) value-added services, we recommend you first perform an area check for each recipient address via your connection to the Digital Commerce API – Swiss Post shipping options.
                'AZS'     => __( 'Evening delivery', 'dhuett' ),
                'ZFZ0912' => __( 'Time slot delivery 9-12', 'dhuett' ),
                'ZFZ1114' => __( 'Time slot delivery 11-14', 'dhuett' ),
                'ZFZ1316' => __( 'Time slot delivery 13-16', 'dhuett' ),
                'ZFZ1518' => __( 'Time slot delivery 15-18', 'dhuett' ),
            ];
        }

        public static function get_additional_services_delicate_goods()
        {
            return [
                'MAN' => __( 'Manual processing', 'dhuett' ),
                'LQ'  => __( 'Limited Quantities (hazardous goods)', 'dhuett' ),
            ];
        }

        public static function get_combination_codes()
        {
            return [
                'PRISI'   => [ 'PRI', 'SI' ],
                'PRIAZS'  => [ 'PRI', 'AZS' ],
                // (NB: only one S in DLC)
                'PRIAZSI' => [ 'PRI', 'AZS', 'SI' ],
                'DIRAZS'  => [ 'DIRECT', 'AZS' ],
                // (NB: only one S in DLC)
                'DIRAZS'  => [ 'DIRECT', 'AZS', 'SI' ],
                'PRI0912' => [ 'PRI', 'ZFZ0912' ],
                'PRI1114' => [ 'PRI', 'ZFZ1114' ],
                'PRI1316' => [ 'PRI', 'ZFZ1316' ],
                'PRI1518' => [ 'PRI', 'ZFZ1518' ],
                'PRISI09' => [ 'PRI', 'SI', 'ZFZ0912' ],
                'PRISI11' => [ 'PRI', 'SI', 'ZFZ1114' ],
                'PRISI13' => [ 'PRI', 'SI', 'ZFZ1316' ],
                'PRISI15' => [ 'PRI', 'SI', 'ZFZ1518' ],
            ];
        }
    }

}