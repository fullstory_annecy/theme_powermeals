<?php

if ( !defined( 'ABSPATH' ) ) {
    exit; // Exit if accessed directly
}

if ( !class_exists( 'Barcode_Label' ) ) {

    /**
     * Class Barcode_Label
     *
     * Represents the label table in the db.
     */
    class Barcode_Label extends Barcode_Label_Definition implements Mame_WP_Active_Record
    {
        /**
         * @var int
         */
        public $id;

        /**
         * @var int
         */
        public $order_id;

        /**
         * Returns a new query object for the calling class.
         *
         * @return Mame_WP_DB_Query
         */
        public static function find()
        {
            return new Mame_WP_DB_Query( Barcode_Database_Manager::table( Barcode_Database_Manager::LABEL ), get_class() );
        }

        /**
         * Saves/updates a record.
         *
         * @return mixed
         */
        public static function save()
        {
            // TODO: Implement save() method.
        }

        /**
         * Updates a record.
         *
         * @return mixed
         */
        public static function update()
        {
            // TODO: Implement update() method.
        }

        /**
         * Returns the content of the stamp image file. Returns false if file not found.
         *
         * @return bool|string
         */
        public function get_file()
        {
            if ( $this->filename ) {
                $upload_dir = wp_upload_dir();
                $file       = $upload_dir[ 'basedir' ] . '/mame_barcode/stamps/' . $this->filename;
                return file_get_contents( $file );
            }
            return false;
        }

        /**
         * Returns the URL to download the label.
         *
         * @return null|string
         */
        public function get_file_url()
        {
            if ( $this->filename )
                return add_query_arg( [ MAME_BC_PREFIX . '-type' => 'stamps', MAME_BC_PREFIX . '-file' => $this->filename, MAME_BC_PREFIX . '-download' => '1' ], get_admin_url() );
            return null;
        }

        /**
         * Returns the html file download button.
         *
         * @param $text
         * @param string $class
         * @return null|string
         */
        public function get_file_button( $text, $class = MAME_BC_PREFIX . '-btn ' . MAME_BC_PREFIX . '-download-stamp-btn' )
        {
            $url = $this->get_file_url();
            if ( $url )
                return '<a class="' . $class . '" .  href="' . $this->get_file_url() . '" target="_blank">' . $text . '</a>';
            return '';
        }
    }

}