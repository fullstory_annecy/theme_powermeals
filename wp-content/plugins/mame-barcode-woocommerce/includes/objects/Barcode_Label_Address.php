<?php
if ( !defined( 'ABSPATH' ) ) {
    exit; // Exit if accessed directly
}

if ( !class_exists( 'Barcode_Label_Address' ) ) {

    /**
     * Class Barcode_Label_Address
     */
    class Barcode_Label_Address extends Mame_Object
    {
        /**
         * @var string Optional, used to declare a specific address on the label. If omitted, the address data of the recipient is taken. Useful if the address data of the recipient exceeds the length of the label.
         */
        public $labelLine;
    }

}