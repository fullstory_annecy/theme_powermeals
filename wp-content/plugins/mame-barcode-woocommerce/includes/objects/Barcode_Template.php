<?php

if ( !defined( 'ABSPATH' ) ) {
    exit; // Exit if accessed directly
}

if ( !class_exists( 'Barcode_Template' ) ) {

    class Barcode_Template extends Barcode_Order
    {
        const LOAD_WC_ADDRESS_SHIPPING = 'shipping';
        const LOAD_WC_ADDRESS_BILLING  = 'billing';
        const LOAD_WC_ADDRESS_CUSTOM   = 'custom';

        /**
         * @var string
         */
        public $name;

        /**
         * @var string
         */
        public $load_wc_address;

        /**
         * Updates a template.
         *
         * @param $data
         * @return array
         */
        public static function save_template_data( $data )
        {
            $errors = [];
            if ( !isset( $data[ MAME_BC_PREFIX . '-name' ] ) || empty( $data[ MAME_BC_PREFIX . '-name' ] ) )
                $errors[] = __( 'Name field is mandatory', 'dhuett' );

            if ( empty( $errors ) ) {

                $sender   = new Barcode_Customer();
                $template = new Barcode_Template( $data );

                foreach ( $data as $k => $v ) {

                    // Convert data names.
                    if ( substr( $k, 0, strlen( MAME_BC_PREFIX . '-' ) ) === MAME_BC_PREFIX . '-' ) {
                        $key = substr( $k, strlen( MAME_BC_PREFIX . '-' ) );
                        if ( substr( $key, 0, strlen( 'sender-' ) ) === 'sender-' ) {
                            // Get sender address
                            $s_key          = substr( $key, strlen( 'sender-' ) );
                            $sender->$s_key = $v;
                        } else {
                            $template->$key = $v;
                        }
                    }
                }

                if ( !isset( $data[ MAME_BC_PREFIX . '-additions' ] ) ) {
                    $data[ MAME_BC_PREFIX . '-additions' ] = [];
                }
                $template->additions     = json_encode( $data[ MAME_BC_PREFIX . '-additions' ] );
                $template->dispatch_type = json_encode( $data[ MAME_BC_PREFIX . '-dispatch_type' ] );

                $template->customer = $sender;
                $template->time     = time();

                //var_dump($template);die();

                // Save data
                $id = Barcode_Database_Manager::update_template( $template );
                if ( $id ) {
                    wp_redirect( admin_url( 'admin.php?page=mame_bc_menu_add_template&mame_bc_template_saved=1&id=' . $id ) );
                    exit;
                } else {
                    $errors[] = __( 'Failed to save order.', 'dhuett' );
                }

            }
            return $errors;
        }

        /**
         * Returns form errors as formatted html.
         *
         * @param $errors
         */
        public static function template_errors_html( $errors )
        {
            ?>
            <div class="<?= MAME_BC_PREFIX ?>-template-errors">
                <?= '<p>' . implode( '</p><p>', $errors ) . '</p>'; ?>
            </div>
            <?php
        }

        /**
         * Notice after successfully updating a template.
         */
        public static function saved_success()
        {
            if ( !isset( $_GET[ 'mame_ws_template_saved' ] ) )
                return;
            ?>
            <div class="notice notice-success is-dismissible">
                <p><?php _e( 'Template updated.', 'sample-text-domain' ); ?></p>
            </div>
            <?php
        }
    }

}