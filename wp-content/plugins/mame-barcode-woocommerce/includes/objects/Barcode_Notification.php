<?php
if ( !defined( 'ABSPATH' ) ) {
    exit; // Exit if accessed directly
}

if ( !class_exists( 'Barcode_Notification' ) ) {

    /**
     * Class Barcode_Notification
     */
    class Barcode_Notification extends Mame_Object
    {
        const TYPE_EMAIL = 'EMAIL';
        const TYPE_SMS   = 'SMS';

        /**
         * @var Barcode_Communication
         */
        public $communication;

        /**
         * @var string
         */
        public $service;

        /**
         * @var string
         */
        public $freeText1;

        /**
         * @var string
         */
        public $freeText2;

        /**
         * @var string One of [ DE, FR, IT, EN ]
         */
        public $language;

        /**
         * @var string One of [ EMAIL, SMS ]
         */
        public $type;

        /**
         * Returns all possible values for $language.
         *
         * @return string[]
         */
        public static function get_language_values()
        {
            return [
                'DE', 'FR', 'IT', 'EN',
            ];
        }

        /**
         * Returns all possible values for $type.
         *
         * @return string[]
         */
        public static function get_type_values()
        {
            return [
               static::TYPE_EMAIL,
               static::TYPE_SMS,
            ];
        }
    }

}