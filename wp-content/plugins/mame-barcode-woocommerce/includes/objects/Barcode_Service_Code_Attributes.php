<?php
if ( !defined( 'ABSPATH' ) ) {
    exit; // Exit if accessed directly
}

if ( !class_exists( 'Barcode_Service_Code_Attributes' ) ) {

    /**
     * Class Barcode_Service_Code_Attributes
     */
    class Barcode_Service_Code_Attributes extends Mame_Object
    {
        /**
         * @var string[]
         */
        public $przl;

        /**
         * @var string
         */
        public $freeText;

        /**
         * @var string
         */
        public $deliveryDate;

        /**
         * @var int
         */
        public $parcelNo;

        /**
         * @var int
         */
        public $parcelTotal;

        /**
         * @var string
         */
        public $deliveryPlace;

        /**
         * @var bool
         */
        public $proClima;

        /**
         * @var int
         */
        public $weight;

        /**
         * @var int[]
         */
        public $unnumbers;

    }

}