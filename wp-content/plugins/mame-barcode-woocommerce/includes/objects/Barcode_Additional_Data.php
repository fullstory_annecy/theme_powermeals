<?php
if ( !defined( 'ABSPATH' ) ) {
    exit; // Exit if accessed directly
}

if ( !class_exists( 'Barcode_Additional_Data' ) ) {

    /**
     * Class Barcode_Additional_Data
     */
    class Barcode_Additional_Data extends Mame_Object
    {
        /**
         * @var string
         */
        public $type;

        /**
         * @var string
         */
        public $value;

    }

}