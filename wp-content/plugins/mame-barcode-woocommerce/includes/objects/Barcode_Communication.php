<?php
if ( !defined( 'ABSPATH' ) ) {
    exit; // Exit if accessed directly
}

if ( !class_exists( 'Barcode_Communication' ) ) {

    /**
     * Class Barcode_Communication
     */
    class Barcode_Communication extends Mame_Object
    {
        /**
         * @var string
         */
        public $email;

        /**
         * @var string
         */
        public $mobile;

    }

}