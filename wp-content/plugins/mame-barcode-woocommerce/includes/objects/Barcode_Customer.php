<?php
if ( !defined( 'ABSPATH' ) ) {
    exit; // Exit if accessed directly
}

if ( !class_exists( 'Barcode_Customer' ) ) {

    /**
     * Class Barcode_Customer
     */
    class Barcode_Customer extends Mame_Object implements Mame_WP_Active_Record
    {
        const LOGO_ASPECT_RATIO_EXPAND           = 'EXPAND';
        const LOGO_ASPECT_RATIO_KEEP             = 'KEEP';
        const LOGO_HORIZONTAL_ALIGN_WITH_CONTENT = 'WITH_CONTENT';
        const LOGO_HORIZONTAL_ALIGN_LEFT         = 'LEFT';
        const LOGO_VERTICAL_ALIGN_TOP            = 'TOP';
        const LOGO_VERTICAL_ALIGN_MIDDLE         = 'MIDDLE';

        /**
         * @var string
         */
        public $name1;

        /**
         * @var string
         */
        public $name2;

        /**
         * @var string
         */
        public $street;

        /**
         * @var string
         */
        public $zip;

        /**
         * @var string
         */
        public $city;

        /**
         * @var string
         */
        public $country;

        /**
         * @var string
         */
        public $logo;

        /**
         * @var string
         */
        public $logoFormat;

        /**
         * @var int One of [ 0, 90, 180, 270 ]
         */
        public $logoRotation;

        /**
         * @var string One of [ EXPAND, KEEP ]
         */
        public $logoAspectRatio;

        /**
         * @var string One of [ WITH_CONTENT, LEFT ]
         */
        public $logoHorizontalAlign;

        /**
         * @var string One of [ TOP, MIDDLE ]
         */
        public $logoVerticalAlign;

        /**
         * @var string
         */
        public $domicilePostOffice;
        public $domicile_post_office;

        /**
         * @var string
         */
        public $pobox;

        /**
         * Returns all possible values for $logoRotation.
         *
         * @return int[]
         */
        public static function get_logo_rotation_values()
        {
            return [
                0,
                90,
                180,
                270,
            ];
        }

        /**
         * Returns all possible values for $logoAspectRatio.
         *
         * @return array
         */
        public static function get_logo_aspect_ration_values()
        {
            return [
                static::LOGO_ASPECT_RATIO_EXPAND,
                static::LOGO_ASPECT_RATIO_KEEP,
            ];
        }

        /**
         * Returns all possible values for $logoHorizontalAlign.
         *
         * @return array
         */
        public static function get_logo_horizontal_align_values()
        {
            return [
                static::LOGO_HORIZONTAL_ALIGN_WITH_CONTENT,
                static::LOGO_HORIZONTAL_ALIGN_LEFT,
            ];
        }

        /**
         * Returns all possible values for $logoVerticalAlign.
         *
         * @return array
         */
        public static function get_logo_vertical_align_values()
        {
            return [
                static::LOGO_VERTICAL_ALIGN_MIDDLE,
                static::LOGO_VERTICAL_ALIGN_TOP,
            ];
        }

        /**
         * Returns the default customer address saved in the settings.
         *
         * @return Barcode_Customer
         */
        public static function get_default_address()
        {
            $address = new self();
            $options = get_option( 'mame_bc_options_group' );
            $vars    = get_object_vars( $address );
            foreach ( $vars as $name => $value ) {
                $name = Mame_Helper_Functions::camelcase_to_underscore( $name );
                if ( array_key_exists( $name, $options ) ) {
                    $address->$name = $options[ $name ];
                }
            }

            return $address;
        }

        public function get_html( $delimiter = '<br>' )
        {
            $array = [];

            if ( $this->name1 )
                $array[] = $this->name1;
            if ( $this->name2 )
                $array[] = $this->name2;
            if ( $this->street )
                $array[] = $this->street;
            if ( $this->zip )
                $array[] = $this->zip;
            if ( $this->city )
                $array[] = $this->city;

            return implode( $delimiter, $array );
        }

        public static function table_name()
        {
            // TODO: Implement table_name() method.
        }

        public static function class_name()
        {
            // TODO: Implement class_name() method.
        }

        /**
         * Returns a new query object for the calling class.
         *
         * @return Mame_WP_DB_Query
         */
        public static function find()
        {
            return new Mame_WP_DB_Query( Barcode_Database_Manager::table( Barcode_Database_Manager::CUSTOMER ), get_class() );
        }

        /**
         * Saves/updates a record.
         *
         * @return mixed
         */
        public static function save()
        {
            // TODO: Implement save() method.
        }

        /**
         * Updates a record.
         *
         * @return mixed
         */
        public static function update()
        {
            // TODO: Implement update() method.
        }
    }

}