<?php

if ( !defined( 'ABSPATH' ) ) {
    exit; // Exit if accessed directly
}

if ( !class_exists( 'Barcode_Order' ) ) {

    /**
     * Class Barcode_Order
     *
     * The order table in the db.
     */
    class Barcode_Order extends Barcode_Generate_Label_Object implements Mame_WP_Active_Record
    {
        const TYPE_ORDER    = 'order';
        const TYPE_DRAFT    = 'draft';
        const TYPE_TEMPLATE = 'template';

        const ADDRESS_R_C = 'RECIPIENT_AND_CUSTOMER';
        const ADDRESS_C   = 'ONLY_CUSTOMER';
        const ADDRESS_R   = 'ONLY_RECIPIENT';
        const ADDRESS_    = 'NONE';

        /**
         * @var int
         */
        public $id;

        /**
         * @var string one of [order, draft, template]
         */
        public $order_type;

        /**
         * @var int id of the Barcode_Customer object.
         */
        public $customer_id;

        /**
         * @var int id of the Barcode_Label object.
         */
        public $label_id;

        /**
         * @var int id of the Barcode_Recipient object;
         */
        public $recipient_id;

        /**
         * @var Barcode_Recipient The recipients address.
         */
        public $address;

        /**
         * @var Barcode_Customer
         */
        public $sender;

        /**
         * @var string
         */
        public $przl;

        /**
         * @var string
         */
        public $label_size;

        /**
         * @var string
         */
        public $dispatch_type;

        /**
         * @var string
         */
        public $additions;

        /**
         * @var string
         */
        public $free_text;

        /**
         * @var int
         */
        public $delivery_date;

        /**
         * @var int
         */
        public $parcel_no;

        /**
         * @var int
         */
        public $parcel_total;

        /**
         * @var string
         */
        public $delivery_place;

        /**
         * @var bool
         */
        public $proclima;

        /**
         * @var int
         */
        public $weight;

        /**
         * @var string
         */
        public $print_addresses;

        /**
         * @var string
         */
        public $unnumber;

        /**
         * @var int
         */
        public $image_id;

        /**
         * @var string
         */
        public $image_url;

        /**
         * @var string
         */
        public $time;

        /**
         * @var int
         */
        public $wc_order_id;

        /**
         * @var string
         */
        public $ident_code;

        /**
         * @var Barcode_Label_Definition
         */
        public $labelDefinition;

        /**
         * Returns a new query object for the calling class.
         *
         * @return Mame_WP_DB_Query
         */
        public static function find()
        {
            return new Mame_WP_DB_Query( Barcode_Database_Manager::table( Barcode_Database_Manager::ORDER ), get_class() );
        }

        /**
         * Saves/updates a record.
         *
         * @return mixed
         */
        public static function save()
        {
            // TODO: Implement save() method.
        }

        /**
         * Updates a record.
         *
         * @return mixed
         */
        public static function update()
        {
            // TODO: Implement update() method.
        }

        /**
         * Deletes the $properties from the order.
         *
         * @param $properties
         */
        public function delete_properties( $properties )
        {
            if ( !empty( $properties ) ) {
                foreach ( $properties as $p ) {
                    Barcode_Database_Manager::update( Barcode_Database_Manager::table( Barcode_Database_Manager::ORDER ), [ $p => '' ], [ 'id' => $this->id ] );
                }
            }
        }

        /**
         * Returns the recipeint address as a Barcode_Recipient object.
         *
         * @return Barcode_Recipient
         */
        public function get_recipient()
        {
            return Barcode_Database_Manager::get_recipient( $this->recipient_id );
        }

        /**
         * Returns the customer (sender) address as a Barcode_Customer object.
         *
         * @return Barcode_Customer
         */
        public function get_customer()
        {
            return Barcode_Database_Manager::get_customer( $this->customer_id );
        }

        /**
         * Returns all labels belonging to this order.
         *
         * @return Barcode_Label[]|null
         */
        public function get_labels()
        {
            return Barcode_Database_Manager::get_labels_from_order( $this->id );
        }

        /**
         * Returns all Webstamp_Stamp objects belonging to the WC order $order.
         *
         * @param $order
         * @return bool|Barcode_Label[]
         */
        public static function get_labels_from_woocommerce_order( $order )
        {
            $bc_orders = Barcode_Database_Manager::get_orders_by_wc_order( Mame_WC_Helper::get ('id', $order) );
            if ( !empty( $bc_orders ) ) {
                foreach ( $bc_orders as $o ) {
                    $labels = $o->get_labels();
                    if ( !empty( $labels ) ) {
                        return $labels;
                    }
                }
            }
            return false;
        }

        /**
         * Returns the formatted HTML string of all the stamp image files of the WC order $order.
         *
         * @param WC_Order $order
         * @param bool $width
         * @param bool $height
         * @param bool $newline
         * @return string
         */
        public static function get_label_images_from_woocommerce_order( $order, $width = false, $height = false, $newline = true )
        {
            $labels = static::get_labels_from_woocommerce_order( $order );
            $str    = '';
            if ( $labels ) {
                foreach ( $labels as $s ) {
                    $str .= '<img ' . ( $width ? 'width="' . $width . '" ' : '' ) . ( $height ? 'height="' . $height . '" ' : '' ) . 'src="' . $s->get_file_url() . '">';
                    if ( $newline )
                        $str .= '<br>';
                }
            }
            return $str;
        }

        /**
         * Prints the formatted HTML string of all the stamp image files of the WC order $order.
         *
         * @param WC_Order $order
         * @param bool $width
         * @param bool $height
         * @param bool $newline
         */
        public static function print_label_images_from_woocommerce_order( $order, $width = false, $height = false, $newline = true )
        {
            echo static::get_label_images_from_woocommerce_order( $order, $width, $height, $newline );
        }
    }

}