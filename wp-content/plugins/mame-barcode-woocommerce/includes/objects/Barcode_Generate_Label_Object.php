<?php

if ( !defined( 'ABSPATH' ) ) {
    exit; // Exit if accessed directly
}

if ( !class_exists( 'Barcode_Generate_Label_Object' ) ) {

    /**
     * Class Barcode_Generate_Label_Object
     */
    class Barcode_Generate_Label_Object extends Mame_Object
    {
        /**
         * @var string One of [ DE, FR, IT, EN ]
         */
        public $language;

        /**
         * @var string
         */
        public $frankingLicense;

        /**
         * @var bool
         */
        public $ppFranking;

        /**
         * @var Barcode_Customer
         */
        public $customer;

        /**
         * @var string
         */
        public $customerSystem;

        /**
         * @var Barcode_Label_Definition
         */
        public $labelDefinition;

        /**
         * @var string
         */
        public $sendingID;

        /**
         * @var Barcode_Item
         */
        public $item;

        /**
         * Returns all possible values for $language.
         *
         * @return string[]
         */
        public static function get_language_values()
        {
            return [
                'DE', 'FR', 'IT', 'EN',
            ];
        }

    }

}