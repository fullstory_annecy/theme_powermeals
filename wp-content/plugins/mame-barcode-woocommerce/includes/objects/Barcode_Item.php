<?php
if ( !defined( 'ABSPATH' ) ) {
    exit; // Exit if accessed directly
}

if ( !class_exists( 'Barcode_Item' ) ) {

    /**
     * Class Barcode_Item
     */
    class Barcode_Item extends Mame_Object
    {

        public function __construct( $object = null )
        {
            $this->notification = [];
            parent::__construct( $object );
        }

        /**
         * @var string
         */
        public $itemID;

        /**
         * @var string
         */
        public $itemNumber;

        /**
         * @var Barcode_Recipient
         */
        public $recipient;

        /**
         * @var Barcode_Additional_Data[]
         */
        public $additionalData;

        /**
         * @var Barcode_Service_Code_Attributes
         */
        public $attributes;

        /**
         * @var Barcode_Notification[]
         */
        public $notification;


    }

}