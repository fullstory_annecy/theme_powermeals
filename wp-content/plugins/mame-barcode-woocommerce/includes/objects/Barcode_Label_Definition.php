<?php
if ( !defined( 'ABSPATH' ) ) {
    exit; // Exit if accessed directly
}

if ( !class_exists( 'Barcode_Label_Definition' ) ) {

    /**
     * Class Barcode_Label_Definition
     */
    class Barcode_Label_Definition extends Mame_Object
    {
        const PRINT_ADRESSES_NONE               = 'NONE';
        const PRINT_ADRESSES_RECIPIENT          = 'ONLY_RECIPIENT';
        const PRINT_ADRESSES_CUSTOMER           = 'ONLY_CUSTOMER';
        const PRINT_ADRESSES_RECIPIENT_CUSTOMER = 'RECIPIENT_AND_CUSTOMER';

        /**
         * @var string
         */
        public $labelLayout;

        /**
         * @var string One of [ NONE, ONLY_RECIPIENT, ONLY_CUSTOMER, RECIPIENT_AND_CUSTOMER ]
         */
        public $printAddresses;

        /**
         * @var string
         */
        public $imageFileType;

        /**
         * @var int
         */
        public $imageResolution;

        /**
         * @var bool
         */
        public $printPreview;

        /**
         * @var bool
         */
        public $colorPrintRequired;

        /**
         * @var string
         */
        public $itemId;

        /**
         * @var string
         */
        public $identCode;

        /**
         * @var string
         */
        public $filename;

        /**
         * Retunrs all possible values for $printAddresses.
         *
         * @return array
         */
        public static function get_print_addresses_values()
        {
            return [
                static::PRINT_ADRESSES_NONE,
                static::PRINT_ADRESSES_RECIPIENT,
                static::PRINT_ADRESSES_CUSTOMER,
                static::PRINT_ADRESSES_RECIPIENT_CUSTOMER,
            ];
        }

    }

}