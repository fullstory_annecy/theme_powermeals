<?php

if ( !defined( 'ABSPATH' ) ) {
    exit; // Exit if accessed directly
}

if ( !class_exists( 'Barcode_Order_List_Table' ) ) :

    /**
     * Displays a WP_List_Table with Barcode orders.
     */
    class Barcode_Order_List_Table extends WP_List_Table
    {
        public $table;
        public $wc_order_id;

        /**
         * Barcode_Order_List_Table constructor.
         * @param null $wc_order_id
         */
        public function __construct( $wc_order_id = null )
        {
            parent::__construct( [
                'singular' => __( 'Order', 'dhuett' ), //singular name of the listed records
                'plural'   => __( 'Orders', 'dhuett' ), //plural name of the listed records
                'ajax'     => false //should this table support ajax?

            ] );

            $this->table       = Barcode_Database_Manager::table( Barcode_Database_Manager::ORDER );
            $this->wc_order_id = $wc_order_id;
        }

        /**
         * Retrieves all Barcode orders.
         *
         * @param int $per_page
         * @param int $page_number
         * @param null $wc_order_id
         * @return mixed
         */
        public static function get_orders( $per_page = 10, $page_number = 1, $wc_order_id = null )
        {
            global $wpdb;

            $table = Barcode_Database_Manager::table( Barcode_Database_Manager::ORDER );
            $sql   = "SELECT * FROM $table WHERE order_type = 'order'";

            if ( MAME_WC_ACTIVE && $wc_order_id )
                $sql .= " AND wc_order_id = '$wc_order_id'";

            if ( !empty( $_REQUEST[ 'orderby' ] ) ) {
                $sql .= ' ORDER BY ' . esc_sql( $_REQUEST[ 'orderby' ] );
                $sql .= !empty( $_REQUEST[ 'order' ] ) ? ' ' . esc_sql( $_REQUEST[ 'order' ] ) : ' DESC';
            } else {
                $sql .= ' ORDER BY id DESC';

            }

            $sql .= " LIMIT $per_page";

            $sql .= ' OFFSET ' . ( $page_number - 1 ) * $per_page;

            $result = $wpdb->get_results( $sql, 'ARRAY_A' );
            return $result;
        }

        /**
         * Deletes a Barcode order (only locally, not in Barcode itself).
         *
         * @param $id
         */
        public static function delete_order( $id )
        {
            global $wpdb;

            $table = Barcode_Database_Manager::table( Barcode_Database_Manager::ORDER );
            $wpdb->delete(
                "$table",
                [ 'id' => $id ],
                [ '%d' ]
            );
        }

        /**
         * Returns the count of all records from the database.
         *
         * @param null $wc_order_id
         * @return mixed
         */
        public static function record_count( $wc_order_id = null )
        {
            global $wpdb;
            $table = Barcode_Database_Manager::table( Barcode_Database_Manager::ORDER );


            $sql = "SELECT COUNT(*) FROM $table WHERE order_type = 'order'";
            if ( MAME_WC_ACTIVE && $wc_order_id )
                $sql .= " AND wc_order_id = '$wc_order_id'";

            return $wpdb->get_var( $sql );
        }

        /**
         * Text when no items are found.
         */
        public function no_items()
        {
            _e( 'No orders found.', 'dhuett' );
        }

        /**
         * Returns an array of available columns.
         *
         * @return array
         */
        function get_columns()
        {
            $columns = [
//            'cb'          => '<input type="checkbox" />',
'id'            => __( 'Order ID', 'dhuett' ),
'time'          => __( 'Time', 'dhuett' ),
'dispatch_type' => __( 'Dispatch type', 'dhuett' ),
'weight'        => __( 'Weight', 'dhuett' ),
'label_size'    => __( 'Label size', 'dhuett' ),
'additions'     => __( 'Additions', 'dhuett' ),
'receiver'      => __( 'Receiver', 'dhuett' ),
'files'         => __( 'Files', 'dhuett' ),
'tracking'      => __( 'Tracking', 'dhuuett' ),
            ];

            if ( MAME_WC_ACTIVE && !$this->wc_order_id ) {
                $columns[ 'wc_order_id' ] = __( 'WooCommerce order', 'dhuett' );
            }

            return $columns;
        }

        /**
         * Defines which columns are sortable.
         *
         * @return array
         */
        public function get_sortable_columns()
        {
            $sortable_columns = array(
                'id'         => array( 'id', true ),
                'time'       => array( 'time', false ),
                'weight'     => array( 'weight', false ),
                'label_size' => array( 'label_size', false ),
                'tracking'   => array( 'tracking', false ),
            );

            if ( MAME_WC_ACTIVE && !$this->wc_order_id )
                $sortable_columns[ 'wc_order_id' ] = [ 'wc_order_id', false ];

            return $sortable_columns;
        }

        /* public function get_bulk_actions()
         {
             $actions = [
                 'bulk-delete' => __( 'Delete', 'dhuett' ),
             ];

             return $actions;
         }*/

        /**
         * Prepares the data to be displayed.
         *
         * @param null $wc_order_id
         */
        public function prepare_items( $wc_order_id = null )
        {

            $this->_column_headers = $this->get_column_info();

            /** Process bulk action */
            $this->process_bulk_action();

            $per_page     = $this->get_items_per_page( 'orders_per_page', 10 );
            $current_page = $this->get_pagenum();
            $total_items  = self::record_count( $wc_order_id );

            $this->set_pagination_args( [
                'total_items' => $total_items, //WE have to calculate the total number of items
                'per_page'    => $per_page //WE have to determine how many items to show on a page
            ] );


            $this->items = self::get_orders( $per_page, $current_page, $wc_order_id );
        }
        /*
            public function process_bulk_action()
            {

                //Detect when a bulk action is being triggered...
                if ( 'delete' === $this->current_action() ) {

                    // In our file that handles the request, verify the nonce.
                    $nonce = esc_attr( $_REQUEST[ '_wpnonce' ] );

                    if ( !wp_verify_nonce( $nonce, MAME_WS_PREFIX . '_delete_order' ) ) {
                        die( 'Permission denied' );
                    } else {
                        self::delete_order( absint( $_GET[ 'order_id' ] ) );

                        wp_redirect( esc_url( add_query_arg() ) );
                        exit;
                    }

                }

                // If the delete bulk action is triggered
                if ( ( isset( $_POST[ 'action' ] ) && $_POST[ 'action' ] == 'bulk-delete' )
                    || ( isset( $_POST[ 'action2' ] ) && $_POST[ 'action2' ] == 'bulk-delete' )
                ) {

                    $delete_ids = esc_sql( $_POST[ 'bulk-delete' ] );

                    // loop over the array of record IDs and delete them
                    foreach ( $delete_ids as $id ) {
                        self::delete_order( $id );

                    }

                    wp_redirect( esc_url( add_query_arg() ) );
                    exit;
                }
            }*/

        /*
            function column_order_id( $item )
            {
                // create a nonce
                $delete_nonce = wp_create_nonce( MAME_BC_PREFIX . '_delete_order' );

                $title = '<strong>' . $item[ 'order_id' ] . '</strong>';

                $actions = [
                    'delete' => sprintf( '<a href="?page=%s&action=%s&order=%s&_wpnonce=%s">Delete</a>', esc_attr( $_REQUEST[ 'page' ] ), 'delete', absint( $item[ 'order_id' ] ), $delete_nonce )
                ];

                return $title . $this->row_actions( $actions );
            }*/

        /**
         * Displays the column with the receiver addresses.
         *
         * @param $item
         * @return string
         */
        function column_receiver( $item )
        {
            $order         = new Barcode_Order( $item );
            $address       = $order->get_recipient();
            $address->id   = null;
            $address_array = array_filter( get_object_vars( $address ), function ( $v ) {
                return !empty( $v );
            } );

            return implode( ', ', $address_array );
        }

        /**
         * Displays the column for Webstamp files such as stamps and delivery receipts.
         *
         * @param $item
         * @return string
         */
        function column_files( $item )
        {
            $order  = new Barcode_Order( $item );
            $labels = $order->get_labels();

            $html = '';
            if ( !empty( $labels ) ) {
                foreach ( $labels as $label ) {
                    $html .= $label->get_file_button( '<img src="' . plugins_url( '../../assets/images/barcode_download_s.png', __FILE__ ) . '">', '' );
                }
            }
            return $html;
        }

        /**
         * Displays all dispatch types.
         *
         * @param $item
         * @return string
         */
        function column_dispatch_type( $item )
        {
            $order          = new Barcode_Order( $item );
            $dispatch_types = json_decode( $order->dispatch_type );
            $html           = '';
            if ( !empty( $dispatch_types ) ) {
                $html = implode( ', ', $dispatch_types );
            }
            return $html;
        }

        /**
         * Displays all additions.
         *
         * @param $item
         * @return string
         */
        function column_additions( $item )
        {
            $order     = new Barcode_Order( $item );
            $additions = json_decode( $order->additions );
            $html      = '';
            if ( !empty( $additions ) ) {
                $html = implode( ', ', $additions );
            }
            return $html;
        }

        /**
         * Lists all the tracking numbers and with a link to the tracking status.
         *
         * @param $item
         * @return string
         */
        /*
        function column_tracking( $item )
        {
            $order  = new Barcode_Order( $item );
            $labels = $order->get_labels();

            $html = '';
            if ( !empty( $labels ) ) {
                foreach ( $labels as $label ) {
                    $html .= Mame_Html::a( __( $label->tracking_number, 'dhuett' ), $label->get_tracking_url() ) . '<br>';
                }
            }

            return $html;
        }*/

        /**
         * Displays the WooCommerce order id column.
         *
         * @param $item
         * @return null|string
         */
        function column_wc_order_id( $item )
        {
            //
            if ( !MAME_WC_ACTIVE || empty( $item[ 'wc_order_id' ] ) )
                return null;

            $order = new WC_Order( $item[ 'wc_order_id' ] );

            return '<a href="' . $order->get_edit_order_url() . '">' . $item[ 'wc_order_id' ] . '</a>';
        }

        /**
         * Displays the column for the tracking code.
         *
         * @param $item
         * @return null|string
         */
        function column_tracking( $item )
        {
            $order     = new Barcode_Order( $item );
            $ident_code = $order->ident_code ? Mame_Html::a( $order->ident_code, 'https://www.post.ch/swisspost-tracking?formattedParcelCodes=' . $order->ident_code, null, '_blank' ) : '';

            return $ident_code;
        }

        /**
         * Displays default column.
         *
         * @param $item
         * @param $column_name
         * @return mixed
         */
        public function column_default( $item, $column_name )
        {
            switch ( $column_name ) {
                case 'id':
                case 'dispatch_type':
                case 'weight':
                case 'label_size':
                case 'additions':
                    return $item[ $column_name ];
                case 'time':
                    return date_i18n( 'j. F Y - H:i', $item[ $column_name ] );
                default:
                    return print_r( $item, true );
            }
        }

        /*function column_cb( $item )
        {
            return sprintf(
                '<input type="checkbox" name="bulk-delete[]" value="%s" />', $item[ 'order_id' ]
            );
        }*/

    }

endif;