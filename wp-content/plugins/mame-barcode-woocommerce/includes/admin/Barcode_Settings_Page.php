<?php

if ( !defined( 'ABSPATH' ) ) {
    exit; // Exit if accessed directly
}

if ( !class_exists( 'Barcode_Settings_Page' ) ) :

    /**
     * Handles the Barcode settings menu page.
     *
     * Class Barcode_Settings_Page
     */
    class Barcode_Settings_Page
    {
        static $instance;

        public $order_list;
        public $template_list;

        private $options;
        private $checkout_fields;
        private $field_options;

        public function __construct()
        {
            if ( is_admin() ) { // admin actions
                register_activation_hook( __FILE__, array( $this, 'license_install' ) );

                add_action( 'admin_menu', array( $this, 'add_menu' ) );
                add_action( 'admin_init', array( $this, 'register_settings' ) );
            }
            // For workaround
            $this->page_sections = array();
            // add_action( 'whitelist_options', array( $this, 'whitelist_custom_options_page' ),11 );

            $this->options       = get_option( 'mame_bc_options_group' );
            $this->field_options = [ 'none' => __( '-- no value --', 'dhuett' ) ];

            add_filter( 'set-screen-option', [ $this, 'set_screen' ], 10, 3 );

        }

        public static function get_instance()
        {
            if ( !isset( self::$instance ) ) {
                self::$instance = new self();
            }

            return self::$instance;
        }

        public function set_screen( $status, $option, $value )
        {
            return $value;
        }

        public function screen_option()
        {

            $option = 'per_page';
            $args   = [
                'label'   => 'Orders',
                'default' => 5,
                'option'  => 'orders_per_page'
            ];

            add_screen_option( $option, $args );

            $this->order_list = new Barcode_Order_List_Table();
        }

        public function screen_template()
        {

            $option = 'per_page';
            $args   = [
                'label'   => 'Templates',
                'default' => 5,
                'option'  => 'templates_per_page'
            ];

            add_screen_option( $option, $args );

            $this->template_list = new Barcode_Template_List_Table();
        }

        public function whitelist_custom_options_page( $whitelist_options )
        {
            // Custom options are mapped by section id; Re-map by page slug.
            foreach ( $this->page_sections as $page => $sections ) {
                $whitelist_options[ $page ] = array();
                foreach ( $sections as $section )
                    if ( !empty( $whitelist_options[ $section ] ) )
                        foreach ( $whitelist_options[ $section ] as $option )
                            $whitelist_options[ $page ][] = $option;
            }
            return $whitelist_options;
        }

        private function add_settings_section( $id, $title, $cb, $page )
        {
            add_settings_section( $id, $title, $cb, $page );
            if ( $id != $page ) {
                if ( !isset( $this->page_sections[ $page ] ) )
                    $this->page_sections[ $page ] = array();
                $this->page_sections[ $page ][ $id ] = $id;
            }
        }

        public function add_menu()
        {
            add_menu_page( __( 'Barcode', 'dhuett' ), __( 'Barcode', 'dhuett' ), 'manage_options', 'mame_bc_menu', array( $this, 'display_barcode_editor_page' ), plugin_dir_url( dirname( dirname( __FILE__ ) ) ) . '/assets/images/barcode_menu_icon.png' );

            add_submenu_page( 'mame_bc_menu', __( 'Editor', 'dhuett' ), __( 'Editor', 'dhuett' ), 'manage_options', 'mame_bc_menu', array( $this, 'display_barcode_editor_page' ) );
            $hook          = add_submenu_page( 'mame_bc_menu', __( 'Orders', 'dhuett' ), __( 'Orders', 'dhuett' ), 'manage_options', 'mame_bc_menu_orders', array( $this, 'display_orders_page' ) );
            $template_hook = add_submenu_page( 'mame_bc_menu', __( 'Templates', 'dhuett' ), __( 'Templates', 'dhuett' ), 'manage_options', 'mame_bc_menu_templates', array( $this, 'display_barcode_templates_page' ) );
            add_submenu_page( 'mame_bc_menu', __( 'Settings', 'dhuett' ), __( 'Settings', 'dhuett' ), 'manage_options', 'mame_bc_menu_settings', array( $this, 'display_settings_page' ) );

            add_submenu_page( null, __( 'Add template', 'dhuett' ), __( 'Add template', 'dhuett' ), 'manage_options', 'mame_bc_menu_add_template', array( $this, 'display_barcode_add_template_page' ) );

            add_action( "load-$hook", [ $this, 'screen_option' ] );
            add_action( "load-$template_hook", [ $this, 'screen_template' ] );
        }

        public function register_settings()
        {
            register_setting( 'mame_bc_options_group', 'mame_bc_options_group', array( $this, 'options_validate' ) );

            add_settings_section( 'mame_bc_option_group_main_section', __( 'Main Settings', 'dhuett' ), array( $this, 'main_section_text' ), 'mame_bc_options_group' );
            add_settings_field( 'mame_bc_option_group_license_key', __( 'License key', 'dhuett' ), array( $this, 'license_key_field' ), 'mame_bc_options_group', 'mame_bc_option_group_main_section' );
            add_settings_field( 'mame_bc_option_group_environment', __( 'Environment', 'dhuett' ), array( $this, 'environment_field' ), 'mame_bc_options_group', 'mame_bc_option_group_main_section' );
//            add_settings_field( 'mame_bc_option_group_customer_type', __( 'Customer type', 'dhuett' ), array( $this, 'customer_type_field' ), 'mame_bc_options_group', 'mame_bc_option_group_main_section' );
            add_settings_field( 'mame_bc_option_group_franking_license', __( 'Franking license', 'dhuett' ), array( $this, 'franking_license_field' ), 'mame_bc_options_group', 'mame_bc_option_group_main_section' );
            add_settings_field( 'mame_bc_option_group_client_id', __( 'Client Identifier', 'dhuett' ), array( $this, 'client_id_field' ), 'mame_bc_options_group', 'mame_bc_option_group_main_section' );
            add_settings_field( 'mame_bc_option_group_client_secret', __( 'Client Secret', 'dhuett' ), array( $this, 'client_secret_field' ), 'mame_bc_options_group', 'mame_bc_option_group_main_section' );
            add_settings_field( 'mame_bc_option_group_franking_license_test', __( 'Franking license Test', 'dhuett' ), array( $this, 'franking_license_test_field' ), 'mame_bc_options_group', 'mame_bc_option_group_main_section' );
            add_settings_field( 'mame_bc_option_group_client_id_test', __( 'Client Identifier Test', 'dhuett' ), array( $this, 'client_id_test_field' ), 'mame_bc_options_group', 'mame_bc_option_group_main_section' );
            add_settings_field( 'mame_bc_option_group_client_secret_test', __( 'Client Secret Test', 'dhuett' ), array( $this, 'client_secret_test_field' ), 'mame_bc_options_group', 'mame_bc_option_group_main_section' );
            add_settings_section( 'mame_bc_option_group_address_section', __( 'Address', 'dhuett' ), array( $this, 'address_section_text' ), 'mame_bc_options_group' );

            add_settings_field( 'mame_bc_option_group_name1', __( 'First name and surname, or company name', 'dhuett' ), array( $this, 'name1_field' ), 'mame_bc_options_group', 'mame_bc_option_group_address_section' );
            add_settings_field( 'mame_bc_option_group_name2', __( 'Additional name', 'dhuett' ), array( $this, 'name2_field' ), 'mame_bc_options_group', 'mame_bc_option_group_address_section' );
            add_settings_field( 'mame_bc_option_group_street', __( 'Street', 'dhuett' ), array( $this, 'street_field' ), 'mame_bc_options_group', 'mame_bc_option_group_address_section' );
            add_settings_field( 'mame_bc_option_group_pobox', __( 'Postbox', 'dhuett' ), array( $this, 'pobox_field' ), 'mame_bc_options_group', 'mame_bc_option_group_address_section' );
            add_settings_field( 'mame_bc_option_group_zip', __( 'Postcode', 'dhuett' ), array( $this, 'zip_field' ), 'mame_bc_options_group', 'mame_bc_option_group_address_section' );
            add_settings_field( 'mame_bc_option_group_city', __( 'Place', 'dhuett' ), array( $this, 'city_field' ), 'mame_bc_options_group', 'mame_bc_option_group_address_section' );
            add_settings_field( 'mame_bc_option_group_domicile_post_office', __( 'Domicile post office', 'dhuett' ), array( $this, 'domicile_post_office_field' ), 'mame_bc_options_group', 'mame_bc_option_group_address_section' );

            add_settings_section( 'mame_bc_option_group_stamp_section', __( 'Label settings', 'dhuett' ), array( $this, 'stamp_section_text' ), 'mame_bc_options_group' );
            add_settings_field( 'mame_bc_option_group_image', __( 'Image', 'dhuett' ), array( $this, 'image_field' ), 'mame_bc_options_group', 'mame_bc_option_group_stamp_section' );
            add_settings_field( 'mame_bc_option_group_proclima', __( 'ProClima', 'dhuett' ), array( $this, 'proclima_field' ), 'mame_bc_options_group', 'mame_bc_option_group_stamp_section' );
            add_settings_field( 'mame_bc_option_stamp_file_type', __( 'Label file type', 'dhuett' ), array( $this, 'stamp_file_type' ), 'mame_bc_options_group', 'mame_bc_option_group_stamp_section' );
            add_settings_field( 'mame_bc_option_open_print_dialog', __( 'Automatically open print dialog', 'dhuett' ), array( $this, 'open_print_dialog_field' ), 'mame_bc_options_group', 'mame_bc_option_group_stamp_section' );

            add_settings_section( 'mame_bc_option_group_template_section', __( 'Template settings', 'dhuett' ), array( $this, 'template_section_text' ), 'mame_bc_options_group' );
            add_settings_field( 'mame_bc_option_group_template_order_overview', __( 'Directly jump to order confirmation screen on template load?', 'dhuett' ), array( $this, 'template_order_overview_field' ), 'mame_bc_options_group', 'mame_bc_option_group_template_section' );

            if ( MAME_WC_ACTIVE ) {

                if ( isset( $_GET[ 'page' ] ) && $_GET[ 'page' ] == 'mame_bc_menu_settings' ) {

                    include_once( WC()->plugin_path() . '/includes/wc-cart-functions.php' );

                    if ( !class_exists( 'WC_Session' ) ) {
                        include_once( WP_PLUGIN_DIR . '/woocommerce/includes/abstracts/abstract-wc-session.php' );
                    }
                    WC()->session  = new WC_Session_Handler;
                    WC()->customer = new WC_Customer;

                    if ( Mame_WC_Helper::is_woocommerce_version_up( '2.7.0' ) ) {
                        $this->checkout_fields = WC()->checkout->get_checkout_fields();
                    } else {
                        $this->checkout_fields = WC()->checkout->checkout_fields;
                    }

                    if ( !empty( $this->checkout_fields ) ) {

                        $billing_fields  = $this->checkout_fields[ 'billing' ];
                        $shipping_fields = $this->checkout_fields[ 'shipping' ];

                        foreach ( $shipping_fields as $k => $v ) {
                            $this->field_options[ $k ] = __( 'Shipping', 'dhuett' ) . ' - ' . $v[ 'label' ];
                        }

                        foreach ( $billing_fields as $k => $v ) {
                            $this->field_options[ $k ] = __( 'Billing', 'dhuett' ) . ' - ' . $v[ 'label' ];
                        }
                    }
                }

                add_settings_section( 'mame_bc_option_group_woocommerce_section', __( 'WooCommerce settings', 'dhuett' ), array( $this, 'woocommerce_section_text' ), 'mame_bc_options_group' );
                add_settings_field( 'mame_bc_option_group_templates_email', __( 'Template links in emails', 'dhuett' ), array( $this, 'wc_templates_email_field' ), 'mame_bc_options_group', 'mame_bc_option_group_woocommerce_section' );
                add_settings_field( 'mame_bc_option_group_email_tracking_code', __( 'Tracking code links in emails', 'dhuett' ), array( $this, 'wc_email_tracking_code_field' ), 'mame_bc_options_group', 'mame_bc_option_group_woocommerce_section' );
                add_settings_field( 'mame_bc_option_group_wc_field_description', __( 'Field assignments', 'dhuett' ), array( $this, 'wc_field_description_field' ), 'mame_bc_options_group', 'mame_bc_option_group_woocommerce_section' );
                add_settings_field( 'mame_bc_option_group_wc_first_name', __( 'First name', 'dhuett' ), array( $this, 'wc_first_name_field' ), 'mame_bc_options_group', 'mame_bc_option_group_woocommerce_section' );
                add_settings_field( 'mame_bc_option_group_wc_name1', __( 'Last name or company', 'dhuett' ), array( $this, 'wc_name1_field' ), 'mame_bc_options_group', 'mame_bc_option_group_woocommerce_section' );
                add_settings_field( 'mame_bc_option_group_wc_name2', __( 'Addition 1 (company suffix, department, Pickpost, ...)', 'dhuett' ), array( $this, 'wc_name2_field' ), 'mame_bc_options_group', 'mame_bc_option_group_woocommerce_section' );
                add_settings_field( 'mame_bc_option_group_wc_name3', __( 'Addition 2 (c/o, department, ...)', 'dhuett' ), array( $this, 'wc_name3_field' ), 'mame_bc_options_group', 'mame_bc_option_group_woocommerce_section' );
                add_settings_field( 'mame_bc_option_group_wc_address_suffix', __( 'Address suffix', 'dhuett' ), array( $this, 'wc_address_suffix_field' ), 'mame_bc_options_group', 'mame_bc_option_group_woocommerce_section' );
                add_settings_field( 'mame_bc_option_group_wc_street', __( 'Street', 'dhuett' ), array( $this, 'wc_street_field' ), 'mame_bc_options_group', 'mame_bc_option_group_woocommerce_section' );
                add_settings_field( 'mame_bc_option_group_wc_house_no', __( 'House number', 'dhuett' ), array( $this, 'wc_house_no_field' ), 'mame_bc_options_group', 'mame_bc_option_group_woocommerce_section' );
                add_settings_field( 'mame_bc_option_group_wc_pobox', __( 'Postbox', 'dhuett' ), array( $this, 'wc_pobox_field' ), 'mame_bc_options_group', 'mame_bc_option_group_woocommerce_section' );
                add_settings_field( 'mame_bc_option_group_wc_floor_no', __( 'Floor number', 'dhuett' ), array( $this, 'wc_floor_no_field' ), 'mame_bc_options_group', 'mame_bc_option_group_woocommerce_section' );
                add_settings_field( 'mame_bc_option_group_wc_mailbox_no', __( 'Letter box number', 'dhuett' ), array( $this, 'wc_mailbox_no_field' ), 'mame_bc_options_group', 'mame_bc_option_group_woocommerce_section' );
                add_settings_field( 'mame_bc_option_group_wc_zip', __( 'Postcode', 'dhuett' ), array( $this, 'wc_zip_field' ), 'mame_bc_options_group', 'mame_bc_option_group_woocommerce_section' );
                add_settings_field( 'mame_bc_option_group_wc_city', __( 'Place', 'dhuett' ), array( $this, 'wc_city_field' ), 'mame_bc_options_group', 'mame_bc_option_group_woocommerce_section' );
                add_settings_field( 'mame_bc_option_group_wc_phone', __( 'Telephone number', 'dhuett' ), array( $this, 'wc_phone_field' ), 'mame_bc_options_group', 'mame_bc_option_group_woocommerce_section' );
                add_settings_field( 'mame_bc_option_group_wc_mobile', __( 'Mobile number', 'dhuett' ), array( $this, 'wc_mobile_field' ), 'mame_bc_options_group', 'mame_bc_option_group_woocommerce_section' );
                add_settings_field( 'mame_bc_option_group_wc_email', __( 'Email address', 'dhuett' ), array( $this, 'wc_email_field' ), 'mame_bc_options_group', 'mame_bc_option_group_woocommerce_section' );
            }
        }

        public function load_data_field_field()
        {
            ?>
            <p><?= __( 'Click the button to reload the data from the WebStamp server.', 'dhuett' ) ?></p>
            <div>
                <button id="mame_bc-soap-request" class="button"><?= __( 'Load', 'dhuett' ) ?></button>
                <img id="<?= MAME_BC_PREFIX ?>-load-webstamp-data" class="<?= MAME_BC_PREFIX ?>-admin-loader"
                     src="<?= plugins_url( '../../assets/images/loader-5.gif', __FILE__ ) ?>">
            </div>
            <?php
        }

        public function license_key_field()
        {
            $licensing = Mame_Licensing_Handler::get_instance( MAME_BC_PLUGIN_NAME, MAME_BC_DISPLAY_NAME, MAME_BC_UPDATE_URL, MAME_BC_PREFIX, 'barcode', MAME_BC_PLUGIN_PATH, false );
            $licensing->license_activation_field();
        }

        public function proclima_field()
        {
            $checked = isset( $this->options[ 'proclima' ] ) ? $this->options[ 'proclima' ] : false;
            ?>
            <input type="checkbox" name="mame_bc_options_group[proclima]"
                   value="1" <?= $checked ? 'checked="checked"' : '' ?>> <?= __( 'Check if the ProClima logo should be printed on the label. Additional costs may apply.', 'dhuett' ) ?>
            <?php
        }

        public function image_field()
        {
            echo Mame_WP_Helper::image_upload_field( 'mame_bc_options_group', 'image', null, 200, plugins_url( '../../assets/images/stamp_white.png', __FILE__ ) );
        }

        public function stamp_file_type()
        {
            $type = ( isset( $this->options[ 'stamp_file_type' ] ) ? $this->options[ 'stamp_file_type' ] : 'pdf' );
            echo '<select name="mame_bc_options_group[stamp_file_type]"><option value="pdf" ' . ( $type == 'pdf' ? 'selected' : '' ) . '>' . __( 'PDF', 'dhuett' ) . '</option><option value="png"' . ( $type == 'png' ? 'selected' : '' ) . '>' . __( 'PNG', 'dhuett' ) . '</option></select><br><label for="mame_bc_options_group[stamp_file_type]">' . __( 'Choose PDF to enable media selection in the editor. If you choose PNG you can integrate the label into packing slips and invoices.', 'dhuett' ) . '</label>';
        }

        public function open_print_dialog_field()
        {
            $checked = isset( $this->options[ 'open_print_dialog' ] ) ? $this->options[ 'open_print_dialog' ] : false;
            ?>
            <input type="checkbox" name="mame_bc_options_group[open_print_dialog]"
                   value="1" <?= $checked ? 'checked="checked"' : '' ?>> <?= __( 'Check if the stamp print dialog should be opened automatically after an order has been placed.', 'dhuett' ) ?>
            <?php
        }

        public function template_order_overview_field()
        {
            $checked = isset( $this->options[ 'template_order_overview' ] ) ? $this->options[ 'template_order_overview' ] : false;
            ?>
            <input type="checkbox" name="mame_bc_options_group[template_order_overview]"
                   value="1" <?= $checked ? 'checked="checked"' : '' ?>> <?= __( 'Check if the order confirmation screen should be shown when a template is loaded to speed up the process of purchasing stamps.', 'dhuett' ) ?>
            <?php
        }

        public function environment_field()
        {
            $env = ( isset( $this->options[ 'environment' ] ) ? $this->options[ 'environment' ] : 'test' );
            echo '<select name="mame_bc_options_group[environment]"><option value="test" ' . ( $env == 'test' ? 'selected' : '' ) . '>' . __( 'Test', 'dhuett' ) . '</option><option value="production"' . ( $env == 'production' ? 'selected' : '' ) . '>' . __( 'Production', 'dhuett' ) . '</option></select>';
        }

        public function franking_license_field()
        {
            echo "<input id='plugin_text_string' name='mame_bc_options_group[franking_license]' size='40' type='text' value='" . ( isset( $this->options[ 'franking_license' ] ) ? esc_attr( $this->options[ 'franking_license' ] ) : '' ) . "' />";
        }

        public function franking_license_test_field()
        {
            echo "<input id='plugin_text_string' name='mame_bc_options_group[franking_license_test]' size='40' type='text' value='" . ( isset( $this->options[ 'franking_license_test' ] ) ? esc_attr( $this->options[ 'franking_license_test' ] ) : '' ) . "' />";
        }

        public function client_id_field()
        {
            echo "<input id='plugin_text_string' name='mame_bc_options_group[client_id]' size='40' type='text' value='" . ( isset( $this->options[ 'client_id' ] ) ? esc_attr( $this->options[ 'client_id' ] ) : '' ) . "' />";
        }

        public function client_id_test_field()
        {
            echo "<input id='plugin_text_string' name='mame_bc_options_group[client_id_test]' size='40' type='text' value='" . ( isset( $this->options[ 'client_id_test' ] ) ? esc_attr( $this->options[ 'client_id_test' ] ) : '' ) . "' />";
        }

        public function client_secret_field()
        {
            echo "<input id='plugin_text_string' name='mame_bc_options_group[client_secret]' size='40' type='text' value='" . ( isset( $this->options[ 'client_secret' ] ) ? esc_attr( $this->options[ 'client_secret' ] ) : '' ) . "' />";
        }

        public function client_secret_test_field()
        {
            echo "<input id='plugin_text_string' name='mame_bc_options_group[client_secret_test]' size='40' type='text' value='" . ( isset( $this->options[ 'client_secret_test' ] ) ? esc_attr( $this->options[ 'client_secret_test' ] ) : '' ) . "' />";
        }

        public function name1_field()
        {
            echo '<input id="plugin_text_string" name="mame_bc_options_group[name1]" size="40" type="text" value="' . ( isset( $this->options[ 'name1' ] ) ? esc_attr( $this->options[ 'name1' ] ) : '' ) . '" />';
        }

        public function name2_field()
        {
            echo '<input id="plugin_text_string" name="mame_bc_options_group[name2]" size="40" type="text" value="' . ( isset( $this->options[ 'name1' ] ) ? esc_attr( $this->options[ 'name2' ] ) : '' ) . '" />';
        }

        public function street_field()
        {
            echo '<input id="plugin_text_string" name="mame_bc_options_group[street]" size="40" type="text" value="' . ( isset( $this->options[ 'street' ] ) ? esc_attr( $this->options[ 'street' ] ) : '' ) . '" />';
        }

        public function pobox_field()
        {
            echo '<input id="plugin_text_string" name="mame_bc_options_group[pobox]" size="40" type="text" value="' . ( isset( $this->options[ 'pobox' ] ) ? esc_attr( $this->options[ 'pobox' ] ) : '' ) . '" />';
        }

        public function zip_field()
        {
            echo '<input id="plugin_text_string" name="mame_bc_options_group[zip]" size="40" type="text" value="' . ( isset( $this->options[ 'zip' ] ) ? esc_attr( $this->options[ 'zip' ] ) : '' ) . '" />';
        }

        public function city_field()
        {
            echo '<input id="plugin_text_string" name="mame_bc_options_group[city]" size="40" type="text" value="' . ( isset( $this->options[ 'city' ] ) ? esc_attr( $this->options[ 'city' ] ) : '' ) . '" />';
        }

        public function domicile_post_office_field()
        {
            echo '<input id="plugin_text_string" name="mame_bc_options_group[domicile_post_office]" size="40" type="text" value="' . ( isset( $this->options[ 'domicile_post_office' ] ) ? esc_attr( $this->options[ 'domicile_post_office' ] ) : '' ) . '" />';
        }

        public function wc_templates_email_field()
        {
            $wc_templates_email = [];
            if ( !empty( $this->options[ 'wc_templates_email' ] ) ) {
                $wc_templates_email = json_decode( $this->options[ 'wc_templates_email' ] );
            }

            // Get templates
            $templates = Barcode_Database_Manager::get_templates();
            if ( !empty( $templates ) ) {

                ?>
                <div>
                    <?php foreach ( $templates as $t ) { ?>
                        <input type="checkbox" name="mame_bc_options_group[wc_templates_email][]"
                               value="<?= esc_attr( $t->id ) ?>" <?= ( in_array( $t->id, $wc_templates_email ) ? 'checked="checked"' : '' ) ?>> <?= esc_html( $t->name ) ?>
                        <br>
                    <?php } ?>
                </div>
                <?php

            } else {
                echo '<p>' . __( 'No templates found', 'dhuett' ) . '</p>';
            }
        }

        public function wc_email_tracking_code_field()
        {
            $pos = ( isset( $this->options[ 'wc_email_tracking_code' ] ) ? $this->options[ 'wc_email_tracking_code' ] : 'none' );
            echo '<select name="mame_bc_options_group[wc_email_tracking_code]"><option value="none" ' . ( $pos == 'none' ? 'selected' : '' ) . '>' . __( 'None', 'dhuett' ) . '</option><option value="after_table" ' . ( $pos == 'after_table' ? 'selected' : '' ) . '>' . __( 'After order table', 'dhuett' ) . '</option><option value="before_table"' . ( $pos == 'before_table' ? 'selected' : '' ) . '>' . __( 'Before order table', 'dhuett' ) . '</option></select><br><label for="mame_bc_options_group[wc_email_tracking_code]">' . __( 'Choose the position of the tracking code in the confirmation email.', 'dhuett' ) . '</label>';
        }

        private function print_wc_field_options( $selected = null )
        {
            foreach ( $this->field_options as $k => $v ) {
                echo '<option value="' . $k . '" ' . ( $selected == $k ? 'selected' : '' ) . '>' . $v . '</option>';
            }
        }

        private function print_select_field( $name )
        {
            $selected = ( isset( $this->options[ $name ] ) ? $this->options[ $name ] : 'none' );

            echo '<select name="mame_bc_options_group[' . $name . ']">';

            $this->print_wc_field_options( $selected );

            echo '</select>';
        }

        public function wc_field_description_field()
        {
            echo __( 'These are the custom field assignments for loading WooCommerce addresses into WebStamp orders.', 'dhuett' );
        }

        public function wc_first_name_field()
        {
            $this->print_select_field( 'wc_first_name' );
        }

        public function wc_name1_field()
        {
            $this->print_select_field( 'wc_name1' );
        }

        public function wc_name2_field()
        {
            $this->print_select_field( 'wc_name2' );
        }

        public function wc_name3_field()
        {
            $this->print_select_field( 'wc_name3' );
        }

        public function wc_address_suffix_field()
        {
            $this->print_select_field( 'wc_address_suffix' );
        }

        public function wc_street_field()
        {
            $this->print_select_field( 'wc_street' );
        }

        public function wc_house_no_field()
        {
            $this->print_select_field( 'wc_house_no' );
        }

        public function wc_pobox_field()
        {
            $this->print_select_field( 'wc_pobox' );
        }

        public function wc_floor_no_field()
        {
            $this->print_select_field( 'wc_floor_no' );
        }

        public function wc_mailbox_no_field()
        {
            $this->print_select_field( 'wc_mailbox_no' );
        }

        public function wc_zip_field()
        {
            $this->print_select_field( 'wc_zip' );
        }

        public function wc_city_field()
        {
            $this->print_select_field( 'wc_city' );
        }

        public function wc_phone_field()
        {
            $this->print_select_field( 'wc_phone' );
        }

        public function wc_mobile_field()
        {
            $this->print_select_field( 'wc_mobile' );
        }

        public function wc_email_field()
        {
            $this->print_select_field( 'wc_email' );
        }

        public function options_validate( $input )
        {
            $newinput = get_option( 'mame_bc_options_group' );

            $newinput[ 'environment' ]           = sanitize_text_field( $input[ 'environment' ] );
            $newinput[ 'franking_license' ]      = sanitize_text_field( $input[ 'franking_license' ] );
            $newinput[ 'franking_license_test' ] = sanitize_text_field( $input[ 'franking_license_test' ] );
            $newinput[ 'client_id' ]             = sanitize_text_field( $input[ 'client_id' ] );
            $newinput[ 'client_id_test' ]        = sanitize_text_field( $input[ 'client_id_test' ] );
            $newinput[ 'client_secret' ]         = sanitize_text_field( $input[ 'client_secret' ] );
            $newinput[ 'client_secret_test' ]    = sanitize_text_field( $input[ 'client_secret_test' ] );

            $newinput[ 'name1' ]                = sanitize_text_field( $input[ 'name1' ] );
            $newinput[ 'name2' ]                = sanitize_text_field( $input[ 'name2' ] );
            $newinput[ 'street' ]               = sanitize_text_field( $input[ 'street' ] );
            $newinput[ 'pobox' ]                = sanitize_text_field( $input[ 'pobox' ] );
            $newinput[ 'zip' ]                  = sanitize_text_field( $input[ 'zip' ] );
            $newinput[ 'city' ]                 = sanitize_text_field( $input[ 'city' ] );
            $newinput[ 'domicile_post_office' ] = sanitize_text_field( $input[ 'domicile_post_office' ] );

            $newinput[ 'image' ]             = sanitize_text_field( $input[ 'image' ] );
            $newinput[ 'proclima' ]          = sanitize_text_field( $input[ 'proclima' ] );
            $newinput[ 'stamp_file_type' ]   = sanitize_text_field( $input[ 'stamp_file_type' ] );
            $newinput[ 'open_print_dialog' ] = sanitize_text_field( $input[ 'open_print_dialog' ] );

            $newinput[ 'wc_first_name' ]     = sanitize_text_field( $input[ 'wc_first_name' ] );
            $newinput[ 'wc_name1' ]          = sanitize_text_field( $input[ 'wc_name1' ] );
            $newinput[ 'wc_name2' ]          = sanitize_text_field( $input[ 'wc_name2' ] );
            $newinput[ 'wc_name3' ]          = sanitize_text_field( $input[ 'wc_name3' ] );
            $newinput[ 'wc_address_suffix' ] = sanitize_text_field( $input[ 'wc_address_suffix' ] );
            $newinput[ 'wc_street' ]         = sanitize_text_field( $input[ 'wc_street' ] );
            $newinput[ 'wc_house_no' ]       = sanitize_text_field( $input[ 'wc_house_no' ] );
            $newinput[ 'wc_pobox' ]          = sanitize_text_field( $input[ 'wc_pobox' ] );
            $newinput[ 'wc_floor_no' ]       = sanitize_text_field( $input[ 'wc_floor_no' ] );
            $newinput[ 'wc_mailbox_no' ]     = sanitize_text_field( $input[ 'wc_mailbox_no' ] );
            $newinput[ 'wc_zip' ]            = sanitize_text_field( $input[ 'wc_zip' ] );
            $newinput[ 'wc_city' ]           = sanitize_text_field( $input[ 'wc_city' ] );
            $newinput[ 'wc_phone' ]          = sanitize_text_field( $input[ 'wc_phone' ] );
            $newinput[ 'wc_mobile' ]         = sanitize_text_field( $input[ 'wc_mobile' ] );
            $newinput[ 'wc_email' ]          = sanitize_text_field( $input[ 'wc_email' ] );

            $newinput[ 'template_order_overview' ] = sanitize_text_field( $input[ 'template_order_overview' ] );
            if ( isset( $input[ 'wc_email_tracking_code' ] ) )
                $newinput[ 'wc_email_tracking_code' ] = sanitize_text_field( $input[ 'wc_email_tracking_code' ] );

            if ( !empty ( $input[ 'wc_templates_email' ] ) ) {
                $templates = [];
                foreach ( $input[ 'wc_templates_email' ] as $t ) {
                    $templates[] = sanitize_text_field( $t );
                }
                $newinput[ 'wc_templates_email' ] = json_encode( $templates );
            }

            return $newinput;
        }

        public function main_section_text()
        {
            echo '<p>' . __( 'Please fill in the fields below to connect the website to your Barcode account', 'dhuett' ) . '</p>';
        }

        public function address_section_text()
        {
            echo '<p>' . __( 'This is the default sender address.', 'dhuett' ) . '</p>';
        }

        public function stamp_section_text()
        {
            echo '<p>' . __( 'Default settings for Barcode labels.', 'dhuett' ) . '</p>';
        }

        public function template_section_text()
        {

        }

        public function woocommerce_section_text()
        {
            echo '<p>' . __( 'Settings for the WooCommerce integration.', 'dhuett' ) . '</p>';
        }

        public function display_barcode_editor_page()
        {
            $editor_handler = new Barcode_Editor_Handler();
            $editor_handler->load_editor( 1 );
        }

        public function display_orders_page()
        {
            ?>
            <div class="wrap">
                <h2><?= __( 'Orders', 'dhuett' ) ?></h2>

                <div id="poststuff">
                    <div id="post-body" class="metabox-holder columns-1">
                        <div id="post-body-content">
                            <div class="meta-box-sortables ui-sortable">
                                <form method="post">
                                    <?php
                                    $this->order_list->prepare_items();
                                    $this->order_list->display();
                                    ?>
                                </form>
                            </div>
                        </div>
                    </div>
                    <br class="clear">
                </div>
            </div>
            <?php
        }

        public function display_barcode_templates_page()
        {
            ?>
            <div class="wrap">
                <h2><?= __( 'Templates', 'dhuett' ) ?></h2>
                <a href="<?= admin_url( 'admin.php?page=mame_bc_menu_add_template' ) ?>"
                   class="page-title-action"><?= __( 'Add new', 'dhuett' ) ?></a>

                <div id="poststuff">
                    <div id="post-body" class="metabox-holder columns-1">
                        <div id="post-body-content">
                            <div class="meta-box-sortables ui-sortable">
                                <form method="post">
                                    <?php
                                    $this->template_list->prepare_items();
                                    $this->template_list->display(); ?>
                                </form>
                            </div>
                        </div>
                    </div>
                    <br class="clear">
                </div>
            </div>
            <?php
        }

        public function display_barcode_add_template_page()
        {
            $id = null;
            if ( !empty( $_POST ) ) {
                $errors = Barcode_Template::save_template_data( $_POST );

            } elseif ( isset( $_GET[ 'id' ] ) ) {
                $id = $_GET[ 'id' ];
            }

            $template_editor = new Barcode_Template_Editor_Handler( $id );
            ?>
            <div class="wrap">
                <h1><?php $id ? _e( 'Edit template', 'dhuett' ) : _e( 'Add template', 'dhuett' ); ?></h1>
                <form id="<?= MAME_BC_PREFIX ?>-template-form" method="post" action="#">
                    <?php
                    if ( isset( $errors ) && !empty( $errors ) )
                        Barcode_Template::template_errors_html( $errors );
                    $template_editor->load_editor();
                    ?>
                    <?php submit_button(); ?>
                </form>
            </div>
            <?php
        }

        public function display_settings_page()
        {
            ?>
            <div class="wrap">
                <h1><?php _e( 'Barcode Settings', 'dhuett' ); ?></h1>
                <form method="post" action="options.php">
                    <?php
                    settings_fields( 'mame_bc_options_group' );

                    do_settings_sections( 'mame_bc_options_group' );
                    ?>
                    <?php submit_button(); ?>
                </form>
            </div>
            <?php
        }

    }

endif;