<?php

if ( !defined( 'ABSPATH' ) ) {
    exit; // Exit if accessed directly
}

if ( !class_exists( 'Barcode_Template_List_Table' ) ) :

    /**
     * Displays a WP_List_Table with Barcode templates.
     */
    class Barcode_Template_List_Table extends WP_List_Table
    {
        public $table;

        /**
         * Barcode_Template_List_Table constructor.
         */
        public function __construct()
        {
            parent::__construct( [
                'singular' => __( 'Order', 'dhuett' ), //singular name of the listed records
                'plural'   => __( 'Orders', 'dhuett' ), //plural name of the listed records
                'ajax'     => false //should this table support ajax?

            ] );

            $this->table = Barcode_Database_Manager::table( Barcode_Database_Manager::TEMPLATE );
        }

        /**
         * Retrieves all Barcode templates.
         *
         * @param int $per_page
         * @param int $page_number
         * @param null $wc_order_id
         * @return mixed
         */
        public static function get_templates( $per_page = 10, $page_number = 1 )
        {
            global $wpdb;

            $table = Barcode_Database_Manager::table( Barcode_Database_Manager::TEMPLATE );
            $sql   = "SELECT * FROM $table";

            if ( !empty( $_REQUEST[ 'orderby' ] ) ) {
                $sql .= ' ORDER BY ' . esc_sql( $_REQUEST[ 'orderby' ] );
                $sql .= !empty( $_REQUEST[ 'order' ] ) ? ' ' . esc_sql( $_REQUEST[ 'order' ] ) : ' ASC';
            }

            $sql .= " LIMIT $per_page";

            $sql .= ' OFFSET ' . ( $page_number - 1 ) * $per_page;

            $result = $wpdb->get_results( $sql, 'ARRAY_A' );
            return $result;
        }

        /**
         * Deletes a Barcode template.
         *
         * @param $id
         */
        public static function delete_template( $id )
        {
            global $wpdb;

            $table = Barcode_Database_Manager::table( Barcode_Database_Manager::TEMPLATE );
            $wpdb->delete(
                "$table",
                [ 'id' => $id ],
                [ '%d' ]
            );
        }

        /**
         * Returns the count of all records from the database.
         *
         * @param null $wc_order_id
         * @return mixed
         */
        public static function record_count( $wc_order_id = null )
        {
            global $wpdb;
            $table = Barcode_Database_Manager::table( Barcode_Database_Manager::TEMPLATE );
            $sql   = "SELECT COUNT(*) FROM $table";

            return $wpdb->get_var( $sql );
        }

        /**
         * Text when no items are found.
         */
        public function no_items()
        {
            _e( 'No templates found.', 'dhuett' );
        }

        /**
         * Returns an array of available columns.
         *
         * @return array
         */
        function get_columns()
        {
            $columns = [
                'cb'      => '<input type="checkbox" />',
                'id'      => __( 'ID', 'dhuett' ),
                'name'    => __( 'Name', 'dhuett' ),
                'product' => __( 'Product', 'dhuett' ),
                'time'    => __( 'Updated', 'dhuett' ),
            ];

            return $columns;
        }

        /**
         * Defines which columns are sortable.
         *
         * @return array
         */
        public function get_sortable_columns()
        {
            $sortable_columns = array(
                'id'   => array( 'id', true ),
                'name' => array( 'name', false ),
                'time' => array( 'time', false ),
            );
            return $sortable_columns;
        }

        public function get_bulk_actions()
        {
            $actions = [
                'bulk-delete' => __( 'Delete', 'dhuett' ),
            ];

            return $actions;
        }

        /**
         * Prepares the data to be displayed.
         */
        public function prepare_items()
        {

            $this->_column_headers = $this->get_column_info();

            /** Process bulk action */
            $this->process_bulk_action();

            $per_page     = $this->get_items_per_page( 'orders_per_page', 10 );
            $current_page = $this->get_pagenum();
            $total_items  = self::record_count();

            $this->set_pagination_args( [
                'total_items' => $total_items, //WE have to calculate the total number of items
                'per_page'    => $per_page //WE have to determine how many items to show on a page
            ] );


            $this->items = self::get_templates( $per_page, $current_page );
        }

        public function process_bulk_action()
        {

            //Detect when a bulk action is being triggered...
            if ( 'delete' === $this->current_action() ) {

                // In our file that handles the request, verify the nonce.
                $nonce = esc_attr( $_REQUEST[ '_wpnonce' ] );

                if ( !wp_verify_nonce( $nonce, MAME_BC_PREFIX . '_delete_template' ) ) {
                    die( 'Permission denied' );
                } else {
                    self::delete_template( absint( $_GET[ 'id' ] ) );

                    wp_redirect( esc_url( add_query_arg() ) );
                    exit;
                }

            }

            // If the delete bulk action is triggered
            if ( ( isset( $_POST[ 'action' ] ) && $_POST[ 'action' ] == 'bulk-delete' )
                || ( isset( $_POST[ 'action2' ] ) && $_POST[ 'action2' ] == 'bulk-delete' )
            ) {

                $delete_ids = esc_sql( $_POST[ 'bulk-delete' ] );

                // loop over the array of record IDs and delete them
                foreach ( $delete_ids as $id ) {
                    self::delete_template( $id );

                }

                wp_redirect( esc_url( add_query_arg() ) );
                exit;
            }
        }

        function column_id( $item )
        {
            // create a nonce
            $delete_nonce = wp_create_nonce( MAME_BC_PREFIX . '_delete_template' );

            $title = '<strong>' . $item[ 'id' ] . '</strong>';

            $actions = [
                'edit'   => sprintf( '<a href="?page=%s&action=%s&id=%s">' . __( 'Edit', 'dhuett' ) . '</a>', 'mame_bc_menu_add_template', 'edit', $item[ 'id' ] ),
                'delete' => sprintf( '<a href="?page=%s&action=%s&id=%s&_wpnonce=%s">Delete</a>', esc_attr( $_REQUEST[ 'page' ] ), 'delete', absint( $item[ 'id' ] ), $delete_nonce )
            ];

            return $title . $this->row_actions( $actions );
        }

        function column_product( $item )
        {
            $template = new Barcode_Template( $item );
            $array    = [];
            /*
            if ( $template->category )
                $array[] = $template->get_category()->name;
            if ( $template->zone )
                $array[] = $template->get_zone()->name;
            if ( $template->delivery_method )
                $array[] = $template->delivery_method;
            if ( $template->format )
                $array[] = $template->format;
            if ( $template->media_type )
                $array[] = $template->get_media_type()->name;
            if ( $template->media )
                $array[] = $template->get_media()->name;
            */

            return implode( ', ', $array );
        }

        /**
         * Displays default column.
         *
         * @param $item
         * @param $column_name
         * @return mixed
         */
        public function column_default( $item, $column_name )
        {
            switch ( $column_name ) {
                case 'name':
                    return $item[ $column_name ];
                case 'time':
                    return date_i18n( 'j. F Y - H:i', $item[ $column_name ] );
                default:
                    return print_r( $item, true );
            }
        }

        function column_cb( $item )
        {
            return sprintf(
                '<input type="checkbox" name="bulk-delete[]" value="%s" />', $item[ 'id' ]
            );
        }

    }

endif;