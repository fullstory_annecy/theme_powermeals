<?php

if ( !defined( 'ABSPATH' ) ) {
    exit; // Exit if accessed directly
}

if ( !class_exists( 'Barcode_Order_Admin' ) ) :

    class Barcode_Order_Admin
    {
        static $instance;

        public function __construct()
        {
            add_action( 'add_meta_boxes_shop_order', array( $this, 'add_meta_box' ) );

        }

        public static function get_instance()
        {
            if ( !isset( self::$instance ) ) {
                self::$instance = new self();
            }

            return self::$instance;
        }


        /**
         * Adds a meta box containing the editor.
         */
        public function add_meta_box()
        {
            if ( is_plugin_active( 'mame-webstamp-woocommerce/mame-webstamp-woocommerce.php' ) ) {
                add_meta_box( 'mame_bc_wc_editor_metabox', __( 'WebStamp & Barcode', 'dhuett' ), array( $this, 'display_bc_ws_metabox' ), 'shop_order', 'normal', 'high' );
            } else {
                add_meta_box( 'mame_bc_editor_metabox', __( 'Barcode', 'dhuett' ), array( $this, 'display_metabox' ), 'shop_order', 'normal', 'high' );
            }
        }

        /**
         * Displays the meta box for the editor.
         *
         * @param $post
         */
        public function display_metabox( $post )
        {
            global $post;
            $wc_order = new WC_Order( $post->ID );

            $address = static::get_address( $wc_order );
            $editor  = new Barcode_Editor_Handler( Mame_WC_Helper::get ('id', $wc_order), $address );

            if ( isset( $_GET[ MAME_BC_PREFIX . '_template' ] ) ) {
                $editor->load_template_into_draft_order( $_GET[ MAME_BC_PREFIX . '_template' ] );
                $editor->autoload_draft = true;
                $editor->load_editor( 4 );
            } else {
                $editor->load_editor( 1 );
            }

            $this->display_orders( $wc_order );
        }

        public function display_bc_ws_metabox( $post )
        {
            global $post;
            $wc_order = new WC_Order( $post->ID );

            if ( isset( $_GET[ MAME_WS_PREFIX . '_template' ] ) ) {
                // Webstamp template.

                $address = Webstamp_Order_Admin::get_address( $wc_order );
                $editor  = new Webstamp_Editor_Handler( Mame_WC_Helper::get ('id', $wc_order), $address );
                $editor->load_template_into_draft_order( $_GET[ MAME_WS_PREFIX . '_template' ] );
                $editor->autoload_draft = true;
                $editor->load_editor( 4 );


            } else {
                $address = static::get_address( $wc_order );
                $editor  = new Barcode_Editor_Handler( Mame_WC_Helper::get ('id', $wc_order), $address );

                if ( isset( $_GET[ MAME_BC_PREFIX . '_template' ] ) ) {
                    $editor->load_template_into_draft_order( $_GET[ MAME_BC_PREFIX . '_template' ] );
                    $editor->autoload_draft = true;
                    $editor->load_editor( 4 );
                } else {
                    $editor->load_editor_selector();
                }
            }

            $webstamp_order_admin = Webstamp_Order_Admin::get_instance();
            $webstamp_order_admin->display_orders( $wc_order );

            $this->display_orders( $wc_order );
        }

        public static function get_address( $wc_order )
        {
            $street = Mame_WC_Helper::get( 'shipping_address_1', $wc_order );
            if ( $street ) {
                $street .= ' ' . Mame_WC_Helper::get( 'shipping_address_2', $wc_order );
            } else {
                $street = Mame_WC_Helper::get( 'shipping_address_2', $wc_order );
            }
            if ( !$street ) {
                $street = Mame_WC_Helper::get( 'billing_address_1', $wc_order );
                if ( $street ) {
                    $street .= ' ' . Mame_WC_Helper::get( 'billing_address_2', $wc_order );
                } else {
                    $street = Mame_WC_Helper::get( 'billing_address_2', $wc_order );
                }
            }

            $address = new Barcode_Recipient();
            $company = Mame_WC_Helper::get( 'shipping_company', $wc_order ) ?: Mame_WC_Helper::get( 'billing_company', $wc_order );
            $firstname = Mame_WC_Helper::get( 'shipping_first_name', $wc_order ) ?: Mame_WC_Helper::get( 'billing_first_name', $wc_order );
            $lastname  = Mame_WC_Helper::get( 'shipping_last_name', $wc_order ) ?: Mame_WC_Helper::get( 'billing_last_name', $wc_order );

            if ( $company ) {
                $address->first_name = $company;
                $address->name1      = $firstname . ' ' . $lastname;
            } else {
                $address->first_name = $firstname;
                $address->name1      = $lastname;
            }

            $address->street  = $street;
            $address->zip     = Mame_WC_Helper::get( 'shipping_postcode', $wc_order ) ?: Mame_WC_Helper::get( 'billing_postcode', $wc_order );
            $address->city    = Mame_WC_Helper::get( 'shipping_city', $wc_order ) ?: Mame_WC_Helper::get( 'billing_city', $wc_order );
            $address->country = Mame_WC_Helper::get( 'shipping_country', $wc_order ) ?: Mame_WC_Helper::get( 'billing_country', $wc_order );

            return $address;
        }

        /**
         * Lists all past orders for the order $wc_order.
         *
         * @param $wc_order
         */
        function display_orders( $wc_order )
        {
            $orders = Barcode_Database_Manager::get_orders_by_wc_order( Mame_WC_Helper::get ('id', $wc_order) );
            if ( !empty( $orders ) ) {
                echo Mame_Html::h3( __( 'Barcode orders', 'dhuett' ) );
                ?>
                <div id="<?= MAME_BC_PREFIX ?>-orders-table">
                    <table>
                        <tr>
                            <th><?= __( 'Order ID', 'dhuett' ) ?></th>
                            <th><?= __( 'Time', 'dhuett' ) ?></th>
                            <th><?= __( 'Dispatch type', 'dhuett' ) ?></th>
                            <th><?= __( 'Tracking', 'dhuett' ) ?></th>
                            <th><?= __( 'Files', 'dhuett' ) ?></th>
                        </tr>

                        <?php
                        foreach ( $orders as $order ) {
                            if ( $order->order_type === 'order' ) {
                                $labels         = $order->get_labels();
                                $dispatch_types = json_decode( $order->dispatch_type );
                                $dispatch_type  = '';
                                if ( !empty( $dispatch_types ) ) {
                                    $dispatch_type = implode( ', ', $dispatch_types );
                                }
                                $ident_code = $order->ident_code ? Mame_Html::a( $order->ident_code, 'https://www.post.ch/swisspost-tracking?formattedParcelCodes=' . $order->ident_code, null, '_blank' ) : '';
                                $html       = '';
                                foreach ( $labels as $label ) {
                                    $html .= $label->get_file_button( __( 'Barcode', 'dhuett' ) );
                                }
                                echo '<tr><td>' . $order->id . '</td><td>' . date_i18n( 'j. F Y - H:i', $order->time ) . '</td><td>' . $dispatch_type . ', ' . $order->weight . 'g, ' . $order->label_size . '</td><td>' . $ident_code . '</td><td>' . $html . '</td></tr>';
                            }
                        }
                        ?>
                    </table>
                </div>
                <?php
            }
        }

    }

endif;