<?php
function mame_bc_add_template_links( $order, $is_admin )
{
    // Only for admin emails
    if ( !$is_admin ) {
        return;
    }

    $options   = get_option( 'mame_bc_options_group' );
    $templates = json_decode( $options[ 'wc_templates_email' ] );

    $saved_templates = Barcode_Database_Manager::get_templates();

    if ( empty( $templates ) || empty ( $saved_templates ) )
        return;

    ?>
    <h2><?= __( 'Barcode templates' ); ?></h2>
    <?php
    $saved_templates = Mame_WP_Helper::object_array_map( $saved_templates, 'id', 'name' );
    foreach ( $templates as $t ) {
        if ( isset( $saved_templates[ $t ] ) ) {
            $name = $saved_templates[ $t ];
            ?>
            <p>
                <a href="<?= admin_url( 'post.php?post=' . absint( Mame_WC_Helper::get( 'id', $order ) ) . '&action=edit&mame_bc_template=' . absint( $t ) ) ?>"><?= $name ?></a>
            </p>
            <?php
        }
    }
}

function mame_bc_add_tracking_link_after_table( $order, $is_admin )
{
    $options = get_option( 'mame_bc_options_group' );

    if ( !isset( $options[ 'wc_email_tracking_code' ] ) )
        return;

    $position = $options[ 'wc_email_tracking_code' ];

    if ( $position !== 'after_table' )
        return;

    echo mame_bc_get_tracking_html( $order );
}

function mame_bc_add_tracking_link_before_table( $order, $is_admin )
{
    $options = get_option( 'mame_bc_options_group' );

    if ( !isset( $options[ 'wc_email_tracking_code' ] ) )
        return;

    $position = $options[ 'wc_email_tracking_code' ];

    if ( $position !== 'before_table' )
        return;

    echo mame_bc_get_tracking_html( $order );
}

function mame_bc_get_tracking_html( $order )
{
    $bc_orders = Barcode_Database_Manager::get_orders_by_wc_order( Mame_WC_Helper::get( 'id', $order ) );

    if ( empty( $bc_orders ) )
        return;

    $html = Mame_Html::h2( __( 'Tracking code' ) );
    foreach ( $bc_orders as $o ) {
        if ( $o->ident_code ) {
            $html .= Mame_Html::open_tag( 'p' );
            $html .= Mame_Html::a( $o->ident_code, 'https://www.post.ch/swisspost-tracking?formattedParcelCodes=' . $o->ident_code );
            $html .= Mame_Html::close_tag( 'p' );
        }
    }
    return $html;
}

add_action( 'woocommerce_email_after_order_table', MAME_BC_PREFIX . '_add_template_links', 10, 2 );
add_action( 'woocommerce_email_after_order_table', MAME_BC_PREFIX . '_add_tracking_link_after_table', 11, 2 );
add_action( 'woocommerce_email_before_order_table', MAME_BC_PREFIX . '_add_tracking_link_before_table', 10, 2 );