<?php

if ( !defined( 'ABSPATH' ) ) {
    exit;
}

if ( !class_exists( 'Barcode_Template_Editor_Handler' ) ) {

    /**
     * Class Barcode_Template_Editor_Handler
     */
    class Barcode_Template_Editor_Handler extends Barcode_Editor_Handler
    {
        /**
         * @var Barcode_Template|null
         */
        public $template;

        public function __construct( $template_id = null )
        {
            parent::__construct( null, null, null );

            add_action( 'wp_ajax_mame_bc_ajax_template_editor_request', array( $this, 'ajax_handler' ) );

            $this->template = new Barcode_Template();
            if ( $template_id )
                $this->template = Barcode_Database_Manager::get_template( $template_id );

            if ( $this->template->customer_id )
                $this->sender = $this->template->get_customer();

        }

        public function load_editor( $page = 1 )
        {
            ?>
            <div id="<?= MAME_BC_PREFIX ?>-editor-wrapper">
                <div id="<?= MAME_BC_PREFIX ?>-content-wrapper">
                    <div id="<?= MAME_BC_PREFIX ?>-content">
                        <div id="<?= MAME_BC_PREFIX ?>-steps">
                            <?php
                            $content = Mame_Html::div( $this->get_name_field(), [ 'id' => MAME_WS_PREFIX . '-name-wrapper' ] );
                            $content .= Mame_Html::div( $this->get_dispatch_type_fields(), [ 'id' => MAME_BC_PREFIX . '-dispatch_type-wrapper', 'data-page' => 1 ] );
                            $content .= Mame_Html::div( $this->get_weight_fields(), [ 'id' => MAME_BC_PREFIX . '-weight-wrapper', 'data-page' => 1 ] );
                            $content .= Mame_Html::div( $this->get_additions_fields(), [ 'id' => MAME_BC_PREFIX . '-additions-wrapper', 'data-page' => 1 ] );
                            $content .= Mame_Html::div( $this->get_label_size_fields(), [ 'id' => MAME_BC_PREFIX . '-label_size-wrapper', 'data-page' => 1 ] );
                            $content .= Mame_Html::div( $this->get_print_addresses_fields(), [ 'id' => MAME_BC_PREFIX . '-print_address-wrapper', 'data-page' => 1 ] );
                            $content .= Mame_Html::div( $this->get_sender_address_fields(), [ 'id' => MAME_BC_PREFIX . '-sender-wrapper', 'class' => MAME_BC_PREFIX . '-data-wrapper', 'data-page' => 3 ] );
                            $content .= Mame_Html::div( $this->get_receiver_address_fields(), [ 'id' => MAME_BC_PREFIX . '-receiver-wrapper', 'class' => MAME_BC_PREFIX . '-data-wrapper', 'data-page' => 3 ] );

                            if ( $this->template->id )
                                $content .= Mame_Html::input( 'hidden', 'id', $this->template->id );

                            $content .= $this->get_image_fields();
                            echo $content;
                            ?>
                        </div>
                    </div>
                </div>
            </div>
            <script>
                jQuery(document).ready(function ($) {
                    new MameBcEditor();
                    <?php if (!isset( $_GET[ 'id' ] )) { ?>
                    //$("#mame_bc-dispatch_type").change();
                    <?php } ?>
                })
            </script>
            <?php
        }

        /**
         * @return string
         */
        public function get_name_field()
        {
            $content = Mame_Html::open_tag( 'div', [ 'id' => MAME_BC_PREFIX . '-step-name', 'class' => MAME_BC_PREFIX . '-step' ] );
            $content .= Mame_Html::div( Mame_Html::h3( __( 'Name', 'dhuett' ) ), [ 'class' => MAME_BC_PREFIX . '-table-left' ] );
            $content .= Mame_Html::div( Mame_Html::input( 'text', MAME_BC_PREFIX . '-name', ( isset( $this->template->name ) ? $this->template->name : '' ), [ 'maxlength' => 255 ] ), [ 'class' => MAME_BC_PREFIX . '-table-right' ] );
            $content .= Mame_Html::close_tag( 'div' );

            return $content;
        }

        public function get_dispatch_type_fields()
        {
            $dispatch_types = json_decode( $this->template->dispatch_type );
            $gas            = $sp = false;
            if ( !empty( $dispatch_types ) ) {
                if ( $gas = array_search( 'GAS', $dispatch_types ) )
                    unset( $dispatch_types[ $gas ] );
                if ( $sp = array_search( 'SP', $dispatch_types ) )
                    unset( $dispatch_types[ $sp ] );
            }

            $content = Mame_Html::open_tag( 'div', [ 'id' => MAME_BC_PREFIX . '-step-dispatch_type', 'class' => MAME_BC_PREFIX . '-step ' . MAME_BC_PREFIX . '-step-1', 'data-page' => 1 ] );
            $content .= Mame_Html::h2( __( 'Dispatch type', 'dhuett' ) );
            $content .= Mame_Html::select( MAME_BC_PREFIX . '-dispatch_type[]', array_map( function ( $e ) {
                return $e[ 'title' ];
            }, Barcode_Data::get_dispatch_types() ), [ 'id' => MAME_BC_PREFIX . '-dispatch_type', 'class' => MAME_BC_PREFIX . '-select-load', 'data-step' => 'dispatch_type' ], !empty( $dispatch_types ) ? $dispatch_types[ 0 ] : null );

            $content .= Mame_Html::checkbox( __( 'GAS', 'dhuett' ), MAME_BC_PREFIX . '-dispatch_type[]', 'GAS', $gas ? [ 'checked' => 'checked' ] : null );
            $content .= Mame_Html::checkbox( __( 'Bulky goods', 'dhuett' ), MAME_BC_PREFIX . '-dispatch_type[]', 'SP', $sp ? [ 'checked' => 'checked' ] : null );
            $content .= Mame_Html::close_tag( 'div' );

            return $content;
        }

        public function get_weight_fields()
        {
            $content = Mame_Html::open_tag( 'div', [ 'id' => MAME_BC_PREFIX . '-step-weight', 'class' => MAME_BC_PREFIX . '-step ' . MAME_BC_PREFIX . '-step-1', 'data-page' => 1 ] );
            $content .= Mame_Html::h2( __( 'Weight', 'dhuett' ) );
            $content .= Mame_Html::select( MAME_BC_PREFIX . '-weight', Barcode_Data::get_weights(), [ 'id' => MAME_BC_PREFIX . '-weight', 'class' => MAME_BC_PREFIX . '-select-save', 'data-step' => 'weight' ], $this->template->weight ?: null );
            $content .= Mame_Html::close_tag( 'div' );

            return $content;
        }

        public function get_additions_fields()
        {
            $additions = json_decode( $this->template->additions );
            //var_dump($additions);die();

            $content            = Mame_Html::open_tag( 'div', [ 'id' => MAME_BC_PREFIX . '-step-additions', 'class' => MAME_BC_PREFIX . '-step ' . MAME_BC_PREFIX . '-step-1', 'data-page' => 1 ] );
            $content            .= Mame_Html::h2( __( 'Options', 'dhuett' ) );
            $content            .= Mame_Html::div( Mame_Html::p( __( 'Additional services', 'dhuett' ) ), [ 'class' => MAME_BC_PREFIX . '-tab-title active', 'data-target' => MAME_BC_PREFIX . '-tab_additions' ] );
            $content            .= Mame_Html::div( Mame_Html::p( __( 'Delivery instruction', 'dhuett' ) ), [ 'class' => MAME_BC_PREFIX . '-tab-title', 'data-target' => MAME_BC_PREFIX . '-tab_delivery_instruction' ] );
            $content            .= Mame_Html::open_tag( 'div', [ 'id' => MAME_BC_PREFIX . '-tab_additions', 'class' => MAME_BC_PREFIX . '-tab-content active' ] );
            $content            .= Mame_Html::div( Mame_Html::p( __( 'Increased liability and receipt against signature', 'dhuett' ) ), [ 'class' => MAME_BC_PREFIX . '-accordion-title', 'data-target' => MAME_BC_PREFIX . '-accordion_liability' ] );
            $services_liability = Barcode_Data::get_additional_services_liability();
            $content            .= Mame_Html::open_tag( 'div', [ 'id' => MAME_BC_PREFIX . '-accordion_liability', 'class' => MAME_BC_PREFIX . '-accordion-content' ] );

            $content            .= Mame_Html::checkbox_group(
                array_map( function ( $label, $name ) {
                    return array(
                        'label' => $label,
                        'name'  => MAME_BC_PREFIX . '-additions[]',
                        'value' => $name,
                    );
                }, $services_liability, array_keys( $services_liability ) )
                , [], $additions
            );
            $content            .= Mame_Html::close_tag( 'div' );
            $content            .= Mame_Html::div( Mame_Html::p( __( 'Time of delivery', 'dhuett' ) ), [ 'class' => MAME_BC_PREFIX . '-accordion-title', 'data-target' => MAME_BC_PREFIX . '-accordion_time' ] );
            $services_time      = Barcode_Data::get_additional_services_time();
            $content            .= Mame_Html::open_tag( 'div', [ 'id' => MAME_BC_PREFIX . '-accordion_time', 'class' => MAME_BC_PREFIX . '-accordion-content' ] );
            $content            .= Mame_Html::checkbox_group(

                array_map( function ( $label, $name ) {
                    return array(
                        'label' => $label,
                        'name'  => MAME_BC_PREFIX . '-additions[]',
                        'value' => $name,
                    );
                }, $services_time, array_keys( $services_time ) )
                , [], $additions
            );
            $content            .= Mame_Html::close_tag( 'div' );
            $content            .= Mame_Html::div( Mame_Html::p( __( 'Packaging for delicate goods', 'dhuett' ) ), [ 'class' => MAME_BC_PREFIX . '-accordion-title', 'data-target' => MAME_BC_PREFIX . '-accordion_packaging' ] );
            $services_packaging = Barcode_Data::get_additional_services_delicate_goods();
            $content            .= Mame_Html::open_tag( 'div', [ 'id' => MAME_BC_PREFIX . '-accordion_packaging', 'class' => MAME_BC_PREFIX . '-accordion-content' ] );
            $content            .= Mame_Html::checkbox_group(

                array_map( function ( $label, $name ) {
                    return array(
                        'label' => $label,
                        'name'  => MAME_BC_PREFIX . '-additions[]',
                        'value' => $name,
                    );
                }, $services_packaging, array_keys( $services_packaging ) )
                , [], $additions
            );
            $content            .= Mame_Html::close_tag( 'div' );
            $content            .= Mame_Html::close_tag( 'div' );
            $content            .= Mame_Html::open_tag( 'div', [ 'id' => MAME_BC_PREFIX . '-tab_delivery_instruction', 'class' => MAME_BC_PREFIX . '-tab-content' ] );

            $delivery_instructions = Barcode_Data::get_delivery_instructions();
            $content               .= Mame_Html::checkbox_group(

                array_map( function ( $label, $name ) {
                    return array(
                        'label' => $label,
                        'name'  => MAME_BC_PREFIX . '-additions[]',
                        'value' => $name,
                    );
                }, $delivery_instructions, array_keys( $delivery_instructions ) )
                , [], $additions
            );

            $content .= Mame_Html::close_tag( 'div' );
            $content .= Mame_Html::close_tag( 'div' );

            return $content;
        }

        public function get_label_size_fields()
        {
            $content = Mame_Html::open_tag( 'div', [ 'id' => MAME_BC_PREFIX . '-step-label_size', 'class' => MAME_BC_PREFIX . '-step ' . MAME_BC_PREFIX . '-step-1', 'data-page' => 1 ] );
            $content .= Mame_Html::h2( __( 'Label size', 'dhuett' ) );
            $content .= Mame_Html::select( MAME_BC_PREFIX . '-label_size', Barcode_Data::get_label_sizes(), [ 'id' => MAME_BC_PREFIX . '-label_size', 'class' => MAME_BC_PREFIX . '-select-save', 'data-step' => 'label_size' ], $this->template->label_size );
            $content .= Mame_Html::close_tag( 'div' );

            return $content;
        }

        public function get_print_addresses_fields()
        {
            $content = Mame_Html::open_tag( 'div', [ 'id' => MAME_BC_PREFIX . '-step-print_addresses', 'class' => MAME_BC_PREFIX . '-step ' . MAME_BC_PREFIX . '-step-1', 'data-page' => 1 ] );
            $content .= Mame_Html::h2( __( 'Addresses printed on label', 'dhuett' ) );
            $content .= Mame_Html::select( MAME_BC_PREFIX . '-print_addresses', Barcode_Data::get_print_address(), [ 'id' => MAME_BC_PREFIX . '-print_addresses', 'class' => MAME_BC_PREFIX . '-select-save', 'data-step' => 'print_addresses' ], $this->template->print_addresses ?: Barcode_Template::ADDRESS_ );
            $content .= Mame_Html::close_tag( 'div' );

            return $content;
        }

        public function get_receiver_address_fields()
        {
            $class = 'hidden';
            if ( $this->template->print_addresses === Barcode_Template::ADDRESS_R_C || $this->template->print_addresses === Barcode_Template::ADDRESS_R )
                $class = '';

            $content = Mame_Html::open_tag( 'div', [ 'id' => MAME_BC_PREFIX . '-step-recipient', 'class' => $class ] );
            $content .= Mame_Html::div( $this->get_wc_address_select_fields(), [ 'id' => MAME_BC_PREFIX . '-load_wc_address-wrapper', 'class' => MAME_BC_PREFIX . '-data-wrapper' ] );
            $content .= Mame_Html::close_tag( 'div' );
            return $content;
        }

        public function get_sender_address_fields()
        {
            $class = 'hidden';
            if ( $this->template->print_addresses === Barcode_Template::ADDRESS_R_C || $this->template->print_addresses === Barcode_Template::ADDRESS_C )
                $class = '';
            $content = Mame_Html::open_tag( 'div', [ 'id' => MAME_BC_PREFIX . '-step-customer', 'class' => $class ] );
            $content .= $this->sender_address_fields();
            $content .= Mame_Html::close_tag( 'div' );
            return $content;
        }

        public function get_image_fields()
        {
            $img_upload_field = $this->image_upload_field( 'mame_bc_options_group', 'image', null, 200, plugins_url( '../../assets/images/stamp_white.png', __FILE__ ) );
            $content          = Mame_Html::open_tag( 'div', [ 'id' => MAME_BC_PREFIX . '-step-image', 'data-page' => 1 ] );
            $content          .= Mame_Html::h2( __( 'Image', 'dhuett' ) );
            $content          .= Mame_Html::div( Mame_Html::checkbox( __( 'Use image', 'dhuett' ), MAME_BC_PREFIX . '-use_image', 1, $this->template->image_id ? [ 'checked' => 'checked' ] : null ), [ 'id' => MAME_BC_PREFIX . '-use_image-wrapper', 'class' => MAME_BC_PREFIX . '-data-wrapper', 'data-page' => 1 ] );
            $content          .= Mame_Html::div( $img_upload_field, [ 'id' => MAME_BC_PREFIX . '-image-wrapper', 'class' => MAME_BC_PREFIX . '-data-wrapper ' . ( $this->template->image_id ? '' : 'hidden' ), 'data-page' => 1 ] );
            $content          .= Mame_Html::close_tag( 'div' );
            return $content;
        }

        public function get_wc_address_select_fields()
        {
            $content = Mame_Html::open_tag( 'div', [ 'id' => MAME_BC_PREFIX . '-step-load_wc_address' ] );
            $content .= Mame_Html::div( Mame_Html::h3( __( 'Recipient: load WooCommerce address', 'dhuett' ) ), [ 'class' => MAME_BC_PREFIX . '-table-left' ] );
            $content .= Mame_Html::open_tag( 'div', [ 'class' => MAME_BC_PREFIX . '-table-right' ] );
            $content .= Mame_Html::select( MAME_BC_PREFIX . '-load_wc_address', [ 'no' => __( 'Don\'t load', 'dhuett' ), 'shipping' => __( 'Shipping address', 'dhuett' ), 'billing' => __( 'Billing address', 'dhuett' ), 'custom' => __( 'Custom assignment', 'dhuett' ) ], [ 'id' => MAME_BC_PREFIX . '-load_wc_address', 'data-step' => 'load_wc_address' ], ( isset( $this->template->load_wc_address ) ? $this->template->load_wc_address : null ) );
            $content .= Mame_Html::close_tag( 'div' );
            $content .= Mame_Html::close_tag( 'div' );

            return $content;
        }
    }

}