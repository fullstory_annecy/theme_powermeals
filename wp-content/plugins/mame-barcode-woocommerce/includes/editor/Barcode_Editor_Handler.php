<?php

if ( !defined( 'ABSPATH' ) ) {
    exit;
}

if ( !class_exists( 'Barcode_Editor_Handler' ) ) {

    /**
     * Class Barcode_Editor_Handler
     */
    class Barcode_Editor_Handler
    {
        /**
         * @var Barcode_Recipient|null
         */
        public $address;

        /**
         * @var Barcode_Customer|null
         */
        public $sender;

        /**
         * @var string|int
         */
        public $wc_order_id;

        /**
         * @var Barcode_Template
         */
        public $template;

        /**
         * @var bool Automatically load draft from GET params?
         */
        public $autoload_draft;

        /**
         * @var bool
         */
        public $load_order_overview_directly;

        /**
         * @var array
         */
        protected $overview_data;

        /**
         * @var Barcode_Json_Response
         */
        private $json_response;

        /**
         * @var array
         */
        private $direct_dependencies;

        public function __construct( $wc_order_id = null, $address = null, $sender = null )
        {
            $this->wc_order_id                  = $wc_order_id;
            $this->address                      = $address;
            $this->sender                       = $sender;
            $this->json_response                = new Barcode_Json_Response();
            $this->template                     = new Barcode_Template();
            $this->overview_data                = [];
            $this->load_order_overview_directly = false;
            $this->direct_dependencies          = [
                'dispatch_type' => [ 'weight' ],
            ];

            add_action( 'wp_ajax_mame_bc_ajax_editor_request', array( $this, 'ajax_handler' ) );
        }

        /**
         * Handles AJAX requests.
         */
        public function ajax_handler()
        {
            if ( !check_ajax_referer( 'mame_bc_editor_nonce', 'security' ) )
                return;

            switch ( $_POST[ 'bc_action' ] ) {

                case 'save_and_invalidate':

                    $save_data                  = $_POST[ 'bc_data' ][ 'save' ];
                    $wc_order_id                = isset( $_POST[ 'bc_data' ][ 'wc_order_id' ] ) ? $_POST[ 'bc_data' ][ 'wc_order_id' ] : null;
                    $save_data[ 'wc_order_id' ] = $wc_order_id;

                    // Dispatch type workaround.
                    if ( $_POST[ 'bc_data' ][ 'step' ] == 'dispatch_type' ) {
                        if ( !isset( $save_data[ 'dispatch_type' ] ) ) {
                            $save_data[ 'dispatch_type' ] = [];
                        }
                    }

                    // Additions workaround
                    if ( $_POST[ 'bc_data' ][ 'step' ] == 'additions' ) {
                        if ( !isset( $save_data[ 'additions' ] ) ) {
                            $save_data[ 'additions' ] = [];
                        }
                    }

                    // Save data
                    Barcode_Database_Manager::update_draft_order( $save_data );

                    // Invalidate other data.
                    $draft_order = Barcode_Database_Manager::get_draft_order( $wc_order_id );
                    if ( $draft_order ) {
                        if ( isset( $_POST[ 'bc_data' ][ 'invalidate' ] ) )
                            $draft_order->delete_properties( $_POST[ 'bc_data' ][ 'invalidate' ] );
                    }

                    // Return JSON response.
                    $this->json_response->status( Mame_Json_Response::STATUS_SUCCESS );
                    // Populate depending fields.
                    $dependencies = $this->direct_dependencies[ $_POST[ 'bc_data' ][ 'step' ] ];
                    if ( !empty ( $dependencies ) ) {

                        foreach ( $dependencies as $d ) {
                            call_user_func( array( $this, 'json_' . $d ), $save_data );
                            //$this->json_response->html( '#' . MAME_BC_PREFIX . '-overview-' . $d, '' );
                        }
                    }

                    // Add response data.
                    $this->add_response_data( $save_data );

                    $this->json_response->respond();

                    break;
                case 'load_dependencies':

                    $save_data                  = $_POST[ 'bc_data' ][ 'save' ];
                    $wc_order_id                = isset( $_POST[ 'bc_data' ][ 'wc_order_id' ] ) ? $_POST[ 'bc_data' ][ 'wc_order_id' ] : null;
                    $save_data[ 'wc_order_id' ] = $wc_order_id;

                    // Additions workaround.
                    if ( $_POST[ 'bc_data' ][ 'step' ] == 'addition' ) {
                        if ( !isset( $save_data[ 'additions' ] ) ) {
                            $save_data[ 'additions' ] = [];
                        }
                    }

                    // Return JSON response.
                    $this->json_response->status( Mame_Json_Response::STATUS_SUCCESS );
                    // Populate depending fields.
                    $dependencies = $this->direct_dependencies[ $_POST[ 'bc_data' ][ 'step' ] ];
                    if ( !empty ( $dependencies ) ) {

                        foreach ( $dependencies as $d ) {
                            call_user_func( array( $this, 'json_' . $d ), $save_data );
                            //$this->json_response->html( '#' . MAME_WS_PREFIX . '-overview-' . $d, '' );
                        }
                    }
                    $this->json_response->respond();

                    break;
                case 'load':
                    $this->load_editor( 1 );
                    break;

                case 'license_number':
                    $this->json_license()->respond();

                    break;
                case 'preview':
                    $wc_order_id = isset( $_POST[ 'bc_data' ][ 'wc_order_id' ] ) ? $_POST[ 'bc_data' ][ 'wc_order_id' ] : null;

                    $errors = $this->validate( $wc_order_id );

                    if ( $errors )
                        $this->json_response->errors( '#' . MAME_BC_PREFIX . '-overview-wrapper .errors', $errors )->scroll_to( '#' . MAME_BC_PREFIX . '-overview-wrapper .errors' )->respond();

                    // Send download file.
                    $this->preview_order( $wc_order_id );

                    break;

                case 'order':
                    $wc_order_id = isset( $_POST[ 'bc_data' ][ 'wc_order_id' ] ) ? $_POST[ 'bc_data' ][ 'wc_order_id' ] : null;

                    // Send download file.
                    $this->order( $wc_order_id );
                    break;

                case 'order_overview':
                    $wc_order_id = isset( $_POST[ 'bc_data' ][ 'wc_order_id' ] ) ? $_POST[ 'bc_data' ][ 'wc_order_id' ] : null;

                    $errors = $this->validate( $wc_order_id );
                    if ( $errors )
                        $this->json_response->errors( '#' . MAME_BC_PREFIX . '-overview-wrapper .errors', $errors )->scroll_to( '#' . MAME_BC_PREFIX . '-overview-wrapper .errors' )->respond();

                    $this->json_order_overview( $wc_order_id )->respond();

                    break;

                case 'check_draft_exists':
                    $wc_order_id = isset( $_POST[ 'bc_data' ][ 'wc_order_id' ] ) ? $_POST[ 'bc_data' ][ 'wc_order_id' ] : null;
                    $draft_order = Barcode_Database_Manager::get_draft_order( $wc_order_id );

                    if ( $draft_order ) {

                        if ( !empty( $draft_order->image_id ) && ( $draft_order->image_id !== 0 ) && ( $draft_order->image_id !== '0' ) ) {

                            $attachment_image = wp_get_attachment_image_src( $draft_order->image_id, array( '200', 'auto' ) );

                            if ( $attachment_image )
                                $draft_order->image_url = $attachment_image[ 0 ];
                        }

                        $address = $draft_order->get_recipient();
                        $sender  = $draft_order->get_customer();

                        $this->json_response->status( true )->order( $draft_order )->address( $address )->sender( $sender )->data( '#' . MAME_BC_PREFIX . '-overlay', 'order-id', $draft_order->id )->respond();
                    }
                    $this->json_response->status( false )->respond();

                    break;

                case 'load_draft':
                    $this->wc_order_id = isset( $_POST[ 'bc_data' ][ 'wc_order_id' ] ) ? $_POST[ 'bc_data' ][ 'wc_order_id' ] : null;
                    $draft_order       = Barcode_Database_Manager::get_draft_order( $this->wc_order_id );
                    if ( $draft_order ) {

                        $this->template->populate( $draft_order );
                        $this->address = $this->template->get_recipient();
                        $this->sender  = $this->template->get_customer();

                        return $this->json_response->html( '#' . MAME_BC_PREFIX . '-steps', $this->load_editor_form_content() )->show( '.' . MAME_BC_PREFIX . '-editor-content' )->hide( '#' . MAME_BC_PREFIX . '-start' )->respond();

                    }
                    $this->json_response->status( false )->respond();
                    break;

                case 'delete_draft':
                    $where = [ 'order_type' => 'draft' ];
                    if ( isset( $_POST[ 'bc_data' ][ 'wc_order_id' ] ) )
                        $this->wc_order_id = $where[ 'wc_order_id' ] = $_POST[ 'bc_data' ][ 'wc_order_id' ];
                    else
                        $where[ 'wc_order_id' ] = null;
                    Barcode_Database_Manager::delete( Barcode_Database_Manager::table( Barcode_Database_Manager::ORDER ), $where );

                    return $this->json_response->html( '#' . MAME_BC_PREFIX . '-steps', $this->load_editor_form_content() )->show( '.' . MAME_BC_PREFIX . '-editor-content' )->hide( '#' . MAME_BC_PREFIX . '-start' )->respond();
                    break;

                case 'load_template':
                    $template_id = $_POST[ 'bc_data' ][ 'template_id' ];
                    if ( isset( $_POST[ 'bc_data' ][ 'wc_order_id' ] ) ) {
                        $this->wc_order_id = $_POST[ 'bc_data' ][ 'wc_order_id' ];
                    }

                    $this->load_template_into_draft_order( $template_id );

                    $this->json_response->html( '#' . MAME_BC_PREFIX . '-steps', $this->load_editor_form_content( $this->load_order_overview_directly ? 4 : 1 ) )->show( '.' . MAME_BC_PREFIX . '-editor-content' )->hide( '#' . MAME_BC_PREFIX . '-start' );

                    if ( $this->load_order_overview_directly ) {
                        $errors = $this->validate( $this->wc_order_id );

                        $this->json_response->page( 4 );
                        if ( $errors ) {
                            $this->json_response->errors( '#' . MAME_BC_PREFIX . '-overview-wrapper .errors', $errors )->scroll_to( '#' . MAME_BC_PREFIX . '-overview-wrapper .errors' )->respond();
                        } else {
                            $this->json_order_overview( $this->wc_order_id );
                        }
                    }

                    return $this->json_response->respond();
                    break;

                case 'load_shipping_address':

                    if ( class_exists( 'WC_Order' ) ) {
                        $order   = new WC_Order( $_POST[ 'bc_data' ][ 'wc_order_id' ] );
                        $address = Barcode_Recipient::get_shipping_address( $order );
                        $this->json_response->status( true )->address( $address )->respond();
                    }

                    break;

                case 'load_billing_address':
                    if ( class_exists( 'WC_Order' ) ) {
                        $order   = new WC_Order( $_POST[ 'bc_data' ][ 'wc_order_id' ] );
                        $address = Barcode_Recipient::get_billing_address( $order );
                        $this->json_response->status( true )->address( $address )->respond();
                    }
                    break;

                case 'load_custom_address':

                    if ( class_exists( 'WC_Order' ) ) {
                        $order   = new WC_Order( $_POST[ 'bc_data' ][ 'wc_order_id' ] );
                        $address = Barcode_Recipient::get_custom_address( $order );
                        $this->json_response->status( true )->address( $address )->respond();
                    }

                    break;

                case 'load_default_sender':
                    $sender = Barcode_Customer::get_default_address();
                    $this->json_response->status( true )->sender( $sender )->respond();

                    break;

                case 'edit_license':
                    $content = $this->get_license_fields();
                    $content .= Mame_Html::button( __( 'Cancel', 'dhuett' ), [ 'class' => MAME_BC_PREFIX . '-btn ' . MAME_BC_PREFIX . '-close-overlay' ] );
                    return $this->json_response->status( Mame_Json_Response::STATUS_SUCCESS )->html( '#' . MAME_BC_PREFIX . '-overlay .content', $content )->show( '#' . MAME_BC_PREFIX . '-overlay' )->respond();
                    break;

                case 'load_print_address_text':
                    $print_address = $_POST[ 'bc_data' ][ 'print_address' ];
                    if ( isset( $print_address ) )
                        return $this->json_response->status( Mame_Json_Response::STATUS_SUCCESS )->print_address( Barcode_Data::get_print_address()[ $print_address ] )->respond();
                    break;

                case 'load_webstamp_editor':
                    if ( !empty( $_POST[ 'data' ] ) ) {
                        $wc_order  = new WC_Order( $_POST[ 'data' ] );
                        $address   = Webstamp_Order_Admin::get_address( $wc_order );
                        $ws_editor = new Webstamp_Editor_Handler( Mame_WC_Helper::get ('id', $wc_order), $address );
                    } else {
                        $ws_editor = new Webstamp_Editor_Handler();
                    }
                    $ws_editor->load_editor();
                    exit;
                    break;

                case 'load_barcode_editor':
                    if ( !empty( $_POST[ 'data' ] ) ) {
                        $this->wc_order_id = $_POST[ 'data' ];
                        $wc_order          = new WC_Order( $this->wc_order_id );
                        $this->address     = Barcode_Order_Admin::get_address( $wc_order );
                    }

                    $this->load_editor( 1 );
                    exit;
                    break;
            }
        }

        /**
         * Checks if the draft order contains all necessary fields.
         *
         * @param null $wc_order_id
         * @return bool
         */
        public function validate( $wc_order_id = null )
        {
            $draft_order = Barcode_Database_Manager::get_draft_order( $wc_order_id );
            $errors      = [];
            if ( !$draft_order ) {
                $errors[] = __( 'Order not found', 'dhuett' );

            } else {

                $recipient = $draft_order->get_recipient();
                $customer  = $draft_order->get_customer();

                if ( $this->empty_or_missing( $draft_order->dispatch_type ) ) {
                    $errors[] = __( 'Dispatch type missing', 'dhuett' );
                }

                if ( $this->empty_or_missing( $draft_order->weight ) ) {
                    $errors [] = __( 'Weight missing', 'dhuett' );
                }

                if ( $this->empty_or_missing( $draft_order->label_size ) ) {
                    $errors[] = __( 'Label size missing', 'dhuett' );
                }

                if ( ( $draft_order->print_addresses == 'NONE' || $draft_order->print_addresses == 'ONLY_RECIPIENT' ) && empty( $customer->domicilePostOffice ) && empty( $customer->domicile_post_office ) )
                    $errors[] = __( 'The sender\'s domicile post office field must not be empty if the address is not printed.', 'dhuett' );

                if ( !$recipient ) {
                    $errors[] = __( 'Recipient missing', 'dhuett' );
                } else {
                    if ( $this->empty_or_missing( $recipient->name1 ) )
                        $errors[] = __( 'Recipient last name or company missing', 'dhuett' );
                    if ( $this->empty_or_missing( $recipient->street ) )
                        $errors[] = __( 'Recipient street missing', 'dhuett' );
                    if ( $this->empty_or_missing( $recipient->zip ) )
                        $errors[] = __( 'Recipient zip missing', 'dhuett' );
                    if ( $this->empty_or_missing( $recipient->city ) )
                        $errors[] = __( 'Recipient city missing', 'dhuett' );

                }

            }

            if ( empty( $errors ) )
                return false;

            return $errors;
        }

        private function add_response_data( $request )
        {
            if ( isset( $request[ 'print_addresses' ] ) )
                $this->json_response->print_address( Barcode_Data::get_print_address()[ $request[ 'print_addresses' ] ] );
        }

        public function load_template_into_draft_order( $template_id )
        {
            $this->template = Barcode_Database_Manager::get_template( $template_id );
            if ( !$this->template )
                return $this->json_response->status( Mame_Json_Response::STATUS_FAILURE )->respond();

            $options                            = get_option( 'mame_bc_options_group' );
            $this->load_order_overview_directly = isset( $options[ 'template_order_overview' ] ) ? $options[ 'template_order_overview' ] : false;

            // Create new draft.
            $draft_order             = new Barcode_Order( $this->template );
            $draft_order->id         = null;
            $draft_order->order_type = Barcode_Order::TYPE_DRAFT;

            $where = [ 'order_type' => 'draft' ];
            if ( $this->wc_order_id ) {
                $where[ 'wc_order_id' ] = $draft_order->wc_order_id = $this->wc_order_id;
            } else {
                $where[ 'wc_order_id' ] = null;
            }

            // Delete old draft.
            Barcode_Database_Manager::delete( Barcode_Database_Manager::table( Barcode_Database_Manager::ORDER ), $where );

            // Sender
            if ( $this->template->customer_id ) {
                $draft_order->customer = $this->sender = $this->template->get_customer();
            }

            // Receiver
            if ( $this->wc_order_id ) {
                if ( $this->template->print_addresses === Barcode_Template::ADDRESS_R || $this->template->print_addresses === Barcode_Template::ADDRESS_R_C ) {
                    if ( $this->template->load_wc_address === Barcode_Template::LOAD_WC_ADDRESS_SHIPPING ) {
                        $this->address = Barcode_Recipient::get_shipping_address( new WC_Order( $this->wc_order_id ) );
                    } elseif ( $this->template->load_wc_address === Barcode_Template::LOAD_WC_ADDRESS_BILLING ) {
                        $this->address = Barcode_Recipient::get_billing_address( new WC_Order( $this->wc_order_id ) );
                    } elseif ( $this->template->load_wc_address === Barcode_Template::LOAD_WC_ADDRESS_CUSTOM ) {
                        $this->address = Barcode_Recipient::get_custom_address( new WC_Order( $this->wc_order_id ) );
                    }

                    // Save address
                    if ( $this->address )
                        $draft_order->address = $this->address;
                }
            }

            // Update draft order.
            Barcode_Database_Manager::update_draft_order( $draft_order );
        }

        public function load_order_overview()
        {
            $overview = Mame_Html::h3( __( 'Order the following product?', 'dhuett' ) );

            $draft_order = Barcode_Database_Manager::get_draft_order( $this->wc_order_id );
            $overview    .= $this->product_overview( $draft_order );
            $overview    .= Mame_Html::open_tag( 'div', [ 'id' => MAME_BC_PREFIX . '-order-buttons' ] );
            $overview    .= Mame_Html::button( __( 'Order', 'dhuett' ), [ 'id' => MAME_BC_PREFIX . '-order-confirm-btn', 'class' => MAME_BC_PREFIX . '-btn' ] );
            $overview    .= Mame_Html::button( __( 'Cancel', 'dhuett' ), [ 'class' => MAME_BC_PREFIX . '-close-overlay ' . MAME_BC_PREFIX . '-btn' ] );
            $overview    .= Mame_Html::close_tag( 'div' );

            return $overview;
        }

        public function json_license_select_field( $licenses, $action )
        {
            $content = Mame_Html::h3( __( 'Select a license', 'dhuett' ) );
            $content .= Mame_Html::p( __( 'This product needs a license. Please select a license to continue' ) );
            $content .= $this->get_license_fields( $licenses );
            $content .= Mame_Html::button( __( 'Continue', 'dhuett' ), [ 'id' => MAME_BC_PREFIX . '-' . $action, 'class' => MAME_BC_PREFIX . '-btn' ] );
            $content .= Mame_Html::button( __( 'Cancel', 'dhuett' ), [ 'class' => MAME_BC_PREFIX . '-btn ' . MAME_BC_PREFIX . '-close-overlay' ] );
            return $this->json_response->status( Mame_Json_Response::STATUS_SUCCESS )->html( '#' . MAME_BC_PREFIX . '-overlay .content', $content )->show( '#' . MAME_BC_PREFIX . '-overlay' );
        }

        /**
         * Returns a JSON string containing the order overview (without addresses).
         *
         * @param null $wc_order_id
         * @return Barcode_Json_Response
         */
        public function json_order_overview( $wc_order_id = null )
        {
            $overview = Mame_Html::h3( __( 'Order the following product?', 'dhuett' ) );

            $draft_order = Barcode_Database_Manager::get_draft_order( $wc_order_id );

            $overview .= $this->product_overview( $draft_order );

            $overview .= Mame_Html::open_tag( 'div', [ 'id' => MAME_BC_PREFIX . '-order-buttons' ] );
            $overview .= Mame_Html::button( __( 'Order', 'dhuett' ), [ 'id' => MAME_BC_PREFIX . '-order-confirm-btn', 'class' => MAME_BC_PREFIX . '-btn' ] );
            $overview .= Mame_Html::button( __( 'Cancel', 'dhuett' ), [ 'class' => MAME_BC_PREFIX . '-close-overlay ' . MAME_BC_PREFIX . '-btn' ] );
            $overview .= Mame_Html::close_tag( 'div' );

            return $this->json_response->status( Barcode_Json_Response::STATUS_SUCCESS )->html( '#' . MAME_BC_PREFIX . '-overlay .content', $overview )->show( '#' . MAME_BC_PREFIX . '-overlay', '#' . MAME_BC_PREFIX . '-content-wrapper' );

        }

        /**
         * Returns the formatted product data for the order overview.
         *
         * @param Barcode_Order $order
         * @return string
         */
        private function product_overview( $order )
        {
            $dispatch_types    = json_decode( $order->dispatch_type );
            $bc_dispatch_types = Barcode_Data::get_dispatch_types();
            $overview          = Mame_Html::open_tag( 'div', [ 'id' => MAME_BC_PREFIX . '-product-overview' ] );

            // Dispatch type
            $overview .= $this->product_overview_row( __( 'Dispatch type', 'dhuett' ), array_reduce( $dispatch_types, function ( $c, $i ) use ( $bc_dispatch_types ) {
                if ( array_key_exists( $i, $bc_dispatch_types ) )
                    return $c . ' ' . $bc_dispatch_types[ $i ][ 'title' ] . '<br>';
                elseif ( $i === 'SP' )
                    return $c . ' ' . $i . '<br>';
                return '';
            }, '' ) );


            // Weight
            $overview .= $this->product_overview_row( __( 'Weight (g)', 'dhuett' ), $order->weight );

            // Additions
            $additions    = json_decode( $order->additions );
            $bc_additions = Barcode_Data::get_additional_services();
            if ( !empty( $additions ) ) {
                $overview .= $this->product_overview_row( __( 'Additions', 'dhuett' ), array_reduce( $additions, function ( $c, $i ) use ( $bc_additions ) {
                    return $c . ' ' . $bc_additions[ $i ] . '<br>';
                }, '' ) );
            }

            // Label size
            $overview .= $this->product_overview_row( __( 'Label size', 'dhuett' ), $order->label_size );

            $overview .= Mame_Html::div( '', [ 'class' => 'clearfix' ] );
            $overview .= Mame_Html::close_tag( 'div' );
            return $overview;
        }

        /**
         * Formatted row for the order overview.
         *
         * @param $key
         * @param $value
         * @return string
         */
        private function product_overview_row( $key, $value )
        {
            return '<div class="overview-data"><div class="key">' . $key . '</div><div class="value">' . $value . '</div></div>';
        }

        /**
         * Sends a request for a preview order and saves the preview stamp. Returns a link to download the stamp.
         *
         * @param null $wc_order_id
         */
        public function preview_order( $wc_order_id = null )
        {
            $draft_order = Barcode_Database_Manager::get_draft_order( $wc_order_id );

            $order_handler = new Barcode_Order_Handler();
            $response      = $order_handler->new_order_preview( $draft_order );

            // Bail if error in response
            if ( $response [ 'error' ] ) {

                $content = $response[ 'message' ] . '<br>';
                $content .= Mame_Html::button( __( 'Ok', 'dhuett' ), [ 'class' => MAME_BC_PREFIX . '-close-overlay ' . MAME_BC_PREFIX . '-btn' ] );

                return $this->json_response->status( false )->html( '#' . MAME_BC_PREFIX . '-overlay .content', $content )->show( '#' . MAME_BC_PREFIX . '-overlay' )->respond();
            }

            $this->json_response->status( true );
            if ( !empty( $response[ 'files' ] ) ) {
                $count = 0;
                foreach ( $response[ 'files' ] as $file ) {
                    $this->json_response->after( '#mame_bc-preview-btn', '<a id="' . MAME_BC_PREFIX . '-download-preview-btn-' . $count . '" class="' . MAME_BC_PREFIX . '-btn ' . MAME_BC_PREFIX . '-download-preview-btn' . '" href="' . add_query_arg( [ MAME_BC_PREFIX . '-type' => 'temp', MAME_BC_PREFIX . '-file' => $file, MAME_BC_PREFIX . '-download' => '1' ], get_admin_url() ) . '" target="_blank">' . __( 'Download preview', 'dhuett' ) . '</a>' );
                    $count++;
                }
                $this->json_response->hide( '#mame_bc-preview-btn' )->hide( '#' . MAME_BC_PREFIX . '-overlay' );
            }
            return $this->json_response->respond();

        }

        /**
         * Sends a request for a new order. Returns links to the saved stamp.
         *
         * @param null $wc_order_id
         */
        public function order( $wc_order_id = null )
        {
            $draft_order = Barcode_Database_Manager::get_draft_order( $wc_order_id );

            $order_handler = new Barcode_Order_Handler();
            $response      = $order_handler->new_order( $draft_order );

            // Bail if error in response
            if ( $response [ 'error' ] ) {

                $content = $response[ 'message' ] . '<br>';
                $content .= Mame_Html::button( __( 'Ok', 'dhuett' ), [ 'class' => MAME_BC_PREFIX . '-close-overlay ' . MAME_BC_PREFIX . '-btn' ] );

                return $this->json_response->status( false )->html( '#' . MAME_BC_PREFIX . '-overlay .content', $content )->show( '#' . MAME_BC_PREFIX . '-overlay' )->respond();
            }

            $content = '';
            $this->json_response->status( true );
            if ( !empty( $response[ 'files' ] ) ) {

                $options = get_option( 'mame_bc_options_group' );

                $count = 0;
                foreach ( $response[ 'files' ] as $file ) {
                    // $this->json_response->after( '#mame_bc-preview-btn', '<a id="' . MAME_BC_PREFIX . '-download-preview-btn-' . $count . '" class="' . MAME_BC_PREFIX . '-btn" href="' . add_query_arg( [ MAME_BC_PREFIX . '-type' => 'temp', MAME_BC_PREFIX . '-file' => $file, MAME_BC_PREFIX . '-download' => '1' ], get_admin_url() ) . '" target="_blank">' . __( 'Download preview', 'dhuett' ) . '</a>' );
                    $link = add_query_arg( [ MAME_BC_PREFIX . '-type' => 'stamps', MAME_BC_PREFIX . '-file' => $file, MAME_BC_PREFIX . '-download' => '1' ], get_admin_url() );

                    $download_link = add_query_arg( [ MAME_BC_PREFIX . '-type' => 'stamps', MAME_BC_PREFIX . '-file' => $file, MAME_BC_PREFIX . '-download' => '1' ], get_admin_url() );
                    $print_link    = add_query_arg( [ MAME_BC_PREFIX . '-print' => 1, MAME_BC_PREFIX . '-type' => 'stamps', MAME_BC_PREFIX . '-file' => $file ], get_admin_url() );

                    do_action( MAME_BC_PREFIX . '_after_order', $draft_order->wc_order_id, $download_link );

                    $content .= Mame_Html::open_tag( 'div', [ 'class' => MAME_BC_PREFIX . '-download-stamp-wrapper' ] );
                    $content .= Mame_Html::img( plugins_url() . '/mame-barcode-woocommerce/assets/images/barcode_download_icon_wh.png' );
                    $content .= Mame_Html::a( __( 'Download barcode', 'dhuett' ), $download_link, [ 'id' => MAME_BC_PREFIX . '-download-stamp-btn-' . $count, 'class' => MAME_BC_PREFIX . '-btn ' . MAME_BC_PREFIX . '-download-stamp-btn' ], '_blank' );
                    $content .= ' ' . Mame_Html::a( __( 'Print barcode', 'dhuett' ), $print_link, [ 'id' => MAME_BC_PREFIX . '-print-stamp-btn-' . $count, 'class' => MAME_BC_PREFIX . '-btn ' . MAME_BC_PREFIX . '-download-stamp-btn' ], '_blank' );
                    $content .= Mame_Html::close_tag( 'div' );

                    if ( isset( $options[ 'open_print_dialog' ] ) && $options[ 'open_print_dialog' ] )
                        $content .= '<script>jQuery(document).ready(function (){window.open(jQuery("#mame_bc-print-stamp-btn-' . $count . '").attr("href"));});</script>';

                    //$this->json_response->replace( '#' . MAME_BC_PREFIX . '-content-wrapper', '<div id="' . MAME_BC_PREFIX . '-download-stamp-wrapper"><a href="' . $link . '" target="_blank"><img src="' . plugins_url() . '/mame-barcode-woocommerce/assets/images/barcode_download_icon_wh.png' . '"></a><a id="' . MAME_BC_PREFIX . '-download-stamp-btn" class="' . MAME_BC_PREFIX . '-btn" href="' . $link . '" target="_blank">' . __( 'Download barcode', 'dhuett' ) . '</a></div>' );

                    $count++;
                }
            }

            $content = apply_filters( MAME_BC_PREFIX . '_stamp_download_screen', $content, $draft_order->wc_order_id );

            return $this->json_response
                ->status( true )
                ->replace( '#' . MAME_BC_PREFIX . '-content-wrapper', $content )
                ->hide( '#mame_bc-order-btn' )
                ->respond();
        }

        /**
         * Adds dispatch type to the json response.
         *
         * @return Mame_Json_Response
         */
        public function json_dispatch_type()
        {
            return $this->json_response->html( '#' . MAME_BC_PREFIX . '-dispatch_type-wrapper', $this->get_dispatch_type_fields() );
        }

        /**
         * Adds weight to the json response.
         *
         * @return Mame_Json_Response
         */
        public function json_weight()
        {
            return $this->json_response->html( '#' . MAME_BC_PREFIX . '-weight-wrapper', $this->get_weight_fields() );

        }

        /**
         * Adds populated address fields to the json response.
         *
         * @return Webstamp_Editor_Json_Response
         */
        public function json_address()
        {
            return $this->json_response->html( '#' . MAME_BC_PREFIX . '-address-wrapper', $this->get_address_fields() );
        }

        public function json_media_startpos( $args )
        {
            if ( $args[ 'media_type' ] == Webstamp_Database_Manager::LABEL_ID_2 || $args[ 'media_type' ] == Webstamp_Database_Manager::LABEL_ID_1 ) {

                return $this->json_response->html( '#' . MAME_BC_PREFIX . '-media_startpos-wrapper', $this->get_media_startpos_fields() )->show( "#" . MAME_BC_PREFIX . '-media_startpos-wrapper' );
            }
            return $this->json_response->hide( "#" . MAME_BC_PREFIX . '-media_startpos-wrapper' );
        }

        /**
         * Adds populated license fields to the json response.
         *
         * @return Barcode_Json_Response
         */
        public function json_license()
        {
            $content = $this->get_license_fields();
            if ( $content )
                return $this->json_response->html( '#' . MAME_BC_PREFIX . '-license_number-wrapper', $content );
            return $this->json_response;
        }

        /**
         * Displays the dispatch type select field.
         */
        public function get_dispatch_type_fields()
        {
            $dispatch_types = json_decode( $this->template->dispatch_type );
            $gas            = $sp = false;
            if ( !empty( $dispatch_types ) ) {
                if ( $gas = array_search( 'GAS', $dispatch_types ) )
                    unset( $dispatch_types[ $gas ] );
                if ( $sp = array_search( 'SP', $dispatch_types ) )
                    unset( $dispatch_types[ $sp ] );
            }

            $content = Mame_Html::open_tag( 'div', [ 'id' => MAME_BC_PREFIX . '-step-dispatch_type', 'class' => MAME_BC_PREFIX . '-step ' . MAME_BC_PREFIX . '-step-1', 'data-page' => 1 ] );
            $content .= Mame_Html::h2( __( 'Dispatch type', 'dhuett' ) );
            $content .= Mame_Html::select( MAME_BC_PREFIX . '-dispatch_type', array_map( function ( $e ) {
                return $e[ 'title' ];
            }, Barcode_Data::get_dispatch_types() ), [ 'id' => MAME_BC_PREFIX . '-dispatch_type', 'class' => MAME_BC_PREFIX . '-select-save', 'data-step' => 'dispatch_type' ], !empty( $dispatch_types ) ? $dispatch_types[ 0 ] : null );

            $content .= Mame_Html::checkbox( __( 'GAS', 'dhuett' ), MAME_BC_PREFIX . '-dispatch_type_2', 'GAS', $gas ? [ 'checked' => 'checked' ] : null );
            $content .= Mame_Html::checkbox( __( 'Bulky goods', 'dhuett' ), MAME_BC_PREFIX . '-dispatch_type_2', 'SP', $sp ? [ 'checked' => 'checked' ] : null );
            $content .= Mame_Html::button( __( 'Save', 'dhuett' ), [ 'class' => MAME_BC_PREFIX . '-save-btn', 'data-step' => 'dispatch_type' ] );
            $content .= Mame_Html::close_tag( 'div' );

            return $content;
        }


        public function get_weight_fields()
        {
            $content = Mame_Html::open_tag( 'div', [ 'id' => MAME_BC_PREFIX . '-step-weight', 'class' => MAME_BC_PREFIX . '-step ' . MAME_BC_PREFIX . '-step-1', 'data-page' => 1 ] );
            $content .= Mame_Html::h2( __( 'Weight', 'dhuett' ) );
            $content .= Mame_Html::select( MAME_BC_PREFIX . '-weight', Barcode_Data::get_weights(), [ 'id' => MAME_BC_PREFIX . '-weight', 'class' => MAME_BC_PREFIX . '-select-save', 'data-step' => 'weight' ], $this->template->weight ?: null );

            $content .= Mame_Html::button( __( 'Save', 'dhuett' ), [ 'class' => MAME_BC_PREFIX . '-save-btn', 'data-step' => 'weight' ] );
            $content .= Mame_Html::close_tag( 'div' );

            return $content;
        }

        public function get_additions_fields()
        {
            $additions = json_decode( $this->template->additions );

            $content = Mame_Html::open_tag( 'div', [ 'id' => MAME_BC_PREFIX . '-step-additions', 'class' => MAME_BC_PREFIX . '-step ' . MAME_BC_PREFIX . '-step-1', 'data-page' => 1 ] );
            $content .= Mame_Html::h2( __( 'Options', 'dhuett' ) );

            $content .= Mame_Html::div( Mame_Html::p( __( 'Additional services', 'dhuett' ) ), [ 'class' => MAME_BC_PREFIX . '-tab-title active', 'data-target' => MAME_BC_PREFIX . '-tab_additions' ] );
            $content .= Mame_Html::div( Mame_Html::p( __( 'Delivery instruction', 'dhuett' ) ), [ 'class' => MAME_BC_PREFIX . '-tab-title', 'data-target' => MAME_BC_PREFIX . '-tab_delivery_instruction' ] );

            $content .= Mame_Html::open_tag( 'div', [ 'id' => MAME_BC_PREFIX . '-tab_additions', 'class' => MAME_BC_PREFIX . '-tab-content active' ] );

            $content            .= Mame_Html::div( Mame_Html::p( __( 'Increased liability and receipt against signature', 'dhuett' ) ), [ 'class' => MAME_BC_PREFIX . '-accordion-title', 'data-target' => MAME_BC_PREFIX . '-accordion_liability' ] );
            $services_liability = Barcode_Data::get_additional_services_liability();
            $content            .= Mame_Html::open_tag( 'div', [ 'id' => MAME_BC_PREFIX . '-accordion_liability', 'class' => MAME_BC_PREFIX . '-accordion-content' ] );

            $content .= Mame_Html::checkbox_group(
                array_map( function ( $label, $name ) {
                    return array(
                        'label' => $label,
                        'name'  => MAME_BC_PREFIX . '-additions',
                        'value' => $name,
                    );
                }, $services_liability, array_keys( $services_liability ) ),
                [], $additions
            );
            $content .= Mame_Html::close_tag( 'div' );

            $content       .= Mame_Html::div( Mame_Html::p( __( 'Time of delivery', 'dhuett' ) ), [ 'class' => MAME_BC_PREFIX . '-accordion-title', 'data-target' => MAME_BC_PREFIX . '-accordion_time' ] );
            $services_time = Barcode_Data::get_additional_services_time();
            $content       .= Mame_Html::open_tag( 'div', [ 'id' => MAME_BC_PREFIX . '-accordion_time', 'class' => MAME_BC_PREFIX . '-accordion-content' ] );
            $content       .= Mame_Html::checkbox_group(

                array_map( function ( $label, $name ) {
                    return array(
                        'label' => $label,
                        'name'  => MAME_BC_PREFIX . '-additions',
                        'value' => $name,
                    );
                }, $services_time, array_keys( $services_time ) ),
                [], $additions

            );
            $content       .= Mame_Html::close_tag( 'div' );

            $content            .= Mame_Html::div( Mame_Html::p( __( 'Packaging for delicate goods', 'dhuett' ) ), [ 'class' => MAME_BC_PREFIX . '-accordion-title', 'data-target' => MAME_BC_PREFIX . '-accordion_packaging' ] );
            $services_packaging = Barcode_Data::get_additional_services_delicate_goods();
            $content            .= Mame_Html::open_tag( 'div', [ 'id' => MAME_BC_PREFIX . '-accordion_packaging', 'class' => MAME_BC_PREFIX . '-accordion-content' ] );
            $content            .= Mame_Html::checkbox_group(

                array_map( function ( $label, $name ) {
                    return array(
                        'label' => $label,
                        'name'  => MAME_BC_PREFIX . '-additions',
                        'value' => $name,
                    );
                }, $services_packaging, array_keys( $services_packaging ) ),
                [], $additions

            );
            $content            .= Mame_Html::close_tag( 'div' );

            $content .= Mame_Html::close_tag( 'div' );

            $content .= Mame_Html::open_tag( 'div', [ 'id' => MAME_BC_PREFIX . '-tab_delivery_instruction', 'class' => MAME_BC_PREFIX . '-tab-content' ] );

            $delivery_instructions = Barcode_Data::get_delivery_instructions();
            $content               .= Mame_Html::checkbox_group(

                array_map( function ( $label, $name ) {
                    return array(
                        'label' => $label,
                        'name'  => MAME_BC_PREFIX . '-additions',
                        'value' => $name,
                    );
                }, $delivery_instructions, array_keys( $delivery_instructions ) ),
                [], $additions

            );

            $content .= Mame_Html::close_tag( 'div' );

            $content .= Mame_Html::button( __( 'Save', 'dhuett' ), [ 'class' => MAME_BC_PREFIX . '-save-btn', 'data-step' => 'additions' ] );
            $content .= Mame_Html::close_tag( 'div' );

            return $content;
        }

        public function get_label_size_fields()
        {
            $content = Mame_Html::open_tag( 'div', [ 'id' => MAME_BC_PREFIX . '-step-label_size', 'class' => MAME_BC_PREFIX . '-step ' . MAME_BC_PREFIX . '-step-1', 'data-page' => 1 ] );
            $content .= Mame_Html::h2( __( 'Label size', 'dhuett' ) );
            $content .= Mame_Html::select( MAME_BC_PREFIX . '-weight', Barcode_Data::get_label_sizes(), [ 'id' => MAME_BC_PREFIX . '-label_size', 'class' => MAME_BC_PREFIX . '-select-save', 'data-step' => 'label_size' ], $this->template->label_size );

            $content .= Mame_Html::button( __( 'Save', 'dhuett' ), [ 'class' => MAME_BC_PREFIX . '-save-btn', 'data-step' => 'label_size' ] );
            $content .= Mame_Html::close_tag( 'div' );

            return $content;
        }

        public function get_print_addresses_fields()
        {
            $content = Mame_Html::open_tag( 'div', [ 'id' => MAME_BC_PREFIX . '-step-print_addresses', 'class' => MAME_BC_PREFIX . '-step ' . MAME_BC_PREFIX . '-step-1', 'data-page' => 1 ] );
            $content .= Mame_Html::h2( __( 'Addresses printed on label', 'dhuett' ) );
            $content .= Mame_Html::select( MAME_BC_PREFIX . '-print_addresses', Barcode_Data::get_print_address(), [ 'id' => MAME_BC_PREFIX . '-print_addresses', 'class' => MAME_BC_PREFIX . '-select-save', 'data-step' => 'print_addresses' ], $this->template->print_addresses ?: Barcode_Template::ADDRESS_ );

            $content .= Mame_Html::button( __( 'Save', 'dhuett' ), [ 'class' => MAME_BC_PREFIX . '-save-btn', 'data-step' => 'print_addresses' ] );
            $content .= Mame_Html::close_tag( 'div' );

            return $content;
        }

        public function get_address_fields( $sender = false, $zone = null )
        {
            $addition = $sender ? '-sender' : '';
            $content  = Mame_Html::input( 'text', MAME_BC_PREFIX . $addition . '-first_name', ( $this->address && isset( $this->address->first_name ) ? $this->address->first_name : null ), [ 'placeholder' => __( 'First name', 'dhuett' ), 'maxlength' => 35 ] );
            $content  .= Mame_Html::input( 'text', MAME_BC_PREFIX . $addition . '-name1', ( $this->address && isset( $this->address->name1 ) ? $this->address->name1 : null ), [ 'placeholder' => __( 'Last name or company', 'dhuett' ), 'maxlength' => 35 ] );
            $content  .= Mame_Html::input( 'text', MAME_BC_PREFIX . $addition . '-name2', ( $this->address && isset( $this->address->name2 ) ? $this->address->name2 : null ), [ 'placeholder' => __( 'Addition 1 (company suffix, department, Pickpost, ...)', 'dhuett' ), 'maxlength' => 35 ] );
            $content  .= Mame_Html::input( 'text', MAME_BC_PREFIX . $addition . '-name3', ( $this->address && isset( $this->address->name3 ) ? $this->address->name3 : null ), [ 'placeholder' => __( 'Addition 2 (c/o, department, ...)', 'dhuett' ), 'maxlength' => 35 ] );

            $content .= Mame_Html::input( 'text', MAME_BC_PREFIX . $addition . '-address_suffix', ( $this->address && isset( $this->address->address_suffix ) ? $this->address->address_suffix : null ), [ 'placeholder' => __( 'Address suffix', 'dhuett' ), 'maxlength' => 35 ] );
            $content .= Mame_Html::input( 'text', MAME_BC_PREFIX . $addition . '-street', ( $this->address && isset( $this->address->street ) ? $this->address->street : null ), [ 'placeholder' => __( 'Street', 'dhuett' ), 'maxlength' => 35 ] );
            $content .= Mame_Html::input( 'text', MAME_BC_PREFIX . $addition . '-house_no', ( $this->address && isset( $this->address->house_no ) ? $this->address->house_no : null ), [ 'placeholder' => __( 'House number', 'dhuett' ), 'maxlength' => 10 ] );
            $content .= Mame_Html::input( 'text', MAME_BC_PREFIX . $addition . '-pobox', ( $this->address && isset( $this->address->pobox ) ? $this->address->pobox : null ), [ 'placeholder' => __( 'Postbox', 'dhuett' ), 'maxlength' => 35 ] );
            $content .= Mame_Html::input( 'text', MAME_BC_PREFIX . $addition . '-floor_no', ( $this->address && isset( $this->address->floor_no ) ? $this->address->floor_no : null ), [ 'placeholder' => __( 'Floor number', 'dhuett' ), 'maxlength' => 5 ] );
            $content .= Mame_Html::input( 'text', MAME_BC_PREFIX . $addition . '-mailbox_no', ( $this->address && isset( $this->address->mailbox_no ) ? $this->address->mailbox_no : null ), [ 'placeholder' => __( 'Letter box number', 'dhuett' ), 'maxlength' => 10 ] );
            $content .= Mame_Html::input( 'text', MAME_BC_PREFIX . $addition . '-zip', ( $this->address && isset( $this->address->zip ) ? $this->address->zip : null ), [ 'placeholder' => __( 'Postcode', 'dhuett' ), 'maxlength' => 10 ] );
            $content .= Mame_Html::input( 'text', MAME_BC_PREFIX . $addition . '-city', ( $this->address && isset( $this->address->city ) ? $this->address->city : null ), [ 'placeholder' => __( 'Place', 'dhuett' ), 'maxlength' => 35 ] );

            $content .= Mame_Html::input( 'text', MAME_BC_PREFIX . $addition . '-phone', ( $this->address && isset( $this->address->phone ) ? $this->address->phone : null ), [ 'placeholder' => __( 'Telephone number', 'dhuett' ), 'maxlength' => 20 ] );
            $content .= Mame_Html::input( 'text', MAME_BC_PREFIX . $addition . '-mobile', ( $this->address && isset( $this->address->mobile ) ? $this->address->mobile : null ), [ 'placeholder' => __( 'Mobile number', 'dhuett' ), 'maxlength' => 20 ] );
            $content .= Mame_Html::input( 'text', MAME_BC_PREFIX . $addition . '-email', ( $this->address && isset( $this->address->email ) ? $this->address->email : null ), [ 'placeholder' => __( 'Email address', 'dhuett' ), 'maxlength' => 160 ] );

            return $content;
        }

        public function receiver_address_fields()
        {
            $content = Mame_Html::open_tag( 'div', [ 'id' => MAME_BC_PREFIX . '-step-address', 'data-page' => 3 ] );
            $content .= Mame_Html::h2( __( 'Address', 'dhuett' ) );

            $content .= $this->get_address_fields();

            if ( !empty( $this->wc_order_id ) ) {
                $content .= Mame_Html::button( __( 'Copy from billing address', 'dhuett' ), [ 'id' => MAME_BC_PREFIX . '-address-copy-billing-btn', 'class' => MAME_BC_PREFIX . '-btn', 'data-step' => 'address' ] );
                $content .= Mame_Html::button( __( 'Copy from shipping address', 'dhuett' ), [ 'id' => MAME_BC_PREFIX . '-address-copy-shipping-btn', 'class' => MAME_BC_PREFIX . '-btn', 'data-step' => 'address' ] );
                $content .= Mame_Html::button( __( 'Copy from custom assignment', 'dhuett' ), [ 'id' => MAME_BC_PREFIX . '-address-copy-custom-btn', 'class' => MAME_BC_PREFIX . '-btn', 'data-step' => 'address' ] );
            }
            $content .= Mame_Html::close_tag( 'div' );

            return $content;
        }

        public function sender_address_fields()
        {
            $content = Mame_Html::open_tag( 'div', [ 'id' => MAME_BC_PREFIX . '-step-sender', 'data-page' => 3 ] );
            $content .= Mame_Html::h2( __( 'Sender address', 'dhuett' ) );

            $content .= Mame_Html::input( 'text', MAME_BC_PREFIX . '-sender-name1', ( $this->sender && isset( $this->sender->name1 ) ? $this->sender->name1 : null ), [ 'placeholder' => __( 'First name and surname, or company name', 'dhuett' ), 'maxlength' => 25 ] );
            $content .= Mame_Html::input( 'text', MAME_BC_PREFIX . '-sender-name2', ( $this->sender && isset( $this->sender->name2 ) ? $this->sender->name2 : null ), [ 'placeholder' => __( 'Additional name', 'dhuett' ), 'maxlength' => 25 ] );
            $content .= Mame_Html::input( 'text', MAME_BC_PREFIX . '-sender-street', ( $this->sender && isset( $this->sender->street ) ? $this->sender->street : null ), [ 'placeholder' => __( 'Street', 'dhuett' ), 'maxlength' => 25 ] );
            $content .= Mame_Html::input( 'text', MAME_BC_PREFIX . '-sender-pobox', ( $this->sender && isset( $this->sender->pobox ) ? $this->sender->pobox : null ), [ 'placeholder' => __( 'Postbox', 'dhuett' ), 'maxlength' => 25 ] );
            $content .= Mame_Html::input( 'text', MAME_BC_PREFIX . '-sender-zip', ( $this->sender && isset( $this->sender->zip ) ? $this->sender->zip : null ), [ 'placeholder' => __( 'Postcode', 'dhuett' ), 'maxlength' => 6 ] );
            $content .= Mame_Html::input( 'text', MAME_BC_PREFIX . '-sender-city', ( $this->sender && isset( $this->sender->city ) ? $this->sender->city : null ), [ 'placeholder' => __( 'Place', 'dhuett' ), 'maxlength' => 25 ] );
            $content .= Mame_Html::input( 'text', MAME_BC_PREFIX . '-sender-domicile_post_office', ( $this->sender && isset( $this->sender->domicile_post_office ) ? $this->sender->domicile_post_office : null ), [ 'placeholder' => __( 'Domicile Post Office (required if sender\'s address is not printed).', 'dhuett' ) ] );

            $content .= Mame_Html::button( __( 'Load default sender address', 'dhuett' ), [ 'id' => MAME_BC_PREFIX . '-load-sender-btn', 'class' => MAME_BC_PREFIX . '-btn' ] );

            $content .= Mame_Html::close_tag( 'div' );

            return $content;
        }

        public function get_image_fields()
        {
            if ( !$this->template->image_id ) {
                $img_upload_field = Mame_WP_Helper::image_upload_field( 'mame_bc_options_group', 'image', null, 200, plugins_url( '../../assets/images/stamp_white.png', __FILE__ ) );
            } else {
                $img_upload_field = $this->image_upload_field( 'mame_bc_options_group', 'image', null, 200, plugins_url( '../../assets/images/stamp_white.png', __FILE__ ) );
            }
            $content = Mame_Html::open_tag( 'div', [ 'id' => MAME_BC_PREFIX . '-step-image', 'data-page' => 1 ] );
            $content .= Mame_Html::h2( __( 'Image', 'dhuett' ) );
            $content .= Mame_Html::div( Mame_Html::checkbox( __( 'Use image', 'dhuett' ), MAME_BC_PREFIX . '-use_image', 1, $this->template->image_id ? [ 'checked' => 'checked' ] : null ), [ 'id' => MAME_BC_PREFIX . '-use_image-wrapper', 'class' => MAME_BC_PREFIX . '-data-wrapper', 'data-page' => 1 ] );
            $content .= Mame_Html::div( $img_upload_field, [ 'id' => MAME_BC_PREFIX . '-image-wrapper', 'class' => MAME_BC_PREFIX . '-data-wrapper ' . ( $this->template->image_id ? '' : 'hidden' ), 'data-page' => 1 ] );
            $content .= Mame_Html::button( __( 'Save', 'dhuett' ), [ 'class' => MAME_BC_PREFIX . '-save-btn', 'data-step' => 'image_id' ] );
            $content .= Mame_Html::close_tag( 'div' );
            return $content;
        }

        protected function image_upload_field( $options_name, $name, $width, $height, $default_image )
        {
            if ( !empty( $this->template->image_id ) ) {
                $sizes = [];
                if ( !empty( $width ) )
                    $sizes[] = $width;
                if ( !empty( $height ) )
                    $sizes[] = $height;
                $image_attributes = wp_get_attachment_image_src( $this->template->image_id, array( $width, $height ) );
                $src              = $image_attributes[ 0 ];
                $value            = $this->template->image_id;
            } else {
                $src   = $default_image;
                $value = '';
            }

            $text = __( 'Upload', 'dhuett' );

            $width_str  = empty( $width ) ? 'auto' : $width . ' px';
            $height_str = empty( $height ) ? 'auto' : $height . ' px';

            return '
        <div class="upload">
            <img data-src="' . $default_image . '" src="' . $src . '" width="' . $width_str . '" height="' . $height_str . '" />
            <div>
                <input type="hidden" name="image_id" id="' . MAME_BC_PREFIX . '-image_id" value="' . $value . '" />
                <button type="submit" class="upload_image_button button">' . $text . '</button>
                <button type="submit" class="remove_image_button button">&times;</button>
            </div>
        </div>
    ';
        }

        public function load_editor_selector()
        {
            ?>
            <div id="<?= MAME_BC_PREFIX ?>-editor-selector-wrapper" <?php if ( $this->wc_order_id ) echo 'data-wc_order_id="' . $this->wc_order_id . '"'; ?> >
                <div id="<?= MAME_BC_PREFIX ?>-editor-wrapper">
                    <div id="<?= MAME_BC_PREFIX ?>-editor"
                         data-page="1" <?= $this->wc_order_id ? 'data-wc-order-id="' . $this->wc_order_id . '"' : '' ?>>
                        <div id="<?= MAME_BC_PREFIX ?>-content-wrapper">
                            <div id="<?= MAME_BC_PREFIX ?>-content">
                                <h1><span><?= __( 'Select editor', 'dhuett' ) ?></span></h1>
                                <div id="<?= MAME_BC_PREFIX ?>-editor-select">
                                    <div id="<?= MAME_BC_PREFIX ?>-editor-select-webstamp"
                                         class="<?= MAME_BC_PREFIX ?>-editor-select-icon">
                                        <img src="<?= plugins_url( '../../assets/images/webstamp_select_s.png', __FILE__ ) ?>">
                                    </div>
                                    <div id="<?= MAME_BC_PREFIX ?>-editor-select-barcode"
                                         class="<?= MAME_BC_PREFIX ?>-editor-select-icon">
                                        <img src="<?= plugins_url( '../../assets/images/barcode_select_s.png', __FILE__ ) ?>">
                                    </div>
                                    <div class="clearfix"></div>
                                </div>
                            </div>
                            <div id="<?= MAME_BC_PREFIX ?>-loader" class="<?= MAME_BC_PREFIX ?>-loader">
                                <div class="content"><img
                                            src="<?= plugin_dir_url( dirname( dirname( __FILE__ ) ) ) . 'assets/images/loader-5.gif' ?>">
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <script>
                jQuery(document).ready(function ($) {
                    new MameWsBcSelector();
                });
            </script>
            <?php
        }

        public function load_start_screen()
        {
            ?>
            <div id="<?= MAME_BC_PREFIX ?>-editor-wrapper">
                <div id="<?= MAME_BC_PREFIX ?>-editor" data-step="category"
                     data-page="1" <?= $this->wc_order_id ? 'data-wc-order-id="' . $this->wc_order_id . '"' : '' ?>>
                    <div id="<?= MAME_BC_PREFIX ?>-content-wrapper">

                        <div id="<?= MAME_BC_PREFIX ?>-content">
                            <h1><span><?= __( 'Barcode Editor' ) ?></span></h1>

                            <form action="" method="post">
                                <div id="<?= MAME_BC_PREFIX ?>-steps">

                                    <?php
                                    $templates   = Barcode_Database_Manager::get_templates();
                                    $draft_order = Barcode_Database_Manager::get_draft_order( $this->wc_order_id );

                                    if ( !$templates && !$draft_order ) {
                                        $this->load_editor_content();
                                    } else {
                                        if ( $draft_order ) {
                                            ?>
                                            <div class="<?= MAME_BC_PREFIX ?>-start-content">
                                                <h3><?= __( 'There is an unfinished order', 'dhuett' ) ?></h3>
                                                <p><?= __( 'Do you want to continue with the order?', 'dhuett' ) ?></p>
                                                <button id="<?= MAME_BC_PREFIX ?>-continue-btn"
                                                        class="<?= MAME_BC_PREFIX ?>-btn"
                                                        data-order_id="<?= $draft_order->id ?>"><?= __( 'Continue', 'dhuett' ) ?></button>
                                            </div>
                                            <?php
                                        }

                                        if ( $templates ) {
                                            ?>
                                            <div class="<?= MAME_BC_PREFIX ?>-start-content">
                                                <h3><?= __( 'Start from template', 'dhuett' ) ?></h3>
                                                <p><?= __( 'Choose a template', 'dhuett' ) ?></p>
                                                <?= Mame_Html::select( MAME_BC_PREFIX . '-load-template', Mame_WP_Helper::object_array_map( $templates, 'id', 'name' ), [ 'id' => MAME_BC_PREFIX . '-load-template' ] ) ?>
                                                <button id="<?= MAME_BC_PREFIX ?>-load-template-btn"
                                                        class="<?= MAME_BC_PREFIX ?>-btn"><?= __( 'Load', 'dhuett' ) ?></button>
                                            </div>
                                            <?php

                                        }
                                        ?>
                                        <div class="<?= MAME_BC_PREFIX ?>-start-content">
                                            <h3><?= __( 'New order', 'dhuett' ) ?></h3>
                                            <button id="<?= MAME_BC_PREFIX ?>-new-order-btn"
                                                    class="<?= MAME_BC_PREFIX ?>-btn"><?= __( 'Start', 'dhuett' ) ?></button>
                                        </div>
                                        <?php
                                    }

                                    ?>

                                </div>
                            </form>
                        </div>
                        <div id="<?= MAME_BC_PREFIX ?>-overlay" class="<?= MAME_BC_PREFIX ?>-overlay">
                            <div class="content">
                                <h3><?= __( 'There is an unfinished order', 'dhuett' ) ?></h3>
                                <p><?= __( 'Do you want to continue with the order?', 'dhuett' ) ?></p>
                                <button id="<?= MAME_BC_PREFIX ?>-continue-btn"
                                        class="<?= MAME_BC_PREFIX ?>-btn"><?= __( 'Continue', 'dhuett' ) ?></button>
                                <button id="<?= MAME_BC_PREFIX ?>-new-order-btn"
                                        class="<?= MAME_BC_PREFIX ?>-btn"><?= __( 'Start new order', 'dhuett' ) ?></button>
                            </div>
                        </div>
                        <div id="<?= MAME_BC_PREFIX ?>-loader" class="<?= MAME_BC_PREFIX ?>-loader">
                            <div class="content"><img
                                        src="<?= plugin_dir_url( dirname( dirname( __FILE__ ) ) ) . 'assets/images/loader-5.gif' ?>">
                            </div>
                        </div>
                    </div>
                </div>

            </div>
            <script>
                jQuery(document).ready(function ($) {
                    //new MameBcEditor();
                });
            </script>
            <?php
        }

        public function load_editor( $page = false )
        {
            $errors = false;
            if ( $this->autoload_draft && $this->load_order_overview_directly )
                $errors = $this->validate( $this->wc_order_id );

            $templates   = Barcode_Database_Manager::get_templates();
            $draft_order = Barcode_Database_Manager::get_draft_order( $this->wc_order_id );
            ?>
            <div id="<?= MAME_BC_PREFIX ?>-editor-wrapper">
                <div id="<?= MAME_BC_PREFIX ?>-editor" data-step="category"
                     data-page="1" <?= $this->wc_order_id ? 'data-wc-order-id="' . $this->wc_order_id . '"' : '' ?>>
                    <div id="<?= MAME_BC_PREFIX ?>-content-wrapper">

                        <div id="<?= MAME_BC_PREFIX ?>-content">
                            <h1><span><?= __( 'Barcode Editor', 'dhuett' ) ?></span></h1>
                            <div id="<?= MAME_BC_PREFIX ?>-steps-nav"
                                 class="<?= MAME_BC_PREFIX ?>-editor-content" <?= !$templates && !$draft_order || $this->autoload_draft ? 'style="display:block !important;"' : '' ?>>
                                <div id="<?= MAME_BC_PREFIX ?>-nav-step-wrapper-1"
                                     class="<?= MAME_BC_PREFIX ?>-nav-step-wrapper <?= $page === 1 ? 'active' : ''; ?>"
                                     data-page="1">
                                    <div class="<?= MAME_BC_PREFIX ?>-nav-step">
                                        <img src="<?= plugins_url( '../../assets/images/barcode_logo.png', __FILE__ ) ?>">
                                        <div class="<?= MAME_BC_PREFIX ?>-number"><p>1</p></div>
                                    </div>
                                    <p class="text"><span><?= __( 'Label', 'dhuett' ) ?></span></p>
                                </div>
                                <div id="<?= MAME_BC_PREFIX ?>-nav-step-wrapper-2"
                                     class="<?= MAME_BC_PREFIX ?>-nav-step-wrapper <?= $page === 2 ? 'active' : ''; ?>"
                                     data-page="2">
                                    <div class="<?= MAME_BC_PREFIX ?>-nav-step">
                                        <img src="<?= plugins_url( '../../assets/images/receiver_address_s.png', __FILE__ ) ?>">
                                        <div class="<?= MAME_BC_PREFIX ?>-number"><p>2</p></div>
                                    </div>
                                    <p class="text"><span><?= __( 'Receiver address', 'dhuett' ) ?></span></p>
                                </div>
                                <div id="<?= MAME_BC_PREFIX ?>-nav-step-wrapper-3"
                                     class="<?= MAME_BC_PREFIX ?>-nav-step-wrapper <?= $page === 3 ? 'active' : ''; ?>"
                                     data-page="3">
                                    <div class="<?= MAME_BC_PREFIX ?>-nav-step">
                                        <img src="<?= plugins_url( '../../assets/images/sender_address_s.png', __FILE__ ) ?>">
                                        <div class="<?= MAME_BC_PREFIX ?>-number"><p>3</p></div>
                                    </div>
                                    <p class="text"><span><?= __( 'Sender', 'dhuett' ) ?></span></p>
                                </div>
                                <div id="<?= MAME_BC_PREFIX ?>-nav-step-wrapper-4"
                                     class="<?= MAME_BC_PREFIX ?>-nav-step-wrapper <?= $page === 4 ? 'active' : ''; ?>"
                                     data-page="4">
                                    <div class="<?= MAME_BC_PREFIX ?>-nav-step">
                                        <img src="<?= plugins_url( '../../assets/images/overview_s.png', __FILE__ ) ?>">
                                        <div class="<?= MAME_BC_PREFIX ?>-number"><p>4</p></div>
                                    </div>
                                    <p class="text"><span><?= __( 'Overview', 'dhuett' ) ?></span></p>
                                </div>
                                <div class="clearfix"></div>
                            </div>
                            <form action="" method="post">
                                <?php


                                if ( !$templates && !$draft_order || $this->autoload_draft ) {
                                    ?>
                                    <div id="<?= MAME_BC_PREFIX ?>-steps"
                                         class="<?= MAME_BC_PREFIX ?>-form-content <?= MAME_BC_PREFIX ?>-editor-content"
                                         style="display:block !important;">
                                        <?php
                                        echo $this->load_editor_form_content( $page, $errors );
                                        ?>
                                    </div>
                                    <?php
                                } else {

                                    ?>
                                    <div id="<?= MAME_BC_PREFIX ?>-steps"
                                         class="<?= MAME_BC_PREFIX ?>-form-content <?= MAME_BC_PREFIX ?>-editor-content"></div>
                                    <div id="<?= MAME_BC_PREFIX ?>-start"
                                         class="<?= MAME_BC_PREFIX ?>-form-content">
                                        <?php
                                        if ( $draft_order ) {
                                            ?>
                                            <div class="<?= MAME_BC_PREFIX ?>-start-content">
                                                <h3><?= __( 'There is an unfinished order', 'dhuett' ) ?></h3>
                                                <p><?= __( 'Do you want to continue with the order?', 'dhuett' ) ?></p>
                                                <button id="<?= MAME_BC_PREFIX ?>-continue-btn"
                                                        class="<?= MAME_BC_PREFIX ?>-btn"
                                                        data-order_id="<?= $draft_order->id ?>"><?= __( 'Continue', 'dhuett' ) ?></button>
                                            </div>
                                            <?php
                                        }

                                        if ( $templates ) {
                                            ?>
                                            <div class="<?= MAME_BC_PREFIX ?>-start-content">
                                                <h3><?= __( 'Start from template', 'dhuett' ) ?></h3>
                                                <p><?= __( 'Choose a template', 'dhuett' ) ?></p>
                                                <?= Mame_Html::select( MAME_BC_PREFIX . '-load-template', Mame_WP_Helper::object_array_map( $templates, 'id', 'name' ), [ 'id' => MAME_BC_PREFIX . '-load-template' ] ) ?>
                                                <button id="<?= MAME_BC_PREFIX ?>-load-template-btn"
                                                        class="<?= MAME_BC_PREFIX ?>-btn"><?= __( 'Load', 'dhuett' ) ?></button>
                                            </div>
                                            <?php

                                        }
                                        ?>
                                        <div class="<?= MAME_BC_PREFIX ?>-start-content">
                                            <h3><?= __( 'New order', 'dhuett' ) ?></h3>
                                            <p><?= __( 'Start a new order', 'dhuett' ) ?></p>
                                            <button id="<?= MAME_BC_PREFIX ?>-new-order-btn"
                                                    class="<?= MAME_BC_PREFIX ?>-btn"><?= __( 'Start', 'dhuett' ) ?></button>
                                        </div>
                                    </div>
                                    <?php
                                }
                                ?>

                                <div class="<?= MAME_BC_PREFIX ?>-editor-content" <?= !$templates && !$draft_order || $this->autoload_draft ? 'style="display:block !important;"' : '' ?>>
                                    <?= Mame_Html::button( __( 'Back', 'dhuett' ), [ 'id' => MAME_BC_PREFIX . '-prev-btn', 'class' => MAME_BC_PREFIX . '-btn hidden' ] ); ?>
                                    <?= Mame_Html::button( __( 'Next', 'dhuett' ), [ 'id' => MAME_BC_PREFIX . '-next-btn', 'class' => MAME_BC_PREFIX . '-btn' ] ); ?>
                                </div>
                            </form>
                        </div>
                        <div id="<?= MAME_BC_PREFIX ?>-overlay"
                             class="<?= MAME_BC_PREFIX ?>-overlay" <?= $this->autoload_draft && $this->load_order_overview_directly && !$errors ? 'style="display:block"' : '' ?>>
                            <div class="content">
                                <?php
                                if ( $this->autoload_draft && $this->load_order_overview_directly && !$errors ) {
                                    echo $this->load_order_overview();
                                }
                                ?>
                            </div>
                        </div>
                        <div id="<?= MAME_BC_PREFIX ?>-loader" class="<?= MAME_BC_PREFIX ?>-loader">
                            <div class="content"><img
                                        src="<?= plugin_dir_url( dirname( dirname( __FILE__ ) ) ) . 'assets/images/loader-5.gif' ?>">
                            </div>
                        </div>
                    </div>
                </div>

            </div>
            <script>
                jQuery(document).ready(function ($) {
                    new MameBcEditor();
                })
            </script>
            <?php
        }

        public function load_editor_form_content( $page = 1, $errors = false )
        {
            $content = $this->get_page_1( $page !== 1 );
            $content .= $this->get_page_2( $page !== 2 );
            $content .= $this->get_page_3( $page !== 3 );
            $content .= $this->get_page_4( $page !== 4, $errors );
            return $content;
        }

        public function load_editor_content()
        {
            ?>
            <h1><span><?= __( 'Barcode Editor' ) ?></span></h1>
            <div id="<?= MAME_BC_PREFIX ?>-steps-nav">
                <div id="<?= MAME_BC_PREFIX ?>-nav-step-wrapper-1"
                     class="<?= MAME_BC_PREFIX ?>-nav-step-wrapper" data-page="1">
                    <div class="<?= MAME_BC_PREFIX ?>-nav-step">
                        <img src="<?= plugins_url( '../../assets/images/barcode_logo.png', __FILE__ ) ?>">
                        <div class="<?= MAME_BC_PREFIX ?>-number"><p>1</p></div>
                    </div>
                    <p class="text"><span><?= __( 'Label', 'dhuett' ) ?></span></p>
                </div>
                <div id="<?= MAME_BC_PREFIX ?>-nav-step-wrapper-2"
                     class="<?= MAME_BC_PREFIX ?>-nav-step-wrapper" data-page="2">
                    <div class="<?= MAME_BC_PREFIX ?>-nav-step">
                        <img src="<?= plugins_url( '../../assets/images/receiver_address_s.png', __FILE__ ) ?>">
                        <div class="<?= MAME_BC_PREFIX ?>-number"><p>2</p></div>
                    </div>
                    <p class="text"><span><?= __( 'Receiver address', 'dhuett' ) ?></span></p>
                </div>
                <div id="<?= MAME_BC_PREFIX ?>-nav-step-wrapper-3"
                     class="<?= MAME_BC_PREFIX ?>-nav-step-wrapper" data-page="3">
                    <div class="<?= MAME_BC_PREFIX ?>-nav-step">
                        <img src="<?= plugins_url( '../../assets/images/sender_address_s.png', __FILE__ ) ?>">
                        <div class="<?= MAME_BC_PREFIX ?>-number"><p>3</p></div>
                    </div>
                    <p class="text"><span><?= __( 'Sender', 'dhuett' ) ?></span></p>
                </div>
                <div id="<?= MAME_BC_PREFIX ?>-nav-step-wrapper-4"
                     class="<?= MAME_BC_PREFIX ?>-nav-step-wrapper" data-page="4">
                    <div class="<?= MAME_BC_PREFIX ?>-nav-step">
                        <img src="<?= plugins_url( '../../assets/images/overview_s.png', __FILE__ ) ?>">
                        <div class="<?= MAME_BC_PREFIX ?>-number"><p>4</p></div>
                    </div>
                    <p class="text"><span><?= __( 'Overview', 'dhuett' ) ?></span></p>
                </div>
                <div class="clearfix"></div>
            </div>
            <form action="" method="post">
                <div id="<?= MAME_BC_PREFIX ?>-steps">
                    <?= $this->get_page_1(); ?>
                    <?= $this->get_page_2( true ); ?>
                    <?= $this->get_page_3( true ); ?>
                    <?= $this->get_page_4( true ); ?>
                </div>
                <?= Mame_Html::button( __( 'Back', 'dhuett' ), [ 'id' => MAME_BC_PREFIX . '-prev-btn', 'class' => MAME_BC_PREFIX . '-btn hidden' ] ); ?>
                <?= Mame_Html::button( __( 'Next', 'dhuett' ), [ 'id' => MAME_BC_PREFIX . '-next-btn', 'class' => MAME_BC_PREFIX . '-btn' ] ); ?>
            </form>
            <?php
        }

        public function get_page_1( $hidden = false )
        {
            $content = Mame_Html::open_tag( 'div', [ 'id' => MAME_BC_PREFIX . '-page-1', 'class' => MAME_BC_PREFIX . '-page ' . ( $hidden ? 'hidden' : '' ) ] );
            $content .= Mame_Html::div( $this->get_dispatch_type_fields(), [ 'id' => MAME_BC_PREFIX . '-dispatch_type-wrapper', 'data-page' => 1 ] );
            $content .= Mame_Html::div( $this->get_weight_fields(), [ 'id' => MAME_BC_PREFIX . '-weight-wrapper', 'data-page' => 1 ] );
            $content .= Mame_Html::div( $this->get_additions_fields(), [ 'id' => MAME_BC_PREFIX . '-additions-wrapper', 'data-page' => 1 ] );
            $content .= Mame_Html::div( $this->get_label_size_fields(), [ 'id' => MAME_BC_PREFIX . '-label_size-wrapper', 'data-page' => 1 ] );
            $content .= Mame_Html::div( $this->get_print_addresses_fields(), [ 'id' => MAME_BC_PREFIX . '-print_address-wrapper', 'data-page' => 1 ] );

            $content .= Mame_Html::close_tag( 'div' );

            return $content;
        }

        public function get_page_2( $hidden = false )
        {
            $content = Mame_Html::open_tag( 'div', [ 'id' => MAME_BC_PREFIX . '-page-2', 'class' => MAME_BC_PREFIX . '-page ' . ( $hidden ? 'hidden' : '' ) ] );
            // $content .= Mame_Html::div( $this->get_quantity_fields(), [ 'id' => MAME_BC_PREFIX . '-quantity-wrapper', 'class' => MAME_BC_PREFIX . '-data-wrapper', 'data-page' => 1 ] );
            $content .= Mame_Html::div( $this->receiver_address_fields(), [ 'id' => MAME_BC_PREFIX . '-address-wrapper', 'class' => MAME_BC_PREFIX . '-data-wrapper', 'data-page' => 3 ] );
            $content .= Mame_Html::button( __( 'Save', 'dhuett' ), [ 'class' => MAME_BC_PREFIX . '-save-btn', 'data-step' => 'address' ] );
            $content .= Mame_Html::close_tag( 'div' );

            return $content;
        }

        public function get_page_3( $hidden = false )
        {
            $content = Mame_Html::open_tag( 'div', [ 'id' => MAME_BC_PREFIX . '-page-3', 'class' => MAME_BC_PREFIX . '-page ' . ( $hidden ? 'hidden' : '' ) ] );
            $content .= Mame_Html::div( $this->sender_address_fields(), [ 'id' => MAME_BC_PREFIX . '-sender-wrapper', 'class' => MAME_BC_PREFIX . '-data-wrapper', 'data-page' => 3 ] );
            $content .= Mame_Html::button( __( 'Save', 'dhuett' ), [ 'class' => MAME_BC_PREFIX . '-save-btn', 'data-step' => 'sender' ] );

            $content .= $this->get_image_fields();
            $content .= Mame_Html::close_tag( 'div' );

            return $content;
        }


        public function get_page_4( $hidden = false )
        {
            $dispatch_type = json_decode( $this->template->dispatch_type );
            $additions     = json_decode( $this->template->additions );

            $content = Mame_Html::open_tag( 'div', [ 'id' => MAME_BC_PREFIX . '-page-4', 'class' => MAME_BC_PREFIX . '-page ' . ( $hidden ? 'hidden' : '' ) ] );
            $content .= Mame_Html::open_tag( 'div', [ 'id' => MAME_BC_PREFIX . '-overview-wrapper', 'class' => MAME_BC_PREFIX . '-data-wrapper', 'data-page' => 6 ] );
            $content .= Mame_Html::div( '', [ 'class' => 'errors' ] );
            $content .= Mame_Html::div( Mame_Html::div( Mame_Html::p( __( 'Dispatch type', 'dhuett' ) ), [ 'class' => 'first' ] ) . Mame_Html::div( Mame_Html::p( !empty( $dispatch_type ) ? implode( '<br>', $dispatch_type ) : '' ), [ 'id' => MAME_BC_PREFIX . '-overview-dispatch_type', 'class' => 'second' ] ) . Mame_Html::button( __( 'Edit' ), [ 'class' => MAME_BC_PREFIX . '-btn overview-btn', 'data-page' => '1' ] ), [ 'class' => 'overview-data' ] );
            $content .= Mame_Html::div( Mame_Html::div( Mame_Html::p( __( 'Weight (g)', 'dhuett' ) ), [ 'class' => 'first' ] ) . Mame_Html::div( Mame_Html::p( ( !empty( $this->template->weight ) ? $this->template->weight : '' ), [ 'id' => MAME_BC_PREFIX . '-overview-weight' ] ), [ 'class' => 'second' ] ) . Mame_Html::button( __( 'Edit' ), [ 'class' => MAME_BC_PREFIX . '-btn overview-btn', 'data-page' => '1' ] ), [ 'class' => 'overview-data' ] );
            $content .= Mame_Html::div( Mame_Html::div( Mame_Html::p( __( 'Additions', 'dhuett' ) ), [ 'class' => 'first' ] ) . Mame_Html::div( Mame_Html::p( !empty( $additions ) ? implode( '<br>', $additions ) : '' ), [ 'id' => MAME_BC_PREFIX . '-overview-additions', 'class' => 'second' ] ) . Mame_Html::button( __( 'Edit' ), [ 'class' => MAME_BC_PREFIX . '-btn overview-btn', 'data-page' => '1' ] ), [ 'class' => 'overview-data' ] );
            $content .= Mame_Html::div( Mame_Html::div( Mame_Html::p( __( 'Label size', 'dhuett' ) ), [ 'class' => 'first' ] ) . Mame_Html::div( Mame_Html::p( ( !empty( $this->template->label_size ) ? $this->template->label_size : '' ), [ 'id' => MAME_BC_PREFIX . '-overview-label_size' ] ), [ 'class' => 'second' ] ) . Mame_Html::button( __( 'Edit' ), [ 'class' => MAME_BC_PREFIX . '-btn overview-btn', 'data-page' => '1' ] ), [ 'class' => 'overview-data' ] );
            $content .= Mame_Html::div( Mame_Html::div( Mame_Html::p( __( 'Print addresses', 'dhuett' ) ), [ 'class' => 'first' ] ) . Mame_Html::div( Mame_Html::p( ( !empty( $this->template->print_addresses ) ? Barcode_Data::get_print_address()[ $this->template->print_addresses ] : '' ), [ 'id' => MAME_BC_PREFIX . '-overview-print_addresses' ] ), [ 'class' => 'second' ] ) . Mame_Html::button( __( 'Edit' ), [ 'class' => MAME_BC_PREFIX . '-btn overview-btn', 'data-page' => '1' ] ), [ 'class' => 'overview-data' ] );
            $content .= Mame_Html::div( Mame_Html::div( Mame_Html::p( __( 'Recipient', 'dhuett' ) ), [ 'class' => 'first' ] ) . Mame_Html::div( Mame_Html::p( $this->address ? $this->address->get_html() : '' ), [ 'id' => MAME_BC_PREFIX . '-overview-address', 'class' => 'second' ] ) . Mame_Html::button( __( 'Edit' ), [ 'class' => MAME_BC_PREFIX . '-btn overview-btn', 'data-page' => '2' ] ), [ 'class' => 'overview-data' ] );
            $content .= Mame_Html::div( Mame_Html::div( Mame_Html::p( __( 'Sender address', 'dhuett' ) ), [ 'class' => 'first' ] ) . Mame_Html::div( Mame_Html::p( $this->sender ? $this->sender->get_html() : '' ), [ 'id' => MAME_BC_PREFIX . '-overview-sender', 'class' => 'second' ] ) . Mame_Html::button( __( 'Edit' ), [ 'class' => MAME_BC_PREFIX . '-btn overview-btn', 'data-page' => '3' ] ), [ 'class' => 'overview-data' ] );
            // $content .= Mame_Html::div( Mame_Html::div( Mame_Html::p( __( 'License', 'dhuett' ) ), [ 'class' => 'first' ] ) . Mame_Html::div( Mame_Html::p( '', [ 'id' => MAME_BC_PREFIX . '-overview-license_number' ] ), [ 'class' => 'second' ] ) . Mame_Html::button( __( 'Edit' ), [ 'id' => MAME_BC_PREFIX . '-edit-license-btn', 'class' => MAME_BC_PREFIX . '-btn' ] ), [ 'class' => 'overview-data' ] );
            $content .= Mame_Html::div( '', [ 'class' => 'clearfix' ] );
            $content .= Mame_Html::close_tag( 'div' );
            $content .= Mame_Html::open_tag( 'div', [ 'id' => MAME_BC_PREFIX . '-order-buttons' ] );
            $content .= Mame_Html::button( __( 'Preview', 'dhuett' ), [ 'id' => MAME_BC_PREFIX . '-preview-btn', 'class' => MAME_BC_PREFIX . '-btn' ] );
            $content .= Mame_Html::button( __( 'Order', 'dhuett' ), [ 'id' => MAME_BC_PREFIX . '-order-btn', 'class' => MAME_BC_PREFIX . '-btn' ] );
            $content .= Mame_Html::close_tag( 'div' );
            $content .= Mame_Html::close_tag( 'div' );
            return $content;
        }

        /**
         * Checks if a variable is empty or equals '0' or 0.
         *
         * @param $property
         * @return bool
         */
        private function empty_or_missing( $property )
        {
            if ( empty( $property ) || $property === 0 || $property === '0' )
                return true;
            return false;
        }
    }

}