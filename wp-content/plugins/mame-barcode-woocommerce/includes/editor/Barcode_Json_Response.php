<?php
if ( !defined( 'ABSPATH' ) ) {
    exit;
}

if ( !class_exists( 'Barcode_Json_Response' ) ) {

    /**
     * Class Barcode_Json_Response
     */
    class Barcode_Json_Response extends Mame_Json_Response
    {
        public function __construct()
        {
            parent::__construct();
        }

        public function action( $action )
        {
            $this->response[ 'action' ] = $action;
            return $this;
        }

        public function order( $order )
        {
            $this->response[ 'order' ] = $order;
            return $this;
        }

        public function address( $address )
        {
            $this->response[ 'address' ] = $address;
            return $this;
        }

        public function sender( $sender )
        {
            $this->response[ 'sender' ] = $sender;
            return $this;
        }

        public function print_address( $text )
        {
            $this->response[ 'print_address' ] = $text;
            return $this;
        }

        /**
         * @param $page
         * @return Barcode_Json_Response
         */
        public function page( $page )
        {
            $this->response[ 'page' ] = $page;
            return $this;
        }
    }

}