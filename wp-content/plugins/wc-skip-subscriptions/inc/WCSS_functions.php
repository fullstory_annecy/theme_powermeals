<?php
/**
 * Created by PhpStorm.
 * User: Surfer
 */

function wcss_user_has_subscription_skip_caps( $the_subscription, $user = null ) {
    $subscription = wc_od_get_subscription( $the_subscription );

    if ( ! $subscription ) {
        return false;
    }

    if ( ! $user ) {
        $user = wp_get_current_user();
    }

    $has_caps = user_can( $user, 'view_order', wc_od_get_order_prop( $subscription, 'id' ) );

    /**
     * Filter if the user has the capabilities to work with the subscription delivery.
     *
     * @since 1.3.0
     *
     * @param bool            $has_caps     True if the user has subscription delivery caps. False otherwise.
     * @param WC_Subscription $subscription The subscription instance.
     * @param WP_User         $user         Optional. The user to check.
     */
    return apply_filters( 'wc_od_user_has_subscription_delivery_caps', $has_caps, $subscription, $user );
}

function wcss_get_current_subscription_id() {
    global $wp_query;

    $subscription_id = false;

    if ( is_ajax() ) {
        $subscription_id = wcss_get_posted_data( 'subscription_id', false );
    } elseif ( wc_is_skip_delivery_endpoint() ) {
        $subscription_id = (int) $wp_query->query_vars['skip-delivery'];
    } elseif ( wcs_is_view_subscription_page() ) {
        $subscription_id = (int) $wp_query->query_vars['view-subscription'];
    }

    return $subscription_id;
}

function wcss_get_posted_data( $key, $default = null ) {
    $value = $default;

    if ( isset( $_POST[ $key ] ) ) { // WPCS: input var ok, CSRF OK.
        $value = wc_clean( wp_unslash( $_POST[ $key ] ) ); // WPCS: CSRF ok, sanitization ok.
    } elseif ( isset( $_POST['post_data'] ) ) { // Posted by AJAX on refresh the content. WPCS: CSRF ok.
        parse_str( $_POST['post_data'], $post_data ); // WPCS: CSRF ok, sanitization ok.

        if ( isset( $post_data[ $key ] ) ) {
            $value = wc_clean( wp_unslash( $post_data[ $key ] ) );
        }
    }

    return $value;
}

function wc_is_skip_delivery_endpoint() {
    global $wp_query;
    return ( is_account_page() && isset( $wp_query->query_vars['skip-delivery'] ) );
}

function wc_skip_delivery_endpoint( $subscription_id ) {
    return wc_get_endpoint_url( 'skip-delivery', $subscription_id, wc_get_page_permalink( 'myaccount' ) );
}

function wcss_get_subscription( $the_subscription ) {
    return ( $the_subscription instanceof WC_Subscription ? $the_subscription : wcs_get_subscription( $the_subscription ) );
}

function wcss_get_order_meta( $the_order, $key = '', $single = true ) {
    $meta = '';

    $order_id = ( $the_order instanceof WC_Order ? wcss_get_order_prop( $the_order, 'id' ) : intval( $the_order ) );

    if ( $order_id ) {
        $meta = get_post_meta( $order_id, $key, $single );
    }

    return $meta;
}

function wcss_delete_order_meta( $the_order, $key, $save = false ) {
    $is_object = ( $the_order instanceof WC_Order );

    if ( $is_object && method_exists( $the_order, 'delete_meta_data' ) ) {
        $the_order->delete_meta_data( $key );
        $deleted = true;

        // Save the meta immediately.
        if ( $save ) {
            $the_order->save_meta_data();
        }
    } else {
        $order_id = ( $is_object ? wc_store_credit_get_order_prop( $the_order, 'id' ) : intval( $the_order ) );
        $deleted  = delete_post_meta( $order_id, $key );
    }

    return $deleted;
}

function wcss_update_order_meta( $the_order, $key, $value, $save = false ) {
    $updated   = false;
    $is_object = ( $the_order instanceof WC_Order );

    if ( $is_object && method_exists( $the_order, 'update_meta_data' ) ) {
        $old_value = $the_order->get_meta( $key );

        if ( $old_value !== $value ) {
            $the_order->update_meta_data( $key, $value );
            $updated = true;

            // Save the meta immediately.
            if ( $save ) {
                $the_order->save_meta_data();
            }
        }
    } else {
        $order_id = ( $is_object ? wc_store_credit_get_order_prop( $the_order, 'id' ) : intval( $the_order ) );
        $updated  = (bool) update_post_meta( $order_id, $key, $value );
    }

    return $updated;
}

function wcss_get_order_prop( $the_order, $key ) {
    $order = wcss_get_order( $the_order );

    if ( ! $order ) {
        return null;
    }

    $getter = array( $order, "get_{$key}" );

    // Properties renamed in WC 3.0+.
    $renamed_props = array(
        'date_created' => 'order_date',
        'currency'     => 'order_currency',
    );

    if ( is_callable( $getter ) ) {
        $prop = call_user_func( $getter );
    } else {
        $key  = ( array_key_exists( $key, $renamed_props ) ? $renamed_props[ $key ] : $key );
        $prop = $order->{$key};
    }

    return $prop;
}

function wcss_get_order( $the_order ) {
    return ( $the_order instanceof WC_Order ? $the_order : wc_get_order( $the_order ) );
}

function wcss_add_order_note( $order, $note, $is_customer_note = 0, $added_by_user = false ) {
    $note_id = $order->add_order_note( $note, $is_customer_note );

    if ( $note_id ) {
        $type = get_post_type( wcss_get_order_prop( $order, 'id' ) );
        do_action( "wcss_added_{$type}_note", $note, $order, $is_customer_note, $added_by_user );
    }

    return $note_id;
}

function wcss_get_skip_subscription_delivery_field_value( $the_subscription, $input ) {
    $subscription = wcss_get_subscription( $the_subscription );

    if ( ! $subscription ) {
        return null;
    }


    $value = wcss_get_posted_data( $input );

    $fields = wc_get_skip_subscription_fields($the_subscription);

    $_skip_delivery_dates = wcss_get_order_meta($subscription->get_id(), '_skip_subscription_dates', true);
    $value_week = wcss_get_order_meta( $subscription->get_id(), "_{$input}" );

    $skip_delivery_dates = array();
    if(is_array($_skip_delivery_dates)){
        foreach ($_skip_delivery_dates as $k => $v){
            $skip_delivery_dates[$k] = date('F j, Y', $v);
        }
    }

    if ( is_null( $value ) ) {
        //$value = wcss_get_order_meta( $subscription->get_id(), "_{$input}" );
        if(is_array($skip_delivery_dates) && !empty($skip_delivery_dates) && in_array( $fields[$input]['label'], $skip_delivery_dates) ){
            $value = $fields[$input]['label'];
        }
        $value = ( null === $value ? null : 1 );
    }
    return $value;
}

function wc_get_skip_subscription_fields( $the_subscription ) {
    $subscription = wcss_get_subscription( $the_subscription );

    if ( ! $subscription ) {
        return false;
    }

    $_next_payment = wcss_get_order_meta($subscription->get_id(), '_schedule_next_payment', true);
    $_delivery_date = wcss_get_order_meta($subscription->get_id(), '_delivery_date', true);
//    $_skip_delivery_dates = wcss_get_order_meta($subscription->get_id(), '_skip_subscription_dates', true);
//    wcss_pr($_skip_delivery_dates);
    $nd = 0;
    $title_date = 0;
    $t = date('H:i:s', strtotime($_next_payment));

    $td = strtotime( 'today' );

    $ndd = strtotime($_delivery_date);
    //var_dump($_delivery_date);
    $ndd_t = strtotime($_delivery_date . ' ' . $t );

    if($td > $ndd){
        $title_date = $ndd_t + WEEK_IN_SECONDS;
        $nd = $ndd + WEEK_IN_SECONDS;
    }else{
        $title_date = $ndd_t;
        $nd = $ndd;
    }


    $fields = array(
        'skip_subscription_start' => array(
            'type'        => 'wcss_subscription_section_start_field',
            'title'       => __( 'Skip Dates', 'wcss' ),
            'description' => __( 'Skip deliveries for selected dates.', 'wcss' ),
            'class'       => array( 'wc-skip-subscription-section' ),
        )
    );

    for($i = 1; $i <= 5; $i++) {

        $skip_date = $nd + ( WEEK_IN_SECONDS * $i );
        $skip_date_t = $title_date + ( WEEK_IN_SECONDS * $i );
        //$skip_subscription_dates = wcss_get_skip_subscription_delivery_field_value( $subscription, 'skip_subscription_dates_week_'.$i );
        //var_dump($skip_subscription_dates);

        $fields['skip_subscription_dates_week_'.$i] = array(
            'type' => 'checkbox',
            'title' => __(date('F j, Y H:i:s', $skip_date_t), 'wcss'),
            'label' => __(date('F j, Y', $skip_date), 'wcss'),
            'description' => __('', 'wcss'),
            'class' => array('wc-skip-subscription-date'),
        );

    }
    $fields['skip_subscription_end'] = array(
            'type' => 'wcss_subscription_section_end_field',
        );



    return $fields;
}

function wcss_subscription_section_start_field( $field, $key, $args ) {
    ob_start();
    echo '<section class="' . esc_attr( implode( ' ', $args['class'] ) ) . '">';

    if ( ! empty( $args['title'] ) ) :
        echo '<h2>' . esc_html( $args['title'] ) . '</h2>';
    endif;

    if ( ! empty( $args['description'] ) ) :
        echo '<p>' . esc_html( $args['description'] ) . '</p>';
    endif;

    return ob_get_clean();
}

function wcss_subscription_section_end_field() {
    return '</section>';
}

function wcss_validate_skip_subscription_date( $the_subscription, $date ) {
    $subscription = wcss_get_subscription( $the_subscription );

    if ( ! $subscription ) {
        return false;
    }

    // Fetch the subscription delivery date arguments.
    $args = wc_od_get_subscription_delivery_date_args( $subscription );

    return wcss_validate_delivery_date( $date, $args, 'subscription' );
}

function wcss_validate_delivery_date( $date, $args = array(), $context = '' ) {
    $delivery_timestamp = wcss_get_timestamp( $date );

    if ( ! $delivery_timestamp ) {
        return false;
    }

    $defaults = array(
        'shipping_method'    => false,
        'start_date'         => false,
        'end_date'           => false, // The maximum date (Non-inclusive).
        'delivery_days'      => WC_OD()->settings()->get_setting( 'delivery_days' ),
        'disabled_days'      => null, // Use these disabled days if not null.
        'disabled_days_args' => array( // Arguments used by the wc_od_disabled_days() function.
            'type'    => 'delivery',
            'country' => '', // Events for all countries.
        ),
    );

    $args = apply_filters( 'wc_od_validate_delivery_date_args', wp_parse_args( $args, $defaults ), $context );

    $valid = true;

    // Validate start_date.
    if ( $args['start_date'] ) {
        $start_timestamp = wcss_get_timestamp( $args['start_date'] );

        // Out of range.
        if ( ! $start_timestamp || $delivery_timestamp < $start_timestamp ) {
            $valid = false;
        }
    }

    // Validate end_date.
    if ( $valid && $args['end_date'] ) {
        $end_timestamp = wcss_get_timestamp( $args['end_date'] );

        // Out of range.
        if ( ! $end_timestamp || $delivery_timestamp >= $end_timestamp ) {
            $valid = false;
        }
    }

    // Validate delivery day status.
    if ( $valid ) {
        $wday         = date( 'w', $delivery_timestamp );
        $delivery_day = $args['delivery_days'][ $wday ];

        // Calculate the status only for the default delivery days.
        if ( $args['delivery_days'] === $defaults['delivery_days'] ) {
            $status = wc_od_get_delivery_day_status(
                $delivery_day,
                array(
                    'shipping_method' => $args['shipping_method'],
                ),
                $context
            );
        } else {
            $status = $delivery_day['enabled'];
        }

        $valid = wc_od_string_to_bool( $status );
    }

    // Validate disabled days.
    if ( $valid ) {
        $delivery_date = date( 'Y-m-d', $delivery_timestamp );

        if ( ( $args['disabled_days'] && in_array( $delivery_date, $args['disabled_days'], true ) ) ||
            wc_od_is_disabled_day( $delivery_date, $args['disabled_days_args'], $context ) ) {
            $valid = false;
        }
    }

    return apply_filters( 'wc_od_validate_delivery_date', $valid, $delivery_timestamp, $args, $context );
}

function wcss_localize_date( $date, $format = null ) {
    $timestamp = wcss_get_timestamp( $date );
    $date_i18n = null;

    if ( false !== $timestamp ) {
        if ( ! $format ) {
            $format = wcss_get_date_format( 'php' );
        }

        $date_i18n = date_i18n( $format, $timestamp );
    }

    return $date_i18n;
}

function wcss_get_timestamp( $date ) {
    if ( wcss_is_timestamp( $date ) ) {
        return (int) $date;
    }

    // Disambiguate the m/d/Y and d/m/Y formats. (DateTime::createFromFormat was added on PHP 5.3).
    if ( 'd/m/Y' === wc_od_get_date_format( 'php' ) ) {
        $date = str_replace( '/', '-', $date );
    }

    return strtotime( $date );
}

function wcss_is_timestamp( $timestamp ) {
    return ( is_numeric( $timestamp ) && (int) $timestamp == $timestamp );
}

function wcss_get_date_format( $context = 'php' ) {
    $use_wp_format = _x( 'yes', "Use the WordPress date format for this language? Set to 'no' to use a custom format", 'woocommerce-order-delivery' );
    $date_format   = get_option( 'date_format' );

    // Use the translated date format.
    if ( 'yes' !== $use_wp_format ) {
        $date_format = _x( 'Y-m-d', 'Custom PHP date format for this language', 'woocommerce-order-delivery' );
    }

    if ( 'js' === $context ) {
        // Convert the date format from PHP to JS. Keep this order to avoid the double conversion of some characters.
        $format_conversion = array(
            'd' => 'dd',
            'j' => 'd',
            'l' => 'DD',
            'F' => 'MM',
            'm' => 'mm',
            'n' => 'm',
            'y' => 'yy',
            'Y' => 'yyyy',
        );

        $date_format = str_replace( array_keys( $format_conversion ), array_values( $format_conversion ), $date_format );
    } elseif ( 'admin' === $context ) {
        // Use the same format as the 'date' column.
        $format = ( version_compare( WC()->version, '3.3', '<' ) ? get_option( 'date_format' ) : __( 'M j, Y', 'woocommerce' ) );

        /** This filter is documented in woocommerce/includes/admin/class-wc-admin-post-types.php up to WC 3.2 */
        /** This filter is documented in woocommerce/includes/admin/list-tables/class-wc-admin-list-table-orders.php for WC 3.3+ */
        $date_format = apply_filters( 'woocommerce_admin_order_date_format', $format );
    }

    /**
     * Filter the date format.
     *
     * @since 1.2.0
     *
     * @param string $date_format The date format.
     * @param string $context     The context [php, js, admin].
     */
    return $date_format;
}

//Add settings in Woocommerce -> Settings -> Subscriptions
add_filter('woocommerce_subscription_settings', 'wcss_skip_subscription_on_off', 10, 1);
function wcss_skip_subscription_on_off($settings){
    $settings[] =
        array(
            'name'     => __( 'Enable Skip Subscriptions Feature:', 'wcss' ),
            'type'     => 'title',
            'desc'     => '',
            'id'       => 'wcss_skip_subscription_section',
        );

    $settings[] = array(
        'name'        => __( 'Enable', 'wcss' ),
        'desc'        => __( '<br><br>', 'wcss' ),
        'tip'         => '',
        'id'          => 'wcss_skip_subscription',
        'default'     => __( '', 'wcss' ),
        'type'        => 'checkbox',
        'desc_tip'    => false,
    );
    $settings[] = array( 'type' => 'sectionend', 'id' => 'wcss_skip_subscription_section' );

    return $settings;
}