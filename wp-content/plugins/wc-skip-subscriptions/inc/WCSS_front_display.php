<?php
/**
 * Created by PhpStorm.
 * User: Surfer
 */
class WCSS_front_display
{
    public function __construct(){
        $wcss_enable = get_option('wcss_skip_subscription');
        if($wcss_enable == 'yes'){

            add_action('init', array($this, 'add_endpoints'));
            add_filter('query_vars', array($this, 'query_vars'));
            //add_action( 'wp_enqueue_scripts', array( $this, 'enqueue_scripts' ) );

            add_filter('wcs_view_subscription_actions', array($this, 'view_subscription_actions'), 10, 2);
            add_action('woocommerce_after_template_part', array($this, 'insert_delivery_content'));

            // Skip Delivery hooks.
            add_filter('the_title', array($this, 'skip_delivery_title'));
            add_filter('wc_get_template', array($this, 'skip_delivery_template'), 10, 4);
            add_action('woocommerce_account_skip-delivery_endpoint', array($this, 'skip_delivery_content'));
            add_action('template_redirect', array($this, 'save_delivery'));
            add_filter('woocommerce_form_field_wcss_subscription_section_start_field', 'wcss_subscription_section_start_field', 10, 3);
            add_filter('woocommerce_form_field_wcss_subscription_section_end_field', 'wcss_subscription_section_end_field');

            add_filter('wcss_validate_subscription_field_skip_subscription_dates', array($this, 'validate_skip_subscription_dates'), 10, 3);
            add_filter('wcss_sanitize_subscription_field_skip_subscription_dates', array($this, 'sanitize_skip_subscription_dates'));
            add_action('wcss_updated_subscription_fields', array($this, 'updated_skip_subscription_dates'), 10, 3);

            //add_action('woocommerce_subscription_renewal_payment_complete', array($this, 'update_subscription_payment_to_next_delivery_date'), 10, 2);
            add_action('wc_od_subscription_delivery_date_updated', array($this, 'update_subscription_payment_to_next_delivery_date'), 10, 2);
            //add_filter('wc_od_update_subscription_delivery_date', array($this, 'update_subscription_payment_to_next_delivery_date'), 10, 2);
        }
    }

    public function add_endpoints() {
        add_rewrite_endpoint( 'skip-delivery', EP_ROOT | EP_PAGES );
    }

    public function query_vars( $vars ) {
        $vars[] = 'skip-delivery';

        return $vars;
    }

    public function view_subscription_actions( $actions, $subscription ) {
        if ( wc_od_subscription_has_delivery_preferences( $subscription ) ) {
            $actions['skip_delivery'] = array(
                'url'  => wc_skip_delivery_endpoint( wc_od_get_order_prop( $subscription, 'id' ) ),
                'name' => __( 'Skip Delivery', 'woocommerce-order-delivery' ),
            );
        }

        return $actions;
    }

    public function insert_delivery_content( $template_name ) {
        if ( 'myaccount/view-subscription.php' === $template_name ||
            ( 'myaccount/my-account.php' === $template_name && version_compare( WC()->version, '2.6', '<' ) &&
                wcs_is_view_subscription_page()
            )
        ) {
            $this->view_subscription_delivery_content();
        }
    }

    public function view_subscription_delivery_content() {
        $subscription_id = intval( wcss_get_current_subscription_id() );
        $subscription    = wcs_get_subscription( $subscription_id );

        if ( ! $subscription || ! wc_od_user_has_subscription_delivery_caps( $subscription ) ||
            ! wc_od_subscription_needs_delivery_details( $subscription ) ) {
            return;
        }

        $args = array(
            'subscription' => $subscription,
        );

        $delivery_date = false;

        if ( wc_od_subscription_needs_delivery_date( $subscription ) ) {
            $delivery_date = wc_od_get_order_meta( $subscription, '_delivery_date' );

            if ( $delivery_date ) {
                $args['delivery_date'] = wc_od_localize_date( $delivery_date );

                $time_frame = wc_od_get_order_meta( $subscription, '_delivery_time_frame' );

                if ( $time_frame ) {
                    $args['delivery_time_frame'] = wc_od_get_time_frame_for_date( $delivery_date, $time_frame );
                }
            }
        }

        if ( ! $delivery_date ) {
            $args = array_merge(
                $args,
                array(
                    'shipping_date'  => wc_od_localize_date( wc_od_get_subscription_first_shipping_date( $subscription_id ) ),
                    'delivery_range' => WC_OD()->settings()->get_setting( 'delivery_range' ),
                )
            );
        }

        wc_od_order_delivery_details( $args );
    }

    public function skip_delivery_title( $title ) {
        if ( ! is_admin() && is_main_query() && in_the_loop() && wc_is_skip_delivery_endpoint() ) {
            $title = sprintf(
            /* translators: %s: subscription ID. */
                esc_html_x( 'Subscription delivery #%s', 'edit subscription delivery title', 'wcss' ),
                wcss_get_current_subscription_id()
            );

            remove_filter( 'the_title', array( $this, 'skip_delivery_title' ) );
        }

        return $title;
    }

    public function skip_delivery_template( $located, $template_name, $args, $template_path ) {
        if ( 'myaccount/my-account.php' == $template_name && wc_is_skip_delivery_endpoint() && version_compare( WC()->version, '2.6', '<' ) ) {
            $located = wc_locate_template( 'myaccount/skip-delivery.php', $template_path, WCSS_PLUGIN_DIR . 'templates/' );
        }

        return $located;
    }

    public function wcss_get_template( $template_name, $args = array() ) {
        wc_get_template( $template_name, $args, WC()->template_path(), WCSS_PLUGIN_DIR . 'templates/' );
    }

    public function skip_delivery_content() {
        $subscription_id = wcss_get_current_subscription_id();

        $args = array(
            'subscription' => wcs_get_subscription( $subscription_id ),
        );

        $this->wcss_get_template( 'myaccount/skip-delivery.php', $args );
    }

    public function save_delivery() {
        if ('POST' !== strtoupper( $_SERVER['REQUEST_METHOD'] ) || empty( $_POST['action'] ) || 'skip_delivery' !== $_POST['action'] ||
            empty( $_POST['subscription_id'] ) ||
            empty( $_POST['_wpnonce'] ) || ! wp_verify_nonce( $_POST['_wpnonce'], 'wcss_skip_delivery' )
        || is_admin()) {
            return;
        }
        if('skip_delivery' == $_POST['action']) {
            //unset($_POST['action']);
            //wcss_pr($_POST);
        }

        $subscription = wcs_get_subscription( intval( $_POST['subscription_id'] ) );

        //$subscription->update_dates( array( 'next_payment' => '2020-04-20 00:00:00' ));

        if ( ! $subscription ) {
            return;
        }

        //

        $fields = wc_get_skip_subscription_fields( $subscription );
        $values = array();
        $valid  = true;
        //wcss_pr($_POST);
        //$values['skip_subscription_dates'] = array();
        $dates_array = array();
        foreach ( $fields as $key => $field ) {
            $value = null;

            // Ignore section fields.
            if ( in_array( $field['type'], array( 'wcss_subscription_section_start_field', 'wcss_subscription_section_end_field' ), true ) ) {
                unset( $fields[ $key ] ); // Remove it from future loops.
                continue;
            }

            // Validate required.
            if ( ! empty( $field['required'] ) && $field['required'] && empty( $_POST[ $key ] ) ) {
                /* translators: %s: field name */
                wc_add_notice( sprintf( __( '%s is a required field.', 'wcss' ), '<strong>' . esc_html( $field['label'] ) . '</strong>' ), 'error' );
                $valid = false;
            } else {
                if(isset($_POST[ $key ])) {
                    $value = wc_clean(wp_unslash($field['title'])); // WPCS: sanitization ok.
                    //var_dump($value);
                }else{
                    $value = '';
                }
                $value = apply_filters( "wcss_validate_subscription_field_{$key}", $value, $field, $subscription );

                if ( is_null( $value ) ) {
                    $valid = false;
                }
            }
            //$values['skip_subscription_dates'][$key] = $value;
            $values[ $key ] = $value;
            if($value){
                $dates_array[ $key ] = strtotime($value);
            }

        }

        if ( ! $valid ) {
            $values = null;
        }


        if ( ! is_null( $values ) ) {
            $previous_values = array();

            // Sanitize and save the fields.
            foreach ( $fields as $key => $field ) {
                $previous_values[ $key ] = wcss_get_order_meta( $subscription, "_{$key}" );

                //$values[ $key ] = apply_filters( "wcss_sanitize_subscription_field_{$key}", $values[ $key ], $field, $subscription );
                //$values[ $key ] =  $values[ $key ];

                if ( empty( $values[ $key ] ) ) {
                    wcss_delete_order_meta( $subscription, "_{$key}", true );
                } else {
                    wcss_update_order_meta( $subscription, "_{$key}", $values[$key], true );
                }
            }

            wcss_update_order_meta( $subscription, "_skip_subscription_dates", $dates_array, true );

            do_action( 'wcss_updated_subscription_fields', $values, $previous_values, $subscription );

            wc_add_notice( __( 'Skip subscription preferences changed successfully.', 'wcss' ) );

            wp_safe_redirect( $subscription->get_view_order_url() );
            exit;
        }
    }

    public function update_subscription_payment_to_next_delivery_date($delivery_timestamp, $subscription){
        //$fields = wc_get_skip_subscription_fields( $subscription );

        $_next_payment = wcss_get_order_meta($subscription->get_id(), '_schedule_next_payment', true);

        $_next_payment_time = strtotime($_next_payment);

        $_skip_delivery_dates = wcss_get_order_meta($subscription->get_id(), '_skip_subscription_dates', true);
        $delivery_d = (wcss_get_order_meta($subscription->get_id(), '_delivery_date', true));
        $delivery_timestamp = strtotime($delivery_d);
        //wcss_pr($_skip_delivery_dates);

        if($_skip_delivery_dates) {
            $skip_delivery_dates = array();
            foreach ($_skip_delivery_dates as $k => $v) {
                $skip_delivery_dates[$k] = strtotime(date('Y-m-d', $v));
            }

            $delivery_timestamp_next_week = $delivery_timestamp + (WEEK_IN_SECONDS);

            while (1 == 1) {
                if (in_array($delivery_timestamp_next_week, $skip_delivery_dates)) {
                    $delivery_timestamp_next_week = $delivery_timestamp_next_week + (WEEK_IN_SECONDS);
                } else {
                    break;
                }
            }

            if (($delivery_timestamp_next_week - (WEEK_IN_SECONDS)) !== $delivery_timestamp) {
                $_next_payment = wcss_get_order_meta($subscription->get_id(), '_schedule_next_payment', true);
                $_next_payment_time = date('H:i:s',strtotime($_next_payment));
                $_next_payment_date = date('Y-m-d',$delivery_timestamp_next_week);

                $_next_payment_datetime = strtotime($_next_payment_date.' '. $_next_payment_time);

                $subscription->update_dates(array('next_payment' => date('Y-m-d H:i:s', $_next_payment_datetime - WEEK_IN_SECONDS)));
            }
        }
    }

    public function validate_skip_subscription_dates( $value, $field, $subscription ) {
        if ( is_null( $value ) || ! wcss_validate_skip_subscription_date( $subscription, $value ) ) {
            $value = null;

            /* translators: %s: field name */
            wc_add_notice( sprintf( __( '%s is not valid.', 'wcss' ), '<strong>' . esc_html( $field['label'] ) . '</strong>' ), 'error' );
        }

        return $value;
    }

    public function sanitize_skip_subscription_dates( $value ) {
        // Stores the date in the ISO 8601 format.
        return (string) wcss_localize_date( sanitize_text_field( $value ), 'Y-m-d' );
    }

    public function updated_skip_subscription_dates( $values, $previous, $subscription ) {

        if ( $values['skip_subscription_dates'] !== $previous['skip_subscription_dates'] ) {
            //$delivery_details = wcss_localize_date( $values['skip_subscription_dates'] );
            $delivery_details = '';

            // Adds an internal note to the subscription to notify to the merchant.
            wcss_add_order_note(
                $subscription,
                sprintf(
                /* translators: %s: delivery details */
                    __( 'The customer skipped delivery for the next order to: %s', 'wcss' ),
                    "<strong>{$delivery_details}</strong>"
                )
            );
        }
    }

}

new WCSS_front_display();