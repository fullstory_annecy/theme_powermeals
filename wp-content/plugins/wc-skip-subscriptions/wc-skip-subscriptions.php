<?php
/*
Plugin Name: WC Skip Subscriptions
Plugin URI: http://logicfire.in
Description: Enable to skip subscriptions
Version: 1.0
Author: Logicfire
Author URI: http://logicfire.in
License: GPLv2 or later
Text Domain: wcss
*/

// Make sure we don't expose any info if called directly
if ( !function_exists( 'add_action' ) ) {
    echo 'Hi there!  I\'m just a plugin, not much I can do when called directly.';
    exit;
}

define( 'WCSS_VERSION', '1.0' );
define( 'WCSS_MINIMUM_WP_VERSION', '4.0' );
define( 'WCSS_PLUGIN_DIR', plugin_dir_path( __FILE__ ) );
define( 'WCSS_PLUGIN_URL', plugin_dir_url( __FILE__  ) );
define( 'WCSS_PLUGIN_ASSETS', plugin_dir_url( __FILE__  ).'assets/');

require_once( WCSS_PLUGIN_DIR . 'inc/WCSS_functions.php' );
require_once( WCSS_PLUGIN_DIR . 'inc/WCSS_front_display.php' );
//require_once( WCSS_PLUGIN_DIR . 'admin/WCSS_Admin.php' );

add_action('wp_enqueue_scripts', 'wcss_initialize_scripts');
function wcss_initialize_scripts(){
    wp_register_style('wcss-css', WCSS_PLUGIN_ASSETS. 'css/style.css');
    wp_enqueue_style( 'wcss-css' );

    wp_enqueue_script( 'jquery-ui-datepicker' );
    //wp_enqueue_script('parsley-js', WCSS_PLUGIN_ASSETS. 'js/parsley.min.js', array('jquery'), WCSS_VERSION);
    wp_enqueue_script('wcss-js', WCSS_PLUGIN_ASSETS. 'js/functions.js', array('jquery'), WCSS_VERSION);
    wp_localize_script('wcss-js', 'wcss', array('ajaxurl' => admin_url('admin-ajax.php'), 'homeurl' => home_url('/'), 'message' => __('') ));
}

function wcss_pr($obj){
    echo '<pre>';
    print_r($obj);
    echo '</pre>';
}