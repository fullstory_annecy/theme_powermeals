<?php
/**
 * My Account skip-delivery
 */

defined( 'ABSPATH' ) || exit;

/**
 * Global variables.
 *
 * @global WC_Subscription $subscription
 */

if ( ! $subscription || ! wcss_user_has_subscription_skip_caps( $subscription ) ) {
	$notice = sprintf(
		wp_kses(
			__( 'Invalid Subscription. <a href="%s" class="wc-forward">My Account</a>', 'wcss' ),
			array( 'a' => array( 'href' => array(), 'class' => array() ) )
		),
		esc_url( wc_get_page_permalink( 'myaccount' ) )
	);

	wc_print_notice( $notice, 'error' );
	return;
}

$fields = wc_get_skip_subscription_fields( $subscription );
?>
<style>
	.optional{
		display: none;
	}
</style>
<div class="wc-skip-subscription-delivery">
	<p>
		<?php
		echo wp_kses_post(
			sprintf(
				/* translators: %s: view subscription URL */
				__( 'Here you can select dates to skip subscription: %s', 'wcss' ),
				sprintf( '<a href="%1$s">#%2$s</a>', esc_url( $subscription->get_view_order_url() ), esc_html( $subscription->get_id() ) )
			)
		);
		?>
	</p>

	<?php if ( empty( $fields ) ) : ?>

		<p><?php esc_html_e( 'There is no preferences for this subscription.', 'wcss' ); ?></p>

	<?php else : ?>

		<form method="post">

			<?php foreach ( $fields as $key => $field ) : ?>

				<?php woocommerce_form_field( $key, $field, wcss_get_skip_subscription_delivery_field_value( $subscription, $key ) ); ?>

			<?php endforeach; ?>

			<p>
				<?php wp_nonce_field( 'wcss_skip_delivery' ); ?>
				<input type="hidden" name="action" value="skip_delivery" />
				<input type="hidden" name="subscription_id" id="subscription_id" value="<?php echo esc_attr( $subscription->get_id() ); ?>" />
				<input type="submit" class="button" name="save_delivery" value="<?php esc_attr_e( 'Save Preferences', 'wcss' ); ?>" />
			</p>
		</form>

	<?php endif; ?>
</div>
