<?php
/**
 * Created by PhpStorm.
 * User: Surfer
 */

if ( ! defined( 'WPINC' ) ) {
    die;
}

class WCSS_Admin
{
    /**
     * Holds the values to be used in the fields callbacks
     */
    private $options;

    public function __construct(){
        add_action( 'admin_menu', array( $this, 'add_plugin_page' ) );
        add_action( 'admin_init', array( $this, 'page_init' ) );
    }

    /**
     * Add options page
     */
    public function add_plugin_page()
    {
        // This page will be under "Settings"
        add_options_page(
            'Skip Subscriptions',
            'Skip Subscriptions',
            'manage_options',
            'wcss-settings',
            array( $this, 'create_admin_page' )
        );
    }

    /**
     * Options page callback
     */
    public function create_admin_page()
    {
        // Set class property
        $this->options = get_option( 'wcss_options' );
        ?>
        <div class="wrap">
            <h1><?php _e('Skip Subscription Settings', 'wcss');?></h1>
            <form method="post" action="options.php">
                <?php
                // This prints out all hidden setting fields
                settings_fields( 'wcss_settings' );
                do_settings_sections( 'wcss-settings' );

                submit_button();
                ?>
            </form>
        </div>
        <?php
    }

    /**
     * Register and add settings
     */
    public function page_init()
    {

        register_setting(
            'wcss_settings', // Option group
            'wcss_options', // Option name
            array( $this, 'sanitize' ) // Sanitize
        );

        add_settings_section(
            'wcss_general_settings', // ID
            __('General Settings', 'wcss'), // Title
            array( $this, 'print_section_info_gs' ), // Callback
            'wcss-settings' // Page
        );

        add_settings_field(
            'wcss_enable', // ID
            'Enable', // Title
            array( $this, 'wcss_enable_callback' ), // Callback
            'wcss-settings', // Page
            'wcss_general_settings' // Section
        );

    }

    /**
     * Sanitize each setting field as needed
     *
     * @param array $input Contains all settings fields as array keys
     */
    public function sanitize( $input )
    {
        $new_input = array();
        //if( isset( $input['wcss_enable'] ) )
            //$input['wcss_enable'] = ( $input['wcss_enable'] );

        return $input;
    }

    /**
     * Print the Section text
     */
    public function print_section_info_gs()
    {
        print '';
        //var_dump($this->options);
    }

    /**
     * Get the settings option array and print one of its values
     */
    public function wcss_enable_callback(){
        printf(
            '<input type="checkbox" id="wcss_enable" name="wcss_enable" value="%s" />',
            isset( $this->options['wcss_enable'] ) ? esc_attr( $this->options['wcss_enable']) : ''
        );
    }


    public function wcss_claim_rates_callback(){}

}

if( is_admin() ) $wcss_admin = new WCSS_Admin();