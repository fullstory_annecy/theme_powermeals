<?php
add_filter( 'wc_shipment_tracking_get_providers', 'mame_shipment_tracking_add_custom_provider' );

if ( !function_exists( 'mame_shipment_tracking_add_custom_provider' ) ) {
    function mame_shipment_tracking_add_custom_provider( $providers )
    {
        $providers[ 'Switzerland' ][ 'Swiss Post' ] = 'https://www.post.ch/swisspost-tracking?formattedParcelCodes=%1$s';

        return $providers;
    }
}