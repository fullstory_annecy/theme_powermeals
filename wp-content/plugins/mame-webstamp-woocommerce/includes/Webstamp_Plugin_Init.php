<?php

if ( !defined( 'ABSPATH' ) ) {
    exit; // Exit if accessed directly
}

if ( !class_exists( 'Webstamp_Plugin_Init' ) ) :

    /**
     * Class for initializing the plugin.
     */
    class Webstamp_Plugin_Init
    {

        /**
         * Datatrans_Plugin_Init constructor.
         */
        public function __construct()
        {
            // Admin scripts and styles.
            add_action( 'admin_enqueue_scripts', array( $this, 'enqueue_admin_scripts' ) );

            // Frontend scripts and styles.
//            add_action( 'wp_enqueue_scripts', array( $this, 'enqueue_frontend_scripts' ) );

            add_action( 'plugins_loaded', array( $this, 'check_plugin_update' ) );

            // Add weekly cron schedule.
            add_filter( 'cron_schedules', array( $this, 'add_weekly_cron_schedule' ) );

            // Add settings link to plugins page.
            add_filter( 'plugin_action_links_mame-webstamp-woocommerce/mame-webstamp-woocommerce.php', array( $this, 'add_action_links' ) );

            // File download/print
            add_action( 'init', array( $this, 'download_file' ) );
            add_action( 'init', array( $this, 'print_file' ) );

            // Shortcodes
            add_action( 'init', array( $this, 'add_shortcodes' ) );

            // admin notices
            add_action( 'admin_notices', array( 'Webstamp_Template', 'saved_success' ) );
        }

        function check_plugin_update()
        {
            $db_version = is_multisite() ? get_blog_option( 1, MAME_WS_PREFIX . '_db_version' ) : get_option( MAME_WS_PREFIX . '_db_version' );
            if ( MAME_WS_DB_VERSION != $db_version ) {
                // v1.1
                global $wpdb;
                $charset_collate = $wpdb->get_charset_collate();
                $table_template  = Webstamp_Database_Manager::table( Webstamp_Database_Manager::TEMPLATE );
                $sql             = "CREATE TABLE $table_template (
              id int NOT NULL AUTO_INCREMENT,
              name varchar(255),
              product_number int,
              post_product_number bigint(20),
              category smallint,
              zone smallint,
              delivery_method varchar(255),
              format varchar(255),
              additions text,  
              media_type smallint,
              media smallint,
              license_number varchar(40),
              quantity smallint,
              use_address tinyint(1),
              load_wc_address enum('shipping', 'billing', 'custom') NULL default NULL,
              use_sender_address tinyint(1),
              sender_id int,
              media_startpos tinyint,
              image_id int,
              time int(11),
              PRIMARY KEY  (id)
            ) $charset_collate;";


                require_once( ABSPATH . 'wp-admin/includes/upgrade.php' );
                $result = dbDelta( $sql );

                $result = $wpdb->query( "ALTER TABLE  $table_template CHANGE  `load_wc_address`  `load_wc_address` enum('shipping', 'billing', 'custom')"
                );

                // Save db version.
                update_option( MAME_WS_PREFIX . '_db_version', MAME_WS_DB_VERSION );
            }
        }

        /**
         * Checks the request query args and sends the protected file.
         */
        function download_file()
        {
            if ( current_user_can( 'manage_woocommerce' ) ) {
                if ( isset( $_GET[ MAME_WS_PREFIX . "-download" ] ) && isset( $_GET[ MAME_WS_PREFIX . "-type" ] ) && isset( $_GET[ MAME_WS_PREFIX . "-file" ] ) ) {
                    Webstamp_File_Manager::send_file( $_GET[ MAME_WS_PREFIX . "-file" ], $_GET[ MAME_WS_PREFIX . "-type" ] );
                }
            }
        }

        /**
         * Opens the print screen of a file.
         */
        function print_file()
        {
            if ( current_user_can( 'manage_woocommerce' ) ) {
                if ( isset( $_GET[ MAME_WS_PREFIX . "-print" ] ) && isset( $_GET[ MAME_WS_PREFIX . "-type" ] ) && isset( $_GET[ MAME_WS_PREFIX . "-file" ] ) ) {

                    $options = get_option( 'mame_ws_options_group' );
                    $link    = add_query_arg( [ MAME_WS_PREFIX . '-type' => $_GET[ MAME_WS_PREFIX . '-type' ], MAME_WS_PREFIX . '-file' => $_GET[ MAME_WS_PREFIX . "-file" ], MAME_WS_PREFIX . '-download' => '1' ], get_admin_url() );

                    if ( $options && isset( $options[ 'stamp_file_type' ] ) && $options[ 'stamp_file_type' ] === 'pdf' ) {
                        Webstamp_File_Manager::display_pdf( $_GET[ MAME_WS_PREFIX . "-file" ], $_GET[ MAME_WS_PREFIX . '-type' ] );
                        die();
                    }
                    ob_clean();
                    ?>
                    <!doctype html>
                    <html>
                    <head>
                    </head>
                    <body>

                    <img src="<?= $link ?>">
                    <script>
                        window.onload = function () {
                            window.print();
                        }
                    </script>
                    </body>
                    </html>
                    <?php
                    die();
                }
            }
        }

        /**
         * Adds shortcodes.
         */
        function add_shortcodes()
        {
            add_shortcode( MAME_WS_PREFIX . '_stamp_image', array( $this, 'stamp_image_shortcode' ) );
        }

        /**
         * Shortcode function to print image files of a WC order.
         * @param $attributes
         * @param null $content
         * @param string $tag
         * @return string
         */
        function stamp_image_shortcode( $attributes, $content = null, $tag = '' )
        {
            $id = get_the_ID();
            if ( $id ) {
                $wc_order = new WC_Order( $id );
                if ( $wc_order ) {

                    $attributes = array_change_key_case( (array)$attributes, CASE_LOWER );

                    $atts = shortcode_atts( [
                        'width'   => false,
                        'height'  => false,
                        'newline' => false,
                    ], $attributes, $tag );

                    return Webstamp_Order::get_stamp_images_from_woocommerce_order( $wc_order, esc_html( $atts[ 'width' ] ), esc_html( $atts[ 'height' ] ), esc_html( $atts[ 'newline' ] ) );
                }
            }
            return '';
        }

        /**
         * Executed when plugin is deactivated.
         */
        public
        function deactivate_plugin()
        {
            //wp_clear_scheduled_hook( 'mame_ws_check_license' );

            // Don't ignore the license notice.
            global $current_user;
            $user_id = $current_user->ID;
            delete_user_meta( $user_id, 'mame_ws_notice_ignore' );
        }

        function add_weekly_cron_schedule( $schedules )
        {
            $schedules[ MAME_WS_PREFIX . '-weekly' ] = array(
                'interval' => 60 * 60 * 24 * 7, # 604,800, seconds in a week
                'display'  => __( 'Weekly' )
            );
            return $schedules;
        }

        /**
         * Load admin styles and scripts (only on order admin pages)
         */
        public
        function enqueue_admin_scripts()
        {
            // CSS for admin screen.
            $current_screen = get_current_screen();
            //if ( ( $current_screen->post_type == 'shop_order' ) || $current_screen->base == 'woocommerce_page_wc-settings' ) {
            wp_enqueue_style( 'mame_ws_admin_style', plugin_dir_url( dirname( __FILE__ ) ) . 'assets/css/admin-style.css' );
            // }

            // Editor JS.
            wp_register_script( 'mame_ws_editor', plugin_dir_url( dirname( __FILE__ ) ) . '/assets/js/ws-editor.js', 'jquery' );
            wp_localize_script( 'mame_ws_editor', 'mameWsEditorPhpVars', array(
                'ajaxUrl'                => get_bloginfo( 'wpurl' ) . '/wp-admin/admin-ajax.php',
                'ajaxEditorNonce'        => wp_create_nonce( 'mame_ws_editor_nonce' ),
                'prefix'                 => MAME_WS_PREFIX,
                'textSettle'             => __( 'Settle the authorized transaction?', 'dhuett' ),
                'textSettleSuccess'      => __( 'Settlement successful', 'dhuett' ),
                'textSettleError'        => __( 'Error in settlement', 'dhuett' ),
                'textCancel'             => __( 'Cancel the transaction?', 'dhuett' ),
                'textCancelSuccess'      => __( 'Cancellation successful', 'dhuett' ),
                'textCancelError'        => __( 'Error in cancellation', 'dhuett' ),
                'textStatusSuccess'      => __( 'Transaction status updated', 'dhuett' ),
                'textStatusError'        => __( 'Error in status request', 'dhuett' ),
                'textOrderConfirmTitle'  => __( 'Order the following product?', 'dhuett' ),
                'textOrderConfirmButton' => __( 'Order', 'dhuett' ),
                'textCancel'             => __( 'Cancel', 'dhuett' ),
            ) );
            wp_enqueue_script( 'mame_ws_editor' );

            if ( isset( $_GET[ 'page' ] ) && substr( $_GET[ 'page' ], 0, strlen( MAME_WS_PREFIX ) ) === MAME_WS_PREFIX ) {

                wp_enqueue_media();
                wp_enqueue_script( MAME_WS_PREFIX . '-settings', plugin_dir_url( dirname( __FILE__ ) ) . '/assets/js/settings.js', [ 'jquery' ] );
                // Admin JS.
                wp_register_script( 'mame_ws_backend', plugin_dir_url( dirname( __FILE__ ) ) . '/assets/js/backend.js', 'jquery' );
                wp_localize_script( 'mame_ws_backend', 'mameWsPhpVars', array(
                    'ajaxUrl'             => get_bloginfo( 'wpurl' ) . '/wp-admin/admin-ajax.php',
                    'ajaxSoapNonce'       => wp_create_nonce( MAME_WS_PREFIX . '_nonce' ),
                    'ajaxNonce'           => wp_create_nonce( MAME_WS_PREFIX . '_license_nonce' ),
                    'prefix'              => MAME_WS_PREFIX,
                    'textSettle'          => __( 'Settle the authorized transaction?', 'dhuett' ),
                    'textSettleSuccess'   => __( 'Settlement successful', 'dhuett' ),
                    'textSettleError'     => __( 'Error in settlement', 'dhuett' ),
                    'textCancel'          => __( 'Cancel the transaction?', 'dhuett' ),
                    'textCancelSuccess'   => __( 'Cancellation successful', 'dhuett' ),
                    'textCancelError'     => __( 'Error in cancellation', 'dhuett' ),
                    'textStatusSuccess'   => __( 'Transaction status updated', 'dhuett' ),
                    'textStatusError'     => __( 'Error in status request', 'dhuett' ),
                    'textDataLoadSuccess' => __( 'WebStamp data updated', 'dhuett' ),
                    'textDataLoadError'   => __( 'Error in WebStamp response', 'dhuett' ),
                ) );
                wp_enqueue_script( 'mame_ws_backend' );

            }
        }

        /**
         * Load frontend scripts.
         */
        public
        function enqueue_frontend_scripts()
        {
        }

        /**
         * Add settings action links to plugins page
         *
         * @param $links
         *
         * @return array
         */
        public
        function add_action_links( $links )
        {
            $plugin_links = array( '<a href="' . admin_url( 'admin.php?page=mame_ws_menu_settings' ) . '">' . __( 'Settings', 'dhuett' ) . '</a>', );
            return array_merge( $plugin_links, $links );
        }

    }

endif;
