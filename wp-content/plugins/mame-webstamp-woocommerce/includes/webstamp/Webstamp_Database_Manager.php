<?php

if ( !defined( 'ABSPATH' ) ) {
    exit;
}

if ( !class_exists( 'Webstamp_Database_Manager' ) ) {

    /**
     * Class Webstamp_Database_Manager
     */
    class Webstamp_Database_Manager
    {
        // Option
        const CATEGORY               = '_category';
        const PRODUCT                = '_product';
        const MEDIA_TYPE             = '_media_type';
        const MEDIA                  = '_media';
        const ZONE                   = '_zone';
        const COUNTRY                = '_country';
        const LICENSE                = '_license';
        const PRODUCT_LIST           = '_product_list';
        const CUSTOMER_DATA          = '_customer_data';
        const CATEGORY_TO_MEDIA_TYPE = '_category_to_media_type';
        const ADDITION               = '_addition';
        const CUSTOM_MEDIA           = '_custom_media';

        // Order
        const ORDER       = '_order';
        const STAMP       = '_stamp';
        const MESSAGE     = '_message';
        const ORDER_ITEM  = '_order_item';
        const ORDER_FILES = '_order_files';
        const ADDRESS     = '_address';

        // Other
        const TEMPLATE = '_template';

        // Hardcoded db values since there are no relations in soap response.
        const ZONE_EUROPE       = 1;
        const ZONE_WORLD        = 2;
        const ZONE_DOMESTIC     = 3;
        const CATEGORY_DOMESTIC = 1;
        const LABEL_ID_1        = 1;
        const LABEL_ID_2        = 5;

        public static function table( $name )
        {
            global $wpdb;
            return $wpdb->prefix . MAME_WS_PREFIX . $name;
        }

        /**
         * Creates database tables and relations.
         */
        public static function setup()
        {
            global $wpdb;
            $charset_collate = $wpdb->get_charset_collate();

            // Option
            $table_category               = static::table( static::CATEGORY );
            $table_product                = static::table( static::PRODUCT );
            $table_media_type             = static::table( static::MEDIA_TYPE );
            $table_media                  = static::table( static::MEDIA );
            $table_zone                   = static::table( static::ZONE );
            $table_country                = static::table( static::COUNTRY );
            $table_license                = static::table( static::LICENSE );
            $table_product_list           = static::table( static::PRODUCT_LIST );
            $table_customer_data          = static::table( static::CUSTOMER_DATA );
            $table_category_to_media_type = static::table( static::CATEGORY_TO_MEDIA_TYPE );
            $table_addition               = static::table( static::ADDITION );
            $table_custom_media           = static::table( static::CUSTOM_MEDIA );

            // Order
            $table_order       = static::table( static::ORDER );
            $table_stamp       = static::table( static::STAMP );
            $table_message     = static::table( static::MESSAGE );
            $table_order_item  = static::table( static::ORDER_ITEM );
            $table_order_files = static::table( static::ORDER_FILES );
            $table_address     = static::table( static::ADDRESS );

            // Other
            $table_template = static::table( static::TEMPLATE );

            // category
            $sql = "CREATE TABLE $table_category (
              number smallint NOT NULL,
              name varchar(255),
              recipient_mandatory tinyint(1),
              valid_days smallint,
              PRIMARY KEY  (number)
            ) $charset_collate;";

            // zone
            $sql .= "CREATE TABLE $table_zone (
              number smallint NOT NULL,
              name varchar(255),
              PRIMARY KEY  (number)
            ) $charset_collate;";

            // product_list
            $sql .= "CREATE TABLE $table_product_list (
              number smallint NOT NULL,
              name varchar(255),
              customer_type enum('pk','gk'),
              PRIMARY KEY  (number)
            ) $charset_collate;";

            // product
            $sql .= "CREATE TABLE $table_product (
              number int NOT NULL,
              category varchar(254),
              category_number smallint,
              post_product_number bigint(20),
              price float(6,2),
              name varchar(255),
              additions text,
              delivery varchar(255),
              format varchar (255),
              size_din varchar(20),
              max_size_length mediumint,
              max_size_height mediumint,
              max_edge_length mediumint,
              max_scale mediumint,
              max_weight mediumint,
              zone smallint,
              product_list varchar(255),
              product_list_number smallint,
              subsystem mediumint,
              segment mediumint,
              barcode tinyint(1),
              PRIMARY KEY  (number)
            ) $charset_collate;";

            // media_type
            $sql .= "CREATE TABLE $table_media_type (
              number smallint NOT NULL,
              name varchar(255),
              image_possible tinyint(1),
              categories text,
              PRIMARY KEY  (number)
            ) $charset_collate;";

            // media
            $sql .= "CREATE TABLE $table_media (
              number smallint NOT NULL,
              name varchar(255),
              type smallint,
              PRIMARY KEY  (number)
            ) $charset_collate;";

            // country
            $sql .= "CREATE TABLE $table_country (
              alias varchar(10) NOT NULL,
              zone enum('1','2','3'),
              name varchar(255),
              PRIMARY KEY  (alias)
            ) $charset_collate;";

            // license
            $sql .= "CREATE TABLE $table_license (
              number varchar(40) NOT NULL,
              comment tinytext,
              subsystem mediumint,
              segment mediumint,
              PRIMARY KEY  (number)
            ) $charset_collate;";

            // category_to_media_type
            $sql .= "CREATE TABLE $table_category_to_media_type (
              category_number smallint,
              media_type_number smallint,
              PRIMARY KEY  (category_number, media_type_number)
            ) $charset_collate;";

            // addition
            $sql .= "CREATE TABLE $table_addition (
              product_number int,
              code varchar(255),
              short_name varchar(60),
              name varchar(255),
              alias varchar(20),
              optional tinyint(1),
              price float(6,2),
              add_price tinyint(1),
              PRIMARY KEY  (product_number, alias)
            ) $charset_collate;";

            // Custom media
            $sql .= "CREATE TABLE $table_custom_media (
              id int NOT NULL AUTO_INCREMENT,
              name varchar(60),
              type enum('label', 'envelope', 'letter', 'paper'),
              page_width mediumint,
              page_height mediumint,
              margin_top mediumint,
              margin_left mediumint,
              margin_right mediumint,
              cols tinyint,
              rows tinyint,
              colspacing mediumint,
              rowspacing mediumint,
              recipient_orientation enum('top/left', 'top/right', 'bottom/left', 'bottom/right'),
              recipient_x mediumint,
              recipient_y mediumint,
              sender_orientation enum('top/left', 'top/right', 'bottom/left', 'bottom/right'),
              sender_x mediumint,
              sender_y mediumint,
              franking_orientation enum('top/left', 'top/right', 'bottom/left', 'bottom/right'),
              franking_x mediumint,
              franking_y mediumint,           
              PRIMARY KEY  (id)
            ) $charset_collate;";

            // customer_data
            /*
            $sql .= "CREATE TABLE $table_customer_data (
              license_state enum('none','pending','rejected','ok') NOT NULL,
              payment_type enum('prepaid','kurepo'),
              product_lists text,
            ) $charset_collate;";
            */

            /*
             * Order
             */
            // order
            $sql .= "CREATE TABLE $table_order (
              id int NOT NULL AUTO_INCREMENT,
              order_id bigint(20) NOT NULL,
              reference varchar (60),
              price float(6,2),
              item_price float(6,2),
              valid_days smallint(2),
              valid_until int(11),
              product_number int,
              post_product_number bigint(20),
              delivery_receipt varchar(255),
              category smallint,
              zone smallint,
              delivery_method varchar(255),
              format varchar(255),
              additions text,
              address_id int,
              media_type smallint,
              media smallint,
              license_number varchar(40),
              quantity smallint,
              status enum('draft', 'processing', 'complete') DEFAULT NULL,
              wc_order_id int,
              use_address tinyint(1), 
              use_sender_address tinyint(1), 
              sender_id int,
              media_startpos tinyint,
              image_id int,
              order_comment varchar(255),
              time int(11),
              PRIMARY KEY  (id)
            ) $charset_collate;";

            $sql .= "CREATE TABLE $table_stamp (
              id int NOT NULL AUTO_INCREMENT,
              stamp_id bigint(20),
              order_id bigint(20),
              tracking_number varchar(255),
              file_name varchar (255),
              compression varchar(10),
              mime_type varchar(20),
              image_width_mm mediumint,
              image_height_mm mediumint,
              image_width_px mediumint,
              image_height_px mediumint,
              reference varchar(20),
              PRIMARY KEY  (id)
            ) $charset_collate;";

            $sql .= "CREATE TABLE $table_message (
              id int NOT NULL AUTO_INCREMENT,
              message_type enum('announcement', 'error', 'confirm'),
              order_id bigint(20),
              customer_message text,
              system_message text,
              system_message_code bigint(20),
              url varchar(512),
              confirm_until int,
              PRIMARY KEY  (id)
            ) $charset_collate;";

            $sql .= "CREATE TABLE $table_order_item (
              id int NOT NULL AUTO_INCREMENT,
              stamp_id bigint(20),
              label_number bigint(20),
              tracking_number varchar(255),
              PRIMARY KEY  (id)
            ) $charset_collate;";

            // Address
            $sql .= "CREATE TABLE $table_address (
              id int NOT NULL AUTO_INCREMENT,
              organization varchar(255),
              company_addition varchar(255),
              title varchar(255),
              firstname varchar(255),
              lastname varchar(255),
              addition varchar(255),
              street varchar(255),
              pobox varchar(255),
              pobox_lang varchar(255),
              pobox_nr varchar(255),
              zip varchar(255),
              city varchar(255),
              country varchar(255),
              reference varchar(255),
              cash_on_delivery_amount float(6,2),
              cash_on_delivery_esr varchar(255),
              PRIMARY KEY  (id)
            ) $charset_collate;";
            /*print_address text,*/

            $sql .= "CREATE TABLE $table_template (
              id int NOT NULL AUTO_INCREMENT,
              name varchar(255),
              product_number int,
              post_product_number bigint(20),
              category smallint,
              zone smallint,
              delivery_method varchar(255),
              format varchar(255),
              additions text,  
              media_type smallint,
              media smallint,
              license_number varchar(40),
              quantity smallint,
              use_address tinyint(1),
              load_wc_address enum('shipping', 'billing', 'custom') NULL default NULL,
              use_sender_address tinyint(1),
              sender_id int,
              media_startpos tinyint,
              image_id int,
              time int(11),
              PRIMARY KEY  (id)
            ) $charset_collate;";

            require_once( ABSPATH . 'wp-admin/includes/upgrade.php' );
            $result = dbDelta( $sql );

            // Save db version.
            update_option( MAME_WS_PREFIX . '_db_version', MAME_WS_DB_VERSION );
        }

        public static function get_categories()
        {
            return static::get_all_results( static::table( static::CATEGORY ) );
        }

        public static function get_products()
        {
            return static::get_all_results( static::table( static::PRODUCT ) );
        }

        public static function get_product_delivery_methods( $zone )
        {
            global $wpdb;

            $table = static::table( static::PRODUCT );
            return $wpdb->get_results(
                "
	            SELECT DISTINCT delivery
	            FROM $table
	            WHERE zone = $zone
	            "
            );
        }

        public static function get_product_formats( $zone, $delivery )
        {
            global $wpdb;

            $table = static::table( static::PRODUCT );
            return $wpdb->get_results(
                "
	            SELECT DISTINCT format, size_din, max_size_length, max_size_height, max_edge_length, max_scale, max_weight
	            FROM $table
	            WHERE zone = '$zone' and delivery = '$delivery'
	            "
            );
        }

        public static function get_product( $zone, $delivery, $format )
        {
            global $wpdb;

            $table = static::table( static::PRODUCT );
            return $wpdb->get_results(
                "
	            SELECT *
	            FROM $table
	            WHERE zone = '$zone' and delivery = '$delivery' and format = '$format'
	            "
            );
        }

        /**
         * Returns a single ws_product.
         *
         * @param $zone
         * @param $delivery
         * @param $format
         * @param $additions
         * @return Webstamp_Product
         */
        public static function get_unique_product( $zone, $delivery, $format, $additions )
        {

            global $wpdb;

            $table = static::table( static::PRODUCT );

            $query = "
	            SELECT *
	            FROM $table
	            WHERE zone = '$zone' and delivery = '$delivery' and format = '$format'
	            ";

            if ( !empty( $additions ) ) {
                foreach ( $additions as $a ) {
                    $query .= " and additions LIKE '%$a%'
                    ";
                }
            }

            $products = $wpdb->get_results( $query );
            if ( !empty( $products ) )
                return new Webstamp_Product( $products[ 0 ] );
            return null;
        }

        public static function get_product_additions( $zone, $delivery, $format )
        {
            global $wpdb;

            $table_product  = static::table( static::PRODUCT );
            $table_addition = static::table( static::ADDITION );

            $results = $wpdb->get_results(
                "
	            SELECT addition.product_number, addition.name,addition.short_name, addition.price
	            FROM $table_addition addition
	            WHERE EXISTS (SELECT NULL FROM $table_product product WHERE  product.zone = '$zone' and product.delivery = '$delivery' and product.format = '$format' and addition.product_number = product.number and product.additions LIKE CONCAT('%', addition.name, '%'))	        
	            GROUP BY addition.name, addition.price
	            "
            );

            // Remove duplicates by taking the addition with the highest price.
            $additions = array_reduce( $results, function ( $carry, $item ) {
                if ( !empty( $item->name ) && array_key_exists( $item->name, $carry ) ) {
                    if ( floatval( $item->price ) <= floatval( $carry[ $item->name ]->price ) )
                        return $carry;
                }
                if ( $item->name != null )
                    $carry[ $item->name ] = $item;
                return $carry;

            }, [] );

            return $additions;
        }

        public static function get_media_types_by_category( $category )
        {
            global $wpdb;

            $table_category_to_media_type = static::table( static::CATEGORY_TO_MEDIA_TYPE );
            $table_media_type             = static::table( static::MEDIA_TYPE );

            return $wpdb->get_results(
                "
	            SELECT $table_media_type.number, $table_media_type.name, $table_media_type.image_possible
	            FROM $table_media_type
	            INNER JOIN $table_category_to_media_type on $table_media_type.number = $table_category_to_media_type.media_type_number
	            GROUP BY $table_media_type.name
	            "
            );
        }

        public static function get_medias_by_media_type( $media_type )
        {
            global $wpdb;

            $table_media  = static::table( static::MEDIA );
            $custom_media = '72,73,74,76,120,121,122,123,140,147';
            return $wpdb->get_results(
                "
	            SELECT *
	            FROM $table_media
	            WHERE type = '$media_type' AND number NOT IN ($custom_media)
	            "
            );
        }

        public static function get_medias_by_media_types( $media_types )
        {

            global $wpdb;

            $table_media      = static::table( static::MEDIA );
            $table_media_type = static::table( static::MEDIA_TYPE );

            // Get media type id array
            $media_type_ids = array_map( function ( $i ) {
                return $i->number;
            }, $media_types );

            $media_type_ids_string = implode( ',', $media_type_ids );

            return $wpdb->get_results(
                "
	            SELECT *
	            FROM $table_media
	            WHERE type IN ($media_type_ids_string)
	            "
            );

        }

        public static function get_zones( $category = null )
        {
            if ( !$category )
                return static::get_all_results( static::table( static::ZONE ) );

            return static::get_all_where( static::table( static::ZONE ), 'category', $category );
        }

        public static function get_medias()
        {
            return static::get_all_results( static::table( static::MEDIA ) );
        }

        public static function get_media_types()
        {
            return static::get_all_results( static::table( static::MEDIA_TYPE ) );
        }

        public static function get_countries()
        {
            return static::get_all_results( static::table( static::COUNTRY ) );
        }

        public static function get_countries_by_zone( $zone )
        {
            global $wpdb;
            $table = static::table( static::COUNTRY );

            return $wpdb->get_results(
                "
	            SELECT * 
	            FROM $table
	            WHERE zone = '$zone'
	            "
            );
        }

        public static function get_licenses()
        {
            return static::get_all_results( static::table( static::LICENSE ) );
        }

        public static function get_licenses_by_subsystem( $subsystem )
        {
            return static::get_all_where( static::table( static::LICENSE ), 'subsystem', $subsystem );
        }

        public static function get_orders()
        {
            return static::get_all_results( static::table( static::ORDER ) );
        }

        /**
         * @param null $wc_order_id
         * @return null|Webstamp_Order
         */
        public static function get_draft_order( $wc_order_id = null )
        {
            global $wpdb;
            $table = static::table( static::ORDER );

            if ( $wc_order_id ) {
                $orders = $wpdb->get_results(
                    "
	            SELECT * 
	            FROM $table
	            WHERE status = 'draft' and wc_order_id = '$wc_order_id'
	            "
                );

            } else {
//                $orders = static::get_all_where( $table, 'status', 'draft' );
                $orders = $wpdb->get_results(
                    "
	            SELECT * 
	            FROM $table
	            WHERE status = 'draft' and ( wc_order_id IS NULL or wc_order_id = '0' or wc_order_id = 0 or wc_order_id = '' ) 
	            "
                );
            }

            if ( $orders )
                return new Webstamp_Order( $orders[ 0 ] );

            return null;
        }

        public static function get_address( $id )
        {
            if ( $id )
                return new Webstamp_Address( static::get_all_where( static::table( static::ADDRESS ), 'id', $id )[ 0 ] );
            return null;
        }

        /**
         * @param $number
         * @return null|Webstamp_Category
         */
        public static function get_category( $number )
        {
            if ( $number ) {
                $categories = static::get_all_where( static::table( static::CATEGORY ), 'number', $number );
                if ( !empty( $categories ) ) {
                    return new Webstamp_Category( $categories[ 0 ] );
                }
            }
            return null;
        }

        /**
         * @param $number
         * @return null|Webstamp_Zone
         */
        public static function get_zone( $number )
        {
            if ( $number ) {
                $zones = static::get_all_where( static::table( static::ZONE ), 'number', $number );
                if ( !empty( $zones ) ) {
                    return new Webstamp_Zone( $zones[ 0 ] );
                }
            }
            return null;
        }

        /**
         * @param $number
         * @return null|Webstamp_Media_Type
         */
        public static function get_media_type( $number )
        {
            if ( $number ) {
                $media_types = static::get_all_where( static::table( static::MEDIA_TYPE ), 'number', $number );
                if ( !empty( $media_types ) ) {
                    return new Webstamp_Media_Type( $media_types[ 0 ] );
                }
            }
            return null;
        }

        /**
         * @param $number
         * @return null|Webstamp_Media
         */
        public static function get_media( $number )
        {
            if ( $number ) {
                $medias = static::get_all_where( static::table( static::MEDIA ), 'number', $number );
                if ( !empty( $medias ) ) {
                    return new Webstamp_Media( $medias[ 0 ] );
                }
            }
            return null;
        }


        public static function get_order( $order_id )
        {
            if ( $order_id ) {
                $orders = static::get_all_where( static::table( static::ORDER ), 'order_id', $order_id );
                if ( !empty( $orders ) ) {
                    return new Webstamp_Order( $orders[ 0 ] );
                }
            }
            return null;
        }

        /**
         * @param $template_id
         * @return null|Webstamp_Template
         */
        public static function get_template( $template_id )
        {
            if ( $template_id ) {
                $templates = static::get_all_where( static::table( static::TEMPLATE ), 'id', $template_id );
                if ( !empty( $templates ) ) {
                    return new Webstamp_Template( $templates[ 0 ] );
                }
            }
            return null;
        }

        /**
         * @return Webstamp_Template[]|null
         */
        public static function get_templates()
        {
            $templates = static::get_all_results( static::table( static::TEMPLATE ) );
            if ( !empty( $templates ) ) {
                return array_map( function ( $item ) {
                    return new Webstamp_Template( $item );
                }, $templates );
            }
            return null;
        }

        /**
         * @param $wc_order_id
         * @return Webstamp_Order[]|null
         */
        public static function get_orders_by_wc_order( $wc_order_id )
        {
            if ( $wc_order_id ) {
                $orders = static::get_all_where( static::table( static::ORDER ), 'wc_order_id', $wc_order_id );
                if ( !empty( $orders ) ) {
                    return array_map( function ( $item ) {
                        return new Webstamp_Order( $item );
                    }, $orders );
                }
            }
            return null;
        }

        /**
         * @param $order_id
         * @return array[Webstamp_Stamp]|null
         */
        public static function get_stamps( $order_id )
        {
            if ( $order_id ) {
                $stamps = static::get_all_where( static::table( static::STAMP ), 'order_id', $order_id );
                if ( !empty( $stamps ) ) {
                    return array_map( function ( $item ) {
                        return new Webstamp_Stamp( $item );
                    }, $stamps );
                }
            }
            return null;
        }

        public static function get_all_results( $table )
        {
            global $wpdb;

            return $wpdb->get_results(
                "
	            SELECT * 
	            FROM $table
	            "
            );
        }

        public static function get_all_where( $table, $attribute, $value )
        {
            global $wpdb;

            return $wpdb->get_results(
                "
	            SELECT * 
	            FROM $table
	            WHERE $attribute = '$value'
	            "
            );
        }

        public static function delete( $table, $where )
        {
            global $wpdb;

            return $wpdb->delete( $table, $where );
        }

        public static function update( $table, $data, $where )
        {
            global $wpdb;
            return $wpdb->update( $table, $data, $where );
        }

        /**
         * Saves a category in the db.
         *
         * @param StdClass $category
         */
        public static function save_category( $category )
        {
            global $wpdb;
            //  $result = $wpdb->update( $wpdb->prefix . static::CATEGORY, ['number' => $category->number, 'name' => $category->name, 'recipient_mandatory' => $category->recipient_mandatory, 'valid_days' => $category->valid_days], ['number' => $category->number], ['%d', '%s', '%d', '%d'], ['%d', '%s', '%d', '%d'] );
            $result = $wpdb->replace( static::table( static::CATEGORY ), [ 'number' => $category->number, 'name' => $category->name, 'recipient_mandatory' => $category->recipient_mandatory, 'valid_days' => $category->valid_days ], [ '%d', '%s', '%d', '%d' ] );
            if ( $result ) {
                // TODO db insert failed.
            }
        }

        /**
         * Saves a product in the db.
         *
         * @param StdClass $product
         */
        public static function save_product( $product )
        {
            global $wpdb;
            $args    = [
                'number'              => $product->number,
                'category'            => $product->category,
                'category_number'     => $product->category_number,
                'post_product_number' => $product->post_product_number,
                'price'               => $product->price,
                'name'                => $product->name,
                'additions'           => isset( $product->additions ) && isset( $product->additions->item ) ? serialize( $product->additions->item ) : '',
                'delivery'            => $product->delivery,
                'format'              => $product->format,
                'size_din'            => $product->size_din,
                'max_size_length'     => $product->max_size_length,
                'max_size_height'     => $product->max_size_height,
                'max_edge_length'     => $product->max_edge_length,
                'max_scale'           => $product->max_scale,
                'max_weight'          => $product->max_weight,
                'zone'                => $product->zone,
                'product_list'        => $product->product_list,
                'product_list_number' => $product->product_list_number,
                'subsystem'           => $product->subsystem,
                'segment'             => $product->segment,
                'barcode'             => $product->barcode,
            ];
            $formats = [
                '%d', // number
                '%s', // category
                '%d', // category_number
                '%d', // post_product_number
                '%f', // price
                '%s', // name
                '%s', // additions
                '%s', // delivery
                '%s', // format
                '%s', // size_din
                '%d', // max_size_length
                '%d', // max_size_height
                '%d', // max_edge_length
                '%d', // max_scale
                '%d', // max_weight
                '%d', // zone
                '%s', // product_list
                '%d', // product_list_number
                '%d', // subsystem
                '%d', // segment
                '%d', // barcode
            ];
            $result  = $wpdb->replace( static::table( static::PRODUCT ), $args, $formats );

            if ( !$result ) {
                // TODO db insert failed.
            }
            if ( !empty( $product->additions ) && !empty( $product->additions->item ) ) {
                foreach ( $product->additions->item as $addition ) {
                    if ( $addition->alias )
                        $result = $wpdb->replace( static::table( static::ADDITION ), [ 'product_number' => $product->number, 'code' => $addition->code, 'short_name' => $addition->short_name, 'name' => $addition->name, 'alias' => $addition->alias, 'optional' => $addition->optional, 'price' => $addition->price, 'add_price' => $addition->add_price ], [ '%d', '%s', '%s', '%s', '%s', '%d', '%f', '%d' ] );
                }
            }

        }

        /**
         * Saves a zone in the db.
         *
         * @param StdClass $zone
         */
        public static function save_zone( $zone )
        {
            global $wpdb;
            $result = $wpdb->replace( static::table( static::ZONE ), [ 'number' => $zone->number, 'name' => $zone->name ], [ '%d', '%s' ] );
            if ( $result ) {
                // TODO db insert failed.
            }
        }

        /**
         * Saves a product list in the db.
         *
         * @param StdClass $product_list
         */
        public static function save_product_list( $product_list )
        {
            global $wpdb;
            $result = $wpdb->replace( static::table( static::PRODUCT_LIST ), [ 'number' => $product_list->number, 'name' => $product_list->name, 'customer_type' => $product_list->customer_type ], [ '%d', '%s', '%s' ] );
            if ( $result ) {
                // TODO db insert failed.
            }
        }

        /**
         * Saves a media in the db.
         *
         * @param StdClass $media
         */
        public static function save_media( $media )
        {
            global $wpdb;
            $result = $wpdb->replace( static::table( static::MEDIA ), [ 'number' => $media->number, 'name' => $media->name, 'type' => $media->type ], [ '%d', '%s', '%s' ] );
            if ( $result ) {
                //  db insert failed.
            }
        }

        /**
         * Saves a media type in the db.
         *
         * @param StdClass $media_type
         */
        public static function save_media_type( $media_type )
        {
            global $wpdb;
            $result = $wpdb->replace( static::table( static::MEDIA_TYPE ), [ 'number' => $media_type->number, 'name' => $media_type->name, 'image_possible' => $media_type->image_possible, 'categories' => ( isset( $media_type->categories ) && isset( $media_type->categories->item ) ? serialize( $media_type->categories->item ) : '' ) ], [ '%d', '%s', '%d', '%s' ] );

            // Save relation media_type->categories in category_to_media_type.
            $categories = $media_type->categories->item;
            if ( !empty( $categories ) ) {
                foreach ( $categories as $category ) {
                    $result = $wpdb->replace( static::table( static::CATEGORY_TO_MEDIA_TYPE ), [ 'category_number' => $category->number, 'media_type_number' => $media_type->number ], [ '%d', '%d' ] );
                }
            }

            if ( $result ) {
                //  db insert failed.
            }
        }

        /**
         * Saves a country in the db.
         *
         * @param StdClass $country
         */
        public static function save_country( $country )
        {
            global $wpdb;
            $result = $wpdb->replace( static::table( static::COUNTRY ), [ 'alias' => $country->alias, 'name' => $country->name, 'zone' => $country->zone ], [ '%s', '%s', '%d' ] );
            if ( $result ) {
                // db insert failed.
            }
        }

        /**
         * Saves a license in the db.
         *
         * @param StdClass $license
         */
        public static function save_license( $license )
        {
            global $wpdb;
            $result = $wpdb->replace( static::table( static::LICENSE ), [ 'number' => $license->number, 'comment' => $license->comment, 'subsystem' => $license->subsystem, 'segment' => $license->segment ], [ '%s', '%s', '%d', '%d' ] );
            if ( $result ) {
                //  db insert failed.
            }
        }

        /**
         * @param Webstamp_Order|array $args
         */
        public static function update_draft_order( $args )
        {
            global $wpdb;

            $table_order = static::table( static::ORDER );
            $wc_order_id = is_object( $args ) ? $args->wc_order_id : $args[ 'wc_order_id' ];
            $order       = null;

            if ( $wc_order_id ) {
                $orders = $wpdb->get_results(
                    "
	            SELECT * 
	            FROM $table_order
	            WHERE status = 'draft' and wc_order_id = '$wc_order_id'
	            "
                );
                if ( !empty( $orders ) )
                    $order = $orders[ 0 ];

            } else {
                $orders = $wpdb->get_results(
                    "
	            SELECT * 
	            FROM $table_order
	            WHERE status = 'draft' and ( wc_order_id IS NULL or wc_order_id = '0' or wc_order_id = 0 or wc_order_id = '' ) 
	            "
                );
                if ( !empty( $orders ) )
                    $order = $orders[ 0 ];

            }
            if ( $order ) {
                $order->additions = json_decode( $order->additions );
                $order            = new Webstamp_Order( $order );
                //var_dump($order->additions);die();
            } else {
                $order         = new Webstamp_Order();
                $order->status = 'draft';
                if ( $wc_order_id )
                    $order->wc_order_id = $wc_order_id;
            }

            $order->populate( $args );

            // Update receiver address.
            if ( $order->address_id ) {
                // Update existing address
                static::update_address( $order->address_id, new Webstamp_Address( $order->address ) );
            } else {
                // Create new address and assign id.
                $address_id        = static::save_address( $order->address );
                $order->address_id = $address_id;
            }


            // Update sender address.
            if ( $order->sender_id ) {
                // Update existing address
                static::update_address( $order->sender_id, $order->sender );
            } else {
                // Create new address and assign id.
                $sender_id        = static::save_address( $order->sender );
                $order->sender_id = $sender_id;
            }

            static::save_order( $order );

        }

        /**
         * @param $order
         * @return int
         */
        public static function save_order( $order )
        {
            global $wpdb;

            // Save receipt file
            if ( $order->delivery_receipt ) {
                $upload_path = str_replace( '/', DIRECTORY_SEPARATOR, wp_upload_dir()[ 'basedir' ] ) . DIRECTORY_SEPARATOR . 'webstamp' . DIRECTORY_SEPARATOR . 'receipts' . DIRECTORY_SEPARATOR;
                $receipt     = static::save_file( $order->delivery_receipt, $upload_path );
            }

            $args    = [];
            $formats = [];

            if ( isset( $order->order_id ) ) {
                $args[ 'order_id' ] = sanitize_text_field( $order->order_id );
                $formats[]          = '%d';
            }

            // Only for templates
            if ( isset( $order->name ) ) {
                $args[ 'name' ] = sanitize_text_field( $order->name );
                $formats[]      = '%s';
            }

            // Only for templates
            if ( isset( $order->load_wc_address ) ) {
                $args[ 'load_wc_address' ] = sanitize_text_field( $order->load_wc_address );
                $formats[]                 = '%s';
            }

            if ( isset( $order->reference ) ) {
                $args[ 'reference' ] = sanitize_text_field( $order->reference );
                $formats[]           = '%s';
            }

            if ( isset( $order->price ) ) {
                $args[ 'price' ] = sanitize_text_field( $order->price );
                $formats[]       = '%f';
            }

            if ( isset( $order->item_price ) ) {
                $args[ 'item_price' ] = sanitize_text_field( $order->item_price );
                $formats[]            = '%f';
            }

            if ( isset( $order->valid_days ) ) {
                $args[ 'valid_days' ] = sanitize_text_field( $order->valid_days );
                $formats[]            = '%d';
            }

            if ( isset( $order->valid_until ) ) {
                $date                  = DateTime::createFromFormat( 'Y-m-d\TG:i:sP', $order->valid_until );
                $args[ 'valid_until' ] = $date->getTimestamp();
                $formats[]             = '%d';
            }

            if ( isset( $receipt ) ) {
                $args[ 'delivery_receipt' ] = sanitize_text_field( $receipt );
                $formats[]                  = '%s';
            }

            if ( isset( $order->product_number ) ) {
                $args[ 'product_number' ] = sanitize_text_field( $order->product_number );
                $formats[]                = '%d';
            }

            if ( isset( $order->post_product_number ) ) {
                $args[ 'post_product_number' ] = sanitize_text_field( $order->post_product_number );
                $formats[]                     = '%d';
            }

            if ( isset( $order->time ) ) {
                $args[ 'time' ] = sanitize_text_field( $order->time );
                $formats[]      = '%d';
            }

            if ( isset( $order->id ) ) {
                $args[ 'id' ] = sanitize_text_field( $order->id );
                $formats[]    = '%d';
            }

            if ( isset( $order->category ) ) {
                $args[ 'category' ] = sanitize_text_field( $order->category );
                $formats[]          = '%d';
            }

            if ( isset( $order->zone ) ) {
                $args[ 'zone' ] = sanitize_text_field( $order->zone );
                $formats[]      = '%d';
            }

            if ( isset( $order->delivery_method ) ) {
                $args[ 'delivery_method' ] = sanitize_text_field( $order->delivery_method );
                $formats[]                 = '%s';
            }

            if ( isset( $order->format ) ) {
                $args[ 'format' ] = sanitize_text_field( $order->format );
                $formats[]        = '%s';
            }

            if ( isset( $order->additions ) ) {
                $order->additions    = array_map( function ( $i ) {
                    return sanitize_text_field( $i );
                }, $order->additions );
                $args[ 'additions' ] = json_encode( $order->additions );
                $formats[]           = '%s';
            }

            if ( isset( $order->address_id ) ) {
                $args[ 'address_id' ] = sanitize_text_field( $order->address_id );
                $formats[]            = '%d';
            }

            if ( isset( $order->media_type ) ) {
                $args[ 'media_type' ] = sanitize_text_field( $order->media_type );
                $formats[]            = '%d';
            }

            if ( isset( $order->media ) ) {
                $args[ 'media' ] = sanitize_text_field( $order->media );
                $formats[]       = '%d';
            }

            if ( isset( $order->license_number ) ) {
                $args[ 'license_number' ] = sanitize_text_field( $order->license_number );
                $formats[]                = '%s';
            }

            if ( isset( $order->status ) ) {
                $args[ 'status' ] = sanitize_text_field( $order->status );
                $formats[]        = '%s';
            }

            if ( isset( $order->wc_order_id ) ) {
                $args[ 'wc_order_id' ] = sanitize_text_field( $order->wc_order_id );
                $formats[]             = '%d';
            }

            if ( isset( $order->use_address ) ) {
                $args[ 'use_address' ] = ( $order->use_address == 1 || $order->use_address == 'true' ) ? 1 : 0;
                $formats[]             = '%d';
            }

            if ( isset( $order->quantity ) ) {
                $args[ 'quantity' ] = sanitize_text_field( $order->quantity );
                $formats[]          = '%d';
            }

            if ( isset( $order->use_sender_address ) ) {
                $args[ 'use_sender_address' ] = ( $order->use_sender_address == 1 || $order->use_sender_address == 'true' ) ? 1 : 0;
                $formats[]                    = '%d';
            }

            if ( isset( $order->sender_id ) ) {
                $args[ 'sender_id' ] = sanitize_text_field( $order->sender_id );
                $formats[]           = '%d';
            }

            if ( isset( $order->image_id ) ) {
                $args[ 'image_id' ] = sanitize_text_field( $order->image_id );
                $formats[]          = '%s';
            }

            if ( isset( $order->media_startpos ) ) {
                $args[ 'media_startpos' ] = sanitize_text_field( $order->media_startpos );
                $formats[]                = '%d';
            }

            if ( isset( $order->order_comment ) ) {
                $args[ 'order_comment' ] = sanitize_text_field( $order->order_comment );
                $formats[]               = '%s';
            }

            $table = static::table( static::ORDER );
            if ( $order instanceof Webstamp_Template )
                $table = static::table( static::TEMPLATE );

            $result = $wpdb->replace( $table, $args, $formats );

            if ( !$result ) {
                return 0;
            }

            $id = $wpdb->insert_id;

            return $id;
        }

        /**
         * @param Webstamp_Template|null $template
         * @return int
         */
        public static function update_template( $template = null )
        {
            // Update sender address.
            if ( $template->sender_id ) {
                // Update existing address
                static::update_address( $template->sender_id, $template->sender );
            } else {
                // Create new address and assign id.
                $sender_id           = static::save_address( $template->sender );
                $template->sender_id = $sender_id;
            }

            if ( isset( $template->id ) && !empty( $template->id ) ) {
                $old_template = static::get_template( $template->id );
                $old_template->populate( $template );
                $template = $old_template;

            }
            return static::save_order( $template );
        }

        public static function save_stamp( $stamp )
        {
            global $wpdb;

            //$filename = static::save_file( $stamp->print_data );

            $args    = [
                'stamp_id'        => $stamp->stamp_id,
                'order_id'        => $stamp->order_id,
                'tracking_number' => $stamp->tracking_number,
                'file_name'       => $stamp->file_name,
                'compression'     => $stamp->compression,
                'mime_type'       => $stamp->mime_type,
                'image_width_mm'  => $stamp->image_width_mm,
                'image_height_mm' => $stamp->image_height_mm,
                'image_width_px'  => $stamp->image_width_px,
                'image_height_px' => $stamp->image_height_px,
                'reference'       => $stamp->reference,

            ];
            $formats = [
                '%d',
                '%d',
                '%s',
                '%s',
                '%s',
                '%s',
                '%d',
                '%d',
                '%d',
                '%d',
                '%s',
            ];

            $result = $wpdb->replace( static::table( static::STAMP ), $args, $formats );
            if ( !$result ) {
                //  db insert failed.
            }
        }

        public static function update_address( $id, $args )
        {
            // $address = static::get_all_where( static::table( static::ADDRESS ), 'id', $id )[ 0 ];
            $address = static::get_address( $id );
            if ( !$address ) {
                $address = new Webstamp_Address();
            }
            $address->populate( $args );
            $address->id = $id;

            return static::save_address( $address );
        }

        public static function save_address( $address )
        {
            if ( !$address )
                return 0;

            global $wpdb;
            if ( is_array( $address ) )
                $address = (object)$address;

            $country = !empty( $address->country ) ? $address->country : 'ch';

            $args = [
                'organization'            => $address->organization,
                'company_addition'        => $address->company_addition,
                'title'                   => $address->title,
                'firstname'               => $address->firstname,
                'lastname'                => $address->lastname,
                'addition'                => $address->addition,
                'street'                  => $address->street,
                'pobox'                   => $address->pobox,
                'pobox_lang'              => $address->pobox_lang,
                'pobox_nr'                => $address->pobox_nr,
                'zip'                     => $address->zip,
                'city'                    => $address->city,
                'country'                 => $country,
                'reference'               => $address->reference,
                'cash_on_delivery_amount' => $address->cash_on_delivery_amount,
                'cash_on_delivery_esr'    => $address->cash_on_delivery_esr,
            ];

            $formats = [
                '%s', // organization
                '%s', // company_addition
                '%s', // title
                '%s', // firstname
                '%s', // lastname
                '%s', // addition
                '%s', // street
                '%s', // pobox
                '%s', // pobox_lang
                '%s', // pobox_nr
                '%s', // zip
                '%s', // city
                '%s', // country
                '%s', // reference
                '%f', // cash_on_delivery_amount
                '%s', // cash_on_delivery_esr
            ];

            if ( isset( $address->id ) ) {
                $args[ 'id' ] = $address->id;
                $formats[]    = '%d';
            }

            $result = $wpdb->replace( static::table( static::ADDRESS ), $args, $formats );
            if ( !$result ) {
                return 0;
            }

            // The id of the inserted result.
            return $wpdb->insert_id;
        }

        public static function save_file( $file, $upload_path = null, $filename = null )
        {
            if ( $file ) {

                if ( !$upload_path ) {
                    $upload_dir  = wp_upload_dir();
                    $upload_path = str_replace( '/', DIRECTORY_SEPARATOR, $upload_dir[ 'basedir' ] ) . DIRECTORY_SEPARATOR . 'webstamp' . DIRECTORY_SEPARATOR . 'stamps' . DIRECTORY_SEPARATOR;
                }

                if ( !$filename ) {
                    $options = get_option( 'mame_ws_options_group' );
                    //var_dump($options);die();
                    $filename = uniqid() . ( $options[ 'stamp_file_type' ] === 'pdf' ? '.pdf' : '.png' );
                }
                $hashed_filename = md5( $filename . microtime() ) . '_' . $filename;
                $upload          = file_put_contents( $upload_path . $hashed_filename, $file );
                return $hashed_filename;
            }
            return false;

        }
    }

}