<?php

if ( !defined( 'ABSPATH' ) ) {
    exit;
}

if ( !class_exists( 'Webstamp_Editor_Handler' ) ) {

    /**
     * Class Webstamp_Editor_Handler
     */
    class Webstamp_Editor_Handler
    {
        public $address;
        public $sender_address;
        public $wc_order_id;
        public $template;
        public $autoload_draft;
        public $load_order_overview_directly;

        protected $overview_data;
        protected $json_response;
        protected $direct_dependencies;

        public function __construct( $wc_order_id = null, $address = null )
        {
            $this->wc_order_id                  = $wc_order_id;
            $this->address                      = $address;
            $this->json_response                = new Webstamp_Editor_Json_Response();
            $this->template                     = new Webstamp_Template();
            $this->overview_data                = [];
            $this->load_order_overview_directly = false;
            $this->direct_dependencies          = [
                'category'        => [ 'zone', 'media_type', 'country' ],
                'zone'            => [ 'addition', 'delivery_method', 'format', 'country' ],
                'delivery_method' => [ 'addition', 'format' ],
                'additions'       => [],
                'format'          => [ 'addition' ],
                'media_type'      => [ 'media', 'media_startpos' ],
                'media'           => [],
            ];

            add_action( 'wp_ajax_mame_ws_ajax_editor_request', array( $this, 'ajax_handler' ) );
        }

        /**
         * Handles AJAX requests.
         */
        public function ajax_handler()
        {
            if ( !check_ajax_referer( 'mame_ws_editor_nonce', 'security' ) ) {
                return;
            }

            switch ( $_POST[ 'ws_action' ] ) {

                case 'save_and_invalidate':

                    $save_data                  = $_POST[ 'ws_data' ][ 'save' ];
                    $wc_order_id                = isset( $_POST[ 'ws_data' ][ 'wc_order_id' ] ) ? $_POST[ 'ws_data' ][ 'wc_order_id' ] : null;
                    $save_data[ 'wc_order_id' ] = $wc_order_id;

                    // Additions workaround.
                    if ( $_POST[ 'ws_data' ][ 'step' ] == 'addition' ) {
                        if ( !isset( $save_data[ 'additions' ] ) ) {
                            $save_data[ 'additions' ] = [];
                        }
                    }

                    // Save data
                    Webstamp_Database_Manager::update_draft_order( $save_data );

                    // Invalidate other data.
                    $draft_order = Webstamp_Database_Manager::get_draft_order( $wc_order_id );
                    if ( $draft_order ) {
                        $draft_order->delete( $_POST[ 'ws_data' ][ 'invalidate' ] );
                    }

                    // Return JSON response.
                    $this->json_response->status( Mame_Json_Response::STATUS_SUCCESS );
                    // Populate depending fields.
                    $dependencies = $this->direct_dependencies[ $_POST[ 'ws_data' ][ 'step' ] ];
                    if ( !empty ( $dependencies ) ) {

                        foreach ( $dependencies as $d ) {
                            call_user_func( array( $this, 'json_' . $d ), $save_data );
                            //$this->json_response->html( '#' . MAME_WS_PREFIX . '-overview-' . $d, '' );
                        }
                    }
                    $this->json_response->respond();

                    break;

                case 'load_dependencies':
                    $save_data                  = $_POST[ 'ws_data' ][ 'save' ];
                    $wc_order_id                = isset( $_POST[ 'ws_data' ][ 'wc_order_id' ] ) ? $_POST[ 'ws_data' ][ 'wc_order_id' ] : null;
                    $save_data[ 'wc_order_id' ] = $wc_order_id;

                    // Additions workaround.
                    if ( $_POST[ 'ws_data' ][ 'step' ] == 'addition' ) {
                        if ( !isset( $save_data[ 'additions' ] ) ) {
                            $save_data[ 'additions' ] = [];
                        }
                    }

                    // Return JSON response.
                    $this->json_response->status( Mame_Json_Response::STATUS_SUCCESS );
                    // Populate depending fields.
                    $dependencies = $this->direct_dependencies[ $_POST[ 'ws_data' ][ 'step' ] ];
                    if ( !empty ( $dependencies ) ) {

                        foreach ( $dependencies as $d ) {
                            call_user_func( array( $this, 'json_' . $d ), $save_data );
                            //$this->json_response->html( '#' . MAME_WS_PREFIX . '-overview-' . $d, '' );
                        }
                    }
                    $this->json_response->respond();

                    break;
                case 'load':
                    $this->load_editor();
                    break;

                case 'category':
                    $this->json_category()->respond();

                    break;

                case 'zone':
                    $this->json_zone( $_POST[ 'ws_data' ] )->respond();

                    break;

                case 'delivery_method':
                    $this->json_delivery_method( $_POST[ 'ws_data' ] )->respond();

                    break;

                case 'format' :
                    $this->json_format( $_POST[ 'ws_data' ] )->respond();

                    break;

                case 'addition' :
                    $this->json_addition( $_POST[ 'ws_data' ] )->respond();

                    break;

                case 'address':
                    $this->json_address()->respond();

                    break;

                case 'media_type':
                    $this->json_media_type( $_POST[ 'ws_data' ] )->respond();

                    break;

                case 'media':
                    $this->json_media( $_POST[ 'ws_data' ] )->respond();

                    break;

                case 'country':
                    $this->json_country( $_POST[ 'ws_data' ] )->respond();

                    break;

                case 'media_startpos':
                    $this->json_media_startpos( $_POST[ 'ws_data' ] )->show( "#" . MAME_WS_PREFIX . '-media_startpos' )->respond();
                    break;

                case 'license_number':
                    $this->json_license()->respond();

                    break;
                case 'preview':
                    $wc_order_id = isset( $_POST[ 'ws_data' ][ 'wc_order_id' ] ) ? $_POST[ 'ws_data' ][ 'wc_order_id' ] : null;

                    $errors = $this->validate( $wc_order_id );
                    if ( $errors )
                        $this->json_response->errors( '#' . MAME_WS_PREFIX . '-overview-wrapper .errors', $errors )->respond();

                    $licenses = $this->check_if_license_needed( $wc_order_id );
                    if ( $licenses ) {
                        // Get license select field.

                        $this->json_license_select_field( $licenses, 'preview-btn' )->respond();
                    }

                    // Send download file.
                    $this->preview_order( $wc_order_id );

                    break;

                case 'order_overview':
                    $wc_order_id = isset( $_POST[ 'ws_data' ][ 'wc_order_id' ] ) ? $_POST[ 'ws_data' ][ 'wc_order_id' ] : null;
                    $errors      = $this->validate( $wc_order_id );
                    if ( $errors )
                        $this->json_response->errors( '#' . MAME_WS_PREFIX . '-overview-wrapper .errors', $errors )->respond();

                    $licenses = $this->check_if_license_needed( $wc_order_id );
                    if ( $licenses ) {
                        // Get license select field.
                        $this->json_license_select_field( $licenses, 'order-btn' )->respond();
                    }

                    $this->json_order_overview( $wc_order_id )->respond();

                    break;

                case 'order':
                    $wc_order_id = isset( $_POST[ 'ws_data' ][ 'wc_order_id' ] ) ? $_POST[ 'ws_data' ][ 'wc_order_id' ] : null;
                    $errors      = $this->validate( $wc_order_id );

                    if ( !$errors ) {
                        // Send download file.
                        $this->order( $wc_order_id );
                    }
                    break;

                case 'check_draft_exists':
                    $wc_order_id = isset( $_POST[ 'ws_data' ][ 'wc_order_id' ] ) ? $_POST[ 'ws_data' ][ 'wc_order_id' ] : null;
                    $draft_order = Webstamp_Database_Manager::get_draft_order( $wc_order_id );
                    if ( $draft_order ) {
                        if ( !empty( $draft_order->image ) ) {
                            $attachment_image = wp_get_attachment_image_src( $draft_order->image, array( '200', 'auto' ) );
                            if ( $attachment_image )
                                $draft_order->image_url = $attachment_image[ 0 ];
                        }

                        $address                        = $draft_order->get_address();
                        $sender                         = $draft_order->get_sender();
                        $draft_order->category_object   = $draft_order->get_category();
                        $draft_order->zone_object       = $draft_order->get_zone();
                        $draft_order->media_object      = $draft_order->get_media();
                        $draft_order->media_type_object = $draft_order->get_media_type();
                        $this->json_response->status( true )->order( $draft_order )->address( $address )->sender( $sender )->data( '#' . MAME_WS_PREFIX . '-overlay', 'order-id', $draft_order->id )->respond();
                    }
                    $this->json_response->status( false )->respond();

                    break;

                case 'load_draft':
                    $this->wc_order_id = isset( $_POST[ 'ws_data' ][ 'wc_order_id' ] ) ? $_POST[ 'ws_data' ][ 'wc_order_id' ] : null;
                    $draft_order       = Webstamp_Database_Manager::get_draft_order( $this->wc_order_id );
                    if ( $draft_order ) {

                        $this->template->populate( $draft_order );
                        $this->address        = $this->template->get_address();
                        $this->sender_address = $this->template->get_sender();

                        return $this->json_response->html( '#' . MAME_WS_PREFIX . '-steps', $this->load_editor_form_content() )->show( '.' . MAME_WS_PREFIX . '-editor-content' )->hide( '#' . MAME_WS_PREFIX . '-start' )->respond();

                    }
                    $this->json_response->status( false )->respond();
                    break;
                case 'delete_draft':
                    $where = [ 'status' => 'draft' ];
                    if ( isset( $_POST[ 'ws_data' ][ 'wc_order_id' ] ) )
                        $this->wc_order_id = $where[ 'wc_order_id' ] = $_POST[ 'ws_data' ][ 'wc_order_id' ];
                    else
                        $where[ 'wc_order_id' ] = null;
                    Webstamp_Database_Manager::delete( Webstamp_Database_Manager::table( Webstamp_Database_Manager::ORDER ), $where );

                    return $this->json_response->html( '#' . MAME_WS_PREFIX . '-steps', $this->load_editor_form_content() )->show( '.' . MAME_WS_PREFIX . '-editor-content' )->hide( '#' . MAME_WS_PREFIX . '-start' )->respond();
                    break;

                case 'delete':
                    $wc_order_id = isset( $_POST[ 'ws_data' ][ 'wc_order_id' ] ) ? $_POST[ 'ws_data' ][ 'wc_order_id' ] : null;
                    $draft_order = Webstamp_Database_Manager::get_draft_order( $wc_order_id );
                    if ( $draft_order ) {
                        $draft_order->delete( $_POST[ 'ws_data' ][ 'attributes' ] );
                    }
                    break;

                case 'load_shipping_address':

                    if ( class_exists( 'WC_Order' ) ) {
                        $order   = new WC_Order( $_POST[ 'ws_data' ][ 'wc_order_id' ] );
                        $address = Webstamp_Address::get_shipping_address( $order );
                        $this->json_response->status( true )->address( $address )->respond();
                    }

                    break;

                case 'load_billing_address':
                    if ( class_exists( 'WC_Order' ) ) {
                        $order   = new WC_Order( $_POST[ 'ws_data' ][ 'wc_order_id' ] );
                        $address = Webstamp_Address::get_billing_address( $order );
                        $this->json_response->status( true )->address( $address )->respond();
                    }
                    break;

                case 'load_custom_address':

                    if ( class_exists( 'WC_Order' ) ) {
                        $order   = new WC_Order( $_POST[ 'ws_data' ][ 'wc_order_id' ] );
                        $address = Webstamp_Address::get_custom_address( $order );
                        $this->json_response->status( true )->address( $address )->respond();
                    }

                    break;

                case 'load_default_sender':
                    $sender = Webstamp_Address::get_sender_address();
                    $this->json_response->status( true )->sender( $sender )->respond();

                    break;

                case 'edit_license':
                    $content = $this->get_license_fields();
                    $content .= Mame_Html::button( __( 'Cancel', 'dhuett' ), [ 'class' => MAME_WS_PREFIX . '-btn ' . MAME_WS_PREFIX . '-close-overlay' ] );
                    return $this->json_response->status( Mame_Json_Response::STATUS_SUCCESS )->html( '#' . MAME_WS_PREFIX . '-overlay .content', $content )->show( '#' . MAME_WS_PREFIX . '-overlay' )->respond();
                    break;

                case 'load_template':
                    $template_id = $_POST[ 'ws_data' ][ 'template_id' ];
                    if ( isset( $_POST[ 'ws_data' ][ 'wc_order_id' ] ) ) {
                        $this->wc_order_id = $_POST[ 'ws_data' ][ 'wc_order_id' ];
                    }

                    $this->load_template_into_draft_order( $template_id );

                    $this->json_response->html( '#' . MAME_WS_PREFIX . '-steps', $this->load_editor_form_content( $this->load_order_overview_directly ? 4 : 1 ) )->show( '.' . MAME_WS_PREFIX . '-editor-content' )->hide( '#' . MAME_WS_PREFIX . '-start' );

                    if ( $this->load_order_overview_directly ) {
                        $errors = $this->validate( $this->wc_order_id );

                        $this->json_response->page( 4 );
                        if ( $errors ) {
                            $this->json_response->errors( '#' . MAME_WS_PREFIX . '-overview-wrapper .errors', $errors )->scroll_to( '#' . MAME_WS_PREFIX . '-overview-wrapper .errors' )->respond();
                        } else {
                            $this->json_order_overview( $this->wc_order_id );
                        }
                    }

                    return $this->json_response->respond();
                    break;
            }
        }

        public function load_template_into_draft_order( $template_id )
        {
            $this->template = Webstamp_Database_Manager::get_template( $template_id );
            if ( !$this->template )
                return $this->json_response->status( Mame_Json_Response::STATUS_FAILURE )->respond();

            $options                            = get_option( 'mame_ws_options_group' );
            $this->load_order_overview_directly = isset( $options[ 'template_order_overview' ] ) ? $options[ 'template_order_overview' ] : false;

            // Create new draft.
            $draft_order         = new Webstamp_Order( $this->template );
            $draft_order->id     = null;
            $draft_order->status = Webstamp_Order::STATUS_DRAFT;

            $where = [ 'status' => 'draft' ];
            if ( $this->wc_order_id ) {
                $where[ 'wc_order_id' ] = $draft_order->wc_order_id = $this->wc_order_id;
            } else {
                $where[ 'wc_order_id' ] = null;
            }

            // Delete old draft.
            Webstamp_Database_Manager::delete( Webstamp_Database_Manager::table( Webstamp_Database_Manager::ORDER ), $where );

            // Sender
            if ( $this->template->sender_id ) {
                $draft_order->sender = $this->sender_address = $this->template->get_sender();
            }

            // Receiver
            if ( $this->wc_order_id ) {
                if ( $this->template->use_address ) {
                    if ( $this->template->load_wc_address === Webstamp_Template::LOAD_WC_ADDRESS_SHIPPING ) {
                        $this->address = Webstamp_Address::get_shipping_address( new WC_Order( $this->wc_order_id ) );
                    } elseif ( $this->template->load_wc_address === Webstamp_Template::LOAD_WC_ADDRESS_BILLING ) {
                        $this->address = Webstamp_Address::get_billing_address( new WC_Order( $this->wc_order_id ) );
                    } elseif ($this->template->load_wc_address === Webstamp_Template::LOAD_WC_ADDRESS_CUSTOM){
                        $this->address = Webstamp_Address::get_custom_address(new WC_Order($this->wc_order_id));
                    }

                    // Save address
                    if ( $this->address )
                        $draft_order->address = $this->address;
                }
            }

            // Update draft order.
            Webstamp_Database_Manager::update_draft_order( $draft_order );
        }

        public function load_order_overview()
        {
            $licenses = $this->check_if_license_needed( $this->wc_order_id );
            if ( $licenses ) {
                // Get license select field.
                return $this->get_license_select_field( $licenses, 'order-btn' );
            }

            return $this->get_order_overview_fields();
        }

        public function errors_html( $errors )
        {
            if ( $errors ) {
                return '<p>' . implode( '<br>', $errors ) . '</p>';
            }
        }

        public function get_license_select_field( $licenses, $action )
        {
            $content = Mame_Html::h3( __( 'Select a license', 'dhuett' ) );
            $content .= Mame_Html::p( __( 'This product needs a license. Please select a license to continue' ) );
            $content .= $this->get_license_fields( $licenses );
            $content .= Mame_Html::button( __( 'Continue', 'dhuett' ), [ 'id' => MAME_WS_PREFIX . '-' . $action, 'class' => MAME_WS_PREFIX . '-btn' ] );
            $content .= Mame_Html::button( __( 'Cancel', 'dhuett' ), [ 'class' => MAME_WS_PREFIX . '-btn ' . MAME_WS_PREFIX . '-close-overlay' ] );

            return $content;
        }

        public function get_order_overview_fields()
        {
            $overview = Mame_Html::h3( __( 'Order the following product?', 'dhuett' ) );

            $draft_order = Webstamp_Database_Manager::get_draft_order( $this->wc_order_id );
            $product     = Webstamp_Database_Manager::get_unique_product( $draft_order->zone, $draft_order->delivery_method, $draft_order->format, $draft_order->additions );
            $overview    .= $this->product_overview( $product );
            $overview    .= Mame_Html::open_tag( 'div', [ 'id' => MAME_WS_PREFIX . '-order-buttons' ] );
            $overview    .= Mame_Html::button( __( 'Order', 'dhuett' ), [ 'id' => MAME_WS_PREFIX . '-order-confirm-btn', 'class' => MAME_WS_PREFIX . '-btn' ] );
            $overview    .= Mame_Html::button( __( 'Cancel', 'dhuett' ), [ 'class' => MAME_WS_PREFIX . '-close-overlay ' . MAME_WS_PREFIX . '-btn' ] );
            $overview    .= Mame_Html::close_tag( 'div' );

            return $overview;
        }

        /**
         * Checks if the draft order contains all necessary fields.
         *
         * @param null $wc_order_id
         * @return bool
         */
        public function validate( $wc_order_id = null )
        {
            $draft_order = Webstamp_Database_Manager::get_draft_order( $wc_order_id );
            $errors      = [];
            if ( !$draft_order ) {
                $errors[] = __( 'Order not found', 'dhuett' );

            } else {

                if ( $this->empty_or_missing( $draft_order->category ) ) {
                    $errors[] = __( 'Category missing', 'dhuett' );
                }

                if ( $this->empty_or_missing( $draft_order->zone ) ) {
                    $errors [] = __( 'Zone missing', 'dhuett' );
                }

                if ( $this->empty_or_missing( $draft_order->delivery_method ) ) {
                    $errors[] = __( 'Delivery method missing', 'dhuett' );
                }

                if ( $this->empty_or_missing( $draft_order->format ) ) {
                    $errors[] = __( 'Format missing', 'dhuett' );
                }

                $options = get_option( 'mame_ws_options_group' );
                if ( $options[ 'stamp_file_type' ] === 'pdf' ) {
                    if ( $this->empty_or_missing( $draft_order->media_type ) ) {
                        $errors[] = __( 'Media type missing', 'dhuett' );
                    }

                    if ( $this->empty_or_missing( $draft_order->media ) ) {
                        $errors[] = __( 'Media missing', 'dhuett' );
                    }
                }

                if ( !$draft_order->quantity ) {
                    $address = $draft_order->get_address();

                    if ( !$address ) {
                        $errors[] = __( 'Recipient missing', 'dhuett' );
                    } else {
                        if ( $this->empty_or_missing( $address->organization ) ) {
                            if ( $this->empty_or_missing( $address->firstname ) || $this->empty_or_missing( $address->lastname ) )
                                $errors[] = __( 'Recipient name or company missing', 'dhuett' );
                            if ( $this->empty_or_missing( $address->street ) )
                                $errors[] = __( 'Recipient street missing', 'dhuett' );
                            if ( $this->empty_or_missing( $address->zip ) )
                                $errors[] = __( 'Recipient zip missing', 'dhuett' );
                            if ( $this->empty_or_missing( $address->city ) )
                                $errors[] = __( 'Recipient city missing', 'dhuett' );
                        }
                    }

                }
            }

            // Check if product needs license.
            if ( empty( $errors ) )
                return false;

            return $errors;
        }

        /**
         * Checks if the product from the draft order needs a license.
         * Returns false if no license is necessary or if a single matching license exists.
         * Returns an array of licenses if more than 1 license exists.
         *
         * @param null $wc_order_id
         * @return bool
         */
        public function check_if_license_needed( $wc_order_id = null )
        {
            $draft_order = Webstamp_Database_Manager::get_draft_order( $wc_order_id );
            $product     = Webstamp_Database_Manager::get_unique_product( $draft_order->zone, $draft_order->delivery_method, $draft_order->format, $draft_order->additions );

            if ( !empty( $product->subsystem ) ) {

                // Get license that matches subsystem.
                $licenses = Webstamp_Database_Manager::get_licenses_by_subsystem( $product->subsystem );
                if ( empty( $licenses ) ) {
                    // Error

                    $errors[] = __( 'License missing for product.', 'dhuett' );
                    $this->json_response->errors( '#' . MAME_WS_PREFIX . '-overview-wrapper .errors', $errors )->respond();

                } else {

                    foreach ( $licenses as $license ) {
                        if ( $license->number == $draft_order->license_number )
                            return false;
                    }

                    if ( count( $licenses ) > 1 ) {
                        return $licenses;

                    } else {

                        $draft_order->license = $licenses[ 0 ]->number;
                        Webstamp_Database_Manager::save_order( $draft_order );

                        return false;
                    }
                }
            }
            return false;
        }

        /**
         * @return Webstamp_Editor_Json_Response
         */
        public function json_editor()
        {
            return $this->json_response->html( '#' . MAME_WS_PREFIX . '-steps', $this->load_editor_form_content() )->show( '.' . MAME_WS_PREFIX . '-editor-content' )->hide( '#' . MAME_WS_PREFIX . '-start' );
        }

        public function json_license_select_field( $licenses, $action )
        {
            $content = Mame_Html::h3( __( 'Select a license', 'dhuett' ) );
            $content .= Mame_Html::p( __( 'This product needs a license. Please select a license to continue' ) );
            $content .= $this->get_license_fields( $licenses );
            $content .= Mame_Html::button( __( 'Continue', 'dhuett' ), [ 'id' => MAME_WS_PREFIX . '-' . $action, 'class' => MAME_WS_PREFIX . '-btn' ] );
            $content .= Mame_Html::button( __( 'Cancel', 'dhuett' ), [ 'class' => MAME_WS_PREFIX . '-btn ' . MAME_WS_PREFIX . '-close-overlay' ] );
            return $this->json_response->status( Mame_Json_Response::STATUS_SUCCESS )->html( '#' . MAME_WS_PREFIX . '-overlay .content', $content )->show( '#' . MAME_WS_PREFIX . '-overlay' );
        }

        public function json_order_overview( $wc_order_id = null )
        {
            $overview = Mame_Html::h3( __( 'Order the following product?', 'dhuett' ) );

            $draft_order = Webstamp_Database_Manager::get_draft_order( $wc_order_id );
            $product     = Webstamp_Database_Manager::get_unique_product( $draft_order->zone, $draft_order->delivery_method, $draft_order->format, $draft_order->additions );
            $overview    .= $this->product_overview( $product );
            $overview    .= Mame_Html::open_tag( 'div', [ 'id' => MAME_WS_PREFIX . '-order-buttons' ] );
            $overview    .= Mame_Html::button( __( 'Order', 'dhuett' ), [ 'id' => MAME_WS_PREFIX . '-order-confirm-btn', 'class' => MAME_WS_PREFIX . '-btn' ] );
            $overview    .= Mame_Html::button( __( 'Cancel', 'dhuett' ), [ 'class' => MAME_WS_PREFIX . '-close-overlay ' . MAME_WS_PREFIX . '-btn' ] );
            $overview    .= Mame_Html::close_tag( 'div' );
            return $this->json_response->status( Mame_Json_Response::STATUS_SUCCESS )->html( '#' . MAME_WS_PREFIX . '-overlay .content', $overview )->show( '#' . MAME_WS_PREFIX . '-overlay' );
        }

        /**
         * @param Webstamp_Product $product
         * @return string
         */
        private function product_overview( $product )
        {
            $overview = Mame_Html::open_tag( 'div', [ 'id' => MAME_WS_PREFIX . '-product-overview' ] );
            $overview .= $this->product_overview_row( __( 'Delivery', 'dhuett' ), $product->delivery );
            $overview .= $this->product_overview_row( __( 'Format', 'dhuett' ), $product->format );
            $overview .= $this->product_overview_row( __( 'Category', 'dhuett' ), $product->get_category()->name );
            $overview .= $this->product_overview_row( __( 'Zone', 'dhuett' ), $product->get_zone()->name );
            $overview .= $this->product_overview_row( __( 'Price', 'dhuett' ), $product->price );

            $additions = unserialize( $product->additions );
            if ( !empty( $additions ) ) {
                if ( is_array( $additions ) ) {
                    $overview .= $this->product_overview_row( __( 'Additions', 'dhuett' ), implode( ', ', Mame_Helper_Functions::object_array_map( $additions, 'name', 'name' ) ) );
                } else {
                    $overview .= $this->product_overview_row( __( 'Additions', 'dhuett' ), $additions->name );
                }
            }

            $overview .= Mame_Html::div( '', [ 'class' => 'clearfix' ] );
            $overview .= Mame_Html::close_tag( 'div' );
            return $overview;
        }

        private function product_overview_row( $key, $value )
        {
            return '<div class="overview-data"><div class="key">' . $key . '</div><div class="value">' . $value . '</div></div>';
        }

        /**
         * Sends a request for a preview order and saves the preview stamp. Returns a link to download the stamp.
         *
         * @param null $wc_order_id
         */
        public function preview_order( $wc_order_id = null )
        {
            $order_manager = new Webstamp_Order_Manager();
            // Get product.
            $draft_order = Webstamp_Database_Manager::get_draft_order( $wc_order_id );
            $product     = Webstamp_Database_Manager::get_unique_product( $draft_order->zone, $draft_order->delivery_method, $draft_order->format, $draft_order->additions );

            // Uses post_product_number.
            $draft_order->product = $product->post_product_number;
            $result               = $order_manager->new_order_preview( $draft_order );

            /*if (isset($result['error']))
                $this->json_response->respond();*/

            if ( is_array( $result ) && isset( $result[ 'error' ] ) )
                $this->json_response->errors( '#' . MAME_WS_PREFIX . '-overview-wrapper .errors', [ [ $result[ 'message' ] ] ] )->hide( '#' . MAME_WS_PREFIX . '-overlay' )->respond();
            //var_dump($result);die();

            // Get stamp
            if ( !empty ( $result->print_data ) ) {
                $stamp = $result->print_data;
            } elseif ( ( $result->stamps && ( $result->stamps->item ) && !empty( $result->stamps->item->print_data ) ) ) {
                $stamp = $result->stamps->item->print_data;
            }

            // Send preview PDF.
            if ( $stamp ) {

                $upload_dir  = wp_upload_dir();
                $upload_path = str_replace( '/', DIRECTORY_SEPARATOR, $upload_dir[ 'basedir' ] ) . DIRECTORY_SEPARATOR . 'webstamp' . DIRECTORY_SEPARATOR . 'temp' . DIRECTORY_SEPARATOR;

                $filename = Webstamp_Database_Manager::save_file( $stamp, $upload_path );
                if ( $filename ) {

                    $upload_url = str_replace( '/', DIRECTORY_SEPARATOR, $upload_dir[ 'baseurl' ] ) . DIRECTORY_SEPARATOR . 'webstamp' . DIRECTORY_SEPARATOR . 'temp' . DIRECTORY_SEPARATOR;

                    $link = add_query_arg( [ MAME_WS_PREFIX . '-type' => 'temp', MAME_WS_PREFIX . '-file' => $filename, MAME_WS_PREFIX . '-download' => '1' ], get_admin_url() );
                    do_action( MAME_WS_PREFIX . '_after_preview_order', $this->wc_order_id, $link );
                    return $this->json_response->status( true )->after( '#mame_ws-preview-btn', '<a id="' . MAME_WS_PREFIX . '-download-preview-btn" class="' . MAME_WS_PREFIX . '-btn" href="' . $link . '" target="_blank">' . __( 'Download preview', 'dhuett' ) . '</a>' )->hide( '#mame_ws-preview-btn' )->hide( '#' . MAME_WS_PREFIX . '-overlay' )->respond();
                }
                return $this->json_response->status( false )->respond();
            }
            /*
                        http_response_code(405);
                        exit();
                        */
        }

        /**
         * Sends a request for a new order. Returns a link to the saved stamp.
         *
         * @param null $wc_order_id
         */
        public function order( $wc_order_id = null )
        {
            $order_manager        = new Webstamp_Order_Manager();
            $draft_order          = Webstamp_Database_Manager::get_draft_order( $wc_order_id );
            $product              = Webstamp_Database_Manager::get_unique_product( $draft_order->zone, $draft_order->delivery_method, $draft_order->format, $draft_order->additions );
            $draft_order->product = $product->post_product_number;
            $result               = $order_manager->new_order( $draft_order );

            if ( is_array( $result ) && isset( $result[ 'error' ] ) )
                $this->json_response->errors( '#' . MAME_WS_PREFIX . '-overview-wrapper .errors', [ [ $result[ 'message' ] ] ] )->hide( '#' . MAME_WS_PREFIX . '-overlay' )->respond();

            if ( ( $result->stamps && ( $stamps = $result->stamps->item ) ) ) {

                $upload_dir  = wp_upload_dir();
                $upload_path = str_replace( '/', DIRECTORY_SEPARATOR, $upload_dir[ 'basedir' ] ) . DIRECTORY_SEPARATOR . 'webstamp' . DIRECTORY_SEPARATOR . 'stamps' . DIRECTORY_SEPARATOR;
                $options     = get_option( 'mame_ws_options_group' );
                $content     = '';
                $count       = 0;

                if ( !is_array( $stamps ) )
                    $stamps = [ $stamps ];

                foreach ( $stamps as $s ) {

                    $stamp = new Webstamp_Stamp();
                    $stamp->populate( $s );
                    $stamp->order_id = $draft_order->order_id;

                    if ( !empty ( $result->print_data ) )
                        $stamp->print_data = $result->print_data;

                    $stamp->file_name = Webstamp_Database_Manager::save_file( $stamp->print_data, $upload_path );;
                    Webstamp_Database_Manager::save_stamp( $stamp );

                    if ( MAME_WOO_TRACKING_ACTIVE ) {
                        if ( function_exists( 'wc_st_add_tracking_number' ) && $stamp->tracking_number ) {
                            wc_st_add_tracking_number( $draft_order->wc_order_id, $stamp->tracking_number, 'Swiss Post' );
                        }
                    }

                    $download_link = add_query_arg( [ MAME_WS_PREFIX . '-type' => 'stamps', MAME_WS_PREFIX . '-file' => $stamp->file_name, MAME_WS_PREFIX . '-download' => '1' ], get_admin_url() );
                    $print_link    = add_query_arg( [ MAME_WS_PREFIX . '-print' => 1, MAME_WS_PREFIX . '-type' => 'stamps', MAME_WS_PREFIX . '-file' => $stamp->file_name ], get_admin_url() );

                    if ( $options[ 'stamp_file_type' ] === 'pdf' )
                        $content = Mame_Html::open_tag( 'div', [ 'id' => MAME_WS_PREFIX . '-download-stamp-wrapper' ] );
                    else
                        $content .= Mame_Html::open_tag( 'div', [ 'id' => MAME_WS_PREFIX . '-download-stamp-wrapper' ] );
                    $content .= Mame_Html::img( plugins_url() . '/mame-webstamp-woocommerce/assets/images/stamp_download.png' );
                    $content .= Mame_Html::a( __( 'Download stamp', 'dhuett' ), $download_link, [ 'id' => MAME_WS_PREFIX . '-download-stamp-btn-' . $count, 'class' => MAME_WS_PREFIX . '-btn' ], '_blank' );
                    $content .= ' ' . Mame_Html::a( __( 'Print stamp', 'dhuett' ), $print_link, [ 'id' => MAME_WS_PREFIX . '-print-stamp-btn-' . $count, 'class' => MAME_WS_PREFIX . '-btn' ], '_blank' );
                    $content .= Mame_Html::close_tag( 'div' );
                    if ( isset( $options[ 'open_print_dialog' ] ) && $options[ 'open_print_dialog' ] )
                        $content .= '<script>jQuery(document).ready(function (){window.open(jQuery("#mame_ws-print-stamp-btn-' . $count . '").attr("href"));});</script>';
                    $count++;
                }

                $content = apply_filters( MAME_WS_PREFIX . '_stamp_download_screen', $content, $draft_order->wc_order_id );

                return $this->json_response
                    ->status( true )
                    ->replace( '#' . MAME_WS_PREFIX . '-content-wrapper', $content )
                    ->hide( '#mame_ws-order-btn' )
                    ->respond();
            }

            return $this->json_response->status( false )->respond();
        }

        /**
         * Returns the unique product from the draft order.
         *
         * @param null $wc_order_id
         * @return Webstamp_Product
         */
        public function get_product( $wc_order_id = null )
        {
            $draft_order = Webstamp_Database_Manager::get_draft_order( $wc_order_id );
            return Webstamp_Database_Manager::get_unique_product( $draft_order->zone, $draft_order->delivery_method, $draft_order->format, json_decode( $draft_order->additions ) );
        }

        /**
         * Adds populated category fields to the json response.
         *
         * @return Webstamp_Editor_Json_Response
         */
        public function json_category()
        {
            return $this->json_response->html( '#' . 　MAME_WS_PREFIX . '-category-wrapper', $this->get_category_fields() );
        }

        /**
         * Adds populated zone fields to the json response.
         *
         * @param $args
         * @return Webstamp_Editor_Json_Response
         */
        public function json_zone( $args )
        {
            $zone_fields = $this->get_zone_fields( $args[ 'category' ] );

            $zones = Webstamp_Database_Manager::get_zones();

            if ( !empty( $zones ) )
                $zone = $zones[ 0 ]->number;

            $this->json_response->html( '#' . MAME_WS_PREFIX . '-zone-wrapper', $zone_fields[ 'content' ] );
            if ( $zone_fields[ 'action' ] === 'continue' ) {
                $this->json_response->html( '#' . MAME_WS_PREFIX . '-overview-zone', $zone_fields[ 'zone_name' ] );
                return $this->json_delivery_method( [ 'zone' => Webstamp_Database_Manager::ZONE_DOMESTIC ] );
            } elseif ( isset( $zone ) )
                return $this->json_delivery_method( [ 'zone' => $zone ] );
            return $this->json_response;
        }

        /**
         * Adds populated delivery_method fields to the json response.
         *
         * @param $args
         * @return Webstamp_Editor_Json_Response
         */
        public function json_delivery_method( $args )
        {
            $delivery_methods = Webstamp_Database_Manager::get_product_delivery_methods( $args[ 'zone' ] );
            if ( !empty( $delivery_methods ) )
                $delivery_method = $delivery_methods[ 0 ]->delivery;
            if ( isset( $delivery_method ) )
                $this->json_format( [ 'zone' => $args[ 'zone' ], 'delivery_method' => $delivery_method ] );
            return $this->json_response->html( "#" . MAME_WS_PREFIX . '-delivery_method-wrapper', $this->get_delivery_methods( $args[ 'zone' ] ) );
        }

        /**
         * Adds populated format fields to the json response.
         *
         * @param $args
         * @return Webstamp_Editor_Json_Response
         */
        public function json_format( $args )
        {
            $formats = Webstamp_Database_Manager::get_product_formats( $args[ 'zone' ], $args[ 'delivery_method' ] );
            if ( !empty( $formats ) )
                $format = $formats[ 0 ]->format;
            if ( isset( $format ) )
                $this->json_addition( [ 'zone' => $args[ 'zone' ], 'delivery_method' => $args[ 'delivery_method' ], 'format' => $format ] );
            return $this->json_response->html( '#' . MAME_WS_PREFIX . '-format-wrapper', $this->get_product_formats( $args[ 'zone' ], $args[ 'delivery_method' ] ) );
        }

        /**
         * Adds populated addition fields to the json response.
         *
         * @param $args
         * @return Webstamp_Editor_Json_Response
         */
        public function json_addition( $args )
        {
            return $this->json_response->html( '#' . MAME_WS_PREFIX . '-addition-wrapper', $this->get_additions( $args[ 'zone' ], $args[ 'delivery_method' ], $args[ 'format' ] ) );
        }

        /**
         * Adds populated address fields to the json response.
         *
         * @return Webstamp_Editor_Json_Response
         */
        public function json_address()
        {
            return $this->json_response->html( '#' . MAME_WS_PREFIX . '-address-wrapper', $this->get_address_fields() );
        }

        /**
         * Adds populated media_type fields to the json response.
         *
         * @param $args
         * @return Webstamp_Editor_Json_Response
         */
        public function json_media_type( $args )
        {
            $media_types = Webstamp_Database_Manager::get_media_types_by_category( $args[ 'category' ] );
            if ( !empty( $media_types ) )
                $media_type = $media_types[ 0 ]->number;
            if ( isset( $media_type ) )
                $this->json_media( [ 'media_type' => $media_type ] );

            return $this->json_response->html( '#' . MAME_WS_PREFIX . '-media_type-wrapper', $this->get_media_type_fields( $args[ 'category' ] ) );
        }

        /**
         * Adds populated media fields to the json response.
         *
         * @param $args
         * @return Webstamp_Editor_Json_Response
         */
        public function json_media( $args )
        {
            return $this->json_response->html( '#' . MAME_WS_PREFIX . '-media-wrapper', $this->get_media_fields( $args[ 'media_type' ] ) );
        }

        public function json_media_startpos( $args )
        {
            if ( $args[ 'media_type' ] == Webstamp_Database_Manager::LABEL_ID_2 || $args[ 'media_type' ] == Webstamp_Database_Manager::LABEL_ID_1 ) {

                return $this->json_response->html( '#' . MAME_WS_PREFIX . '-media_startpos-wrapper', $this->get_media_startpos_fields() )->show( "#" . MAME_WS_PREFIX . '-media_startpos-wrapper' );
            }
            return $this->json_response->hide( "#" . MAME_WS_PREFIX . '-media_startpos-wrapper' );
        }

        /**
         * Adds populated license fields to the json response.
         *
         * @return Webstamp_Editor_Json_Response
         */
        public function json_license()
        {
            $content = $this->get_license_fields();
            if ( $content )
                return $this->json_response->html( '#' . MAME_WS_PREFIX . '-license_number-wrapper', $content );
            return $this->json_response;
        }

        public function json_country( $args )
        {
            $zone = null;
            if ( isset( $args[ 'zone' ] ) )
                $zone = $args[ 'zone' ];
            if ( isset( $args[ 'category' ] ) && $args[ 'category' ] == Webstamp_Database_Manager::CATEGORY_DOMESTIC )
                $zone = Webstamp_Database_Manager::ZONE_DOMESTIC;

            return $this->json_response->html( '#' . MAME_WS_PREFIX . '-country', $this->get_country_fields( $zone ) );
        }

        /**
         * Displays the category select field.
         */
        public function get_category_fields()
        {
            $categories_array = Mame_Helper_Functions::object_array_map( Webstamp_Database_Manager::get_categories(), 'number', 'name' );
            if ( isset( $this->template->category ) )
                $this->overview_data[ 'category' ] = $categories_array[ $this->template->category ];

            $content = Mame_Html::open_tag( 'div', [ 'id' => MAME_WS_PREFIX . '-step-category', 'class' => MAME_WS_PREFIX . '-step ' . MAME_WS_PREFIX . '-step-1', 'data-page' => 1 ] );
            $content .= Mame_Html::h2( __( 'Category', 'dhuett' ) );
            $content .= Mame_Html::select( MAME_WS_PREFIX . '-category', $categories_array, [ 'id' => MAME_WS_PREFIX . '-category', 'class' => MAME_WS_PREFIX . '-select-save', 'data-step' => 'category' ], ( isset( $this->template->category ) ? $this->template->category : null ) );
            $content .= Mame_Html::button( __( 'Save', 'dhuett' ), [ 'class' => MAME_WS_PREFIX . '-save-btn', 'data-step' => 'category' ] );
            $content .= Mame_Html::close_tag( 'div' );

            return $content;
        }

        /**
         * Displays the zone select field based on the category.
         * $category can have one of the following values:
         * - 1 : domestic letters
         * - 2 : international letters
         *
         * @param $category
         * @return array
         */
        public function get_zone_fields( $category )
        {
            if ( $category == Webstamp_Database_Manager::CATEGORY_DOMESTIC ) {
                $zone                          = Webstamp_Database_Manager::get_zone( Webstamp_Database_Manager::ZONE_DOMESTIC );
                $this->overview_data[ 'zone' ] = $zone->name;
                // Domestic letter
                return [ 'content' => Mame_Html::input( 'hidden', MAME_WS_PREFIX . '-zone', Webstamp_Database_Manager::ZONE_DOMESTIC ), 'action' => 'continue', 'zone_name' => $zone->name ];
            }

            $zone_array = Mame_Helper_Functions::object_array_map( Webstamp_Database_Manager::get_zones(), 'number', 'name' );
            if ( isset( $this->template->zone ) )
                $this->overview_data[ 'zone' ] = $zone_array[ $this->template->zone ];

            // International letter
            $content = Mame_Html::open_tag( 'div', [ 'id' => MAME_WS_PREFIX . '-step-zone', 'class' => MAME_WS_PREFIX . '-step ' . MAME_WS_PREFIX . '-step-1', 'data-page' => 1 ] );
            $content .= Mame_Html::h2( __( 'Zone', 'dhuett' ) );
            $content .= Mame_Html::select( MAME_WS_PREFIX . '-zone', $zone_array, [ 'id' => MAME_WS_PREFIX . '-zone', 'class' => MAME_WS_PREFIX . '-select-save', 'data-step' => 'zone' ], ( isset( $this->template->zone ) ? $this->template->zone : null ) );
            $content .= Mame_Html::button( __( 'Save', 'dhuett' ), [ 'class' => MAME_WS_PREFIX . '-save-btn', 'data-step' => 'zone' ] );
            $content .= Mame_Html::close_tag( 'div' );

            return [ 'content' => $content, 'action' => 'none' ];

        }

        public function get_delivery_methods( $zone = null )
        {
            $content = Mame_Html::open_tag( 'div', [ 'id' => MAME_WS_PREFIX . '-step-delivery_method', 'class' => MAME_WS_PREFIX . '-step ' . MAME_WS_PREFIX . '-step-1', 'data-page' => 2 ] );
            $content .= Mame_Html::h2( __( 'Delivery method', 'dhuett' ) );

            if ( !empty( $zone ) ) {

                $content .= Mame_Html::select( MAME_WS_PREFIX . '-delivery_method', Mame_Helper_Functions::object_array_map( Webstamp_Database_Manager::get_product_delivery_methods( $zone ), 'delivery', 'delivery' ), [ 'id' => MAME_WS_PREFIX . '-delivery_method', 'class' => MAME_WS_PREFIX . '-step ' . MAME_WS_PREFIX . '-step-2', 'class' => MAME_WS_PREFIX . '-select-save', 'data-step' => 'delivery_method' ], ( isset( $this->template->delivery_method ) ? $this->template->delivery_method : null ) );
                $content .= Mame_Html::button( __( 'Save', 'dhuett' ), [ 'class' => MAME_WS_PREFIX . '-save-btn', 'data-step' => 'delivery_method' ] );
            } else {
                $content .= Mame_Html::p( __( 'Select zone to show delivery methods.', 'dhuett' ) );
            }

            $content .= Mame_Html::close_tag( 'div' );
            return $content;
        }

        public function get_product_fields( $zone )
        {
            $content = Mame_Html::open_tag( 'div', [ 'id' => MAME_WS_PREFIX . '-step-product' ] );
            $content .= Mame_Html::h2( __( 'Product', 'dhuett' ) );
            $content .= $this->get_delivery_methods( $zone );
            $content .= Mame_Html::div( '', [ 'id' => MAME_WS_PREFIX . '-product_formats' ] );
            $content .= Mame_Html::div( '', [ 'id' => MAME_WS_PREFIX . '-product_product' ] );
            $content .= Mame_Html::div( '', [ 'id' => MAME_WS_PREFIX . '-product_additions' ] );
            $content .= Mame_Html::close_tag( 'div' );

            return $content;
        }

        public function get_product_formats( $zone = null, $delivery_method = null )
        {
            $content = Mame_Html::open_tag( 'div', [ 'id' => MAME_WS_PREFIX . '-step-format', 'class' => MAME_WS_PREFIX . '-step ' . MAME_WS_PREFIX . '-step-1', 'data-page' => 2 ] );
            $content .= Mame_Html::h2( __( 'Formats', 'dhuett' ) );

            if ( empty( $zone ) ) {
                $content .= Mame_Html::p( __( 'Select zone to show formats.', 'dhuett' ) );
            } elseif ( empty( $delivery_method ) ) {
                $content .= Mame_Html::p( __( 'Select delivery method to show formats.', 'dhuett' ) );
            } else {
                $content .= Mame_Html::select( MAME_WS_PREFIX . '-format', Mame_Helper_Functions::object_array_map( Webstamp_Database_Manager::get_product_formats( $zone, $delivery_method ), 'format', 'format' ), [ 'id' => MAME_WS_PREFIX . '-format', 'class' => MAME_WS_PREFIX . '-select-save', 'data-step' => 'format' ], ( isset( $this->template->format ) ? $this->template->format : null ) );
                $content .= Mame_Html::button( __( 'Save', 'dhuett' ), [ 'class' => MAME_WS_PREFIX . '-save-btn', 'data-step' => 'format' ] );
            }
            $content .= Mame_Html::close_tag( 'div' );
            return $content;
        }

        public function get_products( $zone, $delivery_method, $format )
        {
            return Mame_Html::div( Mame_Html::select( 'product', Mame_Helper_Functions::object_array_map( Webstamp_Database_Manager::get_product( $zone, $delivery_method, $format ), 'number', 'name' ) ), [ 'id' => MAME_WS_PREFIX . '-product' ] );
        }

        public function get_additions( $zone = null, $delivery_method = null, $format = null )
        {
            $additions = Webstamp_Database_Manager::get_product_additions( $zone, $delivery_method, $format );

            $additions = array_map( function ( $a ) {
                return [ 'name' => MAME_WS_PREFIX . '-additions', 'value' => $a->name, 'label' => $a->name ];
            }, $additions );
            $content   = Mame_Html::open_tag( 'div', [ 'id' => MAME_WS_PREFIX . '-step-addition', 'class' => MAME_WS_PREFIX . '-step ' . MAME_WS_PREFIX . '-step-1', 'data-page' => 2 ] );
            $content   .= Mame_Html::h2( __( 'Additions', 'dhuett' ) );
            //  $content .= Mame_Html::div( Mame_Html::select( MAME_WS_PREFIX . '-addition', Mame_Helper_Functions::object_array_map( Webstamp_Database_Manager::get_product_additions( $zone, $delivery_method, $format ), 'name', 'name' ) ), [ 'id' => MAME_WS_PREFIX . '-additions' ] );
            if ( empty( $zone ) ) {
                $content .= Mame_Html::p( __( 'Select zone to show additions.', 'dhuett' ) );
            } elseif ( empty( $delivery_method ) ) {
                $content .= Mame_Html::p( __( 'Select delivery method to show additions.', 'dhuett' ) );
            } elseif ( empty( $format ) ) {
                $content .= Mame_Html::p( __( 'Select format to show additions.', 'dhuett' ) );
            } else {
                $content .= Mame_Html::checkbox_group( $additions, [ 'id' => MAME_WS_PREFIX . '-additions' ], ( $this->template->additions ? $this->template->additions : null ) );
                $content .= Mame_Html::button( __( 'Save', 'dhuett' ), [ 'class' => MAME_WS_PREFIX . '-save-btn', 'data-step' => 'addition' ] );
            }
            $content .= Mame_Html::close_tag( 'div' );

            return $content;
        }

        public function get_country_fields( $zone )
        {
            $countries = null;

            if ( $zone )
                $countries = Webstamp_Database_Manager::get_countries_by_zone( $zone );
            elseif ( isset( $this->template->zone ) && $this->template->zone )
                $countries = Webstamp_Database_Manager::get_countries_by_zone( $this->template->zone );

            $countries_array = [];
            if ( $countries ) {
                $countries_array = Mame_WP_Helper::object_array_map( $countries, 'alias', 'name' );
            }
            $content = Mame_Html::label( __( 'Country', 'dhuett' ) );
            $content .= Mame_Html::select( MAME_WS_PREFIX . '-country', $countries_array, [], ( $this->address && isset( $this->address->country ) ? $this->address->country : null ) );
            $content .= Mame_Html::div( '', [ 'class' => 'clearfix' ] );

            return $content;
        }

        public function get_address_fields( $sender = false, $zone = null )
        {
            $addition = $sender ? '-sender' : '';
            $content  = Mame_Html::input( 'text', MAME_WS_PREFIX . $addition . '-organization', ( $sender ? ( $this->sender_address && isset( $this->sender_address->organization ) ? $this->sender_address->organization : null ) : ( $this->address && isset( $this->address->organization ) ? $this->address->organization : null ) ), [ 'placeholder' => __( 'Organization', 'dhuett' ) ] );
            $content  .= Mame_Html::input( 'text', MAME_WS_PREFIX . $addition . '-company_addition', ( $sender ? ( $this->sender_address && isset( $this->sender_address->company_addition ) ? $this->sender_address->company_addition : null ) : ( $this->address && isset( $this->address->company_addition ) ? $this->address->company_addition : null ) ), [ 'placeholder' => __( 'Company addition', 'dhuett' ) ] );
            $content  .= Mame_Html::input( 'text', MAME_WS_PREFIX . $addition . '-title', ( $sender ? ( $this->sender_address && isset( $this->sender_address->title ) ? $this->sender_address->title : null ) : ( $this->address && isset( $this->address->title ) ? $this->address->title : null ) ), [ 'placeholder' => __( 'Title', 'dhuett' ) ] );
            $content  .= Mame_Html::input( 'text', MAME_WS_PREFIX . $addition . '-firstname', ( $sender ? ( $this->sender_address && isset( $this->sender_address->firstname ) ? $this->sender_address->firstname : null ) : ( $this->address && isset( $this->address->firstname ) ? $this->address->firstname : null ) ), [ 'placeholder' => __( 'First name', 'dhuett' ) ] );
            $content  .= Mame_Html::input( 'text', MAME_WS_PREFIX . $addition . '-lastname', ( $sender ? ( $this->sender_address && isset( $this->sender_address->lastname ) ? $this->sender_address->lastname : null ) : ( $this->address && isset( $this->address->lastname ) ? $this->address->lastname : null ) ), [ 'placeholder' => __( 'Last name', 'dhuett' ) ] );
            $content  .= Mame_Html::input( 'text', MAME_WS_PREFIX . $addition . '-addition', ( $sender ? ( $this->sender_address && isset( $this->sender_address->addition ) ? $this->sender_address->addition : null ) : ( $this->address && isset( $this->address->addition ) ? $this->address->addition : null ) ), [ 'placeholder' => __( 'Addition', 'dhuett' ) ] );
            $content  .= Mame_Html::input( 'text', MAME_WS_PREFIX . $addition . '-street', ( $sender ? ( $this->sender_address && isset( $this->sender_address->street ) ? $this->sender_address->street : null ) : ( $this->address && isset( $this->address->street ) ? $this->address->street : null ) ), [ 'placeholder' => __( 'Street', 'dhuett' ) ] );
            $content  .= Mame_Html::input( 'text', MAME_WS_PREFIX . $addition . '-pobox', ( $sender ? ( $this->sender_address && isset( $this->sender_address->pobox ) ? $this->sender_address->pobox : null ) : ( $this->address && isset( $this->address->pobox ) ? $this->address->pobox : null ) ), [ 'placeholder' => __( 'Postbox', 'dhuett' ) ] );
            $content  .= Mame_Html::input( 'text', MAME_WS_PREFIX . $addition . '-pobox_nr', ( $sender ? ( $this->sender_address && isset( $this->sender_address->pobox_nr ) ? $this->sender_address->pobox_nr : null ) : ( $this->address && isset( $this->address->pobox_nr ) ? $this->address->postbox_nr : null ) ), [ 'placeholder' => __( 'Postbox number', 'dhuett' ) ] );
            $content  .= Mame_Html::input( 'text', MAME_WS_PREFIX . $addition . '-zip', ( $sender ? ( $this->sender_address && isset( $this->sender_address->zip ) ? $this->sender_address->zip : null ) : ( $this->address && isset( $this->address->zip ) ? $this->address->zip : null ) ), [ 'placeholder' => __( 'ZIP', 'dhuett' ) ] );
            $content  .= Mame_Html::input( 'text', MAME_WS_PREFIX . $addition . '-city', ( $sender ? ( $this->sender_address && isset( $this->sender_address->city ) ? $this->sender_address->city : null ) : ( $this->address && isset( $this->address->city ) ? $this->address->city : null ) ), [ 'placeholder' => __( 'City', 'dhuett' ) ] );
            if ( !$sender )
                $content .= Mame_Html::input( 'text', MAME_WS_PREFIX . $addition . '-cash_on_delivery_amount', ( $sender ? null : ( $this->address && isset( $this->address->cash_on_delivery_amount ) ? $this->address->cash_on_delivery_amount : null ) ), [ 'placeholder' => __( 'Cash on delivery amount', 'dhuett' ) ] );

            $content .= Mame_Html::open_tag( 'div', [ 'id' => MAME_WS_PREFIX . $addition . '-country' ] );

            if ( $sender ) {
                $content .= Mame_Html::input( 'text', MAME_WS_PREFIX . $addition . '-country', ( $sender ? ( $this->sender_address && isset( $this->sender_address->country ) ? $this->sender_address->country : null ) : ( $this->address && isset( $this->address->country ) ? $this->address->country : null ) ), [ 'placeholder' => __( 'Country', 'dhuett' ) ] );
            } else {
                $content .= $this->get_country_fields( $zone );
            }

            $content .= Mame_Html::close_tag( 'div' );

            return $content;
        }

        public function receiver_address_fields()
        {
            if ( $this->address )
                $this->overview_data[ 'address' ] = $this->address->get_html();

            $content = Mame_Html::open_tag( 'div', [ 'id' => MAME_WS_PREFIX . '-step-address', 'data-page' => 3 ] );
            $content .= Mame_Html::h2( __( 'Address', 'dhuett' ) );

            $content .= $this->get_address_fields();

            if ( !empty( $this->wc_order_id ) ) {
                $content .= Mame_Html::button( __( 'Copy from billing address', 'dhuett' ), [ 'id' => MAME_WS_PREFIX . '-address-copy-billing-btn', 'class' => MAME_WS_PREFIX . '-btn', 'data-step' => 'address' ] );
                $content .= Mame_Html::button( __( 'Copy from shipping address', 'dhuett' ), [ 'id' => MAME_WS_PREFIX . '-address-copy-shipping-btn', 'class' => MAME_WS_PREFIX . '-btn', 'data-step' => 'address' ] );
                $content .= Mame_Html::button( __( 'Copy from custom assignment', 'dhuett' ), [ 'id' => MAME_WS_PREFIX . '-address-copy-custom-btn', 'class' => MAME_WS_PREFIX . '-btn', 'data-step' => 'address' ] );
            }
            $content .= Mame_Html::close_tag( 'div' );

            return $content;
        }

        public function sender_address_fields()
        {
            if ( $this->sender_address )
                $this->overview_data[ 'sender' ] = $this->sender_address->get_html();

            $content = Mame_Html::open_tag( 'div', [ 'id' => MAME_WS_PREFIX . '-step-sender', 'data-page' => 3 ] );
            $content .= Mame_Html::h2( __( 'Sender address', 'dhuett' ) );
            $content .= $this->get_address_fields( true );
            $content .= Mame_Html::button( __( 'Load default sender address', 'dhuett' ), [ 'id' => MAME_WS_PREFIX . '-load-sender-btn', 'class' => MAME_WS_PREFIX . '-btn' ] );
            $content .= Mame_Html::close_tag( 'div' );

            return $content;
        }

        public function get_media_type_fields( $category = null )
        {
            $media_type_array = Mame_Helper_Functions::object_array_map( Webstamp_Database_Manager::get_media_types_by_category( $category ), 'number', 'name' );
            if ( isset( $this->template->media_type ) )
                $this->overview_data[ 'media_type' ] = $media_type_array[ $this->template->media_type ];

            $content = Mame_Html::open_tag( 'div', [ 'id' => MAME_WS_PREFIX . '-step-media_type', 'data-page' => 1 ] );
            $content .= Mame_Html::h2( __( 'Media type', 'dhuett' ) );
            if ( empty( $category ) ) {

                $content .= Mame_Html::p( __( 'Select category to show media types.', 'dhuett' ) );
            } else {
                $content .= Mame_Html::select( MAME_WS_PREFIX . '-media_type', $media_type_array, [ 'id' => MAME_WS_PREFIX . '-media_type', 'class' => MAME_WS_PREFIX . '-select-save', 'data-step' => 'media_type' ], ( isset( $this->template->media_type ) ? $this->template->media_type : null ) );
                $content .= Mame_Html::button( __( 'Save', 'dhuett' ), [ 'class' => MAME_WS_PREFIX . '-save-btn', 'data-step' => 'media_type' ] );
            }
            $content .= Mame_Html::close_tag( 'div' );

            return $content;
        }

        public function get_media_fields( $media_type = null )
        {
            $media_array = Mame_Helper_Functions::object_array_map( Webstamp_Database_Manager::get_medias_by_media_type( $media_type ), 'number', 'name' );
            if ( isset( $this->template->media ) )
                $this->overview_data[ 'media' ] = $media_array[ $this->template->media ];

            $content = Mame_Html::open_tag( 'div', [ 'id' => MAME_WS_PREFIX . '-step-media', 'data-page' => 1 ] );
            $content .= Mame_Html::h2( __( 'Media', 'dhuett' ) );
            if ( empty( $media_type ) ) {
                $content .= Mame_Html::p( __( 'Select media type to show media.', 'dhuett' ) );
            } else {
                $content .= Mame_Html::select( MAME_WS_PREFIX . '-media', $media_array, [ 'id' => MAME_WS_PREFIX . '-media', 'class' => MAME_WS_PREFIX . '-select-save', 'data-step' => 'media' ], ( isset( $this->template->media ) ? $this->template->media : null ) );
                $content .= Mame_Html::button( __( 'Save', 'dhuett' ), [ 'class' => MAME_WS_PREFIX . '-save-btn', 'data-step' => 'media' ] );
            }
            $content .= Mame_Html::close_tag( 'div' );

            return $content;
        }

        public function get_license_fields( $licenses = null )
        {
            if ( !$licenses )
                $licenses = Webstamp_Database_Manager::get_licenses();
            if ( !empty( $licenses ) ) {
                $content = Mame_Html::open_tag( 'div', [ 'id' => MAME_WS_PREFIX . '-step-license_number', 'data-page' => 5 ] );
                $content .= Mame_Html::h2( __( 'License', 'dhuett' ) );
                $content .= Mame_Html::select_with_string_keys( MAME_WS_PREFIX . '-license_number', array_merge( [ [ 'key' => '', 'value' => __( 'None', 'dhuett' ) ] ], Mame_Helper_Functions::object_array_map_string_keys( $licenses, 'number', 'number' ) ), [ 'id' => MAME_WS_PREFIX . '-license_number' ], ( isset( $this->template->license_number ) ? $this->template->license_number : null ) );
                $content .= Mame_Html::button( __( 'Save', 'dhuett' ), [ 'class' => MAME_WS_PREFIX . '-save-btn', 'data-step' => 'license_number' ] );
                $content .= Mame_Html::close_tag( 'div' );

                return $content;
            }
            return null;
        }

        public function get_media_startpos_fields()
        {
            $content = Mame_Html::open_tag( 'div', [ 'id' => MAME_WS_PREFIX . '-step-media_startpos', 'data-page' => 5 ] );
            $content .= Mame_Html::h2( __( 'Label start position', 'dhuett' ) );
            $content .= Mame_Html::number( MAME_WS_PREFIX . '-media_startpos', ( isset( $this->template->media_startpos ) ? $this->template->media_startpos : null ) );
            $content .= Mame_Html::button( __( 'Save', 'dhuett' ), [ 'class' => MAME_WS_PREFIX . '-save-btn', 'data-step' => 'media_startpos' ] );
            $content .= Mame_Html::close_tag( 'div' );

            return $content;
        }

        public function get_quantity_fields()
        {
            $content = Mame_Html::open_tag( 'div', [ 'id' => MAME_WS_PREFIX . '-step-quantity', 'data-page' => 5 ] );
            $content .= Mame_Html::h2( __( 'Quantity', 'dhuett' ) );
            $content .= Mame_Html::number( MAME_WS_PREFIX . '-quantity', ( $this->template->quantity ? $this->template->quantity : null ) );
            $content .= Mame_Html::close_tag( 'div' );

            return $content;
        }

        public function get_image_fields()
        {
            if ( !$this->template->image_id ) {
                $img_upload_field = Mame_WP_Helper::image_upload_field( 'mame_ws_options_group', 'image', null, 200, plugins_url( '../../assets/images/stamp_white.png', __FILE__ ) );
            } else {
                $img_upload_field = $this->image_upload_field( 'image_id', '', null, 200, plugins_url( '../../assets/images/stamp_white.png', __FILE__ ) );
            }
            $content          = Mame_Html::open_tag( 'div', [ 'id' => MAME_WS_PREFIX . '-step-image', 'data-page' => 1 ] );
            $content          .= Mame_Html::h2( __( 'Image', 'dhuett' ) );
            $checkbox_options = $this->template->image_id ? [ 'checked' => 'checked' ] : null;
            $content          .= Mame_Html::div( Mame_Html::checkbox( __( 'Use image', 'dhuett' ), MAME_WS_PREFIX . '-use_image', 1, $checkbox_options ), [ 'id' => MAME_WS_PREFIX . '-use_image-wrapper', 'class' => MAME_WS_PREFIX . '-data-wrapper', 'data-page' => 1 ] );
            $content          .= Mame_Html::div( $img_upload_field, [ 'id' => MAME_WS_PREFIX . '-image-wrapper', 'class' => MAME_WS_PREFIX . '-data-wrapper' . ( $this->template->image_id ? '' : ' hidden' ), 'data-page' => 1 ] );
            $content          .= Mame_Html::button( __( 'Save', 'dhuett' ), [ 'class' => MAME_WS_PREFIX . '-save-btn', 'data-step' => 'image_id' ] );
            $content          .= Mame_Html::close_tag( 'div' );
            return $content;
        }

        protected function image_upload_field( $options_name, $name, $width, $height, $default_image )
        {
            if ( !empty( $this->template->image_id ) ) {
                $sizes = [];
                if ( !empty( $width ) )
                    $sizes[] = $width;
                if ( !empty( $height ) )
                    $sizes[] = $height;
                $image_attributes = wp_get_attachment_image_src( $this->template->image_id, array( $width, $height ) );
                $src              = $image_attributes[ 0 ];
                $value            = $this->template->image_id;
            } else {
                $src   = $default_image;
                $value = '';
            }

            $text = __( 'Upload', 'dhuett' );

            $width_str  = empty( $width ) ? 'auto' : $width . ' px';
            $height_str = empty( $height ) ? 'auto' : $height . ' px';

            return '
        <div class="upload">
            <img data-src="' . $default_image . '" src="' . $src . '" width="' . $width_str . '" height="' . $height_str . '" />
            <div>
                <input type="hidden" name="image_id" id="' . MAME_WS_PREFIX . '-image_id" value="' . $value . '" />
                <button type="submit" class="upload_image_button button">' . $text . '</button>
                <button type="submit" class="remove_image_button button">&times;</button>
            </div>
        </div>
    ';
        }

        public function load_start_screen()
        {
            ?>
            <div id="<?= MAME_WS_PREFIX ?>-editor-wrapper">
                <div id="<?= MAME_WS_PREFIX ?>-editor" data-step="category"
                     data-page="1" <?= $this->wc_order_id ? 'data-wc-order-id="' . $this->wc_order_id . '"' : '' ?>>
                    <div id="<?= MAME_WS_PREFIX ?>-content-wrapper">

                        <div id="<?= MAME_WS_PREFIX ?>-content">
                            <h1><span><?= __( 'WebStamp Editor' ) ?></span></h1>

                            <form action="" method="post">
                                <div id="<?= MAME_WS_PREFIX ?>-steps">

                                    <?php
                                    $templates   = Webstamp_Database_Manager::get_templates();
                                    $draft_order = Webstamp_Database_Manager::get_draft_order( $this->wc_order_id );

                                    if ( !$templates && !$draft_order ) {
                                        $this->load_editor_content();
                                    } else {
                                        if ( $draft_order ) {
                                            ?>
                                            <div class="<?= MAME_WS_PREFIX ?>-start-content">
                                                <h3><?= __( 'There is an unfinished order', 'dhuett' ) ?></h3>
                                                <p><?= __( 'Do you want to continue with the order?', 'dhuett' ) ?></p>
                                                <button id="<?= MAME_WS_PREFIX ?>-continue-btn"
                                                        class="<?= MAME_WS_PREFIX ?>-btn"
                                                        data-order_id="<?= $draft_order->id ?>"><?= __( 'Continue', 'dhuett' ) ?></button>
                                            </div>
                                            <?php
                                        }

                                        if ( $templates ) {
                                            ?>
                                            <div class="<?= MAME_WS_PREFIX ?>-start-content">
                                                <h3><?= __( 'Start from template', 'dhuett' ) ?></h3>
                                                <p><?= __( 'Choose a template', 'dhuett' ) ?></p>
                                                <?= Mame_Html::select( MAME_WS_PREFIX . '-load-template', Mame_WP_Helper::object_array_map( $templates, 'id', 'name' ), [ 'id' => MAME_WS_PREFIX . '-load-template' ] ) ?>
                                                <button id="<?= MAME_WS_PREFIX ?>-load-template-btn"
                                                        class="<?= MAME_WS_PREFIX ?>-btn"><?= __( 'Load', 'dhuett' ) ?></button>
                                            </div>
                                            <?php

                                        }
                                        ?>
                                        <div class="<?= MAME_WS_PREFIX ?>-start-content">
                                            <h3><?= __( 'New order', 'dhuett' ) ?></h3>
                                            <button id="<?= MAME_WS_PREFIX ?>-new-order-btn"
                                                    class="<?= MAME_WS_PREFIX ?>-btn"><?= __( 'Start', 'dhuett' ) ?></button>
                                        </div>
                                        <?php
                                    }

                                    ?>

                                </div>
                            </form>
                        </div>
                        <div id="<?= MAME_WS_PREFIX ?>-overlay" class="<?= MAME_WS_PREFIX ?>-overlay">
                            <div class="content">
                                <h3><?= __( 'There is an unfinished order', 'dhuett' ) ?></h3>
                                <p><?= __( 'Do you want to continue with the order?', 'dhuett' ) ?></p>
                                <button id="<?= MAME_WS_PREFIX ?>-continue-btn"
                                        class="<?= MAME_WS_PREFIX ?>-btn"><?= __( 'Continue', 'dhuett' ) ?></button>
                                <button id="<?= MAME_WS_PREFIX ?>-new-order-btn"
                                        class="<?= MAME_WS_PREFIX ?>-btn"><?= __( 'Start new order', 'dhuett' ) ?></button>
                            </div>
                        </div>
                        <div id="<?= MAME_WS_PREFIX ?>-loader" class="<?= MAME_WS_PREFIX ?>-loader">
                            <div class="content"><img
                                        src="<?= plugin_dir_url( dirname( dirname( __FILE__ ) ) ) . 'assets/images/loader-5.gif' ?>">
                            </div>
                        </div>
                    </div>
                </div>

            </div>
            <script>
                jQuery(document).ready(function ($) {
                    new MameWsEditor();
                });
            </script>
            <?php
        }

        public function load_editor( $page = 1 )
        {
            $errors = false;
            if ( $this->autoload_draft && $this->load_order_overview_directly )
                $errors = $this->validate( $this->wc_order_id );

            $templates   = Webstamp_Database_Manager::get_templates();
            $draft_order = Webstamp_Database_Manager::get_draft_order( $this->wc_order_id );
            ?>
            <div id="<?= MAME_WS_PREFIX ?>-editor-wrapper">
                <div id="<?= MAME_WS_PREFIX ?>-editor" data-step="category"
                     data-page="1" <?= $this->wc_order_id ? 'data-wc-order-id="' . $this->wc_order_id . '"' : '' ?>>
                    <div id="<?= MAME_WS_PREFIX ?>-content-wrapper">

                        <div id="<?= MAME_WS_PREFIX ?>-content">
                            <h1><span><?= __( 'WebStamp Editor' ) ?></span></h1>
                            <div id="<?= MAME_WS_PREFIX ?>-steps-nav"
                                 class="<?= MAME_WS_PREFIX ?>-editor-content" <?= !$templates && !$draft_order || $this->autoload_draft ? 'style="display:block !important;"' : '' ?>>
                                <div id="<?= MAME_WS_PREFIX ?>-nav-step-wrapper-1"
                                     class="<?= MAME_WS_PREFIX ?>-nav-step-wrapper <?= $page === 1 ? 'active' : ''; ?>"
                                     data-page="1">
                                    <div class="<?= MAME_WS_PREFIX ?>-nav-step">
                                        <img src="<?= plugins_url( '../../assets/images/stamp_gray_s.png', __FILE__ ) ?>">
                                        <div class="<?= MAME_WS_PREFIX ?>-number"><p>1</p></div>
                                    </div>
                                    <p class="text"><span><?= __( 'Stamp', 'dhuett' ) ?></span></p>
                                </div>
                                <div id="<?= MAME_WS_PREFIX ?>-nav-step-wrapper-2"
                                     class="<?= MAME_WS_PREFIX ?>-nav-step-wrapper <?= $page === 2 ? 'active' : ''; ?>"
                                     data-page="2">
                                    <div class="<?= MAME_WS_PREFIX ?>-nav-step">
                                        <img src="<?= plugins_url( '../../assets/images/receiver_address_s.png', __FILE__ ) ?>">
                                        <div class="<?= MAME_WS_PREFIX ?>-number"><p>2</p></div>
                                    </div>
                                    <p class="text"><span><?= __( 'Receiver address', 'dhuett' ) ?></span></p>
                                </div>
                                <div id="<?= MAME_WS_PREFIX ?>-nav-step-wrapper-3"
                                     class="<?= MAME_WS_PREFIX ?>-nav-step-wrapper <?= $page === 3 ? 'active' : ''; ?>"
                                     data-page="3">
                                    <div class="<?= MAME_WS_PREFIX ?>-nav-step">
                                        <img src="<?= plugins_url( '../../assets/images/sender_address_s.png', __FILE__ ) ?>">
                                        <div class="<?= MAME_WS_PREFIX ?>-number"><p>3</p></div>
                                    </div>
                                    <p class="text"><span><?= __( 'Sender address', 'dhuett' ) ?></span></p>
                                </div>
                                <div id="<?= MAME_WS_PREFIX ?>-nav-step-wrapper-4"
                                     class="<?= MAME_WS_PREFIX ?>-nav-step-wrapper <?= $page === 4 ? 'active' : ''; ?>"
                                     data-page="4">
                                    <div class="<?= MAME_WS_PREFIX ?>-nav-step">
                                        <img src="<?= plugins_url( '../../assets/images/overview_s.png', __FILE__ ) ?>">
                                        <div class="<?= MAME_WS_PREFIX ?>-number"><p>4</p></div>
                                    </div>
                                    <p class="text"><span><?= __( 'Overview', 'dhuett' ) ?></span></p>
                                </div>
                                <div class="clearfix"></div>
                            </div>
                            <form action="" method="post">
                                <?php


                                if ( !$templates && !$draft_order || $this->autoload_draft ) {
                                    ?>
                                    <div id="<?= MAME_WS_PREFIX ?>-steps"
                                         class="<?= MAME_WS_PREFIX ?>-form-content <?= MAME_WS_PREFIX ?>-editor-content"
                                         style="display:block !important;">
                                        <?php
                                        echo $this->load_editor_form_content( $page, $errors );
                                        ?>
                                    </div>
                                    <?php
                                } else {

                                    ?>
                                    <div id="<?= MAME_WS_PREFIX ?>-steps"
                                         class="<?= MAME_WS_PREFIX ?>-form-content <?= MAME_WS_PREFIX ?>-editor-content"></div>
                                    <div id="<?= MAME_WS_PREFIX ?>-start"
                                         class="<?= MAME_WS_PREFIX ?>-form-content">
                                        <?php
                                        if ( $draft_order ) {
                                            ?>
                                            <div class="<?= MAME_WS_PREFIX ?>-start-content">
                                                <h3><?= __( 'There is an unfinished order', 'dhuett' ) ?></h3>
                                                <p><?= __( 'Do you want to continue with the order?', 'dhuett' ) ?></p>
                                                <button id="<?= MAME_WS_PREFIX ?>-continue-btn"
                                                        class="<?= MAME_WS_PREFIX ?>-btn"
                                                        data-order_id="<?= $draft_order->id ?>"><?= __( 'Continue', 'dhuett' ) ?></button>
                                            </div>
                                            <?php
                                        }

                                        if ( $templates ) {
                                            ?>
                                            <div class="<?= MAME_WS_PREFIX ?>-start-content">
                                                <h3><?= __( 'Start from template', 'dhuett' ) ?></h3>
                                                <p><?= __( 'Choose a template', 'dhuett' ) ?></p>
                                                <?= Mame_Html::select( MAME_WS_PREFIX . '-load-template', Mame_WP_Helper::object_array_map( $templates, 'id', 'name' ), [ 'id' => MAME_WS_PREFIX . '-load-template' ] ) ?>
                                                <button id="<?= MAME_WS_PREFIX ?>-load-template-btn"
                                                        class="<?= MAME_WS_PREFIX ?>-btn"><?= __( 'Load', 'dhuett' ) ?></button>
                                            </div>
                                            <?php

                                        }
                                        ?>
                                        <div class="<?= MAME_WS_PREFIX ?>-start-content">
                                            <h3><?= __( 'New order', 'dhuett' ) ?></h3>
                                            <p><?= __( 'Start a new order', 'dhuett' ) ?></p>
                                            <button id="<?= MAME_WS_PREFIX ?>-new-order-btn"
                                                    class="<?= MAME_WS_PREFIX ?>-btn"><?= __( 'Start', 'dhuett' ) ?></button>
                                        </div>
                                    </div>
                                    <?php
                                }
                                ?>

                                <div class="<?= MAME_WS_PREFIX ?>-editor-content" <?= !$templates && !$draft_order || $this->autoload_draft ? 'style="display:block !important;"' : '' ?>>
                                    <?= Mame_Html::button( __( 'Back', 'dhuett' ), [ 'id' => MAME_WS_PREFIX . '-prev-btn', 'class' => MAME_WS_PREFIX . '-btn hidden' ] ); ?>
                                    <?= Mame_Html::button( __( 'Next', 'dhuett' ), [ 'id' => MAME_WS_PREFIX . '-next-btn', 'class' => MAME_WS_PREFIX . '-btn' ] ); ?>
                                </div>
                            </form>
                        </div>
                        <div id="<?= MAME_WS_PREFIX ?>-overlay"
                             class="<?= MAME_WS_PREFIX ?>-overlay" <?= $this->autoload_draft && $this->load_order_overview_directly && !$errors ? 'style="display:block"' : '' ?>>
                            <div class="content">
                                <?php
                                if ( $this->autoload_draft && $this->load_order_overview_directly && !$errors ) {
                                    echo $this->load_order_overview();
                                }
                                ?>
                            </div>
                        </div>
                        <div id="<?= MAME_WS_PREFIX ?>-loader" class="<?= MAME_WS_PREFIX ?>-loader">
                            <div class="content"><img
                                        src="<?= plugin_dir_url( dirname( dirname( __FILE__ ) ) ) . 'assets/images/loader-5.gif' ?>">
                            </div>
                        </div>
                    </div>
                </div>

            </div>
            <script>
                jQuery(document).ready(function ($) {
                    new MameWsEditor();
                });
            </script>
            <?php
        }

        public function load_editor_form_content( $page = 1, $errors = false )
        {
            $content = $this->get_page_1( $page !== 1 );
            $content .= $this->get_page_2( $page !== 2 );
            $content .= $this->get_page_3( $page !== 3 );
            $content .= $this->get_page_4( $page !== 4, $errors );
            return $content;
        }

        public function load_editor_content()
        {
            ?>
            <h1><span><?= __( 'WebStamp Editor' ) ?></span></h1>
            <div id="<?= MAME_WS_PREFIX ?>-steps-nav">
                <div id="<?= MAME_WS_PREFIX ?>-nav-step-wrapper-1"
                     class="<?= MAME_WS_PREFIX ?>-nav-step-wrapper" data-page="1">
                    <div class="<?= MAME_WS_PREFIX ?>-nav-step">
                        <img src="<?= plugins_url( '../../assets/images/stamp_gray_s.png', __FILE__ ) ?>">
                        <div class="<?= MAME_WS_PREFIX ?>-number"><p>1</p></div>
                    </div>
                    <p class="text"><span><?= __( 'Stamp', 'dhuett' ) ?></span></p>
                </div>
                <div id="<?= MAME_WS_PREFIX ?>-nav-step-wrapper-2"
                     class="<?= MAME_WS_PREFIX ?>-nav-step-wrapper" data-page="2">
                    <div class="<?= MAME_WS_PREFIX ?>-nav-step">
                        <img src="<?= plugins_url( '../../assets/images/receiver_address_s.png', __FILE__ ) ?>">
                        <div class="<?= MAME_WS_PREFIX ?>-number"><p>2</p></div>
                    </div>
                    <p class="text"><span><?= __( 'Receiver address', 'dhuett' ) ?></span></p>
                </div>
                <div id="<?= MAME_WS_PREFIX ?>-nav-step-wrapper-3"
                     class="<?= MAME_WS_PREFIX ?>-nav-step-wrapper" data-page="3">
                    <div class="<?= MAME_WS_PREFIX ?>-nav-step">
                        <img src="<?= plugins_url( '../../assets/images/sender_address_s.png', __FILE__ ) ?>">
                        <div class="<?= MAME_WS_PREFIX ?>-number"><p>3</p></div>
                    </div>
                    <p class="text"><span><?= __( 'Sender address', 'dhuett' ) ?></span></p>
                </div>
                <div id="<?= MAME_WS_PREFIX ?>-nav-step-wrapper-4"
                     class="<?= MAME_WS_PREFIX ?>-nav-step-wrapper" data-page="4">
                    <div class="<?= MAME_WS_PREFIX ?>-nav-step">
                        <img src="<?= plugins_url( '../../assets/images/overview_s.png', __FILE__ ) ?>">
                        <div class="<?= MAME_WS_PREFIX ?>-number"><p>4</p></div>
                    </div>
                    <p class="text"><span><?= __( 'Overview', 'dhuett' ) ?></span></p>
                </div>
                <div class="clearfix"></div>
            </div>
            <form action="" method="post">
                <div id="<?= MAME_WS_PREFIX ?>-steps">
                    <?= $this->get_page_1(); ?>
                    <?= $this->get_page_2( true ); ?>
                    <?= $this->get_page_3( true ); ?>
                    <?= $this->get_page_4( true ); ?>
                </div>
                <?= Mame_Html::button( __( 'Back', 'dhuett' ), [ 'id' => MAME_WS_PREFIX . '-prev-btn', 'class' => MAME_WS_PREFIX . '-btn hidden' ] ); ?>
                <?= Mame_Html::button( __( 'Next', 'dhuett' ), [ 'id' => MAME_WS_PREFIX . '-next-btn', 'class' => MAME_WS_PREFIX . '-btn' ] ); ?>
            </form>
            <?php
        }

        public function get_page_1( $hidden = false )
        {
            $options = get_option( 'mame_ws_options_group' );
            $content = Mame_Html::open_tag( 'div', [ 'id' => MAME_WS_PREFIX . '-page-1', 'class' => MAME_WS_PREFIX . '-page ' . ( $hidden ? 'hidden' : '' ) ] );
            $content .= Mame_Html::div( $this->get_category_fields(), [ 'id' => MAME_WS_PREFIX . '-category-wrapper', 'data-page' => 1 ] );
            $content .= Mame_Html::div( ( $this->template->category ? $this->get_zone_fields( $this->template->category )[ 'content' ] : '' ), [ 'id' => MAME_WS_PREFIX . '-zone-wrapper', 'class' => MAME_WS_PREFIX . '-data-wrapper', 'data-page' => 1 ] );
            $content .= Mame_Html::div( $this->get_delivery_methods( $this->template->zone ), [ 'id' => MAME_WS_PREFIX . '-delivery_method-wrapper', 'data-page' => 1 ] );
            $content .= Mame_Html::div( $this->get_product_formats( $this->template->zone, $this->template->delivery_method ), [ 'id' => MAME_WS_PREFIX . '-format-wrapper', 'data-page' => 1 ] );
            $content .= Mame_Html::div( $this->get_additions( $this->template->zone, $this->template->delivery_method, $this->template->format ), [ 'id' => MAME_WS_PREFIX . '-addition-wrapper', 'class' => MAME_WS_PREFIX . '-data-wrapper', 'data-page' => 1 ] );
            if ( $options[ 'stamp_file_type' ] === 'pdf' ) {
                $content .= Mame_Html::div( $this->get_media_type_fields( $this->template->category ), [ 'id' => MAME_WS_PREFIX . '-media_type-wrapper', 'data-page' => 1 ] );
                $content .= Mame_Html::div( $this->get_media_fields( $this->template->media_type ), [ 'id' => MAME_WS_PREFIX . '-media-wrapper', 'class' => MAME_WS_PREFIX . '-data-wrapper', 'data-page' => 1 ] );
                $content .= Mame_Html::div( $this->get_media_startpos_fields(), [ 'id' => MAME_WS_PREFIX . '-media_startpos-wrapper', 'class' => MAME_WS_PREFIX . '-data-wrapper ' . ( $this->template->media_type == Webstamp_Database_Manager::LABEL_ID_2 || $this->template->media_type == Webstamp_Database_Manager::LABEL_ID_1 ? '' : 'hidden' ), 'data-page' => 1 ] );
            }
            $content .= $this->get_image_fields();
            $content .= Mame_Html::close_tag( 'div' );

            return $content;
        }

        public function get_page_2( $hidden = false )
        {
            $recv_address_options = [ 'class' => MAME_WS_PREFIX . '-template-input' ];
            if ( $this->template->use_address )
                $recv_address_options[ 'checked' ] = 'checked';
            $content = Mame_Html::open_tag( 'div', [ 'id' => MAME_WS_PREFIX . '-page-2', 'class' => MAME_WS_PREFIX . '-page ' . ( $hidden ? 'hidden' : '' ) ] );
            $content .= Mame_Html::div( Mame_Html::checkbox( __( 'Include receiver address', 'dhuett' ), MAME_WS_PREFIX . '-use_address', 1, $recv_address_options ), [ 'id' => MAME_WS_PREFIX . '-use_address-wrapper', 'class' => MAME_WS_PREFIX . '-data-wrapper', 'data-page' => 3 ] );
            $content .= Mame_Html::div( $this->get_quantity_fields(), [ 'id' => MAME_WS_PREFIX . '-quantity-wrapper', 'class' => MAME_WS_PREFIX . '-data-wrapper' . ( $this->template->use_address ? ' hidden' : '' ), 'data-page' => 1 ] );
            $content .= Mame_Html::div( $this->receiver_address_fields(), [ 'id' => MAME_WS_PREFIX . '-address-wrapper', 'class' => MAME_WS_PREFIX . '-data-wrapper ' . ( $this->template->use_address ? '' : 'hidden' ), 'data-page' => 3 ] );
            $content .= Mame_Html::button( __( 'Save', 'dhuett' ), [ 'class' => MAME_WS_PREFIX . '-save-btn', 'data-step' => 'address' ] );
            $content .= Mame_Html::close_tag( 'div' );

            return $content;
        }

        public function get_page_3( $hidden = false )
        {
            $content = Mame_Html::open_tag( 'div', [ 'id' => MAME_WS_PREFIX . '-page-3', 'class' => MAME_WS_PREFIX . '-page ' . ( $hidden ? 'hidden' : '' ) ] );
            $content .= Mame_Html::div( Mame_Html::checkbox( __( 'Include sender address', 'dhuett' ), MAME_WS_PREFIX . '-use_sender_address', 1, ( $this->template->use_sender_address ? [ 'checked' => 'checked' ] : null ) ), [ 'id' => MAME_WS_PREFIX . '-use_sender_address-wrapper', 'class' => MAME_WS_PREFIX . '-data-wrapper', 'data-page' => 3 ] );
            $content .= Mame_Html::div( $this->sender_address_fields(), [ 'id' => MAME_WS_PREFIX . '-sender-wrapper', 'class' => MAME_WS_PREFIX . '-data-wrapper ' . ( $this->template->use_sender_address ? '' : 'hidden' ), 'data-page' => 3 ] );
            $content .= Mame_Html::button( __( 'Save', 'dhuett' ), [ 'class' => MAME_WS_PREFIX . '-save-btn', 'data-step' => 'sender' ] );
            $content .= Mame_Html::close_tag( 'div' );

            return $content;
        }


        public function get_page_4( $hidden = false, $errors = false )
        {
            $options = get_option( 'mame_ws_options_group' );

            $content = Mame_Html::open_tag( 'div', [ 'id' => MAME_WS_PREFIX . '-page-4', 'class' => MAME_WS_PREFIX . '-page ' . ( $hidden ? 'hidden' : '' ) ] );
            $content .= Mame_Html::open_tag( 'div', [ 'id' => MAME_WS_PREFIX . '-overview-wrapper', 'class' => MAME_WS_PREFIX . '-data-wrapper', 'data-page' => 6 ] );
            $content .= Mame_Html::div( $this->errors_html( $errors ), [ 'class' => 'errors' ] );
            $content .= Mame_Html::div( Mame_Html::div( Mame_Html::p( __( 'Category', 'dhuett' ) ), [ 'class' => 'first' ] ) . Mame_Html::div( Mame_Html::p( ( isset( $this->overview_data[ 'category' ] ) ? $this->overview_data[ 'category' ] : '' ), [ 'id' => MAME_WS_PREFIX . '-overview-category' ] ), [ 'class' => 'second' ] ) . Mame_Html::button( __( 'Edit' ), [ 'class' => MAME_WS_PREFIX . '-btn overview-btn', 'data-page' => '1' ] ), [ 'class' => 'overview-data' ] );
            $content .= Mame_Html::div( Mame_Html::div( Mame_Html::p( __( 'Zone', 'dhuett' ) ), [ 'class' => 'first' ] ) . Mame_Html::div( Mame_Html::p( ( isset( $this->overview_data[ 'zone' ] ) ? $this->overview_data[ 'zone' ] : '' ), [ 'id' => MAME_WS_PREFIX . '-overview-zone' ] ), [ 'class' => 'second' ] ) . Mame_Html::button( __( 'Edit' ), [ 'class' => MAME_WS_PREFIX . '-btn overview-btn', 'data-page' => '1' ] ), [ 'class' => 'overview-data' ] );
            $content .= Mame_Html::div( Mame_Html::div( Mame_Html::p( __( 'Delivery method', 'dhuett' ) ), [ 'class' => 'first' ] ) . Mame_Html::div( Mame_Html::p( ( isset( $this->template->delivery_method ) ? $this->template->delivery_method : '' ), [ 'id' => MAME_WS_PREFIX . '-overview-delivery_method' ] ), [ 'class' => 'second' ] ) . Mame_Html::button( __( 'Edit' ), [ 'class' => MAME_WS_PREFIX . '-btn overview-btn', 'data-page' => '1' ] ), [ 'class' => 'overview-data' ] );
            $content .= Mame_Html::div( Mame_Html::div( Mame_Html::p( __( 'Format', 'dhuett' ) ), [ 'class' => 'first' ] ) . Mame_Html::div( Mame_Html::p( ( isset( $this->template->format ) ? $this->template->format : '' ), [ 'id' => MAME_WS_PREFIX . '-overview-format' ] ), [ 'class' => 'second' ] ) . Mame_Html::button( __( 'Edit' ), [ 'class' => MAME_WS_PREFIX . '-btn overview-btn', 'data-page' => '1' ] ), [ 'class' => 'overview-data' ] );
            $content .= Mame_Html::div( Mame_Html::div( Mame_Html::p( __( 'Additions', 'dhuett' ) ), [ 'class' => 'first' ] ) . Mame_Html::div( '<p>' . ( isset( $this->template->additions ) ? implode( '<br>', $this->template->additions ) : '' ) . '</p>', [ 'id' => MAME_WS_PREFIX . '-overview-addition', 'class' => 'second' ] ) . Mame_Html::button( __( 'Edit' ), [ 'class' => MAME_WS_PREFIX . '-btn overview-btn', 'data-page' => '1' ] ), [ 'class' => 'overview-data' ] );

            if ( $options[ 'stamp_file_type' ] === 'pdf' ) {
                $content .= Mame_Html::div( Mame_Html::div( Mame_Html::p( __( 'Media type', 'dhuett' ) ), [ 'class' => 'first' ] ) . Mame_Html::div( Mame_Html::p( ( isset( $this->overview_data[ 'media_type' ] ) ? $this->overview_data[ 'media_type' ] : '' ), [ 'id' => MAME_WS_PREFIX . '-overview-media_type' ] ), [ 'class' => 'second' ] ) . Mame_Html::button( __( 'Edit' ), [ 'class' => MAME_WS_PREFIX . '-btn overview-btn', 'data-page' => '1' ] ), [ 'class' => 'overview-data' ] );
                $content .= Mame_Html::div( Mame_Html::div( Mame_Html::p( __( 'Media', 'dhuett' ) ), [ 'class' => 'first' ] ) . Mame_Html::div( Mame_Html::p( ( isset( $this->overview_data[ 'media' ] ) ? $this->overview_data[ 'media' ] : '' ), [ 'id' => MAME_WS_PREFIX . '-overview-media' ] ), [ 'class' => 'second' ] ) . Mame_Html::button( __( 'Edit' ), [ 'class' => MAME_WS_PREFIX . '-btn overview-btn', 'data-page' => '1' ] ), [ 'class' => 'overview-data' ] );
                $content .= Mame_Html::div( Mame_Html::div( Mame_Html::p( __( 'Label start position', 'dhuett' ) ), [ 'class' => 'first' ] ) . Mame_Html::div( Mame_Html::p( ( isset( $this->template->media_startpos ) ? $this->template->media_startpos : '' ), [ 'id' => MAME_WS_PREFIX . '-overview-media_startpos' ] ), [ 'class' => 'second' ] ) . Mame_Html::button( __( 'Edit' ), [ 'class' => MAME_WS_PREFIX . '-btn overview-btn', 'data-page' => '1' ] ), [ 'class' => 'overview-data' ] );
            }

            $content .= Mame_Html::div( Mame_Html::div( Mame_Html::p( __( 'Address', 'dhuett' ) ), [ 'class' => 'first' ] ) . Mame_Html::div( '<p>' . ( isset( $this->overview_data[ 'address' ] ) ? $this->overview_data[ 'address' ] : '' ) . '</p>', [ 'id' => MAME_WS_PREFIX . '-overview-address', 'class' => 'second' ] ) . Mame_Html::button( __( 'Edit' ), [ 'class' => MAME_WS_PREFIX . '-btn overview-btn', 'data-page' => '2' ] ), [ 'class' => 'overview-data' ] );
            $content .= Mame_Html::div( Mame_Html::div( Mame_Html::p( __( 'Sender address', 'dhuett' ) ), [ 'class' => 'first' ] ) . Mame_Html::div( '<p>' . ( isset( $this->overview_data[ 'sender' ] ) ? $this->overview_data[ 'sender' ] : '' ) . '</p>', [ 'id' => MAME_WS_PREFIX . '-overview-sender', 'class' => 'second' ] ) . Mame_Html::button( __( 'Edit' ), [ 'class' => MAME_WS_PREFIX . '-btn overview-btn', 'data-page' => '3' ] ), [ 'class' => 'overview-data' ] );
            $content .= Mame_Html::div( Mame_Html::div( Mame_Html::p( __( 'License', 'dhuett' ) ), [ 'class' => 'first' ] ) . Mame_Html::div( Mame_Html::p( ( isset( $this->template->license_number ) ? $this->template->license_number : '' ), [ 'id' => MAME_WS_PREFIX . '-overview-license_number' ] ), [ 'class' => 'second' ] ) . Mame_Html::button( __( 'Edit' ), [ 'id' => MAME_WS_PREFIX . '-edit-license-btn', 'class' => MAME_WS_PREFIX . '-btn' ] ), [ 'class' => 'overview-data' ] );
            $content .= Mame_Html::div( '', [ 'class' => 'clearfix' ] );
            $content .= Mame_Html::close_tag( 'div' );
            $content .= Mame_Html::open_tag( 'div', [ 'id' => MAME_WS_PREFIX . '-order-buttons' ] );
            $content .= Mame_Html::button( __( 'Preview', 'dhuett' ), [ 'id' => MAME_WS_PREFIX . '-preview-btn', 'class' => MAME_WS_PREFIX . '-btn' ] );
            $content .= Mame_Html::button( __( 'Order', 'dhuett' ), [ 'id' => MAME_WS_PREFIX . '-order-btn', 'class' => MAME_WS_PREFIX . '-btn' ] );
            $content .= Mame_Html::close_tag( 'div' );
            $content .= Mame_Html::close_tag( 'div' );
            return $content;
        }

        /**
         * Checks if a variable is empty or equals '0' or 0.
         *
         * @param $property
         * @return bool
         */
        private function empty_or_missing( $property )
        {
            if ( empty( $property ) || $property === 0 || $property === '0' )
                return true;
            return false;
        }
    }

}