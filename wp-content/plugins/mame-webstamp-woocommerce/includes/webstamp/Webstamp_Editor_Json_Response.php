<?php

if ( !defined( 'ABSPATH' ) ) {
    exit;
}

if ( !class_exists( 'Webstamp_Editor_Json_Response' ) ) {

    /**
     * Class Webstamp_Editor_Json_Response
     */
    class Webstamp_Editor_Json_Response extends Mame_Json_Response
    {
        public function __construct()
        {
            parent::__construct();
        }

        public function action( $action )
        {
            $this->response[ 'action' ] = $action;
            return $this;
        }

        public function order( $order )
        {
            $this->response[ 'order' ] = $order;
            return $this;
        }

        public function address( $address )
        {
            $this->response[ 'address' ] = $address;
            return $this;
        }

        public function sender( $sender )
        {
            $this->response[ 'sender' ] = $sender;
            return $this;
        }

        public function template_data( $template_data )
        {
            $this->response[ 'template_data' ] = $template_data;
            return $this;
        }

        /**
         * @param $page
         * @return Barcode_Json_Response
         */
        public function page( $page )
        {
            $this->response[ 'page' ] = $page;
            return $this;
        }

    }

}