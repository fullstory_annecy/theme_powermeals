<?php
if ( !defined( 'ABSPATH' ) )
    exit;

if ( !class_exists( 'Webstamp_Template_Editor_Handler' ) ) {

    /**
     * Class Webstamp_Template_Editor_Handler
     */
    class Webstamp_Template_Editor_Handler extends Webstamp_Editor_Handler
    {
        /**
         * @var null|Webstamp_Template
         */
        public $template;

        /**
         * Webstamp_Template_Editor_Handler constructor.
         * @param null $template_id
         */
        public function __construct( $template_id = null )
        {
            parent::__construct( null, null );
            add_action( 'wp_ajax_mame_ws_ajax_template_editor_request', array( $this, 'ajax_handler' ) );

            $this->template = new Webstamp_Template();
            if ( $template_id )
                $this->template = Webstamp_Database_Manager::get_template( $template_id );

            if ( $this->template->sender_id )
                $this->sender_address = $this->template->get_sender();
        }

        /**
         * @param array $data
         */
        public function load_editor( $data = [ 'use_address' => false ] )
        {
            ?>
            <div id="<?= MAME_WS_PREFIX ?>-editor-wrapper">
                <div id="<?= MAME_WS_PREFIX ?>-content-wrapper">
                    <div id="<?= MAME_WS_PREFIX ?>-content">
                        <?php
                        $options = get_option( 'mame_ws_options_group' );
                        $content = Mame_Html::div( $this->get_name_field( $data ), [ 'id' => MAME_WS_PREFIX . '-name-wrapper' ] );
                        $content .= Mame_Html::div( $this->get_category_fields(), [ 'id' => MAME_WS_PREFIX . '-category-wrapper' ] );
                        $content .= Mame_Html::div( ( $this->template->category ? $this->get_zone_fields( $this->template->category )[ 'content' ] : '' ), [ 'id' => MAME_WS_PREFIX . '-zone-wrapper', 'class' => MAME_WS_PREFIX . '-data-wrapper' ] );
                        $content .= Mame_Html::div( $this->get_delivery_methods( $this->template->zone ), [ 'id' => MAME_WS_PREFIX . '-delivery_method-wrapper' ] );
                        $content .= Mame_Html::div( $this->get_product_formats( $this->template->zone, $this->template->delivery_method ), [ 'id' => MAME_WS_PREFIX . '-format-wrapper' ] );
                        $content .= Mame_Html::div( $this->get_additions( $this->template->zone, $this->template->delivery_method, $this->template->format ), [ 'id' => MAME_WS_PREFIX . '-addition-wrapper', 'class' => MAME_WS_PREFIX . '-data-wrapper' ] );
                        if ( $options[ 'stamp_file_type' ] === 'pdf' ) {
                            $content .= Mame_Html::div( $this->get_media_type_fields( $this->template->category ), [ 'id' => MAME_WS_PREFIX . '-media_type-wrapper', 'data-page' => 1 ] );
                            $content .= Mame_Html::div( $this->get_media_fields( $this->template->media_type ), [ 'id' => MAME_WS_PREFIX . '-media-wrapper', 'class' => MAME_WS_PREFIX . '-data-wrapper' ] );
                            $content .= Mame_Html::div( $this->get_media_startpos_fields(), [ 'id' => MAME_WS_PREFIX . '-media_startpos-wrapper', 'class' => MAME_WS_PREFIX . '-data-wrapper ' . ( $this->template->media_type == Webstamp_Database_Manager::LABEL_ID_2 || $this->template->media_type == Webstamp_Database_Manager::LABEL_ID_1 ? '' : 'hidden' ) ] );
                        }
                        $content .= $this->get_image_fields();

                        $content .= $this->get_receiver_address_fields();
                        $content .= $this->get_sender_address_fields();

                        $content .= Mame_Html::div( $this->get_license_fields(), [ 'id' => MAME_WS_PREFIX . '-license-wrapper', 'class' => MAME_WS_PREFIX . '-data-wrapper' ] );

                        if ( $this->template->id )
                            $content .= Mame_Html::input( 'hidden', 'id', $this->template->id );
                        echo $content;
                        ?>
                    </div>

                </div>

            </div>
            <script>
                jQuery(document).ready(function ($) {
                    new MameWsEditor();
                    <?php if (!isset( $_GET[ 'id' ] )) { ?>
                    $("#mame_ws-category").change();
                    <?php } ?>
                });
            </script>
            <?php

        }

        public function get_receiver_address_fields()
        {
            $recv_address_options = [ 'class' => MAME_WS_PREFIX . '-template-input' ];
            if ( $this->template->use_address )
                $recv_address_options[ 'checked' ] = 'checked';

            $content = Mame_Html::open_tag( 'div' );
            $content .= Mame_Html::div( Mame_Html::h3( __( 'Receiver', 'dhuett' ) ), [ 'class' => MAME_WS_PREFIX . '-table-left' ] );
            $content .= Mame_Html::div( Mame_Html::checkbox( __( 'Include receiver address', 'dhuett' ), MAME_WS_PREFIX . '-use_address', 1, $recv_address_options ), [ 'id' => MAME_WS_PREFIX . '-use_address-wrapper', 'class' => MAME_WS_PREFIX . '-data-wrapper ' . MAME_WS_PREFIX . '-table-right' ] );
            $content .= Mame_Html::div( $this->get_quantity_fields(), [ 'id' => MAME_WS_PREFIX . '-quantity-wrapper', 'class' => MAME_WS_PREFIX . '-data-wrapper' . ( $this->template->use_address ? ' hidden' : '' ) ] );
            $content .= Mame_Html::div( $this->get_wc_address_select_fields(), [ 'id' => MAME_WS_PREFIX . '-load_wc_address-wrapper', 'class' => MAME_WS_PREFIX . '-data-wrapper ' . ( $this->template && $this->template->use_address ? '' : 'hidden' ) ] );
            $content .= Mame_Html::close_tag( 'div' );
            return $content;
        }

        public function get_sender_address_fields()
        {
            $content = Mame_Html::div( Mame_Html::h3( __( 'Sender', 'dhuett' ) ), [ 'class' => MAME_WS_PREFIX . '-table-left' ] );
            $content .= Mame_Html::div( Mame_Html::checkbox( __( 'Include sender address', 'dhuett' ), MAME_WS_PREFIX . '-use_sender_address', 1, ( $this->template->use_sender_address ? [ 'checked' => 'checked' ] : null ) ), [ 'id' => MAME_WS_PREFIX . '-use_sender_address-wrapper', 'class' => MAME_WS_PREFIX . '-data-wrapper' ] );
            $content .= Mame_Html::div( $this->sender_address_fields(), [ 'id' => MAME_WS_PREFIX . '-sender-wrapper', 'class' => MAME_WS_PREFIX . '-data-wrapper ' . ( $this->template->use_sender_address ? '' : 'hidden' ) ] );
            return $content;
        }


        /**
         * @param null $args
         * @return string
         */
        public function get_name_field( $args = null )
        {
            $content = Mame_Html::open_tag( 'div', [ 'id' => MAME_WS_PREFIX . '-step-name', 'class' => MAME_WS_PREFIX . '-step' ] );
            $content .= Mame_Html::div( Mame_Html::h3( __( 'Name', 'dhuett' ) ), [ 'class' => MAME_WS_PREFIX . '-table-left' ] );
            $content .= Mame_Html::div( Mame_Html::input( 'text', MAME_WS_PREFIX . '-name', ( isset( $this->template->name ) ? $this->template->name : '' ), [ 'maxlength' => 255 ] ), [ 'class' => MAME_WS_PREFIX . '-table-right' ] );
            $content .= Mame_Html::close_tag( 'div' );

            return $content;
        }

        /**
         * Displays the category select field.
         */
        public function get_category_fields()
        {
            $content = Mame_Html::open_tag( 'div', [ 'id' => MAME_WS_PREFIX . '-step-category', 'class' => MAME_WS_PREFIX . '-step ' . MAME_WS_PREFIX . '-step-1', 'data-page' => 1 ] );
            $content .= Mame_Html::div( Mame_Html::h3( __( 'Category', 'dhuett' ) ), [ 'class' => MAME_WS_PREFIX . '-table-left' ] );
            $content .= Mame_Html::div( Mame_Html::select( MAME_WS_PREFIX . '-category', Mame_Helper_Functions::object_array_map( Webstamp_Database_Manager::get_categories(), 'number', 'name' ), [ 'id' => MAME_WS_PREFIX . '-category', 'class' => MAME_WS_PREFIX . '-select-load', 'data-step' => 'category' ], ( isset( $this->template->category ) ? $this->template->category : null ) ), [ 'class' => MAME_WS_PREFIX . '-table-right' ] );
            $content .= Mame_Html::close_tag( 'div' );

            return $content;
        }

        /**
         * Displays the zone select field based on the category.
         * $category can have one of the following values:
         * - 1 : domestic letters
         * - 2 : international letters
         *
         * @param $category
         * @return array
         */
        public function get_zone_fields( $category )
        {
            if ( $category == Webstamp_Database_Manager::CATEGORY_DOMESTIC ) {
                $zone = Webstamp_Database_Manager::get_zone( Webstamp_Database_Manager::ZONE_DOMESTIC );
                // Domestic letter
                return [ 'content' => Mame_Html::input( 'hidden', MAME_WS_PREFIX . '-zone', Webstamp_Database_Manager::ZONE_DOMESTIC ), 'action' => 'continue', 'zone_name' => $zone->name ];
            }

            // International letter
            $content = Mame_Html::open_tag( 'div', [ 'id' => MAME_WS_PREFIX . '-step-zone', 'class' => MAME_WS_PREFIX . '-step ' . MAME_WS_PREFIX . '-step-1', 'data-page' => 1 ] );
            $content .= Mame_Html::div( Mame_Html::h3( __( 'Zone', 'dhuett' ) ), [ 'class' => MAME_WS_PREFIX . '-table-left' ] );
            $content .= Mame_Html::div( Mame_Html::select( MAME_WS_PREFIX . '-zone', Mame_Helper_Functions::object_array_map( Webstamp_Database_Manager::get_zones(), 'number', 'name' ), [ 'id' => MAME_WS_PREFIX . '-zone', 'class' => MAME_WS_PREFIX . '-select-load', 'data-step' => 'zone' ], ( isset( $this->template->zone ) ? $this->template->zone : null ) ), [ 'class' => MAME_WS_PREFIX . '-table-right' ] );
            $content .= Mame_Html::close_tag( 'div' );

            return [ 'content' => $content, 'action' => 'none' ];

        }

        public function get_delivery_methods( $zone = null )
        {
            $content = Mame_Html::open_tag( 'div', [ 'id' => MAME_WS_PREFIX . '-step-delivery_method', 'class' => MAME_WS_PREFIX . '-step ' . MAME_WS_PREFIX . '-step-1', 'data-page' => 2 ] );
            $content .= Mame_Html::div( Mame_Html::h3( __( 'Delivery method', 'dhuett' ) ), [ 'class' => MAME_WS_PREFIX . '-table-left' ] );
            $content .= Mame_Html::open_tag( 'div', [ 'class' => MAME_WS_PREFIX . '-table-right' ] );
            if ( !empty( $zone ) ) {

                $content .= Mame_Html::select( MAME_WS_PREFIX . '-delivery_method', Mame_Helper_Functions::object_array_map( Webstamp_Database_Manager::get_product_delivery_methods( $zone ), 'delivery', 'delivery' ), [ 'id' => MAME_WS_PREFIX . '-delivery_method', 'class' => MAME_WS_PREFIX . '-step ' . MAME_WS_PREFIX . '-step-2', 'class' => MAME_WS_PREFIX . '-select-load', 'data-step' => 'delivery_method' ], ( isset( $this->template->delivery_method ) ? $this->template->delivery_method : null ) );
            } else {
                $content .= Mame_Html::p( __( 'Select zone to show delivery methods.', 'dhuett' ) );
            }
            $content .= Mame_Html::close_tag( 'div' );
            $content .= Mame_Html::close_tag( 'div' );
            return $content;
        }

        public function get_product_formats( $zone = null, $delivery_method = null )
        {
            $content = Mame_Html::open_tag( 'div', [ 'id' => MAME_WS_PREFIX . '-step-format', 'class' => MAME_WS_PREFIX . '-step ' . MAME_WS_PREFIX . '-step-1', 'data-page' => 2 ] );
            $content .= Mame_Html::div( Mame_Html::h3( __( 'Formats', 'dhuett' ) ), [ 'class' => MAME_WS_PREFIX . '-table-left' ] );
            $content .= Mame_Html::open_tag( 'div', [ 'class' => MAME_WS_PREFIX . '-table-right' ] );
            if ( empty( $zone ) ) {
                $content .= Mame_Html::p( __( 'Select zone to show formats.', 'dhuett' ) );
            } elseif ( empty( $delivery_method ) ) {
                $content .= Mame_Html::p( __( 'Select delivery method to show formats.', 'dhuett' ) );
            } else {
                $content .= Mame_Html::select( MAME_WS_PREFIX . '-format', Mame_Helper_Functions::object_array_map( Webstamp_Database_Manager::get_product_formats( $zone, $delivery_method ), 'format', 'format' ), [ 'id' => MAME_WS_PREFIX . '-format', 'class' => MAME_WS_PREFIX . '-select-save', 'class' => MAME_WS_PREFIX . '-select-load', 'data-step' => 'format' ], ( isset( $this->template->format ) ? $this->template->format : null ) );
            }
            $content .= Mame_Html::close_tag( 'div' );
            $content .= Mame_Html::close_tag( 'div' );
            return $content;
        }

        public function get_additions( $zone = null, $delivery_method = null, $format = null )
        {
            $additions = Webstamp_Database_Manager::get_product_additions( $zone, $delivery_method, $format );
            $additions = array_map( function ( $a ) {
                return [ 'name' => 'additions[]', 'value' => $a->name, 'label' => $a->name ];
            }, $additions );
            $content   = Mame_Html::open_tag( 'div', [ 'id' => MAME_WS_PREFIX . '-step-addition', 'class' => MAME_WS_PREFIX . '-step ' . MAME_WS_PREFIX . '-step-1', 'data-page' => 2 ] );
            $content   .= Mame_Html::div( Mame_Html::h3( __( 'Additions', 'dhuett' ) ), [ 'class' => MAME_WS_PREFIX . '-table-left' ] );
            $content   .= Mame_Html::open_tag( 'div', [ 'class' => MAME_WS_PREFIX . '-table-right' ] );
            if ( empty( $zone ) ) {
                $content .= Mame_Html::p( __( 'Select zone to show additions.', 'dhuett' ) );
            } elseif ( empty( $delivery_method ) ) {
                $content .= Mame_Html::p( __( 'Select delivery method to show additions.', 'dhuett' ) );
            } elseif ( empty( $format ) ) {
                $content .= Mame_Html::p( __( 'Select format to show additions.', 'dhuett' ) );
            } else {
                $content .= Mame_Html::checkbox_group( $additions, [ 'id' => MAME_WS_PREFIX . '-additions' ], ( $this->template->additions ? $this->template->additions : null ) );
            }
            $content .= Mame_Html::close_tag( 'div' );
            $content .= Mame_Html::close_tag( 'div' );

            return $content;
        }

        public function get_image_fields()
        {
            $img_upload_field = $this->image_upload_field( 'image_id', '', null, 200, plugins_url( '../../assets/images/stamp_white.png', __FILE__ ) );
            $content          = Mame_Html::open_tag( 'div', [ 'id' => MAME_WS_PREFIX . '-step-image' ] );
            $content          .= Mame_Html::div( Mame_Html::h3( __( 'Image', 'dhuett' ) ), [ 'class' => MAME_WS_PREFIX . '-table-left' ] );
            $content          .= Mame_Html::open_tag( 'div', [ 'class' => MAME_WS_PREFIX . '-table-right' ] );
            $checkbox_options = $this->template->image_id ? [ 'checked' => 'checked' ] : null;
            $content          .= Mame_Html::div( Mame_Html::checkbox( __( 'Use image', 'dhuett' ), MAME_WS_PREFIX . '-use_image', 1, $checkbox_options ), [ 'id' => MAME_WS_PREFIX . '-use_image-wrapper', 'class' => MAME_WS_PREFIX . '-data-wrapper' ] );
            $content          .= Mame_Html::div( $img_upload_field, [ 'id' => MAME_WS_PREFIX . '-image-wrapper', 'class' => MAME_WS_PREFIX . '-data-wrapper' . ( $this->template->image_id ? '' : ' hidden' ) ] );
            $content          .= Mame_Html::close_tag( 'div' );
            $content          .= Mame_Html::close_tag( 'div' );
            return $content;
        }

        public function get_media_type_fields( $category = null )
        {
            $content = Mame_Html::open_tag( 'div', [ 'id' => MAME_WS_PREFIX . '-step-media_type', 'data-page' => 1 ] );
            $content .= Mame_Html::div( Mame_Html::h3( __( 'Media type', 'dhuett' ) ), [ 'class' => MAME_WS_PREFIX . '-table-left' ] );
            $content .= Mame_Html::open_tag( 'div', [ 'class' => MAME_WS_PREFIX . '-table-right' ] );
            if ( empty( $category ) ) {
                $content .= Mame_Html::p( __( 'Select category to show media types.', 'dhuett' ) );
            } else {
                $content .= Mame_Html::select( MAME_WS_PREFIX . '-media_type', Mame_Helper_Functions::object_array_map( Webstamp_Database_Manager::get_media_types_by_category( $category ), 'number', 'name' ), [ 'id' => MAME_WS_PREFIX . '-media_type', 'class' => MAME_WS_PREFIX . '-select-load', 'data-step' => 'media_type' ], ( isset( $this->template->media_type ) ? $this->template->media_type : null ) );
            }
            $content .= Mame_Html::close_tag( 'div' );
            $content .= Mame_Html::close_tag( 'div' );

            return $content;
        }

        public function get_media_fields( $media_type = null )
        {
            $content = Mame_Html::open_tag( 'div', [ 'id' => MAME_WS_PREFIX . '-step-media', 'data-page' => 1 ] );
            $content .= Mame_Html::div( Mame_Html::h3( __( 'Media', 'dhuett' ) ), [ 'class' => MAME_WS_PREFIX . '-table-left' ] );
            $content .= Mame_Html::open_tag( 'div', [ 'class' => MAME_WS_PREFIX . '-table-right' ] );
            if ( empty( $media_type ) ) {
                $content .= Mame_Html::p( __( 'Select media type to show media.', 'dhuett' ) );
            } else {
                $content .= Mame_Html::select( MAME_WS_PREFIX . '-media', Mame_Helper_Functions::object_array_map( Webstamp_Database_Manager::get_medias_by_media_type( $media_type ), 'number', 'name' ), [ 'id' => MAME_WS_PREFIX . '-media', 'data-step' => 'media' ], ( isset( $this->template->media ) ? $this->template->media : null ) );
            }
            $content .= Mame_Html::close_tag( 'div' );
            $content .= Mame_Html::close_tag( 'div' );

            return $content;
        }

        public function get_license_fields( $licenses = null )
        {
            if ( !$licenses )
                $licenses = Webstamp_Database_Manager::get_licenses();
            if ( !empty( $licenses ) ) {
                $content = Mame_Html::open_tag( 'div', [ 'id' => MAME_WS_PREFIX . '-step-license_number', 'data-page' => 5 ] );
                $content .= Mame_Html::div( Mame_Html::h3( __( 'License', 'dhuett' ) ), [ 'class' => MAME_WS_PREFIX . '-table-left' ] );
                $content .= Mame_Html::div( Mame_Html::select_with_string_keys( MAME_WS_PREFIX . '-license_number', array_merge( [ [ 'key' => '', 'value' => __( 'None', 'dhuett' ) ] ], Mame_Helper_Functions::object_array_map_string_keys( $licenses, 'number', 'number' ) ), [ 'id' => MAME_WS_PREFIX . '-license_number' ], ( isset( $this->template->license_number ) ? $this->template->license_number : null ) ), [ 'class' => MAME_WS_PREFIX . '-table-right' ] );
                $content .= Mame_Html::close_tag( 'div' );

                return $content;
            }
            return null;
        }

        public function get_media_startpos_fields()
        {
            $content = Mame_Html::open_tag( 'div', [ 'id' => MAME_WS_PREFIX . '-step-media_startpos', 'data-page' => 5 ] );
            $content .= Mame_Html::div( Mame_Html::h3( __( 'Label start position', 'dhuett' ) ), [ 'class' => MAME_WS_PREFIX . '-table-left' ] );
            $content .= Mame_Html::div( Mame_Html::number( MAME_WS_PREFIX . '-media_startpos', ( isset( $this->template->media_startpos ) ? $this->template->media_startpos : null ) ), [ 'class' => MAME_WS_PREFIX . '-table-right' ] );
            $content .= Mame_Html::close_tag( 'div' );

            return $content;
        }

        public function get_quantity_fields()
        {
            $content = Mame_Html::open_tag( 'div', [ 'id' => MAME_WS_PREFIX . '-step-quantity', 'data-page' => 5 ] );
            $content .= Mame_Html::div( Mame_Html::h3( __( 'Number of stamps', 'dhuett' ) ), [ 'class' => MAME_WS_PREFIX . '-table-left' ] );
            $content .= Mame_Html::div( Mame_Html::number( MAME_WS_PREFIX . '-quantity', ( $this->template->quantity ? $this->template->quantity : null ) ), [ 'class' => MAME_WS_PREFIX . '-table-right' ] );
            $content .= Mame_Html::close_tag( 'div' );

            return $content;
        }

        public function get_wc_address_select_fields()
        {
            $content = Mame_Html::open_tag( 'div', [ 'id' => MAME_WS_PREFIX . '-step-load_wc_address' ] );
            $content .= Mame_Html::div( Mame_Html::h3( __( 'Load WooCommerce address', 'dhuett' ) ), [ 'class' => MAME_WS_PREFIX . '-table-left' ] );
            $content .= Mame_Html::open_tag( 'div', [ 'class' => MAME_WS_PREFIX . '-table-right' ] );
            $content .= Mame_Html::select( MAME_WS_PREFIX . '-load_wc_address', [ 'no' => __( 'Don\'t load', 'dhuett' ), 'shipping' => __( 'Shipping address', 'dhuett' ), 'billing' => __( 'Billing address', 'dhuett' ), 'custom' => __( 'Custom assignment', 'dhuett' ) ], [ 'id' => MAME_WS_PREFIX . '-load_wc_address', 'data-step' => 'load_wc_address' ], ( isset( $this->template->load_wc_address ) ? $this->template->load_wc_address : null ) );
            $content .= Mame_Html::close_tag( 'div' );
            $content .= Mame_Html::close_tag( 'div' );

            return $content;
        }
    }
}