<?php

if ( !defined( 'ABSPATH' ) ) {
    exit; // Exit if accessed directly
}

if ( !class_exists( 'Webstamp_Order_Manager' ) ) :

    class Webstamp_Order_Manager
    {
        public $client;

        public function __construct()
        {
            $this->client = new Webstamp_Order_SoapClient();
        }

        public function new_order_preview( $order )
        {
            $response = $this->client->new_order_preview( $order );
            if ( $result = $response->new_order_previewResult ) {
                return $result;
            }
            // Error message
            return [ 'error' => true, 'message' => $response ];
        }

        /**
         * @param Webstamp_Order $draft_order
         * @return array
         */
        public function new_order( $draft_order )
        {
            $response = $this->client->new_order( $draft_order );
            if ( $result = $response->new_orderResult ) {

                $draft_order->populate( $result );
                $draft_order->time = time();
                $draft_order->status = Webstamp_Order::STATUS_COMPLETE;
                Webstamp_Database_Manager::save_order( $draft_order );

                return $result;
            }
            // Error message
            return [ 'error' => true, 'message' => $response ];
        }
    }

endif;