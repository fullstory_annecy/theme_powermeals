<?php

if ( !defined( 'ABSPATH' ) ) {
    exit; // Exit if accessed directly
}

if ( !class_exists( 'Webstamp_Options_Manager' ) ) :
    
    class Webstamp_Options_Manager
    {
        public $client;

        //public $db_manager;

        public function __construct()
        {
            $this->client = new Webstamp_Options_SoapClient();
            // $this->db_manager = new Webstamp_Database_Manager();

            add_action( MAME_WS_PREFIX . '_cron', array( $this, 'update' ) );
        }

        public function update()
        {
            // Check if connection successful.
            if ( !$this->update_categories() )
                return false;

            if ( !$this->update_products() )
                return false;

            if ( !$this->update_zones() )
                return false;

            if ( !$this->update_product_lists() )
                return false;

            if ( !$this->update_medias() )
                return false;

            if ( !$this->update_media_types() )
                return false;

            if ( !$this->update_countries() )
                return false;

            if ( !$this->update_licenses() )
                return false;

            if ( !$this->update_customer_data() )
                return false;

            return true;
        }

        public function update_categories()
        {
            $response = $this->client->get_categories();

            if ( $result = $response->get_categoriesResult ) {

                $categories = $result->item;
                foreach ( $categories as $category ) {

                    // Save in db.
                    Webstamp_Database_Manager::save_category( $category );
                }
                return true;
            }

            return false;
        }

        public function update_products()
        {
            $response = $this->client->get_products();

            if ( $result = $response->get_productsResult ) {

                $products = $result->item;
                foreach ( $products as $product ) {

                    // Save in db.
                    Webstamp_Database_Manager::save_product( $product );
                }
                return true;

            }
            return false;
        }

        public function update_zones()
        {
            $response = $this->client->get_zones();

            if ( $result = $response->get_zonesResult ) {

                $zones = $result->item;
                foreach ( $zones as $zone ) {

                    // Save in db.
                    Webstamp_Database_Manager::save_zone( $zone );
                }
                return true;
            }
            return false;
        }

        public function update_product_lists()
        {
            $response = $this->client->get_product_lists();

            if ( $result = $response->get_product_listsResult ) {
                $product_list = $result->item;
                //  foreach ( $product_lists as $product_list ) {

                // Save in db.
                Webstamp_Database_Manager::save_product_list( $product_list );
                //   }

                return true;
            }
            return false;
        }

        public function update_medias()
        {
            $response = $this->client->get_medias();

            if ( $result = $response->get_mediasResult ) {

                $medias = $result->item;
                foreach ( $medias as $media ) {

                    // Save in db.
                    Webstamp_Database_Manager::save_media( $media );
                }

                return true;
            }
            return false;
        }

        public function update_media_types()
        {
            $response = $this->client->get_media_types();

            if ( $result = $response->get_media_typesResult ) {

                $media_types = $result->item;
                foreach ( $media_types as $media_type ) {
                    // Save in db.
                    Webstamp_Database_Manager::save_media_type( $media_type );
                }

                return true;
            }
            return false;
        }

        public function update_countries()
        {
            $response = $this->client->get_countries();

            if ( $result = $response->get_countriesResult ) {

                $countries = $result->item;
                foreach ( $countries as $country ) {

                    // Save in db.
                    Webstamp_Database_Manager::save_country( $country );
                }
                return true;
            }
            return false;
        }

        public function update_licenses()
        {
            $response = $this->client->get_licenses();
            if ( $result = $response->get_licensesResult ) {

                $licenses = $result->item;
                foreach ( $licenses as $license ) {

                    // Save in db.
                    Webstamp_Database_Manager::save_license( $license );
                }
                return true;
            }
            return false;
        }

        public function update_customer_data()
        {
            $response = $this->client->get_customer_data();

            if ( $result = $response->get_customer_dataResult ) {

                update_option( MAME_WS_PREFIX . '_license_state', $result->license_state );
                update_option( MAME_WS_PREFIX . '_payment_type', $result->payment_type );
                if ( $result->product_lists )
                    update_option( MAME_WS_PREFIX . '_product_lists', $result->product_lists->item );

                return true;
            }
            return false;
        }
    }

endif;