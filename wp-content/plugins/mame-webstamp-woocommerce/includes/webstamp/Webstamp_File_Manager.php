<?php

if ( !defined( 'ABSPATH' ) ) {
    exit;
}

if ( !class_exists( 'Webstamp_File_Manager' ) ) {

    /**
     * Class Webstamp_File_Manager
     */
    class Webstamp_File_Manager
    {
        public static function send_file( $filename, $type )
        {
            $upload_dir = wp_upload_dir();
            $file       = $upload_dir[ 'basedir' ] . '/webstamp/' . $type . '/' . $filename;
            $extension  = substr( $filename, -3 );
            if ( $extension === 'pdf' ) {
                header( 'Content-type: application/pdf' );
            } else {
                header( 'Content-type: image/png' );
            }
            http_response_code( 200 );
            header( 'Content-Disposition: attachment; filename="stamp.' . $extension . '"' );
            header( 'Content-Type: application/force-download' );
            header( 'Content-Type: application/octet-stream' );
            header( 'Content-Type: application/download' );
            header( 'Content-Description: File Transfer' );
            ob_clean();
            echo file_get_contents( $file );
            exit;
        }

        public static function display_pdf( $filename, $type )
        {
            header( 'Content-type: application/pdf' );
            header( 'Content-Disposition: inline; filename="stamp.pdf"' );
            //header('Content-Transfer-Encoding: binary');
            //header('Accept-Ranges: bytes');
            //@readfile();
            $upload_dir = wp_upload_dir();
            $file       = $upload_dir[ 'basedir' ] . '/webstamp/' . $type . '/' . $filename;
            ob_clean();
            echo file_get_contents( $file );
            ?>
            <script type="text/javascript">
                window.onload = function () {
                    window.print();
                }
            </script>
            <?php
            exit;
        }
    }
}