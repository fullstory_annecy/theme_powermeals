<?php
/**
 * Created by PhpStorm.
 * User: mozzarella
 * Date: 30.07.18
 * Time: 12:29
 */

class Webstamp_Category extends Webstamp_Object
{
    public $number;
    public $name;
    public $recipient_mandatory;
    public $valid_days;

    public function __construct( $object = null )
    {
        parent::__construct( $object );
    }

    public static function get_all()
    {
        $categories = Webstamp_Database_Manager::get_categories();
        if ( !empty( $categories ) ) {
            return array_map( function ( $item ) {
                return new static( $item );
            }, $categories );
        }
        return null;
    }
}