<?php

if ( !defined( 'ABSPATH' ) ) {
    exit;
}

if ( !class_exists( 'Webstamp_Country' ) ) {

    /**
     * Class Webstamp_Country
     */
    class Webstamp_Country extends Webstamp_Object
    {
        public $alias;
        public $name;
        public $zone;

        public function __construct( $object = null )
        {
            parent::__construct( $object );
        }

        /**
         * Returns all countries saved in the database.
         *
         * @return Webstamp_Country[]|null
         */
        public static function get_all()
        {
            $countries = Webstamp_Database_Manager::get_countries();
            if ( !empty( $countries ) ) {
                return array_map( function ( $item ) {
                    return new static( $item );
                }, $countries );
            }
            return null;
        }

    }
}