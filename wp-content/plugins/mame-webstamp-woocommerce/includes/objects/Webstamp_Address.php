<?php

if ( !defined( 'ABSPATH' ) ) {
    exit;
}

if ( !class_exists( 'Webstamp_Address' ) ) {

    /**
     * Class Webstamp_Address
     */
    class Webstamp_Address extends Webstamp_Object
    {
        public $id;
        public $organization;
        public $company_addition;
        public $title;
        public $firstname;
        public $lastname;
        public $addition;
        public $street;
        public $pobox;
        public $pobox_lang;
        public $pobox_nr;
        public $zip;
        public $city;
        public $country;
        public $reference;
        public $cash_on_delivery_amount;
        public $cash_on_delivery_esr;
        public $print_address;

        public function __construct( $object = null )
        {
            parent::__construct( $object );
        }

        /**
         * Returns the WooCommerce shipping address as a Webstamp_Address object.
         *
         * @param WC_Order $order
         * @return Webstamp_Address
         */
        public static function get_shipping_address( $order )
        {

            $street = $order->get_shipping_address_1();
            if ( $street ) {
                $street .= ' ' . $order->get_shipping_address_2();
            } else {
                $street = $order->get_shipping_address_2();
            }

            $address               = new self();
            $address->organization = $order->get_shipping_company();
            $address->firstname    = $order->get_shipping_first_name();
            $address->lastname     = $order->get_shipping_last_name();
            $address->street       = $street;
            $address->zip          = $order->get_shipping_postcode();
            $address->city         = $order->get_shipping_city();
            $address->country      = $order->get_shipping_country();

            return $address;
        }

        /**
         * Returns the WooCommerce billing address as a Webstamp_Address object.
         *
         * @param WC_Order $order
         * @return Webstamp_Address
         */
        public static function get_billing_address( $order )
        {
            $street = $order->get_billing_address_1();
            if ( $street ) {
                $street .= ' ' . $order->get_billing_address_2();
            } else {
                $street = $order->get_billing_address_2();
            }

            $address               = new self();
            $address->organization = $order->get_billing_company();
            $address->firstname    = $order->get_billing_first_name();
            $address->lastname     = $order->get_billing_last_name();
            $address->street       = $street;
            $address->zip          = $order->get_billing_postcode();
            $address->city         = $order->get_billing_city();
            $address->country      = $order->get_billing_country();

            return $address;
        }

        /**
         * Returns an adress object with the custom assigned WooCommerce checkout field values.
         *
         * @param WC_Order $order
         * @return Webstamp_Address
         */
        public static function get_custom_address( $order )
        {
            $options = get_option( 'mame_ws_options_group' );

            $address                   = new self();
            $address->organization     = static::get_address_field( $order, $options, 'wc_organization' );
            $address->company_addition = static::get_address_field( $order, $options, 'wc_company_addition' );
            $address->title            = static::get_address_field( $order, $options, 'wc_title' );
            $address->firstname        = static::get_address_field( $order, $options, 'wc_firstname' );
            $address->lastname         = static::get_address_field( $order, $options, 'wc_lastname' );
            $address->addition         = static::get_address_field( $order, $options, 'wc_addition' );
            $address->street           = static::get_address_field( $order, $options, 'wc_street' );
            $address->pobox            = static::get_address_field( $order, $options, 'wc_pobox' );
            $address->zip              = static::get_address_field( $order, $options, 'wc_zip' );
            $address->city             = static::get_address_field( $order, $options, 'wc_city' );
            $address->country          = static::get_address_field( $order, $options, 'wc_country' );

            return $address;
        }

        private static function get_address_field( $order, $options, $key )
        {
            if ( !isset( $options[ $key ] ) || $options[ $key ] == 'none' )
                return '';

            $option = ( substr( $options[ $key ], 0, 1 ) == '_' ? $options[ $key ] : '_' . $options[ $key ] );

            $value = get_post_meta( Mame_WC_Helper::get ('id', $order), $option, true );

            if ( empty( $value ) )
                return '';

            return $value;
        }

        /**
         * Returns the default sender address saved in the WebStamp settings.
         *
         * @return Webstamp_Address
         */
        public static function get_sender_address()
        {
            $address = new self();
            $options = get_option( 'mame_ws_options_group' );

            $vars = get_object_vars( $address );
            foreach ( $vars as $name => $value ) {
                if ( array_key_exists( $name, $options ) ) {
                    $address->$name = $options[ $name ];
                }
            }

            return $address;
        }

        /**
         * @return Webstamp_Address
         */
        public static function get_bln_address()
        {
            $address = new self();
            $options = get_option( 'mame_ws_options_group' );

            $vars = get_object_vars( $address );
            foreach ( $vars as $name => $value ) {
                if ( array_key_exists( 'bln_' . $name, $options ) ) {
                    $address->$name = $options[ 'bln_' . $name ];
                }
            }

            return $address;
        }

        public function get_html( $delimiter = '<br>' )
        {
            $array = [];
            if ( $this->organization )
                $array[] = $this->organization;
            if ( $this->company_addition )
                $array[] = $this->company_addition;
            if ( $this->title )
                $array[] = $this->title;
            if ( $this->firstname )
                $array[] = $this->firstname;
            if ( $this->lastname )
                $array[] = $this->lastname;
            if ( $this->addition )
                $array[] = $this->addition;
            if ( $this->street )
                $array[] = $this->street;
            if ( $this->pobox )
                $array[] = $this->pobox;
            if ( $this->pobox_nr )
                $array[] = $this->pobox_nr;
            if ( $this->zip )
                $array[] = $this->zip;
            if ( $this->city )
                $array[] = $this->city;
            if ( $this->country )
                $array[] = $this->country;

            return implode( $delimiter, $array );
        }
    }

}