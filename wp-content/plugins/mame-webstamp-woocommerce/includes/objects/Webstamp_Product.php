<?php

if ( !defined( 'ABSPATH' ) ) {
    exit;
}

if ( !class_exists( 'Webstamp_Product' ) ) {

    /**
     * Class Webstamp_Product
     */
    class Webstamp_Product extends Webstamp_Object
    {
        public $number;
        public $category;
        public $category_number;
        public $post_product_number;
        public $price;
        public $name;
        public $additions;
        public $delivery;
        public $format;
        public $size_din;
        public $max_size_length;
        public $max_size_height;
        public $max_edge_length;
        public $max_scale;
        public $max_weight;
        public $zone;
        public $product_list;
        public $product_list_number;
        public $subsystem;
        public $segment;
        public $barcode;

        public function __construct( $object = null )
        {
            parent::__construct( $object );

        }

        /**
         * @return null|Webstamp_Category
         */
        public function get_category()
        {
            return Webstamp_Database_Manager::get_category( $this->category_number );
        }

        /**
         * @return null|Webstamp_Zone
         */
        public function get_zone()
        {
            return Webstamp_Database_Manager::get_zone( $this->zone );
        }

        /**
         * Returns all products saved in the database.
         *
         * @return Webstamp_Product[]|null
         */
        public static function get_all()
        {
            $products = Webstamp_Database_Manager::get_products();
            if ( !empty( $products ) ) {
                return array_map( function ( $item ) {
                    return new static( $item );
                }, $products );
            }
            return null;
        }

    }

}