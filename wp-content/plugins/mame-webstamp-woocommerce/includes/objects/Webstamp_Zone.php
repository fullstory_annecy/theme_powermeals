<?php

if ( !defined( 'ABSPATH' ) ) {
    exit;
}

if ( !class_exists( 'Webstamp_Zone' ) ) {

    /**
     * Class Webstamp_Zone
     */
    class Webstamp_Zone extends Webstamp_Object
    {
        public $number;
        public $name;

        public function __construct( $object = null )
        {
            parent::__construct( $object );
        }

        /**
         * Returns all zones saved in the database.
         * Optionally pass a category id to only retrieve zones of that category.
         *
         * @param null|int $category
         * @return Webstamp_Zone[]|null
         */
        public static function get_all( $category = null )
        {
            $zones = Webstamp_Database_Manager::get_zones( $category );
            if ( !empty( $zones ) ) {
                return array_map( function ( $item ) {
                    return new static( $item );
                }, $zones );
            }
            return null;
        }
    }

}