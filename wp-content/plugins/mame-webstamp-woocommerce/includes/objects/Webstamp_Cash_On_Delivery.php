<?php

if ( !defined( 'ABSPATH' ) ) {
    exit;
}

if ( !class_exists( 'Webstamp_Cash_On_Delivery' ) ) {

    /**
     * Class Webstamp_Cash_On_Delivery
     */
    class Webstamp_Cash_On_Delivery extends Webstamp_Object
    {
        public $transaction_type;
        public $esr_customer_number;
        public $iban;
        public $beneficiary;

        public function __construct( $object = null )
        {
            parent::__construct( $object );
        }

    }
}