<?php

if ( !defined( 'ABSPATH' ) ) {
    exit;
}

if ( !class_exists( 'Webstamp_Stamp' ) ) {

    /**
     * Class Webstamp_Stamp
     */
    class Webstamp_Stamp extends Webstamp_Object
    {
        public $stamp_id;
        public $order_id;
        public $tracking_number;
        public $file_name;
        public $compression;
        public $image_width_mm;
        public $image_height_mm;
        public $image_width_px;
        public $image_height_px;
        public $mime_type;
        public $reference;
        public $print_data;

        public function __construct( $object = null )
        {
            parent::__construct( $object );
        }

        /**
         * Returns the order to which the stamp belongs.
         *
         * @return null|Webstamp_Order
         */
        public function get_order()
        {
            return Webstamp_Database_Manager::get_order( $this->order_id );
        }

        /**
         * Returns the content of the stamp image file. Returns false if file not found.
         *
         * @param string $delimiter
         * @return bool|string
         */
        public function get_file( $delimiter = '<br>' )
        {

            if ( $this->file_name ) {
                $upload_dir = wp_upload_dir();
                $file       = $upload_dir[ 'basedir' ] . '/webstamp/stamps/' . $this->file_name;
                return file_get_contents( $file );
            }

/*
            $files = json_decode( $this->file_name );
            if ( $files ) {

                $result     = '';
                $upload_dir = wp_upload_dir();

                foreach ( $files as $file ) {
                    $file   = $upload_dir[ 'basedir' ] . '/webstamp/stamps/' . $file;
                    $result .= file_get_contents( $file ) . $delimiter;
                }
                return $result;
            }
            return false;
*/
        }

        /**
         * Returns the URL to download the stamp.
         *
         * @return null|string
         */
        public function get_file_url()
        {
            if ( $this->file_name )
                return add_query_arg( [ MAME_WS_PREFIX . '-type' => 'stamps', MAME_WS_PREFIX . '-file' => $this->file_name, MAME_WS_PREFIX . '-download' => '1' ], get_admin_url() );
            return null;
        }

        /**
         * Returns the html file download button.
         *
         * @param $text
         * @return null|string
         */
        public function get_file_button( $text )
        {
            $url = $this->get_file_url();
            if ( $url )
                return '<a class="' . MAME_WS_PREFIX . '-btn ' . MAME_WS_PREFIX . '-download-stamp-btn" .  href="' . $this->get_file_url() . '" target="_blank">' . $text . '</a>';
            return '';
        }

        /**
         * returns the tracking URL if the tracking_number exists.
         *
         * @return null|string
         */
        public function get_tracking_url()
        {
            if ( $this->tracking_number )
                return 'https://www.post.ch/swisspost-tracking?formattedParcelCodes=' . $this->tracking_number;
            return null;
        }

    }

}