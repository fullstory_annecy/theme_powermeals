<?php

if ( !defined( 'ABSPATH' ) ) {
    exit;
}

if ( !class_exists( 'Webstamp_Media_Type' ) ) {

    /**
     * Class Webstamp_Media_Type
     */
    class Webstamp_Media_Type extends Webstamp_Object
    {
        public $number;
        public $name;
        public $image_possible;
        public $categories;

        public function __construct( $object = null )
        {
            parent::__construct( $object );
        }

        /**
         * Returns all media types saved in the database.
         *
         * @return Webstamp_Media_Type[]|null
         */
        public static function get_all()
        {
            $media_types = Webstamp_Database_Manager::get_media_types();
            if ( !empty( $media_types ) ) {
                return array_map( function ( $item ) {
                    return new static( $item );
                }, $media_types );
            }
            return null;
        }

    }

}