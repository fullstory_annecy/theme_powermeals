<?php

if ( !defined( 'ABSPATH' ) ) {
    exit;
}

if ( !class_exists( 'Webstamp_Media' ) ) {

    /**
     * Class Webstamp_Media
     */
    class Webstamp_Media extends Webstamp_Object
    {
        public $number;
        public $name;
        public $type;

        public function __construct( $object = null )
        {
            parent::__construct( $object );
        }

        /**
         * Returns all media saved in the database.
         *
         * @return Webstamp_Media[]|null
         */
        public static function get_all()
        {
            $medias = Webstamp_Database_Manager::get_medias();
            if ( !empty( $medias ) ) {
                return array_map( function ( $item ) {
                    return new static( $item );
                }, $medias );
            }
            return null;
        }
    }

}