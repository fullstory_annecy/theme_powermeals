<?php

if ( !defined( 'ABSPATH' ) ) {
    exit;
}

if ( !class_exists( 'Webstamp_Object' ) ) {

    /**
     * Class Webstamp_Object
     */
    class Webstamp_Object
    {
        /**
         * Webstamp_Object constructor.
         * Loads the values of $object into $this where $object can be an object or an array.
         *
         * @param null|StdClass|array $object
         */
        public function __construct( $object = null )
        {
            if ( $object ) {
                if ( is_object( $object ) )
                    $object = get_object_vars( $object );
                foreach ( $object as $name => $value ) {
                    $this->$name = $value;
                }
            }
        }

        /**
         * populate this object with properties of other object.
         *
         * @param StdClass|array $object
         */
        public function populate( $object )
        {
            if ( is_object( $object ) )
                $object = get_object_vars( $object );
            foreach ( $object as $name => $value ) {
                if ( property_exists( $this, $name ) )
                    $this->$name = $value;
            }

        }
    }

}