<?php

if ( !defined( 'ABSPATH' ) ) {
    exit;
}

if ( !class_exists( 'Webstamp_Order' ) ) {

    /**
     * Class Webstamp_Order
     *
     * @property Webstamp_Media $media_object
     * @property Webstamp_Media_Type $media_type_object
     * @property Webstamp_Address[] $a_addresses
     */
    class Webstamp_Order extends Webstamp_Object
    {
        /**
         * @var int
         */
        public $id;

        /**
         * @var int
         */
        public $order_id;

        /**
         * @var float
         */
        public $price;

        /**
         * @var float
         */
        public $item_price;

        /**
         * @var int
         */
        public $valid_days;

        /**
         * @var int
         */
        public $valid_until;

        /**
         * @var int
         */
        public $product;

        /**
         * @var int
         */
        public $post_product;

        /**
         * @var string
         */
        public $delivery_receipt;

        /**
         * @var int
         */
        public $category;

        /**
         * @var Webstamp_Category
         */
        public $category_object;

        /**
         * @var int
         */
        public $zone;

        /**
         * @var Webstamp_Zone
         */
        public $zone_object;

        /**
         * @var string
         */
        public $delivery_method;

        /**
         * @var string
         */
        public $format;

        /**
         * @var mixed
         */
        public $additions;

        /**
         * @var int
         */
        public $address_id;

        /**
         * @var Webstamp_Address
         */
        public $address;

        /**
         * @var Webstamp_Address[]
         */
        public $a_addresses;

        /**
         * @var int
         */
        public $media;

        /**
         * @var Webstamp_Media
         */
        public $media_object;

        /**
         * @var int
         */
        public $media_type;

        /**
         * @var Webstamp_Media_Type
         */
        public $media_type_object;

        /**
         * @var string
         */
        public $license_number;

        /**
         * @var string
         */
        public $status;

        /**
         * @var int
         */
        public $wc_order_id;

        /**
         * @var string
         */
        public $reference;

        /**
         * @var bool
         */
        public $use_sender_address;

        /**
         * @var bool
         */
        public $use_address;

        /**
         * @var Webstamp_Address
         */
        public $sender;

        /**
         * @var int
         */
        public $sender_id;

        /**
         * @var int
         */
        public $media_startpos;

        /**
         * @var int
         */
        public $quantity;

        /**
         * @var mixed
         */
        public $image;

        /**
         * @var int
         */
        public $image_id;

        /**
         * @var string
         */
        public $image_url;

        /**
         * @var string
         */
        public $order_comment;

        /**
         * @var string
         */
        public $cash_on_delivery_info;

        /**
         * @var Webstamp_Custom_Media
         */
        public $custom_media;

        /**
         * @var int
         */
        public $time;

        const STATUS_DRAFT      = 'draft';
        const STATUS_PROCESSING = 'processing';
        const STATUS_COMPLETE   = 'complete';

        public function __construct( $order_object = null )
        {
            parent::__construct( $order_object );
            if ( !is_array( $this->additions ) )
                $this->additions = json_decode( $this->additions );

        }

        /**
         * Returns the wsws_address receiver address as a Webstamp_Address object.
         *
         * @return Webstamp_Address
         */
        public function get_address()
        {
            return Webstamp_Database_Manager::get_address( $this->address_id );
        }

        /**
         * Returns the wsws_address sender address as a Webstamp_Address object.
         *
         * @return Webstamp_Address
         */
        public function get_sender()
        {
            return Webstamp_Database_Manager::get_address( $this->sender_id );
        }

        /**
         * Returns wsws_category as a Webstamp_Category object.
         *
         * @return null|Webstamp_Category
         */
        public function get_category()
        {
            return Webstamp_Database_Manager::get_category( $this->category );
        }

        /**
         * Returns wsws_zone as a Webstamp_Zone object.
         *
         * @return null|Webstamp_Zone
         */
        public function get_zone()
        {
            return Webstamp_Database_Manager::get_zone( $this->zone );
        }

        /**
         * Returns wsws_media_type as a Webstamp_Media_Type object.
         *
         * @return null|Webstamp_Media_Type
         */
        public function get_media_type()
        {
            return Webstamp_Database_Manager::get_media_type( $this->media_type );

        }

        /**
         * Returns wsws_media as a Webstamp_Media object.
         *
         * @return null|Webstamp_Media
         */
        public function get_media()
        {
            return Webstamp_Database_Manager::get_media( $this->media );
        }

        /**
         * Returns wsws_stamp as an array of Webstamp_Stamp object.
         *
         * @return Webstamp_Stamp[]
         */
        public function get_stamps()
        {
            return Webstamp_Database_Manager::get_stamps( $this->order_id );
        }

        /**
         * Returns the download URL for the delivery receipt. Returns null if no delivery receipt available.
         *
         * @return null|string
         */
        public function get_delivery_receipt_url()
        {
            if ( !empty ( $this->delivery_receipt ) )
                return add_query_arg( [ MAME_WS_PREFIX . '-type' => 'receipts', MAME_WS_PREFIX . '-file' => $this->delivery_receipt, MAME_WS_PREFIX . '-download' => '1' ], get_admin_url() );
            return null;
        }

        /**
         * Deletes the $properties from the order.
         *
         * @param $properties
         */
        public function delete( $properties )
        {
            foreach ( $properties as $p ) {

                if ( $p === "address" ) {
                    Webstamp_Database_Manager::delete( Webstamp_Database_Manager::table( Webstamp_Database_Manager::ADDRESS ), [ 'id' => $this->address_id ] );
                    Webstamp_Database_Manager::update( Webstamp_Database_Manager::table( Webstamp_Database_Manager::ORDER ), [ 'address_id' => '' ], [ 'id' => $this->id ] );
                } elseif ( $p === "addition" ) {
                    Webstamp_Database_Manager::update( Webstamp_Database_Manager::table( Webstamp_Database_Manager::ORDER ), [ $p . 's' => '' ], [ 'id' => $this->id ] );

                } elseif ( $p === 'country' ) {

                    Webstamp_Database_Manager::update( Webstamp_Database_Manager::table( Webstamp_Database_Manager::ADDRESS ), [ $p => '' ], [ 'id' => $this->address_id ] );

                } else {
                    Webstamp_Database_Manager::update( Webstamp_Database_Manager::table( Webstamp_Database_Manager::ORDER ), [ $p => '' ], [ 'id' => $this->id ] );
                }
            }
        }

        /**
         * Creates a new WebStamp order. Returns true if the order is successful and a WP_Error object on failure.
         *
         * @return bool|WP_Error
         */
        public function order()
        {
            return $this->handle_order( array( new Webstamp_Order_Manager(), 'new_order' ), 'stamps' );

        }

        /**
         * Creates a new WebStamp preview order. Returns true if the order is successful and a WP_Error object on failure.
         *
         * @return bool|WP_Error
         */
        public function preview_order()
        {
            return $this->handle_order( array( new Webstamp_Order_Manager(), 'new_order_preview' ), 'temp' );
        }

        /**
         * Sends a new order request.
         * $function is one of new_order or new_order_preview of the Webstamp_Order_Manager.
         * $stamp_directory is one of 'temp' or 'stamps'.
         *
         * @param $function
         * @param $stamp_directory
         * @return bool|WP_Error
         */
        private function handle_order( $function, $stamp_directory )
        {
            // Get product.
            $product = Webstamp_Database_Manager::get_unique_product( $this->zone, $this->delivery_method, $this->format, $this->additions );

            if ( !$product )
                return new WP_Error( 'null', __( 'No product found', 'dhuett' ) );
            $this->product = $product->post_product_number;

            $result = $function( $this );

            if ( is_array( $result ) && isset( $result[ 'error' ] ) )
                return new WP_Error( 'order', __( 'Order failed' ) );

            // Get stamp
            $stamp = new Webstamp_Stamp();
            if ( ( $result->stamps && ( $result->stamps->item ) ) )
                $stamp->populate( $result->stamps->item );

            if ( !empty ( $result->print_data ) )
                $stamp->print_data = $result->print_data;

            $stamp->order_id = $this->order_id;


            // Get stamp
            /* if ( !empty ( $result->print_data ) ) {
                 $stamp             = new Webstamp_Stamp();
                 $stamp->order_id   = $this->order_id;
                 $stamp->print_data = $result->print_data;

             } elseif ( ( $result->stamps && ( $result->stamps->item ) && !empty( $result->stamps->item->print_data ) ) ) {
                 $stamp = $result->stamps->item;

                 $stamp = new Webstamp_Stamp( $stamp );
             }*/
            // Send preview PDF.
            if ( $stamp ) {

                $upload_dir  = wp_upload_dir();
                $upload_path = str_replace( '/', DIRECTORY_SEPARATOR, $upload_dir[ 'basedir' ] ) . DIRECTORY_SEPARATOR . 'webstamp' . DIRECTORY_SEPARATOR . $stamp_directory . DIRECTORY_SEPARATOR;

                $filename = Webstamp_Database_Manager::save_file( $stamp->print_data, $upload_path );
                if ( $filename ) {

                    $stamp->file_name = $filename;
//                    var_dump($stamp);die();
                    Webstamp_Database_Manager::save_stamp( $stamp );

                }

                return true;
            }

            return new WP_Error( 'order', 'Stamp(s) missing.' );
        }

        /**
         * Returns all Webstamp_Stamp objects belonging to the WC order $order.
         *
         * @param $order
         * @return bool|Webstamp_Stamp[]
         */
        public static function get_stamps_from_woocommerce_order( $order )
        {
            $ws_orders = Webstamp_Database_Manager::get_orders_by_wc_order( Mame_WC_Helper::get ('id', $order) );
            if ( !empty( $ws_orders ) ) {
                foreach ( $ws_orders as $o ) {
                    $stamps = $o->get_stamps();
                    if ( !empty( $stamps ) ) {
                        return $stamps;
                    }
                }
            }
            return false;
        }

        /**
         * Returns the formatted HTML string of all the stamp image files of the WC order $order.
         *
         * @param WC_Order $order
         * @param bool $width
         * @param bool $height
         * @param bool $newline
         * @return string
         */
        public static function get_stamp_images_from_woocommerce_order( $order, $width = false, $height = false, $newline = true )
        {
            $stamps = static::get_stamps_from_woocommerce_order( $order );
            $str    = '';
            if ( $stamps ) {
                foreach ( $stamps as $s ) {
                    $str .= '<img ' . ( $width ? 'width="' . $width . '" ' : '' ) . ( $height ? 'height="' . $height . '" ' : '' ) . 'src="' . $s->get_file_url() . '">';
                    if ( $newline )
                        $str .= '<br>';
                }
            }
            return $str;
        }

        /**
         * Prints the formatted HTML string of all the stamp image files of the WC order $order.
         *
         * @param WC_Order $order
         * @param bool $width
         * @param bool $height
         * @param bool $newline
         */
        public static function print_stamp_images_from_woocommerce_order( $order, $width = false, $height = false, $newline = true )
        {
            echo static::get_stamp_images_from_woocommerce_order( $order, $width, $height, $newline );
        }
    }

}