<?php

if ( !defined( 'ABSPATH' ) ) {
    exit;
}

if ( !class_exists( 'Webstamp_Order_SoapClient' ) ) {

    /**
     * Class Webstamp_Order_SoapClient
     */
    class Webstamp_Order_SoapClient extends Webstamp_SoapClient
    {

        public function __construct()
        {
            parent::__construct();
        }

        /**
         * New preview order request.
         *
         * @param $order
         * @return string
         */
        public function new_order_preview( $order )
        {
            //var_dump( $this->prepare_order( $order ));die();
            return $this->send_soap_request( $this->prepare_order( $order ), array( $this->client, 'new_order_preview' ) );
        }

        /**
         * New order request.
         *
         * @param $order
         * @return string
         */
        public function new_order( $order )
        {
            return $this->send_soap_request( $this->prepare_order( $order ), array( $this->client, 'new_order' ) );
        }

        /**
         * @param Webstamp_Order $order
         * @return mixed
         */
        private function prepare_order( $order )
        {
            $options = get_option( 'mame_ws_options_group' );

            // Is cod?
            $is_cod    = false;
            $product   = Webstamp_Database_Manager::get_unique_product( $order->zone, $order->delivery_method, $order->format, $order->additions );
            $additions = unserialize( $product->additions );
            foreach ( $additions as $a ) {
                if ( $a->short_name == 'BLN' ) {
                    $is_cod = true;
                    break;
                }
            }


            //$args->print_zone = 1;

            // Get address.
            if ( $order->use_address && $order->address_id ) {
                $order->a_addresses[] = $order->get_address( $order->address_id );
            } else {
                // Ignored if address.
                if ( empty( $order->quantity ) )
                    $order->quantity = 1;
            }

            if ( $order->use_sender_address && $order->sender_id ) {
                $order->sender = $order->get_sender( $order->sender_id );
            }

            // Output to single PDF?
            if ( $options[ 'stamp_file_type' ] === 'pdf' ) {
                $order->single    = true;
                $order->file_type = 'pdf';
            } else {
                $order->single     = false;
                $order->file_type  = 'png';
                $order->media      = null;
                $order->media_type = null;
            }

            // We use the post_product_number.
            $order->post_product = true;

            // Image
            if ( $order->image_id ) {
                $attachment_image = wp_get_attachment_image_src( $order->image_id );
                if ( !empty( $attachment_image ) )
                    $order->image = file_get_contents( $attachment_image[ 0 ] );
            }

            // BLN
            if ( $is_cod ) {
                $options                      = get_option( 'mame_ws_options_group' );
                $cod                          = new Webstamp_Cash_On_Delivery();
                $cod->beneficiary             = Webstamp_Address::get_bln_address();
                $cod->transaction_type        = $options[ 'bln_transaction_type' ];
                $cod->iban                    = $options[ 'bln_iban' ];
                $cod->esr_customer_number     = $options[ 'bln_isr' ];
                $order->cash_on_delivery_info = $cod;
            }
            $order->reference = uniqid();
            // $args->order_comment = '';

            // $args->cash_on_delivery_info = '';
            // $args->custom_media =

            return $order;
        }

        public function copy_order_by_id( $order_id, $reference = null, $quantity = null, $order_comment = null )
        {
            $args            = new StdClass();
            $args->order_id  = $order_id;
            $args->file_type = 'pdf';

            if ( !empty( $reference ) )
                $args->reference = $reference;

            if ( !empty( $quantity ) )
                $args->quantity = $quantity;

            if ( !empty( $order_comment ) )
                $args->order_comment = $order_comment;

            return $this->send_soap_request( $args, array( $this->client, 'copy_order_by_id' ) );
        }

        public function previous_order( $order_id = null, $stamp_id = null )
        {
            $args = new StdClass();
            if ( !empty( $order_id ) )
                $args->order_id = $order_id;
            if ( !empty( $stamp_id ) )
                $args->stamp_id = $stamp_id;

            return $this->send_soap_request( $args, array( $this->client, 'previous_order' ) );

        }

    }

}