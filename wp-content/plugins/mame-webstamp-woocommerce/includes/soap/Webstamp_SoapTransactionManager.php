<?php

class Webstamp_SoapTransactionManager
{
    public $json_response;

    public function __construct()
    {
        add_action( 'wp_ajax_mame_ws_ajax_request', array( $this, 'ajax_handler' ) );

        $this->json_response = new Mame_Json_Response();
    }

    public function ajax_handler()
    {

        if ( !check_ajax_referer( 'mame_ws_nonce', 'security' ) ) {
            return;
        }


        if ( $_POST[ 'ws_action' ] === 'option' ) {

            Webstamp_Database_Manager::setup();

            $options_manager = new Webstamp_Options_Manager();
            $response = $options_manager->update();

            $this->json_response->status($response)->respond();

        }

    }

}