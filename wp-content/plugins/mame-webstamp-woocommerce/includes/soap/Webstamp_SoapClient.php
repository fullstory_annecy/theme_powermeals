<?php

if ( !defined( 'ABSPATH' ) ) {
    exit;
}

if ( !class_exists( 'Webstamp_SoapClient' ) ) {

    /**
     * Class Webstamp_SoapClient
     */
    class Webstamp_SoapClient
    {

        public $client;
        public $identification;

        public function __construct()
        {

            $this->set_client();
            $this->set_identification();
        }

        public function exit_with_error( $fault )
        {
            exit( $this->return_error( $fault ) );
        }

        public function return_error( $fault )
        {
            $msg = sprintf( __( "ERROR: The WebStamp Server returned Error-Code '%s' with the following message: %s", 'dhuett' ), $fault->faultcode, $fault->faultstring );
            return $msg;
        }

        public function return_error_message( $fault )
        {
            return $fault->faultstring;
        }

        /**
         * Create SOAP headers.
         *
         * @return \SOAPHeader
         */
        public function create_headers( )
        {
            $headers = array();

            $namespace   = 'https://webstamp.post.ch/wsws/soap/v6';
            $header_body = array(
            );
            $headers[]   = new SOAPHeader( $namespace, 'RequestHeaderElement', $header_body );

            return $headers;

        }

        /**
         * Creates a new SOAP client and assigns it to $client.
         *
         * @return \SoapClient
         */
        public function set_client()
        {
            $options     = get_option( 'mame_ws_options_group' );
            $environment = isset( $options[ 'environment' ] ) ? $options[ 'environment' ] : 'test';

            $wsdl = $environment == 'production' ? plugin_dir_path( __FILE__ ) . 'production.wsdl' : plugin_dir_path( __FILE__ ) . 'test.wsdl';

            if ( $environment == 'production' ) {
                $location_url = MAME_WS_PRODUCTION_URL . '/wsws/soap/v6';
            } else {
                $location_url = MAME_WS_TEST_URL . '/wsws/soap/v6';
            }

            $args = array(

                'location'           => $location_url,
                //'trace' => true,
                'encoding'           => 'UTF-8',
                'connection_timeout' => 20,
                //'exceptions' => true, 
                'compression'        => ( SOAP_COMPRESSION_ACCEPT | SOAP_COMPRESSION_GZIP ),
            );

            $proxy_host = get_option( 'mame_ws_settings_proxyhost' );
            $proxy_port = get_option( 'mame_ws_settings_proxyport' );

            if ( !empty( $proxy_host ) ) {
                $args = array_merge( $args, array( 'proxy_host' => $proxy_host ) );
            }
            if ( !empty( $proxy_port ) ) {
                $args = array_merge( $args, array( 'proxy_port' => $proxy_port ) );
            }

            $this->client = new SoapClient( $wsdl, $args );

        }

        /**
         * Adds the identification object to the client.
         */
        public function set_identification()
        {
            $options = get_option( 'mame_ws_options_group' );

            if ( $options && !empty( $options ) && isset( $options[ 'user_id' ] ) && isset( $options[ 'password' ] ) ) {

                $allowed_languages = [ 'de', 'fr', 'it', 'en' ];
                $language          = defined( 'ICL_LANGUAGE_CODE' ) ? ICL_LANGUAGE_CODE : substr( ( Mame_WC_Helper::is_woocommerce_version_up( '2.7.0' ) ? get_user_locale() : get_locale() ), 0, 2 );

                $identification                  = new StdClass();
                $identification->application     = MAME_WS_API_KEY;
                $identification->language        = in_array( $language, $allowed_languages ) ? $language : 'en';
                $identification->userid          = $options[ 'user_id' ];
                $identification->password        = sha1( $options[ 'password' ] );
                $identification->encryption_type = 'sha1';
                $this->identification            = $identification;
            } else {
                // Return error
            }
        }

        public function send_soap_request( $args, $func )
        {
            $request                       = new StdClass();
            $request->args                 = $args;
            $request->args->identification = $this->identification;
            // Optional
            // $request->args->timestamp = null;

            try {
                $response = $func( $request );
                //var_dump($response);die();

            } catch ( SoapFault $e ) {

                if ( MAME_WS_DEBUG ) {
                    print_r( $this->client );
                }

                //$this->exit_with_error( $e );
                $response = $this->return_error( $e );
            }
            // $this->debug( $response );

            return $response;
        }


        /**
         * Prints and logs response.
         *
         * @param $response
         */
        public function debug( $response )
        {

            if ( MAME_WS_DEBUG ) {

                // Show
                echo 'System Status<br>';
                echo '<pre>';
                var_dump( $response );
                echo '</pre>';

                // Log
                Mame_Helper_Functions::write_log_with_tag( get_called_class() . '::get_categories.$response', $response );
            }
        }
    }

}