<?php

if ( !defined( 'ABSPATH' ) ) {
    exit;
}

if ( !class_exists( 'Webstamp_Options_SoapClient' ) ) {

    /**
     * Class Webstamp_Options_SoapClient
     */
    class Webstamp_Options_SoapClient extends Webstamp_SoapClient
    {
        public function __construct()
        {
            parent::__construct();
        }

        /**
         * WSDL: get_categories
         *
         * @return mixed
         */
        public function get_categories()
        {
            return $this->send_soap_request( new StdClass(), array( $this->client, 'get_categories' ) );
        }

        public function get_products()
        {
            $args = new StdClass();

            // Filters
            // $args->product_lists       = null;
            // $args->post_product_number = '';
            // gk/pk
            $options             = get_option( 'mame_ws_options_group' );
            $args->customer_type = isset( $options[ 'customer_type' ] ) ? $options[ 'customer_type' ] : 'gk';

            return $this->send_soap_request( $args, array( $this->client, 'get_products' ) );
        }

        public function get_media_types()
        {
            return $this->send_soap_request( new StdClass(), array( $this->client, 'get_media_types' ) );
        }

        public function get_medias()
        {
            return $this->send_soap_request( new StdClass(), array( $this->client, 'get_medias' ) );
        }

        public function get_zones()
        {
            return $this->send_soap_request( new StdClass(), array( $this->client, 'get_zones' ) );
        }

        public function get_countries()
        {
            return $this->send_soap_request( new StdClass(), array( $this->client, 'get_countries' ) );

        }

        /**
         *
         * Geschäftskunden können neu bei gewissen Produkten eine persönliche Frankierlizenz angeben. Die Frankierlizenzen ermöglichen es dem Kunden gewisse zusätzliche Produkte zu bestellen und wird auch für die Abrechnung der Bestellung verwendet. Eine Frankier- lizenz kann jeweils nur für eine Produktart verwendet werden. Es ist für den Integrator nicht möglich, diese Unterscheidung zu machen – der Kunde muss selber die passende Frankierlizenz auswählen. Alternativ kann bei der Abfrage der Frankierlizenzen eine Produkt Nummer (Typ: post_product_number) angegeben werden um die Auswahl der Frankierlizenzen einzuschränken.
         * Die Frankierlizenzen eines Kunden können sich theoretisch bei jedem Auftrag ändern, deshalb sind die Frankierlizenzen mit Vorsicht zwischen zu speichern.
         */
        public function get_licenses()
        {
            return $this->send_soap_request( new StdClass(), array( $this->client, 'get_licenses' ) );
        }

        /**
         * Returns the various product ranges. The product ranges are divided into two main categories in WebStamp: “pk” (private customers – pc) and “gk”(business customers – bc), and there are further subdivisions in each category. A customer can be assigned to multiple product ranges, but only to one main category (pc or bc). By specifying customer_type, you can query the product ranges relevant for your users. If the parameter is transmitted empty, all product ranges are returned.
         * If a valid user login is specified in the identification object, customer_type is ignored and the product ranges assigned to the customer are returned instead. Queried product range numbers can be included with get_products to limit the product query by range(s).
         * All queries are limited to currently valid product ranges.
         */
        public function get_product_lists()
        {
            return $this->send_soap_request( new StdClass(), array( $this->client, 'get_product_lists' ) );
        }

        /**
         *
         */
        public function get_customer_data()
        {
            return $this->send_soap_request( new StdClass(), array( $this->client, 'get_customer_data' ) );
        }

    }

}