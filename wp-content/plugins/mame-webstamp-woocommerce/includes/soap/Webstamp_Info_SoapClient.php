<?php

if ( !defined( 'ABSPATH' ) ) {
    exit;
}

if ( !class_exists( 'Webstamp_Info_SoapClient' ) ) {

    /**
     * Class Webstamp_Info_SoapClient
     */
    class Webstamp_Info_SoapClient extends Webstamp_SoapClient
    {

        public function __construct()
        {
            parent::__construct();
        }

        public function get_info_receipt(){
            // TODO
        }

        public function get_info_delivery_receipt(){
            // TODO
        }

        public function get_info_journal(){
            // TODO
        }

    }

}