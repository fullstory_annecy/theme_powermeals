<?php

if ( !defined( 'ABSPATH' ) ) {
    exit; // Exit if accessed directly
}

if ( !class_exists( 'Webstamp_Settings_Page' ) ) :

    /**
     * Handlers the WebStamp settings menu page.
     *
     * Class Webstamp_Settings_Page
     */
    class Webstamp_Settings_Page
    {

        static $instance;

        public $order_list;
        public $template_list;

        private $options;
        private $checkout_fields;
        private $field_options;

        public function __construct()
        {
            if ( is_admin() ) { // admin actions
                register_activation_hook( __FILE__, array( $this, 'license_install' ) );

                add_action( 'admin_menu', array( $this, 'add_menu' ) );
                add_action( 'admin_init', array( $this, 'register_settings' ) );
            }
            // For workaround
            $this->page_sections = array();
            // add_action( 'whitelist_options', array( $this, 'whitelist_custom_options_page' ),11 );

            $this->options       = get_option( 'mame_ws_options_group' );
            $this->field_options = [ 'none' => __( '-- no value --', 'dhuett' ) ];

            add_filter( 'set-screen-option', [ $this, 'set_screen' ], 10, 3 );
        }

        public static function get_instance()
        {
            if ( !isset( self::$instance ) ) {
                self::$instance = new self();
            }

            return self::$instance;
        }

        public function set_screen( $status, $option, $value )
        {
            return $value;
        }

        public function screen_option()
        {

            $option = 'per_page';
            $args   = [
                'label'   => 'Orders',
                'default' => 5,
                'option'  => 'orders_per_page'
            ];

            add_screen_option( $option, $args );

            $this->order_list = new Webstamp_Order_List_Table();
        }

        public function screen_template()
        {

            $option = 'per_page';
            $args   = [
                'label'   => 'Templates',
                'default' => 5,
                'option'  => 'templates_per_page'
            ];

            add_screen_option( $option, $args );

            $this->template_list = new Webstamp_Template_List_Table();
        }

        public function whitelist_custom_options_page( $whitelist_options )
        {
            // Custom options are mapped by section id; Re-map by page slug.
            foreach ( $this->page_sections as $page => $sections ) {
                $whitelist_options[ $page ] = array();
                foreach ( $sections as $section )
                    if ( !empty( $whitelist_options[ $section ] ) )
                        foreach ( $whitelist_options[ $section ] as $option )
                            $whitelist_options[ $page ][] = $option;
            }
            return $whitelist_options;
        }

        private function add_settings_section( $id, $title, $cb, $page )
        {
            add_settings_section( $id, $title, $cb, $page );
            if ( $id != $page ) {
                if ( !isset( $this->page_sections[ $page ] ) )
                    $this->page_sections[ $page ] = array();
                $this->page_sections[ $page ][ $id ] = $id;
            }
        }

        public function add_menu()
        {
            add_menu_page( __( 'WebStamp', 'dhuett' ), __( 'WebStamp', 'dhuett' ), 'manage_options', 'mame_ws_menu', array( $this, 'display_webstamp_editor_page' ), plugin_dir_url( dirname( dirname( __FILE__ ) ) ) . '/assets/images/webstamp_menu_icon.png' );

            add_submenu_page( 'mame_ws_menu', __( 'Editor', 'dhuett' ), __( 'Editor', 'dhuett' ), 'manage_options', 'mame_ws_menu', array( $this, 'display_webstamp_editor_page' ) );
            $hook          = add_submenu_page( 'mame_ws_menu', __( 'Orders', 'dhuett' ), __( 'Orders', 'dhuett' ), 'manage_options', 'mame_ws_menu_orders', array( $this, 'display_webstamp_orders_page' ) );
            $template_hook = add_submenu_page( 'mame_ws_menu', __( 'Templates', 'dhuett' ), __( 'Templates', 'dhuett' ), 'manage_options', 'mame_ws_menu_templates', array( $this, 'display_webstamp_templates_page' ) );

            //            add_submenu_page( 'mame_ws_menu', __( 'Products', 'dhuett' ), __( 'Products', 'dhuett' ), 'manage_options', 'mame_ws_menu_products', array( $this, 'display_webstamp_products_page' ) );
//            add_submenu_page( 'mame_ws_menu', __( 'Categories', 'dhuett' ), __( 'Categories', 'dhuett' ), 'manage_options', 'mame_ws_menu_categories', array( $this, 'display_webstamp_categories_page' ) );
            add_submenu_page( 'mame_ws_menu', __( 'Settings', 'dhuett' ), __( 'Settings', 'dhuett' ), 'manage_options', 'mame_ws_menu_settings', array( $this, 'display_webstamp_settings_page' ) );

            add_submenu_page( null, __( 'Add template', 'dhuett' ), __( 'Add template', 'dhuett' ), 'manage_options', 'mame_ws_menu_add_template', array( $this, 'display_webstamp_add_template_page' ) );

            add_action( "load-$hook", [ $this, 'screen_option' ] );
            add_action( "load-$template_hook", [ $this, 'screen_template' ] );
        }

        public function register_settings()
        {
            register_setting( 'mame_ws_options_group', 'mame_ws_options_group', array( $this, 'options_validate' ) );

            add_settings_section( 'mame_ws_option_group_main_section', __( 'Main Settings', 'dhuett' ), array( $this, 'main_section_text' ), 'mame_ws_options_group' );
            add_settings_field( 'mame_ws_option_group_license_key', __( 'License key', 'dhuett' ), array( $this, 'license_key_field' ), 'mame_ws_options_group', 'mame_ws_option_group_main_section' );
            add_settings_field( 'mame_ws_option_group_environment', __( 'Environment', 'dhuett' ), array( $this, 'environment_field' ), 'mame_ws_options_group', 'mame_ws_option_group_main_section' );
            add_settings_field( 'mame_ws_option_group_customer_type', __( 'Customer type', 'dhuett' ), array( $this, 'customer_type_field' ), 'mame_ws_options_group', 'mame_ws_option_group_main_section' );
            add_settings_field( 'mame_ws_option_group_user_id', __( 'User ID', 'dhuett' ), array( $this, 'user_id_field' ), 'mame_ws_options_group', 'mame_ws_option_group_main_section' );
            add_settings_field( 'mame_ws_option_group_password', __( 'Password', 'dhuett' ), array( $this, 'password_field' ), 'mame_ws_options_group', 'mame_ws_option_group_main_section' );

            $options = get_option( 'mame_ws_options_group' );
            if ( isset( $options[ 'user_id' ] ) && !empty( $options[ 'user_id' ] ) && isset( $options[ 'password' ] ) && !empty( $options[ 'password' ] ) )
                add_settings_field( 'mame_ws_option_group_load_data', __( 'Load WebStamp data', 'dhuett' ), array( $this, 'load_data_field_field' ), 'mame_ws_options_group', 'mame_ws_option_group_main_section' );

//            register_setting( 'mame_ws_address_group', 'mame_ws_address_group', array( $this, 'address_validate' ) );
            add_settings_section( 'mame_ws_option_group_address_section', __( 'Address', 'dhuett' ), array( $this, 'address_section_text' ), 'mame_ws_options_group' );
            add_settings_field( 'mame_ws_option_group_organization', __( 'Company', 'dhuett' ), array( $this, 'organization_field' ), 'mame_ws_options_group', 'mame_ws_option_group_address_section' );
            add_settings_field( 'mame_ws_option_group_company_addition', __( 'Company addition', 'dhuett' ), array( $this, 'company_addition_field' ), 'mame_ws_options_group', 'mame_ws_option_group_address_section' );
            add_settings_field( 'mame_ws_option_group_title', __( 'Title', 'dhuett' ), array( $this, 'title_field' ), 'mame_ws_options_group', 'mame_ws_option_group_address_section' );
            add_settings_field( 'mame_ws_option_group_firstname', __( 'First name', 'dhuett' ), array( $this, 'firstname_field' ), 'mame_ws_options_group', 'mame_ws_option_group_address_section' );
            add_settings_field( 'mame_ws_option_group_lastname', __( 'Last name', 'dhuett' ), array( $this, 'lastname_field' ), 'mame_ws_options_group', 'mame_ws_option_group_address_section' );
            add_settings_field( 'mame_ws_option_group_addition', __( 'Additional details', 'dhuett' ), array( $this, 'addition_field' ), 'mame_ws_options_group', 'mame_ws_option_group_address_section' );
            add_settings_field( 'mame_ws_option_group_street', __( 'Street', 'dhuett' ), array( $this, 'street_field' ), 'mame_ws_options_group', 'mame_ws_option_group_address_section' );
            add_settings_field( 'mame_ws_option_group_pobox', __( 'Postbox', 'dhuett' ), array( $this, 'pobox_field' ), 'mame_ws_options_group', 'mame_ws_option_group_address_section' );
            add_settings_field( 'mame_ws_option_group_zip', __( 'ZIP', 'dhuett' ), array( $this, 'zip_field' ), 'mame_ws_options_group', 'mame_ws_option_group_address_section' );
            add_settings_field( 'mame_ws_option_group_city', __( 'City', 'dhuett' ), array( $this, 'city_field' ), 'mame_ws_options_group', 'mame_ws_option_group_address_section' );
            add_settings_field( 'mame_ws_option_group_country', __( 'Country', 'dhuett' ), array( $this, 'country_field' ), 'mame_ws_options_group', 'mame_ws_option_group_address_section' );

            add_settings_section( 'mame_ws_option_group_stamp_section', __( 'Stamp settings', 'dhuett' ), array( $this, 'stamp_section_text' ), 'mame_ws_options_group' );
            add_settings_field( 'mame_ws_option_group_image', __( 'Image', 'dhuett' ), array( $this, 'image_field' ), 'mame_ws_options_group', 'mame_ws_option_group_stamp_section' );
            add_settings_field( 'mame_ws_option_stamp_file_type', __( 'Stamp file type', 'dhuett' ), array( $this, 'stamp_file_type' ), 'mame_ws_options_group', 'mame_ws_option_group_stamp_section' );
            add_settings_field( 'mame_ws_option_open_print_dialog', __( 'Automatically open print dialog', 'dhuett' ), array( $this, 'open_print_dialog_field' ), 'mame_ws_options_group', 'mame_ws_option_group_stamp_section' );

            add_settings_section( 'mame_ws_option_group_template_section', __( 'Template settings', 'dhuett' ), array( $this, 'template_section_text' ), 'mame_ws_options_group' );
            add_settings_field( 'mame_ws_option_group_template_order_overview', __( 'Directly jump to order confirmation screen on template load?', 'dhuett' ), array( $this, 'template_order_overview_field' ), 'mame_ws_options_group', 'mame_ws_option_group_template_section' );

            if ( MAME_WC_ACTIVE ) {

                if ( isset( $_GET[ 'page' ] ) && $_GET[ 'page' ] == 'mame_ws_menu_settings' ) {

                    include_once( WC()->plugin_path() . '/includes/wc-cart-functions.php' );

                    if ( !class_exists( 'WC_Session' ) ) {
                        include_once( WP_PLUGIN_DIR . '/woocommerce/includes/abstracts/abstract-wc-session.php' );
                    }
                    WC()->session          = new WC_Session_Handler;
                    WC()->customer         = new WC_Customer;

                    if (Mame_WC_Helper::is_woocommerce_version_up('2.7.0')){
                        $this->checkout_fields = WC()->checkout->get_checkout_fields();
                    } else {
                        $this->checkout_fields = WC()->checkout->checkout_fields;
                    }

                    if ( !empty( $this->checkout_fields ) ) {

                        $billing_fields  = $this->checkout_fields[ 'billing' ];
                        $shipping_fields = $this->checkout_fields[ 'shipping' ];

                        foreach ( $shipping_fields as $k => $v ) {
                            $this->field_options[ $k ] = __( 'Shipping', 'dhuett' ) . ' - ' . $v[ 'label' ];
                        }

                        foreach ( $billing_fields as $k => $v ) {
                            $this->field_options[ $k ] = __( 'Billing', 'dhuett' ) . ' - ' . $v[ 'label' ];
                        }
                    }
                }

                add_settings_section( 'mame_ws_option_group_woocommerce_section', __( 'WooCommerce settings', 'dhuett' ), array( $this, 'woocommerce_section_text' ), 'mame_ws_options_group' );
                add_settings_field( 'mame_ws_option_group_templates_email', __( 'Template links in emails', 'dhuett' ), array( $this, 'wc_templates_email_field' ), 'mame_ws_options_group', 'mame_ws_option_group_woocommerce_section' );
                add_settings_field( 'mame_ws_option_group_email_tracking_code', __( 'Tracking code links in emails', 'dhuett' ), array( $this, 'wc_email_tracking_code_field' ), 'mame_ws_options_group', 'mame_ws_option_group_woocommerce_section' );
                add_settings_field( 'mame_ws_option_group_wc_field_description', __( 'Field assignments', 'dhuett' ), array( $this, 'wc_field_description_field' ), 'mame_ws_options_group', 'mame_ws_option_group_woocommerce_section' );
                add_settings_field( 'mame_ws_option_group_wc_organization', __( 'Company', 'dhuett' ), array( $this, 'wc_organization_field' ), 'mame_ws_options_group', 'mame_ws_option_group_woocommerce_section' );
                add_settings_field( 'mame_ws_option_group_wc_company_addition', __( 'Company addition', 'dhuett' ), array( $this, 'wc_company_addition_field' ), 'mame_ws_options_group', 'mame_ws_option_group_woocommerce_section' );
                add_settings_field( 'mame_ws_option_group_wc_title', __( 'Title', 'dhuett' ), array( $this, 'wc_title_field' ), 'mame_ws_options_group', 'mame_ws_option_group_woocommerce_section' );
                add_settings_field( 'mame_ws_option_group_wc_firstname', __( 'First name', 'dhuett' ), array( $this, 'wc_firstname_field' ), 'mame_ws_options_group', 'mame_ws_option_group_woocommerce_section' );
                add_settings_field( 'mame_ws_option_group_wc_lastname', __( 'Last name', 'dhuett' ), array( $this, 'wc_lastname_field' ), 'mame_ws_options_group', 'mame_ws_option_group_woocommerce_section' );
                add_settings_field( 'mame_ws_option_group_wc_addition', __( 'Additional details', 'dhuett' ), array( $this, 'wc_addition_field' ), 'mame_ws_options_group', 'mame_ws_option_group_woocommerce_section' );
                add_settings_field( 'mame_ws_option_group_wc_street', __( 'Street', 'dhuett' ), array( $this, 'wc_street_field' ), 'mame_ws_options_group', 'mame_ws_option_group_woocommerce_section' );
                add_settings_field( 'mame_ws_option_group_wc_pobox', __( 'Postbox', 'dhuett' ), array( $this, 'wc_pobox_field' ), 'mame_ws_options_group', 'mame_ws_option_group_woocommerce_section' );
                add_settings_field( 'mame_ws_option_group_wc_zip', __( 'ZIP', 'dhuett' ), array( $this, 'wc_zip_field' ), 'mame_ws_options_group', 'mame_ws_option_group_woocommerce_section' );
                add_settings_field( 'mame_ws_option_group_wc_city', __( 'City', 'dhuett' ), array( $this, 'wc_city_field' ), 'mame_ws_options_group', 'mame_ws_option_group_woocommerce_section' );
                add_settings_field( 'mame_ws_option_group_wc_country', __( 'Country', 'dhuett' ), array( $this, 'wc_country_field' ), 'mame_ws_options_group', 'mame_ws_option_group_woocommerce_section' );

            }

            add_settings_section( 'mame_ws_option_group_bln_section', __( 'Cash on delivery account details', 'dhuett' ), array( $this, 'bln_section_text' ), 'mame_ws_options_group' );
            add_settings_field( 'mame_ws_option_group_bln_organization', __( 'Company', 'dhuett' ), array( $this, 'bln_organization_field' ), 'mame_ws_options_group', 'mame_ws_option_group_bln_section' );
            add_settings_field( 'mame_ws_option_group_bln_company_addition', __( 'Company addition', 'dhuett' ), array( $this, 'bln_company_addition_field' ), 'mame_ws_options_group', 'mame_ws_option_group_bln_section' );
            add_settings_field( 'mame_ws_option_group_bln_title', __( 'Title', 'dhuett' ), array( $this, 'bln_title_field' ), 'mame_ws_options_group', 'mame_ws_option_group_bln_section' );
            add_settings_field( 'mame_ws_option_group_bln_firstname', __( 'First name', 'dhuett' ), array( $this, 'bln_firstname_field' ), 'mame_ws_options_group', 'mame_ws_option_group_bln_section' );
            add_settings_field( 'mame_ws_option_group_bln_lastname', __( 'Last name', 'dhuett' ), array( $this, 'bln_lastname_field' ), 'mame_ws_options_group', 'mame_ws_option_group_bln_section' );
            add_settings_field( 'mame_ws_option_group_bln_addition', __( 'Additional details', 'dhuett' ), array( $this, 'bln_addition_field' ), 'mame_ws_options_group', 'mame_ws_option_group_bln_section' );
            add_settings_field( 'mame_ws_option_group_bln_street', __( 'Street', 'dhuett' ), array( $this, 'bln_street_field' ), 'mame_ws_options_group', 'mame_ws_option_group_bln_section' );
            add_settings_field( 'mame_ws_option_group_bln_pobox', __( 'Postbox', 'dhuett' ), array( $this, 'bln_pobox_field' ), 'mame_ws_options_group', 'mame_ws_option_group_bln_section' );
            add_settings_field( 'mame_ws_option_group_bln_zip', __( 'ZIP', 'dhuett' ), array( $this, 'bln_zip_field' ), 'mame_ws_options_group', 'mame_ws_option_group_bln_section' );
            add_settings_field( 'mame_ws_option_group_bln_city', __( 'City', 'dhuett' ), array( $this, 'bln_city_field' ), 'mame_ws_options_group', 'mame_ws_option_group_bln_section' );
            add_settings_field( 'mame_ws_option_group_bln_country', __( 'Country', 'dhuett' ), array( $this, 'bln_country_field' ), 'mame_ws_options_group', 'mame_ws_option_group_bln_section' );

            add_settings_field( 'mame_ws_option_group_bln_transaction_type', __( 'Transaction type', 'dhuett' ), array( $this, 'bln_transaction_type_field' ), 'mame_ws_options_group', 'mame_ws_option_group_bln_section' );
            add_settings_field( 'mame_ws_option_group_bln_iban', __( 'IBAN number', 'dhuett' ), array( $this, 'bln_iban_field' ), 'mame_ws_options_group', 'mame_ws_option_group_bln_section' );
            add_settings_field( 'mame_ws_option_group_bln_isr', __( 'ISR participant number', 'dhuett' ), array( $this, 'bln_isr_field' ), 'mame_ws_options_group', 'mame_ws_option_group_bln_section' );

        }

        public function load_data_field_field()
        {
            ?>
            <p><?= __( 'Click the button to reload the data from the WebStamp server.', 'dhuett' ) ?></p>
            <div>
                <button id="mame_ws-soap-request" class="button"><?= __( 'Load', 'dhuett' ) ?></button>
                <img id="<?= MAME_WS_PREFIX ?>-load-webstamp-data" class="<?= MAME_WS_PREFIX ?>-admin-loader"
                     src="<?= plugins_url( '../../assets/images/loader-5.gif', __FILE__ ) ?>">
            </div>
            <?php
        }

        public function license_key_field()
        {
            $licensing = Mame_Licensing_Handler::get_instance( MAME_WS_PLUGIN_NAME, MAME_WS_DISPLAY_NAME, MAME_WS_UPDATE_URL, MAME_WS_PREFIX, 'webstamp', MAME_WS_PLUGIN_PATH, false );
            $licensing->license_activation_field();
        }

        public function image_field()
        {
            echo Mame_WP_Helper::image_upload_field( 'mame_ws_options_group', 'image', null, 200, plugins_url( '../../assets/images/stamp_white.png', __FILE__ ) );
        }

        public function stamp_file_type()
        {
            $type = ( isset( $this->options[ 'stamp_file_type' ] ) ? $this->options[ 'stamp_file_type' ] : 'pdf' );
            echo '<select name="mame_ws_options_group[stamp_file_type]"><option value="pdf" ' . ( $type == 'pdf' ? 'selected' : '' ) . '>' . __( 'PDF', 'dhuett' ) . '</option><option value="png"' . ( $type == 'png' ? 'selected' : '' ) . '>' . __( 'PNG', 'dhuett' ) . '</option></select><br><label for="mame_ws_options_group[stamp_file_type]">' . __( 'Choose PDF to enable media selection in the editor. If you choose PNG you can integrate the stamp into packing slips and invoices.', 'dhuett' ) . '</label>';
        }

        public function open_print_dialog_field()
        {
            $checked = isset( $this->options[ 'open_print_dialog' ] ) ? $this->options[ 'open_print_dialog' ] : false;
            ?>
            <input type="checkbox" name="mame_ws_options_group[open_print_dialog]"
                   value="1" <?= $checked ? 'checked="checked"' : '' ?>> <?= __( 'Check if the stamp print dialog should be opened automatically after an order has been placed.', 'dhuett' ) ?>
            <?php
        }

        public function environment_field()
        {
            $env = ( isset( $this->options[ 'environment' ] ) ? $this->options[ 'environment' ] : 'test' );
            echo '<select name="mame_ws_options_group[environment]"><option value="test" ' . ( $env == 'test' ? 'selected' : '' ) . '>' . __( 'Test', 'dhuett' ) . '</option><option value="production"' . ( $env == 'production' ? 'selected' : '' ) . '>' . __( 'Production', 'dhuett' ) . '</option></select>';
        }

        public function customer_type_field()
        {
            $type = ( isset( $this->options[ 'customer_type' ] ) ? $this->options[ 'customer_type' ] : 'gk' );
            echo '<select name="mame_ws_options_group[customer_type]"><option value="gk" ' . ( $type == 'gk' ? 'selected' : '' ) . '>' . __( 'Business customer', 'dhuett' ) . '</option><option value="pk"' . ( $type == 'pk' ? 'selected' : '' ) . '>' . __( 'Private customer', 'dhuett' ) . '</option></select>';
        }

        public function template_order_overview_field()
        {
            $checked = isset( $this->options[ 'template_order_overview' ] ) ? $this->options[ 'template_order_overview' ] : false;
            ?>
            <input type="checkbox" name="mame_ws_options_group[template_order_overview]"
                   value="1" <?= $checked ? 'checked="checked"' : '' ?>> <?= __( 'Check if the order confirmation screen should be shown when a template is loaded to speed up the process of purchasing stamps.', 'dhuett' ) ?>
            <?php
        }

        public function bln_transaction_type_field()
        {
            $tt = ( isset( $this->options[ 'bln_transaction_type' ] ) ? $this->options[ 'bln_transaction_type' ] : 'post_bank' );
            echo '<select name="mame_ws_options_group[bln_transaction_type]"><option value="post_bank" ' . ( $tt == 'post_bank' ? 'selected' : '' ) . '>' . __( 'IBAN (postal account / bank)', 'dhuett' ) . '</option><option value="esr"' . ( $tt == 'esr' ? 'selected' : '' ) . '>' . __( 'ISR Account', 'dhuett' ) . '</option></select>';
        }

        public function bln_iban_field()
        {
            echo '<input id="plugin_text_string" name="mame_ws_options_group[bln_iban]" size="40" type="text" value="' . ( isset( $this->options[ 'bln_iban' ] ) ? esc_attr( $this->options[ 'bln_iban' ] ) : '' ) . '" />';
        }

        public function bln_isr_field()
        {
            echo '<input id="plugin_text_string" name="mame_ws_options_group[bln_isr]" size="40" type="text" value="' . ( isset( $this->options[ 'bln_isr' ] ) ? esc_attr( $this->options[ 'bln_isr' ] ) : '' ) . '" />';
        }

        public function bln_organization_field()
        {
            echo '<input id="plugin_text_string" name="mame_ws_options_group[bln_organization]" size="40" type="text" value="' . ( isset( $this->options[ 'bln_organization' ] ) ? esc_attr( $this->options[ 'bln_organization' ] ) : '' ) . '" />';
        }

        public function bln_company_addition_field()
        {
            echo '<input id="plugin_text_string" name="mame_ws_options_group[bln_company_addition]" size="40" type="text" value="' . ( isset( $this->options[ 'bln_company_addition' ] ) ? esc_attr( $this->options[ 'bln_company_addition' ] ) : '' ) . '" />';
        }

        public function bln_title_field()
        {
            echo '<input id="plugin_text_string" name="mame_ws_options_group[bln_title]" size="40" type="text" value="' . ( isset( $this->options[ 'bln_title' ] ) ? esc_attr( $this->options[ 'bln_title' ] ) : '' ) . '" />';
        }

        public function bln_firstname_field()
        {
            echo '<input id="plugin_text_string" name="mame_ws_options_group[bln_firstname]" size="40" type="text" value="' . ( isset( $this->options[ 'bln_firstname' ] ) ? esc_attr( $this->options[ 'bln_firstname' ] ) : '' ) . '" />';
        }

        public function bln_lastname_field()
        {
            echo '<input id="plugin_text_string" name="mame_ws_options_group[bln_lastname]" size="40" type="text" value="' . ( isset( $this->options[ 'bln_lastname' ] ) ? esc_attr( $this->options[ 'bln_lastname' ] ) : '' ) . '" />';
        }

        public function bln_addition_field()
        {
            echo '<input id="plugin_text_string" name="mame_ws_options_group[bln_addition]" size="40" type="text" value="' . ( isset( $this->options[ 'bln_addition' ] ) ? esc_attr( $this->options[ 'bln_addition' ] ) : '' ) . '" />';
        }

        public function bln_street_field()
        {
            echo '<input id="plugin_text_string" name="mame_ws_options_group[bln_street]" size="40" type="text" value="' . ( isset( $this->options[ 'bln_street' ] ) ? esc_attr( $this->options[ 'bln_street' ] ) : '' ) . '" />';
        }

        public function bln_pobox_field()
        {
            echo '<input id="plugin_text_string" name="mame_ws_options_group[bln_pobox]" size="40" type="text" value="' . ( isset( $this->options[ 'bln_pobox' ] ) ? esc_attr( $this->options[ 'bln_pobox' ] ) : '' ) . '" />';
        }

        public function bln_zip_field()
        {
            echo '<input id="plugin_text_string" name="mame_ws_options_group[bln_zip]" size="40" type="text" value="' . ( isset( $this->options[ 'bln_zip' ] ) ? esc_attr( $this->options[ 'bln_zip' ] ) : '' ) . '" />';
        }

        public function bln_city_field()
        {
            echo '<input id="plugin_text_string" name="mame_ws_options_group[bln_city]" size="40" type="text" value="' . ( isset( $this->options[ 'bln_city' ] ) ? esc_attr( $this->options[ 'bln_city' ] ) : '' ) . '" />';
        }

        public function bln_country_field()
        {
            echo '<input id="plugin_text_string" name="mame_ws_options_group[bln_country]" size="40" type="text" value="' . ( isset( $this->options[ 'bln_country' ] ) ? esc_attr( $this->options[ 'bln_country' ] ) : '' ) . '" />';
        }

        private function print_wc_field_options( $selected = null )
        {
            foreach ( $this->field_options as $k => $v ) {
                echo '<option value="' . $k . '" ' . ( $selected == $k ? 'selected' : '' ) . '>' . $v . '</option>';
            }
        }

        public function wc_field_description_field()
        {
            echo __( 'These are the custom field assignments for loading WooCommerce addresses into WebStamp orders.', 'dhuett' );
        }

        public function wc_organization_field()
        {

            $selected = ( isset( $this->options[ 'wc_organization' ] ) ? $this->options[ 'wc_organization' ] : 'none' );

            echo '<select name="mame_ws_options_group[wc_organization]">';

            $this->print_wc_field_options( $selected );

            echo '</select>';
        }

        public function wc_company_addition_field()
        {
            $selected = ( isset( $this->options[ 'wc_company_addition' ] ) ? $this->options[ 'wc_company_addition' ] : 'none' );

            echo '<select name="mame_ws_options_group[wc_company_addition]">';

            $this->print_wc_field_options( $selected );

            echo '</select>';
        }

        public function wc_title_field()
        {
            $selected = ( isset( $this->options[ 'wc_title' ] ) ? $this->options[ 'wc_title' ] : 'none' );

            echo '<select name="mame_ws_options_group[wc_title]">';

            $this->print_wc_field_options( $selected );

            echo '</select>';
        }

        public function wc_firstname_field()
        {
            $selected = ( isset( $this->options[ 'wc_firstname' ] ) ? $this->options[ 'wc_firstname' ] : 'none' );

            echo '<select name="mame_ws_options_group[wc_firstname]">';

            $this->print_wc_field_options( $selected );

            echo '</select>';
        }

        public function wc_lastname_field()
        {
            $selected = ( isset( $this->options[ 'wc_lastname' ] ) ? $this->options[ 'wc_lastname' ] : 'none' );

            echo '<select name="mame_ws_options_group[wc_lastname]">';

            $this->print_wc_field_options( $selected );

            echo '</select>';
        }

        public function wc_addition_field()
        {
            $selected = ( isset( $this->options[ 'wc_addition' ] ) ? $this->options[ 'wc_addition' ] : 'none' );

            echo '<select name="mame_ws_options_group[wc_addition]">';

            $this->print_wc_field_options( $selected );

            echo '</select>';
        }

        public function wc_street_field()
        {
            $selected = ( isset( $this->options[ 'wc_street' ] ) ? $this->options[ 'wc_street' ] : 'none' );

            echo '<select name="mame_ws_options_group[wc_street]">';

            $this->print_wc_field_options( $selected );

            echo '</select>';
        }

        public function wc_pobox_field()
        {
            $selected = ( isset( $this->options[ 'wc_pobox' ] ) ? $this->options[ 'wc_pobox' ] : 'none' );

            echo '<select name="mame_ws_options_group[wc_pobox]">';

            $this->print_wc_field_options( $selected );

            echo '</select>';
        }

        public function wc_zip_field()
        {
            $selected = ( isset( $this->options[ 'wc_zip' ] ) ? $this->options[ 'wc_zip' ] : 'none' );

            echo '<select name="mame_ws_options_group[wc_zip]">';

            $this->print_wc_field_options( $selected );

            echo '</select>';
        }

        public function wc_city_field()
        {
            $selected = ( isset( $this->options[ 'wc_city' ] ) ? $this->options[ 'wc_city' ] : 'none' );

            echo '<select name="mame_ws_options_group[wc_city]">';

            $this->print_wc_field_options( $selected );

            echo '</select>';
        }

        public function wc_country_field()
        {
            $selected = ( isset( $this->options[ 'wc_country' ] ) ? $this->options[ 'wc_country' ] : 'none' );

            echo '<select name="mame_ws_options_group[wc_country]">';

            $this->print_wc_field_options( $selected );

            echo '</select>';
        }

        public function organization_field()
        {
            echo '<input id="plugin_text_string" name="mame_ws_options_group[organization]" size="40" type="text" value="' . ( isset( $this->options[ 'organization' ] ) ? esc_attr( $this->options[ 'organization' ] ) : '' ) . '" />';
        }

        public function company_addition_field()
        {
            echo '<input id="plugin_text_string" name="mame_ws_options_group[company_addition]" size="40" type="text" value="' . ( isset( $this->options[ 'company_addition' ] ) ? esc_attr( $this->options[ 'company_addition' ] ) : '' ) . '" />';
        }

        public function title_field()
        {
            echo '<input id="plugin_text_string" name="mame_ws_options_group[title]" size="40" type="text" value="' . ( isset( $this->options[ 'title' ] ) ? esc_attr( $this->options[ 'title' ] ) : '' ) . '" />';
        }

        public function firstname_field()
        {
            echo '<input id="plugin_text_string" name="mame_ws_options_group[firstname]" size="40" type="text" value="' . ( isset( $this->options[ 'firstname' ] ) ? esc_attr( $this->options[ 'firstname' ] ) : '' ) . '" />';
        }

        public function lastname_field()
        {
            echo '<input id="plugin_text_string" name="mame_ws_options_group[lastname]" size="40" type="text" value="' . ( isset( $this->options[ 'lastname' ] ) ? esc_attr( $this->options[ 'lastname' ] ) : '' ) . '" />';
        }

        public function addition_field()
        {
            echo '<input id="plugin_text_string" name="mame_ws_options_group[addition]" size="40" type="text" value="' . ( isset( $this->options[ 'addition' ] ) ? esc_attr( $this->options[ 'addition' ] ) : '' ) . '" />';
        }

        public function street_field()
        {
            echo '<input id="plugin_text_string" name="mame_ws_options_group[street]" size="40" type="text" value="' . ( isset( $this->options[ 'street' ] ) ? esc_attr( $this->options[ 'street' ] ) : '' ) . '" />';
        }

        public function pobox_field()
        {
            echo '<input id="plugin_text_string" name="mame_ws_options_group[pobox]" size="40" type="text" value="' . ( isset( $this->options[ 'pobox' ] ) ? esc_attr( $this->options[ 'pobox' ] ) : '' ) . '" />';
        }

        public function zip_field()
        {
            echo '<input id="plugin_text_string" name="mame_ws_options_group[zip]" size="40" type="text" value="' . ( isset( $this->options[ 'zip' ] ) ? esc_attr( $this->options[ 'zip' ] ) : '' ) . '" />';
        }

        public function city_field()
        {
            echo '<input id="plugin_text_string" name="mame_ws_options_group[city]" size="40" type="text" value="' . ( isset( $this->options[ 'city' ] ) ? esc_attr( $this->options[ 'city' ] ) : '' ) . '" />';
        }

        public function country_field()
        {
            echo '<input id="plugin_text_string" name="mame_ws_options_group[country]" size="40" type="text" value="' . ( isset( $this->options[ 'country' ] ) ? esc_attr( $this->options[ 'country' ] ) : '' ) . '" />';
        }

        public function wc_templates_email_field()
        {
            $wc_templates_email = [];
            if ( !empty( $this->options[ 'wc_templates_email' ] ) ) {
                $wc_templates_email = json_decode( $this->options[ 'wc_templates_email' ] );
            }

            // Get templates
            $templates = Webstamp_Database_Manager::get_templates();
            if ( !empty( $templates ) ) {

                ?>
                <div>
                    <?php foreach ( $templates as $t ) { ?>
                        <input type="checkbox" name="mame_ws_options_group[wc_templates_email][]"
                               value="<?= esc_attr( $t->id ) ?>" <?= ( in_array( $t->id, $wc_templates_email ) ? 'checked="checked"' : '' ) ?>> <?= esc_html( $t->name ) ?>
                        <br>
                    <?php } ?>
                </div>
                <?php

            } else {
                echo '<p>' . __( 'No templates found', 'dhuett' ) . '</p>';
            }
        }

        public function wc_email_tracking_code_field()
        {
            $pos = ( isset( $this->options[ 'wc_email_tracking_code' ] ) ? $this->options[ 'wc_email_tracking_code' ] : 'none' );
            echo '<select name="mame_ws_options_group[wc_email_tracking_code]"><option value="none" ' . ( $pos == 'none' ? 'selected' : '' ) . '>' . __( 'None', 'dhuett' ) . '</option><option value="after_table" ' . ( $pos == 'after_table' ? 'selected' : '' ) . '>' . __( 'After order table', 'dhuett' ) . '</option><option value="before_table"' . ( $pos == 'before_table' ? 'selected' : '' ) . '>' . __( 'Before order table', 'dhuett' ) . '</option></select><br><label for="mame_ws_options_group[wc_email_tracking_code]">' . __( 'Choose the position of the tracking code in the confirmation email.', 'dhuett' ) . '</label>';
        }

        public function options_validate( $input )
        {
            $newinput              = get_option( 'mame_ws_option_group' );

            $newinput[ 'environment' ]   = sanitize_text_field( $input[ 'environment' ] );
            $newinput[ 'customer_type' ] = sanitize_text_field( $input[ 'customer_type' ] );
            $newinput[ 'user_id' ]       = sanitize_text_field( $input[ 'user_id' ] );
            $newinput[ 'password' ]      = sanitize_text_field( $input[ 'password' ] );

            $newinput[ 'organization' ]     = sanitize_text_field( $input[ 'organization' ] );
            $newinput[ 'company_addition' ] = sanitize_text_field( $input[ 'company_addition' ] );
            $newinput[ 'title' ]            = sanitize_text_field( $input[ 'title' ] );
            $newinput[ 'firstname' ]        = sanitize_text_field( $input[ 'firstname' ] );
            $newinput[ 'lastname' ]         = sanitize_text_field( $input[ 'lastname' ] );
            $newinput[ 'addition' ]         = sanitize_text_field( $input[ 'addition' ] );
            $newinput[ 'street' ]           = sanitize_text_field( $input[ 'street' ] );
            $newinput[ 'pobox' ]            = sanitize_text_field( $input[ 'pobox' ] );
            $newinput[ 'zip' ]              = sanitize_text_field( $input[ 'zip' ] );
            $newinput[ 'city' ]             = sanitize_text_field( $input[ 'city' ] );
            $newinput[ 'country' ]          = sanitize_text_field( $input[ 'country' ] );

            $newinput[ 'wc_organization' ]     = sanitize_text_field( $input[ 'wc_organization' ] );
            $newinput[ 'wc_company_addition' ] = sanitize_text_field( $input[ 'wc_company_addition' ] );
            $newinput[ 'wc_title' ]            = sanitize_text_field( $input[ 'wc_title' ] );
            $newinput[ 'wc_firstname' ]        = sanitize_text_field( $input[ 'wc_firstname' ] );
            $newinput[ 'wc_lastname' ]         = sanitize_text_field( $input[ 'wc_lastname' ] );
            $newinput[ 'wc_addition' ]         = sanitize_text_field( $input[ 'wc_addition' ] );
            $newinput[ 'wc_street' ]           = sanitize_text_field( $input[ 'wc_street' ] );
            $newinput[ 'wc_pobox' ]            = sanitize_text_field( $input[ 'wc_pobox' ] );
            $newinput[ 'wc_zip' ]              = sanitize_text_field( $input[ 'wc_zip' ] );
            $newinput[ 'wc_city' ]             = sanitize_text_field( $input[ 'wc_city' ] );
            $newinput[ 'wc_country' ]          = sanitize_text_field( $input[ 'wc_country' ] );

            $newinput[ 'template_order_overview' ] = sanitize_text_field( $input[ 'template_order_overview' ] );

            $newinput[ 'bln_organization' ]     = sanitize_text_field( $input[ 'bln_organization' ] );
            $newinput[ 'bln_company_addition' ] = sanitize_text_field( $input[ 'bln_company_addition' ] );
            $newinput[ 'bln_title' ]            = sanitize_text_field( $input[ 'bln_title' ] );
            $newinput[ 'bln_firstname' ]        = sanitize_text_field( $input[ 'bln_firstname' ] );
            $newinput[ 'bln_lastname' ]         = sanitize_text_field( $input[ 'bln_lastname' ] );
            $newinput[ 'bln_addition' ]         = sanitize_text_field( $input[ 'bln_addition' ] );
            $newinput[ 'bln_street' ]           = sanitize_text_field( $input[ 'bln_street' ] );
            $newinput[ 'bln_pobox' ]            = sanitize_text_field( $input[ 'bln_pobox' ] );
            $newinput[ 'bln_zip' ]              = sanitize_text_field( $input[ 'bln_zip' ] );
            $newinput[ 'bln_city' ]             = sanitize_text_field( $input[ 'bln_city' ] );
            $newinput[ 'bln_country' ]          = sanitize_text_field( $input[ 'bln_country' ] );
            $newinput[ 'bln_transaction_type' ] = sanitize_text_field( $input[ 'bln_transaction_type' ] );
            $newinput[ 'bln_iban' ]             = sanitize_text_field( $input[ 'bln_iban' ] );
            $newinput[ 'bln_isr' ]              = sanitize_text_field( $input[ 'bln_isr' ] );

            $newinput[ 'image' ]             = sanitize_text_field( $input[ 'image' ] );
            $newinput[ 'stamp_file_type' ]   = sanitize_text_field( $input[ 'stamp_file_type' ] );
            $newinput[ 'open_print_dialog' ] = sanitize_text_field( $input[ 'open_print_dialog' ] );

            if ( isset( $input[ 'wc_email_tracking_code' ] ) )
                $newinput[ 'wc_email_tracking_code' ] = sanitize_text_field( $input[ 'wc_email_tracking_code' ] );

            if ( !empty ( $input[ 'wc_templates_email' ] ) ) {
                $templates = [];
                foreach ( $input[ 'wc_templates_email' ] as $t ) {
                    $templates[] = sanitize_text_field( $t );
                }
                $newinput[ 'wc_templates_email' ] = json_encode( $templates );
            }
            return $newinput;
        }

        public function user_id_field()
        {
            echo '<input id="plugin_text_string" name="mame_ws_options_group[user_id]" size="40" type="text" value="' . esc_attr( $this->options[ 'user_id' ] ) . '" />';
        }

        public function password_field()
        {
            echo '<input id="plugin_text_string" name="mame_ws_options_group[password]" size="40" type="text" value="' . esc_attr( $this->options[ 'password' ] ) . '" />';
        }

        public function main_section_text()
        {
            echo '<p>' . __( 'Please fill in the fields below to connect the website to your WebStamp account', 'dhuett' ) . '</p>';
        }

        public function address_section_text()
        {
            echo '<p>' . __( 'This is the default sender address.', 'dhuett' ) . '</p>';
        }

        public function stamp_section_text()
        {
            echo '<p>' . __( 'Default settings for WebStamp stamps.', 'dhuett' ) . '</p>';
        }

        public function template_section_text()
        {

        }

        public function bln_section_text()
        {
            echo '<p>' . __( 'Fill in the beneficiary\'s address and account details to use cash on delivery.', 'dhuett' ) . '</p>';
        }

        public function woocommerce_section_text()
        {
            echo '<p>' . __( 'Settings for the WooCommerce integration.', 'dhuett' ) . '</p>';
        }

        public function display_webstamp_editor_page()
        {
            $editor_handler = new Webstamp_Editor_Handler();
            // $editor_handler->load_editor();
            $editor_handler->load_editor();
        }

        public function display_webstamp_orders_page()
        {
            ?>
            <div class="wrap">
                <h2><?= __( 'Orders', 'dhuett' ) ?></h2>

                <div id="poststuff">
                    <div id="post-body" class="metabox-holder columns-1">
                        <div id="post-body-content">
                            <div class="meta-box-sortables ui-sortable">
                                <form method="post">
                                    <?php
                                    $this->order_list->prepare_items();
                                    $this->order_list->display(); ?>
                                </form>
                            </div>
                        </div>
                    </div>
                    <br class="clear">
                </div>
            </div>
            <?php
        }

        public function display_webstamp_templates_page()
        {
            ?>
            <div class="wrap">
                <h2><?= __( 'Templates', 'dhuett' ) ?></h2>
                <a href="<?= admin_url( 'admin.php?page=mame_ws_menu_add_template' ) ?>"
                   class="page-title-action"><?= __( 'Add new', 'dhuett' ) ?></a>

                <div id="poststuff">
                    <div id="post-body" class="metabox-holder columns-1">
                        <div id="post-body-content">
                            <div class="meta-box-sortables ui-sortable">
                                <form method="post">
                                    <?php
                                    $this->template_list->prepare_items();
                                    $this->template_list->display(); ?>
                                </form>
                            </div>
                        </div>
                    </div>
                    <br class="clear">
                </div>
            </div>
            <?php
        }

        public function display_webstamp_add_template_page()
        {
            $id = null;
            if ( !empty( $_POST ) ) {
                $errors = Webstamp_Template::save_template_data( $_POST );

            } elseif ( isset( $_GET[ 'id' ] ) ) {
                $id = $_GET[ 'id' ];
            }

            $template_editor = new Webstamp_Template_Editor_Handler( $id );
            ?>
            <div class="wrap">
                <h1><?php $id ? _e( 'Edit template', 'dhuett' ) : _e( 'Add template', 'dhuett' ); ?></h1>
                <form id="<?= MAME_WS_PREFIX ?>-template-form" method="post" action="#">
                    <?php
                    if ( isset( $errors ) && !empty( $errors ) )
                        Webstamp_Template::template_errors_html( $errors );
                    $template_editor->load_editor();
                    ?>
                    <?php submit_button(); ?>
                </form>
            </div>
            <?php
        }

        public function display_webstamp_products_page()
        {
        }

        public function display_webstamp_categories_page()
        {
        }

        public function display_webstamp_settings_page()
        {
            ?>
            <div class="wrap">
                <h1><?php _e( 'WebStamp Settings', 'dhuett' ); ?></h1>
                <form method="post" action="options.php">
                    <?php
                    settings_fields( 'mame_ws_options_group' );

                    do_settings_sections( 'mame_ws_options_group' );
                    ?>
                    <?php submit_button(); ?>
                </form>
            </div>
            <?php
        }

    }

endif;