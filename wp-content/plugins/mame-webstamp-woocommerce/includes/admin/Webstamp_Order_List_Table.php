<?php

if ( !defined( 'ABSPATH' ) ) {
    exit; // Exit if accessed directly
}

if ( !class_exists( 'Webstamp_Order_List_Table' ) ) :

    /**
     * Displays a WP_List_Table with WebStamp orders.
     */
    class Webstamp_Order_List_Table extends WP_List_Table
    {
        public $table;
        public $wc_order_id;

        /**
         * Webstamp_Order_List_Table constructor.
         * @param null $wc_order_id
         */
        public function __construct( $wc_order_id = null )
        {
            parent::__construct( [
                'singular' => __( 'Order', 'dhuett' ), //singular name of the listed records
                'plural'   => __( 'Orders', 'dhuett' ), //plural name of the listed records
                'ajax'     => false //should this table support ajax?

            ] );

            $this->table       = Webstamp_Database_Manager::table( Webstamp_Database_Manager::ORDER );
            $this->wc_order_id = $wc_order_id;
        }

        /**
         * Retrieves all WebStamp orders.
         *
         * @param int $per_page
         * @param int $page_number
         * @param null $wc_order_id
         * @return mixed
         */
        public static function get_orders( $per_page = 10, $page_number = 1, $wc_order_id = null )
        {
            global $wpdb;

            $table = Webstamp_Database_Manager::table( Webstamp_Database_Manager::ORDER );
            $sql   = "SELECT * FROM $table WHERE status <> 'draft'";

            if ( MAME_WC_ACTIVE && $wc_order_id )
                $sql .= " AND wc_order_id = '$wc_order_id'";

            if ( !empty( $_REQUEST[ 'orderby' ] ) ) {
                $sql .= ' ORDER BY ' . esc_sql( $_REQUEST[ 'orderby' ] );
                $sql .= !empty( $_REQUEST[ 'order' ] ) ? ' ' . esc_sql( $_REQUEST[ 'order' ] ) : ' ASC';
            }

            $sql .= " LIMIT $per_page";

            $sql .= ' OFFSET ' . ( $page_number - 1 ) * $per_page;

            $result = $wpdb->get_results( $sql, 'ARRAY_A' );
            return $result;
        }

        /**
         * Deletes a WebStamp order (only locally, not in WebStamp itself).
         *
         * @param $id
         */
        public static function delete_order( $id )
        {
            global $wpdb;

            $table = Webstamp_Database_Manager::table( Webstamp_Database_Manager::ORDER );
            $wpdb->delete(
                "$table",
                [ 'order_id' => $id ],
                [ '%d' ]
            );
        }

        /**
         * Returns the count of all records from the database.
         *
         * @param null $wc_order_id
         * @return mixed
         */
        public static function record_count( $wc_order_id = null )
        {
            global $wpdb;
            $table = Webstamp_Database_Manager::table( Webstamp_Database_Manager::ORDER );


            $sql = "SELECT COUNT(*) FROM $table WHERE status <> 'draft'";
            if ( MAME_WC_ACTIVE && $wc_order_id )
                $sql .= " AND wc_order_id = '$wc_order_id'";

            return $wpdb->get_var( $sql );
        }

        /**
         * Text when no items are found.
         */
        public function no_items()
        {
            _e( 'No orders found.', 'dhuett' );
        }

        /**
         * Returns an array of available columns.
         *
         * @return array
         */
        function get_columns()
        {
            $columns = [
                'cb'          => '<input type="checkbox" />',
                'order_id'    => __( 'Order ID', 'dhuett' ),
                'price'       => __( 'Price', 'dhuett' ),
                'product'     => __( 'Product', 'dhuett' ),
                'time'        => __( 'Date', 'dhuett' ),
                'valid_until' => __( 'Valid until', 'dhuett' ),
                'receiver'    => __( 'Receiver', 'dhuett' ),
                'files'       => __( 'Files', 'dhuett' ),
                'tracking'    => __( 'Tracking', 'dhuett' ),
            ];

            if ( MAME_WC_ACTIVE && !$this->wc_order_id ) {
                $columns[ 'wc_order_id' ] = __( 'WooCommerce order', 'dhuett' );
            }

            return $columns;
        }

        /**
         * Defines which columns are sortable.
         *
         * @return array
         */
        public function get_sortable_columns()
        {
            $sortable_columns = array(
                'order_id'    => array( 'order_id', true ),
                'price'       => array( 'price', false ),
                'time'        => array( 'time', false ),
                'valid_until' => array( 'valid_until', false ),
            );

            if ( MAME_WC_ACTIVE && !$this->wc_order_id )
                $sortable_columns[ 'wc_order_id' ] = [ 'wc_order_id', false ];

            return $sortable_columns;
        }

        public function get_bulk_actions()
        {
            $actions = [
                'bulk-delete' => __( 'Delete', 'dhuett' ),
            ];

            return $actions;
        }

        /**
         * Prepares the data to be displayed.
         *
         * @param null $wc_order_id
         */
        public function prepare_items( $wc_order_id = null )
        {

            $this->_column_headers = $this->get_column_info();

            /** Process bulk action */
            $this->process_bulk_action();

            $per_page     = $this->get_items_per_page( 'orders_per_page', 10 );
            $current_page = $this->get_pagenum();
            $total_items  = self::record_count( $wc_order_id );

            $this->set_pagination_args( [
                'total_items' => $total_items, //WE have to calculate the total number of items
                'per_page'    => $per_page //WE have to determine how many items to show on a page
            ] );


            $this->items = self::get_orders( $per_page, $current_page, $wc_order_id );
        }

        public function process_bulk_action()
        {

            //Detect when a bulk action is being triggered...
            if ( 'delete' === $this->current_action() ) {

                // In our file that handles the request, verify the nonce.
                $nonce = esc_attr( $_REQUEST[ '_wpnonce' ] );

                if ( !wp_verify_nonce( $nonce, MAME_WS_PREFIX . '_delete_order' ) ) {
                    die( 'Permission denied' );
                } else {
                    self::delete_order( absint( $_GET[ 'order_id' ] ) );

                    wp_redirect( esc_url( add_query_arg() ) );
                    exit;
                }

            }

            // If the delete bulk action is triggered
            if ( ( isset( $_POST[ 'action' ] ) && $_POST[ 'action' ] == 'bulk-delete' )
                || ( isset( $_POST[ 'action2' ] ) && $_POST[ 'action2' ] == 'bulk-delete' )
            ) {

                $delete_ids = esc_sql( $_POST[ 'bulk-delete' ] );

                // loop over the array of record IDs and delete them
                foreach ( $delete_ids as $id ) {
                    self::delete_order( $id );

                }

                wp_redirect( esc_url( add_query_arg() ) );
                exit;
            }
        }

        function column_order_id( $item )
        {
            // create a nonce
            $delete_nonce = wp_create_nonce( MAME_WS_PREFIX . '_delete_order' );

            $title = '<strong>' . $item[ 'order_id' ] . '</strong>';

            $actions = [
                'delete' => sprintf( '<a href="?page=%s&action=%s&order_id=%s&_wpnonce=%s">Delete</a>', esc_attr( $_REQUEST[ 'page' ] ), 'delete', absint( $item[ 'order_id' ] ), $delete_nonce )
            ];

            return $title . $this->row_actions( $actions );
        }

        /**
         * Displays the column with the orderd products.
         *
         * @param $item
         * @return string
         */
        function column_product( $item )
        {
            $order    = new Webstamp_Order( $item );
            $category = $order->get_category();
            $zone     = $order->get_zone();

            return $category->name . ', ' . $zone->name . ', ' . $order->delivery_method . ', ' . $order->format;
        }

        /**
         * Displays the column with the receiver addresses.
         *
         * @param $item
         * @return string
         */
        function column_receiver( $item )
        {
            $order   = new Webstamp_Order( $item );
            $address = $order->get_address();
            if ( $address ) {
                $address->id                      = null;
                $address->cash_on_delivery_esr    = null;
                $address->cash_on_delivery_amount = null;
                $address_array                    = array_filter( get_object_vars( $address ), function ( $v ) {
                    return !empty( $v ) && $v;
                } );

                return implode( ', ', $address_array );
            }
        }

        /**
         * Displays the column for Webstamp files such as stamps and delivery receipts.
         *
         * @param $item
         * @return string
         */
        function column_files( $item )
        {
            $order  = new Webstamp_Order( $item );
            $stamps = $order->get_stamps();

            $html = '';
            if ( !empty( $stamps ) ) {
                foreach ( $stamps as $stamp ) {
                    $html .= $stamp->get_file_button( __( 'Stamp', 'dhuett' ) );
                }
            }

            if ( !empty( $order->delivery_receipt ) ) {
                $html .= '<a id="' . MAME_WS_PREFIX . '-download-receipt-btn" class="' . MAME_WS_PREFIX . '-btn" href="' . add_query_arg( [ MAME_WS_PREFIX . '-type' => 'receipts', MAME_WS_PREFIX . '-file' => $order->delivery_receipt, MAME_WS_PREFIX . '-download' => '1' ], get_admin_url() ) . '" target="_blank">' . __( 'Receipt', 'dhuett' ) . '</a>';
            }

            return $html;
        }

        /**
         * Lists all the tracking numbers and with a link to the tracking status.
         *
         * @param $item
         * @return string
         */
        function column_tracking( $item )
        {
            $order  = new Webstamp_Order( $item );
            $stamps = $order->get_stamps();

            $html = '';
            if ( !empty( $stamps ) ) {
                foreach ( $stamps as $stamp ) {
                    $html .= Mame_Html::a( $stamp->tracking_number, $stamp->get_tracking_url() ) . '<br>';
                }
            }

            return $html;
        }

        /**
         * Displays the WooCommerce order id column.
         *
         * @param $item
         * @return null|string
         */
        function column_wc_order_id( $item )
        {
            //
            if ( !MAME_WC_ACTIVE || empty( $item[ 'wc_order_id' ] ) )
                return null;

            $order = new WC_Order( $item[ 'wc_order_id' ] );

            return '<a href="' . $order->get_edit_order_url() . '">' . $item[ 'wc_order_id' ] . '</a>';
        }

        /**
         * Displays default column.
         *
         * @param $item
         * @param $column_name
         * @return mixed
         */
        public function column_default( $item, $column_name )
        {
            switch ( $column_name ) {
                case 'price':
                    return $item[ $column_name ];
                case 'time':
                case 'valid_until':
                    return date_i18n( 'j. F Y - H:i', $item[ $column_name ] );
                default:
                    return print_r( $item, true );
            }
        }

        function column_cb( $item )
        {
            return sprintf(
                '<input type="checkbox" name="bulk-delete[]" value="%s" />', $item[ 'order_id' ]
            );
        }

    }

endif;