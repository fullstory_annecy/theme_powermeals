<?php

if ( !defined( 'ABSPATH' ) ) {
    exit; // Exit if accessed directly
}

if ( !class_exists( 'Webstamp_Order_Admin' ) ) :

    class Webstamp_Order_Admin
    {
        static $instance;

        public function __construct()
        {
            // add_filter( 'woocommerce_admin_order_actions', array( $this, 'add_overview_action_button' ), 100, 2 );
            // add_action( 'admin_head', array( $this, 'add_overview_action_button_css' ) );

            if ( !is_plugin_active( 'mame-barcode-woocommerce/mame-barcode-woocommerce.php' ) )
                add_action( 'add_meta_boxes_shop_order', array( $this, 'add_meta_box' ) );
        }

        public static function get_instance()
        {
            if ( !isset( self::$instance ) ) {
                self::$instance = new self();
            }

            return self::$instance;
        }

        /**
         * Adds a meta box containing the editor.
         */
        public function add_meta_box()
        {
            add_meta_box(
                'mame_ws_editor_metabox', __( 'WebStamp', 'dhuett' ), array( $this, 'display_metabox' ), 'shop_order', 'normal', 'high'
            );
        }

        /**
         * Displays the meta box for the editor.
         *
         * @param $post
         */
        public function display_metabox( $post )
        {
            global $post;
            $wc_order = new WC_Order( $post->ID );

            $address = static::get_address( $wc_order );

            $editor = new Webstamp_Editor_Handler( Mame_WC_Helper::get ('id', $wc_order), $address );

            if ( isset( $_GET[ MAME_WS_PREFIX . '_template' ] ) ) {
                $editor->load_template_into_draft_order( $_GET[ MAME_WS_PREFIX . '_template' ] );
                $editor->autoload_draft = true;
                $editor->load_editor( 4 );
            } else {
                $editor->load_editor();
            }
            $this->display_orders( $wc_order );
        }

        public static function get_address( $wc_order )
        {
            $street = Mame_WC_Helper::get( 'shipping_address_1', $wc_order );
            if ( $street ) {
                $street .= ' ' . Mame_WC_Helper::get( 'shipping_address_2', $wc_order );
            } else {
                $street = Mame_WC_Helper::get( 'shipping_address_2', $wc_order );
            }
            if ( !$street ) {
                $street = Mame_WC_Helper::get( 'billing_address_1', $wc_order );
                if ( $street ) {
                    $street .= ' ' . Mame_WC_Helper::get( 'billing_address_2', $wc_order );
                } else {
                    $street = Mame_WC_Helper::get( 'billing_address_2', $wc_order );
                }
            }

            $address               = new Webstamp_Address();
            $address->organization = Mame_WC_Helper::get( 'shipping_company', $wc_order ) ?: Mame_WC_Helper::get( 'billing_company', $wc_order );
            $address->firstname    = Mame_WC_Helper::get( 'shipping_first_name', $wc_order ) ?: Mame_WC_Helper::get( 'billing_first_name', $wc_order );
            $address->lastname     = Mame_WC_Helper::get( 'shipping_last_name', $wc_order ) ?: Mame_WC_Helper::get( 'billing_last_name', $wc_order );
            $address->street       = $street;
            $address->zip          = Mame_WC_Helper::get( 'shipping_postcode', $wc_order ) ?: Mame_WC_Helper::get( 'billing_postcode', $wc_order );
            $address->city         = Mame_WC_Helper::get( 'shipping_city', $wc_order ) ?: Mame_WC_Helper::get( 'billing_city', $wc_order );
            $address->country      = Mame_WC_Helper::get( 'shipping_country', $wc_order ) ?: Mame_WC_Helper::get( 'billing_country', $wc_order );

            return $address;
        }

        function display_orders( $wc_order )
        {
            $orders = Webstamp_Database_Manager::get_orders_by_wc_order( Mame_WC_Helper::get ('id', $wc_order) );
            if ( !empty( $orders ) ) {
                echo Mame_Html::h3( __( 'WebStamp orders', 'dhuett' ) );
                ?>
                <div id="<?= MAME_WS_PREFIX ?>-orders-table">
                    <table>
                        <tr>
                            <th><?= __( 'Order ID', 'dhuett' ) ?></th>
                            <th><?= __( 'Price', 'dhuett' ) ?></th>
                            <th><?= __( 'Product', 'dhuett' ) ?></th>
                            <th><?= __( 'Date', 'dhuett' ) ?></th>
                            <th><?= __( 'Files', 'dhuett' ) ?></th>
                        </tr>

                        <?php
                        foreach ( $orders as $order ) {
                            if ( $order->status !== 'draft' ) {
                                $stamps   = $order->get_stamps();
                                $category = $order->get_category();
                                $zone     = $order->get_zone();
                                $html     = '';
                                if ( !empty( $stamps ) ) {
                                    foreach ( $stamps as $stamp ) {
                                        $html .= $stamp->get_file_button( __( 'Stamp', 'dhuett' ) );
                                    }
                                }

                                if ( !empty( $order->delivery_receipt ) )
                                    $html .= '<a id="' . MAME_WS_PREFIX . '-download-receipt-btn" class="' . MAME_WS_PREFIX . '-btn" href="' . add_query_arg( [ MAME_WS_PREFIX . '-type' => 'receipts', MAME_WS_PREFIX . '-file' => $order->delivery_receipt, MAME_WS_PREFIX . '-download' => '1' ], get_admin_url() ) . '" target="_blank">' . __( 'Receipt', 'dhuett' ) . '</a>';

                                echo '<tr><td>' . $order->order_id . '</td><td>' . $order->price . '</td><td>' . $category->name . ', ' . $zone->name . ', ' . $order->delivery_method . ', ' . $order->format . '</td><td>' . date_i18n( 'j. F Y - H:i', $order->time ) . '</td><td>' . $html . '</td></tr>';
                            }
                        }
                        ?>
                    </table>
                </div>
                <?php
            }
        }

        /**
         * Adds an action button to the order overview screen to settle a payment with status authorized.
         *
         * @param $actions
         * @param $order
         * @return mixed
         */
        function add_overview_action_button( $actions, $order )
        {
            if ( $order->has_status( array( 'processing', 'on-hold' ) ) ) {
                $action_slug = 'mame_dt_action_settle_payment';

                $actions[ $action_slug ] = array(
                    'url'    => wp_nonce_url( admin_url( 'admin-ajax.php?action=woocommerce_mark_order_status&status=parcial&order_id=' . Mame_WC_Helper::get ('id', $order) ), 'woocommerce-mark-order-status' ),
                    'name'   => __( 'Settle datatrans authorization', 'dhuett' ),
                    'action' => $action_slug,
                );

            }
            return $actions;
        }

        /**
         * Icon for the action button to settle an authorized payment.
         */

        function add_overview_action_button_css()
        {
            $action_slug = "mame_dt_action_settle_payment";

            echo '<style>.wc-action-button-' . $action_slug . '::after { font-family: woocommerce !important; content: "\e604" !important; }</style>';
        }

    }

endif;