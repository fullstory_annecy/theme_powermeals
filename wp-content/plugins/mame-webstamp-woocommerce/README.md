# README

## Changelog

### Version 1.5.3 - 25.03.2020

* Tweak - Hardcoded API key (previously in settings).
* Fix - Made plugin compatible to older WooCommerce versions.

### Version 1.5.2 - 13.12.2019

* Fix - Check if current page is mame_ws_menu_settings before creating new Session and Customer.

### Version 1.5.1 - 11.11.2019

* Fix - include wc-cart-functions.php before creating new WC_Customer and calling get_checkout_fields().
* Fix - check if current user has permission 'manage_woocommerce' instead of checking for 'administrator' when printing or downloading files.

### Version 1.5.0 - 06.09.2019

* Feature - Custom assignment of WooCommerce fields to WebStamp address fields.

### Version 1.4.3 - 06.05.2019

* Fix - Use property instead of constant for display name.

### Version 1.4.2 - 19.04.2019
    
* Fix - Save uploaded image in editor.
* Fix - Show preview button after save.

### Version 1.4.1 - 18.04.2019

* Fix - Bind click event on document for image upload.

### Version 1.4.0 - 12.04.2019

* Feature - WooCommerce Shipment Tracking integration.
* Fix - Create multiple stamps for orders containing more than one stamp.
* Fix - Open print dialog after order.
* Fix - Load overview on template load.

### Version 1.3.6 - 11.04.2019

* Fix - Correctly check, activate and deactivate licenses of the current blog.

### Version 1.3.5 - 09.04.2019

* Fix - Correctly check activated licenses in multisite installations.

### Version 1.3.4 - 08.04.2019

* Fix - Check if GET index is defined for settings page.

### Version 1.3.3 - 08.04.2019

* Fix - Renamed variable for jQuery noConflict mode.

### Version 1.3.2 - 15.03.2019

* Fix - Escape attributes on settings page.

### Version 1.3.1 - 28.02.2019

* Fix - Use basedir instead of baseurl in get_file() function.

### Version 1.3.0 - 27.01.2019

* Feature - Added option to include tracking numbers in confirmation emails.
* Fix - load WooCommerce buttons on admin screen when loading draft or starting new order.

### Version 1.2.1 - 13.01.2019

* Fix - show editor fields when no draft or template available.

### Version 1.2.0 - 13.01.2019
* Feature - Added order templates (editor, template list view).
* Feature - Added setting to directly jump to order overview screen on template load.
* Feature - Added setting to directly jump to print screen after an order has been placed.
* Feature - Added setting to add links to order confirmation email to load templates directly.
* Tweak - Fill in fields from draft order directly in PHP.
* Fix - Get correct file type when downloading files.

### Version 1.1.2 - 15.11.2018
* Fix - Added media type selection to settings for integration of the image file into other documents.

### Version 1.1.1 - 14.11.2018
* Fix - Stamp shortcode: echo stamps.

### Version 1.1.0 - 13.11.2018

* Feature - Added function and shortcode to get stamp image files from WC order.
* Fix - Licensing option name.

### Version 1.0.3 - 04.11.2018

* Tweak - Compatibility with Barcode for WooCommerce.
* Fix - WC receiver address was loaded into sender address fields.

### Version 1.0.2 - 13.08.2018

* Feature - Added tracking number to order screen.
* Tweak - Order helper functions.
* Fix - Only pass numeric values for width and height in wp_get_attachment_image_src.

### Version 1.0.1 - 08.08.2018

* Fix - Check if dependencies undefined.

### Version 1.0.0 - 06.08.2018

* Initial release.
