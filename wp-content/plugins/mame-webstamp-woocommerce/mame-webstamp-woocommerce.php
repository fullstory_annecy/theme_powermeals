<?php
/**
 * Plugin Name: mame WebStamp for WooCommerce
 * Plugin URI: https://www.mamedev.ch
 * Description: Swiss Post WebStamp plugin for WooCommerce
 * Version: 1.5.3
 * Author: mame webdesign hüttig
 * Author URL: https://www.mamedev.ch
 * License: Purchase a license key at mamedev.ch.
 *
 * Requires at least: 4.0.1
 * Tested up to: 5.1
 * Requires PHP: 5.3
 *
 * WC requires at least: 3.0
 * WC tested up to: 3.6
 */

include_once( ABSPATH . 'wp-admin/includes/plugin.php' );

// Define global constants.
define( 'MAME_WS_PLUGIN_VERSION', '1.5.3' );
define( 'MAME_WS_UPDATE_URL', 'http://www.mamedev.ch' );
define( 'MAME_WS_PLUGIN_NAME', 'WebStamp for WooCommerce' );
define( 'MAME_WS_DISPLAY_NAME', 'mame WebStamp for WooCommerce' );

define( 'MAME_WS_PRODUCTION_URL', 'https://webstamp.post.ch' );
define( 'MAME_WS_TEST_URL', 'https://wsredesignint2.post.ch' );

//define( 'MAME_WS_SPEC_VERSION', '6' );

define( 'MAME_WS_PREFIX', 'mame_ws' );
define( 'MAME_WS_PLUGIN_PATH', 'mame-webstamp-woocommerce' );

define( 'MAME_WS_DB_VERSION', '1.2' );

define( 'MAME_WS_API_KEY', 'c597a9c46d3e775bf68d5f55d9687380');

if ( !defined( 'MAME_SUBSCRIPTIONS_ACTIVE' ) )
    define( 'MAME_SUBSCRIPTIONS_ACTIVE', is_plugin_active( 'woocommerce-subscriptions/woocommerce-subscriptions.php' ) );

if ( !defined( 'MAME_WOO_TRACKING_ACTIVE' ) )
    define( 'MAME_WOO_TRACKING_ACTIVE', is_plugin_active( 'woocommerce-shipment-tracking/woocommerce-shipment-tracking.php' ) );

if ( !defined( 'MAME_WC_ACTIVE' ) )
    define( 'MAME_WC_ACTIVE', is_plugin_active( 'woocommerce/woocommerce.php' ) );

// Debug
if ( !defined( 'MAME_WS_DEBUG' ) )
    define( 'MAME_WS_DEBUG', false );
if ( !defined( 'MAME_DEBUG' ) )
    define( 'MAME_DEBUG', false );

register_activation_hook( __FILE__, MAME_WS_PREFIX . '_on_activation' );
register_deactivation_hook( __FILE__, MAME_WS_PREFIX . '_on_deactivation' );

// Include WP_List_Table
if ( !class_exists( 'WP_List_Table' ) ) {
    require_once( ABSPATH . 'wp-admin/includes/class-wp-list-table.php' );
}

// Helper functions
include_once( plugin_dir_path( __FILE__ ) . 'lib/Mame_Helper_Functions.php' );
include_once( plugin_dir_path( __FILE__ ) . 'lib/Mame_WP_Helper.php' );
if ( MAME_WC_ACTIVE ) {
    include_once( plugin_dir_path( __FILE__ ) . 'lib/Mame_WC_Helper.php' );

}
include_once( plugin_dir_path( __FILE__ ) . 'lib/Mame_Html.php' );
include_once( plugin_dir_path( __FILE__ ) . 'lib/Mame_Json_Response.php' );


// Initialize plugin.
include_once( plugin_dir_path( __FILE__ ) . 'includes/webstamp/Webstamp_Database_Manager.php' );
include_once( plugin_dir_path( __FILE__ ) . 'includes/Webstamp_Plugin_Init.php' );
$webstamp_init = new Webstamp_Plugin_Init();

/*
 *  Include licensing/updates files.
 */
include_once( plugin_dir_path( __FILE__ ) . 'lib/updates/Mame_Licensing_Handler.php' );
$licensing = new Mame_Licensing_Handler( MAME_WS_PLUGIN_NAME, MAME_WS_DISPLAY_NAME, MAME_WS_UPDATE_URL, MAME_WS_PREFIX, 'webstamp', MAME_WS_PLUGIN_PATH, false );

/*
 * Initialize updater.
 */
include( dirname( __FILE__ ) . '/lib/updates/Mame_Plugin_Updater.php' );

$license_options = $licensing->network_valid_license() ? Mame_Licensing_Handler::get_option( MAME_WS_PREFIX . '_license_options' ) : null;
if ( $license_options && isset( $license_options[ 'license_key' ] ) ) {
    $edd_updater = new Mame_Plugin_Updater( MAME_WS_UPDATE_URL, __FILE__, array(
        'version'   => MAME_WS_PLUGIN_VERSION,
        'license'   => $license_options[ 'license_key' ],
        'item_name' => MAME_WS_PLUGIN_NAME,
        'author'    => 'mame webdesign hüttig',
        'url'       => get_home_url( 1 )
    ) );
}

include_once( plugin_dir_path( __FILE__ ) . 'includes/webstamp/Webstamp_File_Manager.php' );

// Objects
include_once( plugin_dir_path( __FILE__ ) . 'includes/objects/Webstamp_Object.php' );
include_once( plugin_dir_path( __FILE__ ) . 'includes/objects/Webstamp_Order.php' );
include_once( plugin_dir_path( __FILE__ ) . 'includes/objects/Webstamp_Template.php' );
include_once( plugin_dir_path( __FILE__ ) . 'includes/objects/Webstamp_Address.php' );
include_once( plugin_dir_path( __FILE__ ) . 'includes/objects/Webstamp_Product.php' );
include_once( plugin_dir_path( __FILE__ ) . 'includes/objects/Webstamp_Category.php' );
include_once( plugin_dir_path( __FILE__ ) . 'includes/objects/Webstamp_Zone.php' );
include_once( plugin_dir_path( __FILE__ ) . 'includes/objects/Webstamp_Stamp.php' );
include_once( plugin_dir_path( __FILE__ ) . 'includes/objects/Webstamp_Cash_On_Delivery.php' );
include_once( plugin_dir_path( __FILE__ ) . 'includes/objects/Webstamp_Media.php' );
include_once( plugin_dir_path( __FILE__ ) . 'includes/objects/Webstamp_Media_Type.php' );

include_once( plugin_dir_path( __FILE__ ) . 'includes/admin/Webstamp_Order_List_Table.php' );
include_once( plugin_dir_path( __FILE__ ) . 'includes/admin/Webstamp_Template_List_Table.php' );

// SOAP
include_once( plugin_dir_path( __FILE__ ) . 'includes/soap/Webstamp_SoapClient.php' );
include_once( plugin_dir_path( __FILE__ ) . 'includes/soap/Webstamp_Options_SoapClient.php' );
include_once( plugin_dir_path( __FILE__ ) . 'includes/soap/Webstamp_Order_SoapClient.php' );
include_once( plugin_dir_path( __FILE__ ) . 'includes/soap/Webstamp_Info_SoapClient.php' );

include_once( plugin_dir_path( __FILE__ ) . 'includes/webstamp/Webstamp_Options_Manager.php' );
include_once( plugin_dir_path( __FILE__ ) . 'includes/webstamp/Webstamp_Order_Manager.php' );

include_once( plugin_dir_path( __FILE__ ) . 'includes/soap/Webstamp_SoapTransactionManager.php' );
$soap_transaction_manager = new Webstamp_SoapTransactionManager();

// Pages
include_once( plugin_dir_path( __FILE__ ) . 'includes/admin/Webstamp_Settings_Page.php' );
//new Webstamp_Settings_Page();

add_action( 'plugins_loaded', function () {
    Webstamp_Settings_Page::get_instance();

    load_plugin_textdomain( 'dhuett', false, plugin_basename( dirname( __FILE__ ) . '/localization/' ) );

} );

include_once( plugin_dir_path( __FILE__ ) . 'includes/webstamp/Webstamp_Editor_Json_Response.php' );
include_once( plugin_dir_path( __FILE__ ) . 'includes/webstamp/Webstamp_Editor_Handler.php' );
include_once( plugin_dir_path( __FILE__ ) . 'includes/webstamp/Webstamp_Template_Editor_Handler.php' );
new Webstamp_Editor_Handler();
new Webstamp_Template_Editor_Handler();

include_once( plugin_dir_path( __FILE__ ) . 'includes/admin/Webstamp_Order_Admin.php' );
Webstamp_Order_Admin::get_instance();

// WooCommerce
include_once( plugin_dir_path( __FILE__ ) . 'includes/woocommerce/functions.php' );
if ( MAME_WOO_TRACKING_ACTIVE )
    include_once( plugin_dir_path( __FILE__ ) . 'includes/woocommerce/shipment-tracking.php' );


/**
 * Initialize license keys and setup db on install.
 *
 * @global type $wp_rewrite
 */
function mame_ws_on_activation()
{
    // Initialize license key and status.
    $license_options = get_option( MAME_WS_PREFIX . '_license_options' );
    if ( !$license_options )
        update_option( MAME_WS_PREFIX . '_license_options', [ 'license_key' => '', 'license_status' => false ] );

    // Setup database.
    Webstamp_Database_Manager::setup();

    // Cronjob
    if ( !wp_next_scheduled( MAME_WS_PREFIX . '_cron' ) ) {
        wp_schedule_event( time(), MAME_WS_PREFIX . '-weekly', MAME_WS_PREFIX . '_cron' );
    }

    // Create upload dir.
    $upload_dir    = wp_upload_dir();
    $ws_upload_dir = $upload_dir[ 'basedir' ] . '/webstamp/stamps';
    wp_mkdir_p( $ws_upload_dir );
    wp_mkdir_p( $upload_dir[ 'basedir' ] . '/webstamp/receipts' );
    wp_mkdir_p( $upload_dir[ 'basedir' ] . '/webstamp/temp' );

    // Protect files.
    file_put_contents( $upload_dir[ 'basedir' ] . '/webstamp/.htaccess', 'deny from all' );
}

function mame_ws_cron()
{
    Webstamp_Database_Manager::setup();
}

function mame_ws_on_deactivation()
{
    $timestamp = wp_next_scheduled( MAME_WS_PREFIX . '_cron' );
    wp_unschedule_event( $timestamp, MAME_WS_PREFIX . '_cron' );
}

function mame_ws_check_plugin_update()
{
    $db_version = get_option( MAME_WS_PREFIX . '_db_version' );
    if ( MAME_WS_DB_VERSION != $db_version ) {
    }
}

add_action( 'plugins_loaded', 'mame_ws_check_plugin_update' );