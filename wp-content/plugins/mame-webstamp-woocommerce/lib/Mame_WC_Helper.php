<?php

if ( !defined( 'ABSPATH' ) ) {
    exit; // Exit if accessed directly
}

if ( !class_exists( 'Mame_WC_Helper' ) ) :

    class Mame_WC_Helper
    {

        /**
         * Checks if WooCommerce higher or equal $version is installed.
         *
         * @param $version
         * @return mixed
         */
        public static function is_woocommerce_version_up( $version )
        {
            return version_compare( WOOCOMMERCE_VERSION, $version, '>=' );
        }

        public static function get( $property, $order )
        {
            if ( is_numeric( $order ) ) {
                $order = new WC_Order( $order );
            }

            if ( static::is_woocommerce_version_up( '2.7.0' ) ) {

                if ( 'id' == strtolower( $property ) ) {
                    $func = 'get_id';
                } else {
                    $func = 'get_' . $property;
                }
                return $order->$func();
            }

            if ( 'id' == strtolower( $property ) ) {
                $property = 'ID';
            }

            return $order->$property;
        }

    }

endif;