<?php
if ( !defined( 'ABSPATH' ) ) {
    exit; // Exit if accessed directly
}

if ( !class_exists( 'Mame_WP_Helper' ) ) :

    class Mame_WP_Helper extends Mame_Helper_Functions
    {

        public static function image_upload_field( $options_name, $name, $width, $height, $default_image )
        {
            $options = get_option( $options_name );

            if ( !empty( $options[ $name ] ) ) {
                $sizes = [];
                if ( !empty( $width ) )
                    $sizes[] = $width;
                if ( !empty( $height ) )
                    $sizes[] = $height;
                $image_attributes = wp_get_attachment_image_src( $options[ $name ], array( $width, $height ) );
                $src              = $image_attributes[ 0 ];
                $value            = $options[ $name ];
            } else {
                $src   = $default_image;
                $value = '';
            }

            $text = __( 'Upload', 'dhuett' );

            $width_str  = empty( $width ) ? 'auto' : $width . ' px';
            $height_str = empty( $height ) ? 'auto' : $height . ' px';

            return '
        <div class="upload">
            <img data-src="' . $default_image . '" src="' . $src . '" width="' . $width_str . '" height="' . $height_str . '" />
            <div>
                <input type="hidden" name="' . $options_name . '[' . $name . ']" id="' . $options_name . '[' . $name . ']" value="' . $value . '" />
                <button type="submit" class="upload_image_button button">' . $text . '</button>
                <button type="submit" class="remove_image_button button">&times;</button>
            </div>
        </div>
    ';
        }


        /*
        public static function image_upload_field( $options_name, $name, $value, $width, $height, $default_image )
        {

            if ( !empty( $options[ $name ] ) ) {
                $image_attributes = wp_get_attachment_image_src( $value, array( $width, $height ) );
                $src              = $image_attributes[ 0 ];
                $val              = $value;
            } else {
                $src = $default_image;
                $val = '';
            }

            $text            = __( 'Upload', 'dhuett' );
            $option_name_str = $name ? $options_name . '[' . $name . ']' : $options_name;

            return '
            <div class="upload">
                <img data-src="' . $default_image . '" src="' . $src . '" width="' . $width . '" height="' . $height . '" />
                <div>
                    <input type="hidden" name="' . $option_name_str . '" id="' . $option_name_str . '" value="' . $val . '" />
                    <button type="submit" class="upload_image_button button">' . $text . '</button>
                    <button type="submit" class="remove_image_button button">&times;</button>
                </div>
            </div>
        ';
        }
        */
    }

endif;