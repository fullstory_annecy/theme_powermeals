<?php

if ( !defined( 'ABSPATH' ) ) {
    exit; // Exit if accessed directly
}

if ( !class_exists( 'Mame_Json_Response' ) ) :

    class Mame_Json_Response
    {
        protected $response;

        const STATUS_SUCCESS = 'success';
        const STATUS_FAILURE = 'failure';
        const STATUS_EMPTY   = 'empty';

        public function __construct()
        {
            $this->response = [];
        }

        public function status( $status )
        {
            $this->response[ 'status' ] = $status;
            return $this;
        }

        public function add_attribute( $key, $value )
        {
            $this->response[ $key ] = $value;
            return $this;
        }

        public function replace( $element, $replacement )
        {
            $this->response [ 'replace' ] = [ 'element' => $element, 'with' => $replacement ];
            return $this;
        }

        public function html( $element, $content )
        {
            $this->response [ 'html' ][] = [ 'element' => $element, 'content' => $content ];
            return $this;
        }

        public function append( $to, $element )
        {
            $this->response [ 'append' ] = [ 'to' => $to, 'element' => $element ];
            return $this;
        }

        public function attr( $elem, $attr, $val )
        {
            $this->response[ 'attr' ][] = [ 'elem' => $elem, 'attr' => $attr, 'val' => $val ];
            return $this;
        }

        public function data( $elem, $attr, $val )
        {
            $this->response[ 'data' ][] = [ 'elem' => $elem, 'attr' => $attr, 'val' => $val ];
            return $this;
        }

        public function hide( $elem )
        {
            $this->response[ 'hide' ][] = $elem;
            return $this;
        }

        public function show( $elem )
        {
            $this->response[ 'show' ][] = $elem;
            return $this;
        }

        public function redirect( $location )
        {
            $this->response[ 'redirect' ][] = $location;
            return $this;
        }

        public function errors( $container, $errors )
        {
            $this->response[ 'errors' ][] = [ 'container' => $container, 'errors' => $errors ];
            return $this;
        }

        public function after( $element, $insert )
        {
            $this->response[ 'after' ][] = [ 'element' => $element, 'insert' => $insert ];
            return $this;
        }

        public function respond()
        {
            ob_clean();
            echo json_encode( $this->response );
            die();
        }

    }

endif;