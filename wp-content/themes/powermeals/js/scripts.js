(function ($, root, undefined) {
	
	$(function () {
		
		'use strict';
		
		// DOM ready, take it away
		
	});
	
})(jQuery, this);



var theheader = document.getElementById("theheader");

var menuMovil = document.getElementById('menu-movil')
var menu = document.getElementById('menu')
var menuActive = false
var icon = menuMovil.children[0]
menuMovil.onclick = function(){
	console.log('i')
	if(menuActive){
		menu.classList.remove('menu-active')
		menuActive = false
		icon.classList.add("fa-bars")
		icon.classList.remove("fa-times")
	}
	else{
		menu.classList.add('menu-active')
		menuActive = true
		icon.classList.remove("fa-bars")
		icon.classList.add("fa-times")
	}
}

window.onscroll = function(){ 
    var scrollY = window.scrollY;
    if(scrollY > 0){
    	theheader.classList.add('ontop');
    }
    else{
    	theheader.classList.remove('ontop');               
    }
}

