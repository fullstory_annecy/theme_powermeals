		</div>
		<!-- /wrapper -->
			<!-- footer -->
			<footer class="footer" role="contentinfo">
				<div class="container">
					<div class="contact-footer">
						<?php if (ICL_LANGUAGE_CODE == "en") : ?>
 
						CONTACT US:  
						<?php endif; ?>

						<?php if (ICL_LANGUAGE_CODE == "fr") : ?>

						CONTACTEZ-NOUS 
						<?php endif; ?>


						<?php if (ICL_LANGUAGE_CODE == "de") : ?>

						KONTAKT 
						<?php endif; ?>

						<ul>
							<li><a href="mailto:info@powermeals.ch">info@powermeals.ch</a></li>
							<li><a href="tel:+41227896060">+41.22.789.60.60</a></li>
							<li><a href="https://www.facebook.com/eatpowermeals" target="_blank"><i class="fa fa-facebook" aria-hidden="true"></i></a></li>
							<li><a href="https://www.instagram.com/eatpowermeals/" target="_blank"><i class="fa fa-instagram" aria-hidden="true"></i></a></li>
						</ul>
					</div>
					<div class="footer-bottom">
						<div class="copyright">
							<?php if (ICL_LANGUAGE_CODE == "en") : ?>
 
							 Copyright &copy;  <?php echo date('Y'); ?>  <?php bloginfo('name');  ?> All rights reserved.

							<?php endif; ?>

							<?php if (ICL_LANGUAGE_CODE == "fr") : ?>

							 Copyright &copy;  <?php echo date('Y'); ?>  <?php bloginfo('name');  ?> Tous droits réservés.

							<?php endif; ?>


							<?php if (ICL_LANGUAGE_CODE == "de") : ?>

							 Copyright &copy;  <?php echo date('Y'); ?>  <?php bloginfo('name');  ?> Alle Rechte vorbehalten.

							<?php endif; ?>
                      
													
						</div>

						<div class="footer-menu">
							<ul>
								<li><a href="#">Impressum</a></li>
								<li><a href="#">Privacy policy</a></li>
								<li><a href="#">Terms of Use</a></li>
							</ul>
						</div>						
					</div>					
				</div>


			</footer>
			<!-- /footer -->


		<?php wp_footer(); ?>

		<script src="<?php echo get_template_directory_uri(); ?>/js/scripts.js"></script>

		<!-- analytics 
		<script>
		(function(f,i,r,e,s,h,l){i['GoogleAnalyticsObject']=s;f[s]=f[s]||function(){
		(f[s].q=f[s].q||[]).push(arguments)},f[s].l=1*new Date();h=i.createElement(r),
		l=i.getElementsByTagName(r)[0];h.async=1;h.src=e;l.parentNode.insertBefore(h,l)
		})(window,document,'script','//www.google-analytics.com/analytics.js','ga');
		ga('create', 'UA-XXXXXXXX-XX', 'yourdomain.com');
		ga('send', 'pageview');
		</script>
		-->

	</body>
</html>
