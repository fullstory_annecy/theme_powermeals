<!doctype html>
<html <?php language_attributes(); ?> class="no-js">
	<head>
		<meta charset="<?php bloginfo('charset'); ?>">
		<title><?php wp_title(''); ?><?php if(wp_title('', false)) { echo ' :'; } ?> <?php bloginfo('name'); ?></title>

		<link href="//www.google-analytics.com" rel="dns-prefetch">
		 <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
        <link href="<?php echo get_template_directory_uri(); ?>/img/icons/favicon.ico" rel="shortcut icon">
        <link href="<?php echo get_template_directory_uri(); ?>/img/icons/touch.png" rel="apple-touch-icon-precomposed">

		<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
		<meta name="viewport" content="width=device-width, initial-scale=1.0">
		<meta name="description" content="<?php bloginfo('description'); ?>">

		<?php wp_head(); ?>
		<script>
        // conditionizr.com
        // configure environment tests
        conditionizr.config({
            assets: '<?php echo get_template_directory_uri(); ?>',
            tests: {}
        });
        </script>

		<link href="https://fonts.googleapis.com/css?family=Lato:300,400,700&display=swap" rel="stylesheet">

        <link rel="stylesheet" href="<?php echo get_template_directory_uri(); ?>/css/style.css">
	
    
    <!-- Google Tag Manager --> <script>(function(w,d,s,l,i){w[l]=w[l]||[];w[l].push({'gtm.start': new Date().getTime(),event:'gtm.js'});var f=d.getElementsByTagName(s)[0], j=d.createElement(s),dl=l!='dataLayer'?'&l='+l:'';j.async=true;j.src= 'https://www.googletagmanager.com/gtm.js?id='+i+dl;f.parentNode.insertBefore(j,f); })(window,document,'script','dataLayer','GTM-5F3K5R3');</script> <!-- End Google Tag Manager -->
	</head>

	<body <?php body_class(); ?>>
    
    <!-- Google Tag Manager (noscript) --> <noscript><iframe src="https://www.googletagmanager.com/ns.html?id=GTM-5F3K5R3" height="0" width="0" style="display:none;visibility:hidden"></iframe></noscript> <!-- End Google Tag Manager (noscript) -->
<?php 

global $woocommerce;

    //foreach ( WC()->cart->get_cart() as $cart_item_key => $cart_item ) {

    	//var_dump($cart_item['wccpf_subscription_subscribe_and_save_10_on_your_total']['user_val'][0]);

            //if($cart_item['wccpf_subscription_subscribe_and_save_10_on_your_total']['user_val'][0]=='None') {
          /* $cart = WC()->cart->cart_contents;
			 foreach( $cart as $cart_item_id=>$cart_item ) {
			 	$_product = wc_get_product( 1287);
			 	$cart_item['data']=$_product;
			 	$cart_item['data']->set_price(88); 
			 	//var_dump($cart_item['data']);
			 //$cart_item['new_meta_data'] = 'Your stuff goes here';
			 WC()->cart->cart_contents[$cart_item_id] = $cart_item;
			 }
			 WC()->cart->set_session();*/
           // }


    //}

  //var_dump($woocommerce->cart->get_cart());

//$thedata->set_price(100);
//var_dump($thedata);




//var_dump(WC()->cart->get_cart());


        
?>

			<!-- header -->
			<header class="header clear" role="banner" id="theheader">
				<div class="container">
					<!-- logo -->
					<div class="logo">
						<a href="<?php echo home_url(); ?>">
							<!-- svg logo - toddmotto.com/mastering-svg-use-for-a-retina-web-fallbacks-with-png-script -->
							<img src="<?php echo get_template_directory_uri(); ?>/img/logo.png" alt="Logo" class="logo-img">
						</a>
					</div>
					<!-- /logo -->
					<div class="phone-header">
						<a href="tel:+41227896060">+41.22.789.60.60</a>
					</div>

					<!-- nav -->
					<nav class="nav fff" role="navigation" id="menu">
						<?php html5blank_nav(); ?>
						<?php do_action('wpml_add_language_selector'); ?>
                        
                        <div class="icons">
                        	<a href="<?php echo home_url(); ?>/cart-2">
                            	<svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" width="42" height="37" viewBox="0 0 42 37">
  <image id="Objeto_inteligente_vectorial" data-name="Objeto inteligente vectorial" width="42" height="37" xlink:href="data:img/png;base64,iVBORw0KGgoAAAANSUhEUgAAACoAAAAlCAYAAAA0lXuOAAADNElEQVRYhcVYW0sVURT+VnftgllhNyooKEIiikyooHooIwjfevClh6gH/4SkD/UDQrpA9VRIEF2pNzELO10wgkKMLpwiBa1QzJT0i53rHBdzZubM2JlzPhjO2mvWmv2dtdesvfaA5A2Go4PkHJQaJMfzEHXYW2qeLlKnATSobFEFYIuOawF0lo5mCEjWmoi2lZqPBN0gOR/AMIC5AAYAnC8utSwmAdwJJIopsikAu4rLyxeDs/IYpEpEzAvJV3a6ADSq7PK0NcTWVYZmla8BuOpjswnAJZUfATgb8rytJt2e5CP6zMiVItIeZEiywgw/+dmS/GmGfXmeV22GnaFLLyK9AL7rsIZkvlQpJGzt7ogycSZPlwDYXESi+/R3FMCrKFujW/46ldtIDgbYLTfyCZL7fWwWGbmOZNDSzwawWuWUiIxHJZpBdbhpFuv1CkOVXvnwb0eMs/SlwuPI85Ls1a30D8nyAJt6s+U2BdhsNzZ+5Stj905tJkguRsSIwkTV5c6OiD4zAskVphl6LSLDcYh2Gbk2SaIA9hg527HFjahDTeE4+cLWz2x+hjYlGWgnNQRgXsIkvVglIn2IGlERGQPQXUyGAN5nSMKnqw9Dyix72mytSeAXgDMzei7JBlNaWhIk6Ys4TUYxX6gcRHqZMtB9vhLACIDLCfCZAHBLRHIOknGJPgBwpKDUcuEKfIWITNo7cfvLVj1sJYm3XpKIG1FMRdV1RUsTIuoI9mg5LDzchkAybhqVJfNfcycSko0ke7RkuU9DD0kGNi0kF5I8R7JffYZdF0VyTZJErwR8pxojecjHvtx9Jwjw+UZyQxIkj3kmekEybcZftT+wPi3m/m+ST0n+MLr7SRC9biY4hek8bTf6wx6fj6p3zfdO1VVpNB0mSS4LmnOmx9+1Rr6J6cblntF78y7j80FEXqpPvzsKq17Mga5gRL8Y+TimorMAQL3Rpz0+mfFGkrvVxxE7oHp6nvv/IHnUk6PdJPvMOO2To03mvqsQz0kOGd3dgpI0E18IeINHSR70sS/TF8gP7o+tS4qoq6MnSb7R0+IIydskt4X4OLLNJD8r2wGSF0muDJ0MwF9H7+6Vz1y+zAAAAABJRU5ErkJggg=="/>
</svg>
                            </a>
                            <a href="<?php echo home_url(); ?>/my-account/orders">
                            	<svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" width="35" height="35" viewBox="0 0 35 35">
  <image id="Objeto_inteligente_vectorial" data-name="Objeto inteligente vectorial" width="35" height="35" xlink:href="data:img/png;base64,iVBORw0KGgoAAAANSUhEUgAAACMAAAAjCAYAAAAe2bNZAAAC4ElEQVRYhbVYz0uUURQ936emTllBRpQhFJQrDW3TMkhDwXbRKqFWLRIqiDZBmzbhIkqINm38CyIKKquV0CaxHxAMg7nJhWJgSFM4WSdu3IHX9PW9e4fpLGYx37nnnu++O/PefQkcINkKYAjAIIABAAcBdAJoAvADwCcAJQBzAJ4DeJYkybonRxQku0lOklylD6sa190IEwWSN0hWnCZqUVGdQr1GekkWDYk+knxDctHAFb1er5Fhkl9zRBdIniO5oyauU79fyIkV3WGPkbxluU9yc0RjC8mHORqVqCFdmryKlPQXZXmpdpIfIhXKXjJt1liPXLEYCTSvRvSKYVOnQew1AD0R/fceMwDeRp73aN4/3qDb+PM96nEifINmpfo/VK3MZQAtBv1tHjMAtho4LZofqTbkmFF8v9PMXiNvTHykutdsNwZ1Oc3sM/Ik/1Cqm54Vj51mnji4g6nuvlbsdprx8Aek45cMHV/FDMkmizLJVPlWLEnQhiNAcM9o5q5TdyORT0cpBcsA9iRJ8jOvKgAWvcuaGji12AVgPMIZr6O/fptZdsZ8NzT9gPI8WIazyQRnLQmE59SdSfXwbMUDAFNG7pTyrZiTNxiNOF4jeYdkv7Ps1Qr1a/xaJM+okNtIfs54OE/yPMmOekxkmOpQvfmMXJK/rUqcDB6s6Ho3N8JEhqlm1V8Jck6GBDnPrOuDi//DRIapC5pv/a+5iuREUDLv7uw10hW0xkQWoaAHbsFs3QNX3IjkmdU8pX/m0emgrMTpRhtSI09VX/L0xQJGgvPwbKOWTJfmleqK/og1UAa5L0EPnSHpurEItGQzPh30SNk8UQYifTVz1GuSJx1DXCvJUxpXRTFvaXLfVntG5ppLADbp12sAHgF4CeCd3sl8A9CudzWHABwBcCKYDmTTvAngepIkZU9RskzJ/9CtOu9nblvvZ+q5uToO4BiAwwAOANipR5Ham6sXAKbNN1cAfgEdrvAmqXHv5AAAAABJRU5ErkJggg=="/>
</svg>
                            </a>
                        </div>
					</nav>

					
					<!-- /nav -->


					<button class="menu-movil" id="menu-movil" >
						<i class="fa fa-bars" aria-hidden="true"></i>						
					</button>
					
				</div>


			</header>
			<!-- /header -->
		<!-- wrapper -->
		<div class="container">