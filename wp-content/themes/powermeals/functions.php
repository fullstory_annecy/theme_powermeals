<?php
/*
 *  Author: Todd Motto | @toddmotto
 *  URL: html5blank.com | @html5blank
 *  Custom functions, support, custom post types and more.
 */

/*------------------------------------*\
	External Modules/Files
\*------------------------------------*/

// Load any external files you have here

/*------------------------------------*\
	Theme Support
\*------------------------------------*/
if (function_exists('add_theme_support'))
{
    // Add Menu Support
    add_theme_support('menus');

    // Add Thumbnail Theme Support
    add_theme_support('post-thumbnails');
    add_image_size('large', 700, '', true); // Large Thumbnail
    add_image_size('medium', 250, '', true); // Medium Thumbnail
    add_image_size('small', 120, '', true); // Small Thumbnail
    add_image_size('custom-size', 700, 200, true); // Custom Thumbnail Size call using the_post_thumbnail('custom-size');

    // Add Support for Custom Backgrounds - Uncomment below if you're going to use
    /*add_theme_support('custom-background', array(
	'default-color' => 'FFF',
	'default-image' => get_template_directory_uri() . '/img/bg.jpg'
    ));*/

    // Add Support for Custom Header - Uncomment below if you're going to use
    /*add_theme_support('custom-header', array(
	'default-image'			=> get_template_directory_uri() . '/img/headers/default.jpg',
	'header-text'			=> false,
	'default-text-color'		=> '000',
	'width'				=> 1000,
	'height'			=> 198,
	'random-default'		=> false,
	'wp-head-callback'		=> $wphead_cb,
	'admin-head-callback'		=> $adminhead_cb,
	'admin-preview-callback'	=> $adminpreview_cb
    ));*/

    // Enables post and comment RSS feed links to head
    add_theme_support('automatic-feed-links');

    // Localisation Support
    load_theme_textdomain('html5blank', get_template_directory() . '/languages');
}

/*------------------------------------*\
	Functions
\*------------------------------------*/

// HTML5 Blank navigation
function html5blank_nav()
{
	wp_nav_menu(
	array(
		'theme_location'  => 'header-menu',
		'menu'            => '',
		'container'       => 'div',
		'container_class' => 'menu-{menu slug}-container',
		'container_id'    => '',
		'menu_class'      => 'menu',
		'menu_id'         => '',
		'echo'            => true,
		'fallback_cb'     => 'wp_page_menu',
		'before'          => '',
		'after'           => '',
		'link_before'     => '',
		'link_after'      => '',
		'items_wrap'      => '<ul>%3$s</ul>',
		'depth'           => 0,
		'walker'          => ''
		)
	);
}

// Load HTML5 Blank scripts (header.php)
function html5blank_header_scripts()
{
    if ($GLOBALS['pagenow'] != 'wp-login.php' && !is_admin()) {

    	wp_register_script('conditionizr', get_template_directory_uri() . '/js/lib/conditionizr-4.3.0.min.js', array(), '4.3.0'); // Conditionizr
        wp_enqueue_script('conditionizr'); // Enqueue it!

        wp_register_script('modernizr', get_template_directory_uri() . '/js/lib/modernizr-2.7.1.min.js', array(), '2.7.1'); // Modernizr
        wp_enqueue_script('modernizr'); // Enqueue it!

        wp_register_script('html5blankscripts', get_template_directory_uri() . '/js/scripts.js', array('jquery'), '1.0.0'); // Custom scripts
        wp_enqueue_script('html5blankscripts'); // Enqueue it!
    }
}

// Load HTML5 Blank conditional scripts
function html5blank_conditional_scripts()
{
    if (is_page('pagenamehere')) {
        wp_register_script('scriptname', get_template_directory_uri() . '/js/scriptname.js', array('jquery'), '1.0.0'); // Conditional script(s)
        wp_enqueue_script('scriptname'); // Enqueue it!
    }
}

// Load HTML5 Blank styles
function html5blank_styles()
{
    wp_register_style('normalize', get_template_directory_uri() . '/normalize.css', array(), '1.0', 'all');
    wp_enqueue_style('normalize'); // Enqueue it!

    wp_register_style('html5blank', get_template_directory_uri() . '/style.css', array(), '1.0', 'all');
    wp_enqueue_style('html5blank'); // Enqueue it!
}

// Register HTML5 Blank Navigation
function register_html5_menu()
{
    register_nav_menus(array( // Using array to specify more menus if needed
        'header-menu' => __('Header Menu', 'html5blank'), // Main Navigation
        'sidebar-menu' => __('Sidebar Menu', 'html5blank'), // Sidebar Navigation
        'extra-menu' => __('Extra Menu', 'html5blank') // Extra Navigation if needed (duplicate as many as you need!)
    ));
}

// Remove the <div> surrounding the dynamic navigation to cleanup markup
function my_wp_nav_menu_args($args = '')
{
    $args['container'] = false;
    return $args;
}

// Remove Injected classes, ID's and Page ID's from Navigation <li> items
function my_css_attributes_filter($var)
{
    return is_array($var) ? array() : '';
}

// Remove invalid rel attribute values in the categorylist
function remove_category_rel_from_category_list($thelist)
{
    return str_replace('rel="category tag"', 'rel="tag"', $thelist);
}

// Add page slug to body class, love this - Credit: Starkers Wordpress Theme
function add_slug_to_body_class($classes)
{
    global $post;
    if (is_home()) {
        $key = array_search('blog', $classes);
        if ($key > -1) {
            unset($classes[$key]);
        }
    } elseif (is_page()) {
        $classes[] = sanitize_html_class($post->post_name);
    } elseif (is_singular()) {
        $classes[] = sanitize_html_class($post->post_name);
    }

    return $classes;
}

// If Dynamic Sidebar Exists
if (function_exists('register_sidebar'))
{
    // Define Sidebar Widget Area 1
    register_sidebar(array(
        'name' => __('Widget Area 1', 'html5blank'),
        'description' => __('Description for this widget-area...', 'html5blank'),
        'id' => 'widget-area-1',
        'before_widget' => '<div id="%1$s" class="%2$s">',
        'after_widget' => '</div>',
        'before_title' => '<h3>',
        'after_title' => '</h3>'
    ));

    // Define Sidebar Widget Area 2
    register_sidebar(array(
        'name' => __('Widget Area 2', 'html5blank'),
        'description' => __('Description for this widget-area...', 'html5blank'),
        'id' => 'widget-area-2',
        'before_widget' => '<div id="%1$s" class="%2$s">',
        'after_widget' => '</div>',
        'before_title' => '<h3>',
        'after_title' => '</h3>'
    ));
}

// Remove wp_head() injected Recent Comment styles
function my_remove_recent_comments_style()
{
    global $wp_widget_factory;
    remove_action('wp_head', array(
        $wp_widget_factory->widgets['WP_Widget_Recent_Comments'],
        'recent_comments_style'
    ));
}

// Pagination for paged posts, Page 1, Page 2, Page 3, with Next and Previous Links, No plugin
function html5wp_pagination()
{
    global $wp_query;
    $big = 999999999;
    echo paginate_links(array(
        'base' => str_replace($big, '%#%', get_pagenum_link($big)),
        'format' => '?paged=%#%',
        'current' => max(1, get_query_var('paged')),
        'total' => $wp_query->max_num_pages
    ));
}

// Custom Excerpts
function html5wp_index($length) // Create 20 Word Callback for Index page Excerpts, call using html5wp_excerpt('html5wp_index');
{
    return 20;
}

// Create 40 Word Callback for Custom Post Excerpts, call using html5wp_excerpt('html5wp_custom_post');
function html5wp_custom_post($length)
{
    return 40;
}

// Create the Custom Excerpts callback
function html5wp_excerpt($length_callback = '', $more_callback = '')
{
    global $post;
    if (function_exists($length_callback)) {
        add_filter('excerpt_length', $length_callback);
    }
    if (function_exists($more_callback)) {
        add_filter('excerpt_more', $more_callback);
    }
    $output = get_the_excerpt();
    $output = apply_filters('wptexturize', $output);
    $output = apply_filters('convert_chars', $output);
    $output = '<p>' . $output . '</p>';
    echo $output;
}

// Custom View Article link to Post
function html5_blank_view_article($more)
{
    global $post;
    return '... <a class="view-article" href="' . get_permalink($post->ID) . '">' . __('View Article', 'html5blank') . '</a>';
}

// Remove Admin bar
function remove_admin_bar()
{
    return false;
}

// Remove 'text/css' from our enqueued stylesheet
function html5_style_remove($tag)
{
    return preg_replace('~\s+type=["\'][^"\']++["\']~', '', $tag);
}

// Remove thumbnail width and height dimensions that prevent fluid images in the_thumbnail
function remove_thumbnail_dimensions( $html )
{
    $html = preg_replace('/(width|height)=\"\d*\"\s/', "", $html);
    return $html;
}

// Custom Gravatar in Settings > Discussion
function html5blankgravatar ($avatar_defaults)
{
    $myavatar = get_template_directory_uri() . '/img/gravatar.jpg';
    $avatar_defaults[$myavatar] = "Custom Gravatar";
    return $avatar_defaults;
}

// Threaded Comments
function enable_threaded_comments()
{
    if (!is_admin()) {
        if (is_singular() AND comments_open() AND (get_option('thread_comments') == 1)) {
            wp_enqueue_script('comment-reply');
        }
    }
}

// Custom Comments Callback
function html5blankcomments($comment, $args, $depth)
{
	$GLOBALS['comment'] = $comment;
	extract($args, EXTR_SKIP);

	if ( 'div' == $args['style'] ) {
		$tag = 'div';
		$add_below = 'comment';
	} else {
		$tag = 'li';
		$add_below = 'div-comment';
	}
?>
    <!-- heads up: starting < for the html tag (li or div) in the next line: -->
    <<?php echo $tag ?> <?php comment_class(empty( $args['has_children'] ) ? '' : 'parent') ?> id="comment-<?php comment_ID() ?>">
	<?php if ( 'div' != $args['style'] ) : ?>
	<div id="div-comment-<?php comment_ID() ?>" class="comment-body">
	<?php endif; ?>
	<div class="comment-author vcard">
	<?php if ($args['avatar_size'] != 0) echo get_avatar( $comment, $args['180'] ); ?>
	<?php printf(__('<cite class="fn">%s</cite> <span class="says">says:</span>'), get_comment_author_link()) ?>
	</div>
<?php if ($comment->comment_approved == '0') : ?>
	<em class="comment-awaiting-moderation"><?php _e('Your comment is awaiting moderation.') ?></em>
	<br />
<?php endif; ?>

	<div class="comment-meta commentmetadata"><a href="<?php echo htmlspecialchars( get_comment_link( $comment->comment_ID ) ) ?>">
		<?php
			printf( __('%1$s at %2$s'), get_comment_date(),  get_comment_time()) ?></a><?php edit_comment_link(__('(Edit)'),'  ','' );
		?>
	</div>

	<?php comment_text() ?>

	<div class="reply">
	<?php comment_reply_link(array_merge( $args, array('add_below' => $add_below, 'depth' => $depth, 'max_depth' => $args['max_depth']))) ?>
	</div>
	<?php if ( 'div' != $args['style'] ) : ?>
	</div>
	<?php endif; ?>
<?php }

/*------------------------------------*\
	Actions + Filters + ShortCodes
\*------------------------------------*/

// Add Actions
add_action('init', 'html5blank_header_scripts'); // Add Custom Scripts to wp_head
add_action('wp_print_scripts', 'html5blank_conditional_scripts'); // Add Conditional Page Scripts
add_action('get_header', 'enable_threaded_comments'); // Enable Threaded Comments
add_action('wp_enqueue_scripts', 'html5blank_styles'); // Add Theme Stylesheet
add_action('init', 'register_html5_menu'); // Add HTML5 Blank Menu

add_action('widgets_init', 'my_remove_recent_comments_style'); // Remove inline Recent Comment Styles from wp_head()
add_action('init', 'html5wp_pagination'); // Add our HTML5 Pagination

// Remove Actions
remove_action('wp_head', 'feed_links_extra', 3); // Display the links to the extra feeds such as category feeds
remove_action('wp_head', 'feed_links', 2); // Display the links to the general feeds: Post and Comment Feed
remove_action('wp_head', 'rsd_link'); // Display the link to the Really Simple Discovery service endpoint, EditURI link
remove_action('wp_head', 'wlwmanifest_link'); // Display the link to the Windows Live Writer manifest file.
remove_action('wp_head', 'index_rel_link'); // Index link
remove_action('wp_head', 'parent_post_rel_link', 10, 0); // Prev link
remove_action('wp_head', 'start_post_rel_link', 10, 0); // Start link
remove_action('wp_head', 'adjacent_posts_rel_link', 10, 0); // Display relational links for the posts adjacent to the current post.
remove_action('wp_head', 'wp_generator'); // Display the XHTML generator that is generated on the wp_head hook, WP version
remove_action('wp_head', 'adjacent_posts_rel_link_wp_head', 10, 0);
remove_action('wp_head', 'rel_canonical');
remove_action('wp_head', 'wp_shortlink_wp_head', 10, 0);

// Add Filters
add_filter('avatar_defaults', 'html5blankgravatar'); // Custom Gravatar in Settings > Discussion
add_filter('body_class', 'add_slug_to_body_class'); // Add slug to body class (Starkers build)
add_filter('widget_text', 'do_shortcode'); // Allow shortcodes in Dynamic Sidebar
add_filter('widget_text', 'shortcode_unautop'); // Remove <p> tags in Dynamic Sidebars (better!)
add_filter('wp_nav_menu_args', 'my_wp_nav_menu_args'); // Remove surrounding <div> from WP Navigation
// add_filter('nav_menu_css_class', 'my_css_attributes_filter', 100, 1); // Remove Navigation <li> injected classes (Commented out by default)
// add_filter('nav_menu_item_id', 'my_css_attributes_filter', 100, 1); // Remove Navigation <li> injected ID (Commented out by default)
// add_filter('page_css_class', 'my_css_attributes_filter', 100, 1); // Remove Navigation <li> Page ID's (Commented out by default)
add_filter('the_category', 'remove_category_rel_from_category_list'); // Remove invalid rel attribute
add_filter('the_excerpt', 'shortcode_unautop'); // Remove auto <p> tags in Excerpt (Manual Excerpts only)
add_filter('the_excerpt', 'do_shortcode'); // Allows Shortcodes to be executed in Excerpt (Manual Excerpts only)
add_filter('excerpt_more', 'html5_blank_view_article'); // Add 'View Article' button instead of [...] for Excerpts
add_filter('show_admin_bar', 'remove_admin_bar'); // Remove Admin bar
add_filter('style_loader_tag', 'html5_style_remove'); // Remove 'text/css' from enqueued stylesheet
add_filter('post_thumbnail_html', 'remove_thumbnail_dimensions', 10); // Remove width and height dynamic attributes to thumbnails
add_filter('image_send_to_editor', 'remove_thumbnail_dimensions', 10); // Remove width and height dynamic attributes to post images

// Remove Filters
remove_filter('the_excerpt', 'wpautop'); // Remove <p> tags from Excerpt altogether

// Shortcodes
add_shortcode('html5_shortcode_demo', 'html5_shortcode_demo'); // You can place [html5_shortcode_demo] in Pages, Posts now.
add_shortcode('html5_shortcode_demo_2', 'html5_shortcode_demo_2'); // Place [html5_shortcode_demo_2] in Pages, Posts now.

// Shortcodes above would be nested like this -
// [html5_shortcode_demo] [html5_shortcode_demo_2] Here's the page title! [/html5_shortcode_demo_2] [/html5_shortcode_demo]


/*------------------------------------*\
	ShortCode Functions
\*------------------------------------*/

// Shortcode Demo with Nested Capability
function html5_shortcode_demo($atts, $content = null)
{
    return '<div class="shortcode-demo">' . do_shortcode($content) . '</div>'; // do_shortcode allows for nested Shortcodes
}

// Shortcode Demo with simple <h2> tag
function html5_shortcode_demo_2($atts, $content = null) // Demo Heading H2 shortcode, allows for nesting within above element. Fully expandable.
{
    return '<h2>' . $content . '</h2>';
}

function contact_short_code(){

    if (ICL_LANGUAGE_CODE == "fr") {
        echo do_shortcode('[contact-form-7 id="599" title="Contact form_fr"]');
    } elseif (ICL_LANGUAGE_CODE == "de") {
        echo do_shortcode('[contact-form-7 id="600" title="Contact form_de"]');
    } else {
        echo do_shortcode('[contact-form-7 id="5" title="Contact form"]');
    }

}

function tabla23() {
echo '<table class="calorias">
	  <tr class="calorias2">
	    <th>Breakfast</th>
	    <th>Snack</th>
	    <th>Meals Per Day</th>
	    <th>Days Per Week</th>
	    <th>Variant</th>
	  </tr>
	  <tr class="calorias3">
	    <td id="ValueBreakfast">NO</td>
	    <td id="ValueSnack">NO</td>
	    <td id="CountMealsDay">1</td>
	    <td id="CountMealsWeek">1</td>
	    <td id="IntervalDelivery">CLASSIC</td>
	  </tr>
	</table>';
}

function imagenkilocalorias() {
echo '<span class="green-buttom-heavy"><img src="https://powermeals.ch/wp-content/uploads/2019/12/kilocalorias.png"><span>';
}


add_shortcode( 'contact_power', 'contact_short_code' );

remove_action('woocommerce_single_product_summary','woocommerce_template_single_title',5);
add_action('woocommerce_before_single_product_summary','woocommerce_template_single_title',5);

remove_action('woocommerce_single_product_summary','woocommerce_template_single_rating',10);
add_action('woocommerce_before_single_product_summary','woocommerce_template_single_rating',7);

add_action('woocommerce_before_single_product_summary','go_add_text',7);
function go_add_text() {
echo '<div class="pull2">From SFr. 9.90 / full meal</div>';
}
remove_action('woocommerce_single_product_summary','woocommerce_template_single_price',10);
add_action('woocommerce_single_product_summary','tabla23',39);
add_action('woocommerce_single_product_summary','woocommerce_template_single_price',39);
remove_action('woocommerce_single_product_summary','woocommerce_template_single_meta',40);
remove_action('woocommerce_single_product_summary','woocommerce_template_single_excerpt',20);
remove_action('woocommerce_single_product_summary','woocommerce_template_single_add_to_cart',30);
add_action('woocommerce_single_product_summary','woocommerce_template_single_add_to_cart',60);
add_action('woocommerce_single_product_summary','imagenkilocalorias',59);

add_filter( 'woocommerce_add_cart_item_data', 'addcustomdata',1, 2);


function addcustomdata ($cart_item_data, $product_id) {

  global $woocommerce;
  $cart_item_data['precio'] = sanitize_text_field($_POST['cprice']);

  return $cart_item_data;

}





add_action( 'woocommerce_before_calculate_totals', 'add_custom_price',2000,1);


function add_custom_price( $cart_object ) {
  //global $woocommerce;

    // This is necessary for WC 3.0+
    if ( is_admin() && ! defined( 'DOING_AJAX' ) )
        return;

    // Avoiding hook repetition (when using price calculations for example)
    if ( did_action( 'woocommerce_before_calculate_totals' ) >= 2 )
        return;

   /// Loop through cart items
    foreach ( $cart_object->cart_contents as $key => $value  ) {

        $product = $value['data'];
        $data=$product->get_data();



        //if($data['id']==787) {


            // discount
           /* $theprice=$value['precio'];
            $discount=($theprice/100)*10;
            $theprice=$theprice-$discount;

            $value['data']->set_price($theprice); */

            /*if($value['wccpf_subscription']['user_val'][0]=='None') {
                 $product_id = 787;
                   $product_cart_id = WC()->cart->generate_cart_id( $product_id );
                   $cart_item_key = WC()->cart->find_product_in_cart( $product_cart_id );
                   if ( $cart_item_key ) {
                     WC()->cart->remove_cart_item( $cart_item_key );
                   }
            }*/

        //}

        //if($data['id']==1287) {

            $value['data']->set_price($value['precio']);
        //}

    }

    //var_dump($cart);

    /*foreach ( $cart_object->cart_contents as $key => $value ) {
        $custom_price = 5;
        $value['data']->set_price(1000);
    }*/

}

/*add_action("template_redirect", 'redirection_function_woo');
function redirection_function_woo(){
    global $woocommerce;
    if( is_product() && WC()->cart->cart_contents_count > 0){
        wp_safe_redirect( wc_get_cart_url() );
    } else if(is_product()) {
        $product = new WC_Product_Simple(787);
        $product->set_regular_price(0);
        $product->save();
    }
}*/


// define the woocommerce_after_add_to_cart_quantity callback
function action_woocommerce_after_add_to_cart_quantity(  ) {
    echo '<input id="upprice" type="hidden" value="0" name="cprice">';
   /* echo '<script>

jQuery( document ).ready(function() {
    console.log("ready simple");
jQuery("body").on("DOMSubtreeModified", ".price", function() {
    var pche=jQuery(".price .woocommerce-Price-amount").text();
    var pche = pche.split("$");
    console.log(pche[1]);
    jQuery("#upprice").val(pche[1]);
});

var mainpid=jQuery(".single_add_to_cart_button.button").val();
jQuery(".wccpf_fields_table.subscription-wrapper label").click(function() {
    var inputval=jQuery(this).find("input").val();

    if (!jQuery(this).hasClass("wcff-active")) {
        if(inputval=="None") {
            jQuery(".single_add_to_cart_button.button").val("1287");
        } else {
            jQuery(".single_add_to_cart_button.button").val(mainpid);
        }
    } else {
        if(inputval=="None") {
            jQuery(".single_add_to_cart_button.button").val(mainpid);
        }
    }
});

});

</script>';*/
};

// add the action
add_action( 'woocommerce_after_add_to_cart_quantity', 'action_woocommerce_after_add_to_cart_quantity', 10, 0 );


add_action( 'wp_logout', 'auto_redirect_external_after_logout');
function auto_redirect_external_after_logout(){
  wp_redirect( home_url() );
  exit();
}

/**
 * Change a currency symbol
 */
add_filter('woocommerce_currency_symbol', 'change_existing_currency_symbol', 10, 2);

function change_existing_currency_symbol( $currency_symbol, $currency ) {
     switch( $currency ) {
          case 'CHF': $currency_symbol = 'SFr'; break;
     }
     return $currency_symbol;
}

/**
 * Bypass logout confirmation.
 */
function iconic_bypass_logout_confirmation() {
    global $wp;

    if ( isset( $wp->query_vars['customer-logout'] ) ) {
        wp_redirect( str_replace( '&amp;', '&', wp_logout_url( wc_get_page_permalink( 'myaccount' ) ) ) );
        exit;
    }
}

add_action( 'template_redirect', 'iconic_bypass_logout_confirmation' );

// Limit Woocommerce phone field to 10 digits number
add_action('woocommerce_checkout_process', 'my_custom_checkout_field_process');

function my_custom_checkout_field_process() {
    global $woocommerce;

    // Check if set, if its not set add an error. This one is only requite for companies
    if ( ! (preg_match('/^[0-9]{10}$/D', $_POST['billing_phone'] ))){
        wc_add_notice( "Incorrect Phone Number! Please enter valid 10 digits phone number"  ,'error' );
    }
}

add_filter( 'woocommerce_checkout_fields' , 'custom_rename_wc_checkout_fields' );

// Change placeholder and label text
function custom_rename_wc_checkout_fields( $fields ) {
$fields['billing']['billing_phone']['label'] = 'Mobile phone number';
return $fields;
}



// Hook in
add_filter( 'woocommerce_checkout_fields' , 'custom_override_checkout_fields' );

// Our hooked in function - $fields is passed via the filter!
function custom_override_checkout_fields( $fields ) {
	
	$fields['shipping']['_shipping_phone_number'] = array(
        'label'     => __('Mobile phone number', 'woocommerce'),
    'required'  => true,
    'class'     => array('form-row-wide'),
    'clear'     => true
     );
	
	$fields['shipping']['shipping_email'] = array(
        'label'     => __('Email address', 'woocommerce'),
    'required'  => true,
    'class'     => array('form-row-wide'),
    'clear'     => true
     );

     return $fields;
}

// Billing and Shipping fields on my account edit-addresses and checkout
add_filter( 'woocommerce_default_address_fields' , 'custom_override_default_address_fields' );
function custom_override_default_address_fields( $fields ) {
    $fields['first_name']['label'] = 'First name';
    $fields['last_name']['label'] = 'Last name';
    $fields['company']['label'] = 'Company name';
    $fields['address_1']['label'] = 'Street address';
    $fields['address_2']['label'] = 'Apartment, unit, etc.';
    $fields['city']['label'] = 'City';
    $fields['country']['label'] = 'Country';
    $fields['state']['label'] = 'County/State';
    $fields['postcode']['label'] = 'Postcode';
	$fields['house_number']['label'] = 'House number';

    return $fields;
}

// Billing fields on my account edit-addresses and checkout
add_filter( 'woocommerce_billing_fields' , 'custom_billing_fields' );
function custom_billing_fields( $fields ) {

    // Billing Fields
    $fields['billing_first_name']['label'] = 'First name';
    $fields['billing_last_name']['label'] = 'Last name';
    $fields['billing_company']['label'] = 'Company name';
    $fields['billing_address_1']['label'] = 'Street address';
    $fields['billing_address_2']['label'] = 'Apartment, unit, etc.';
    $fields['billing_city']['label'] = 'City';
    $fields['billing_country']['label'] = 'Country';
    $fields['billing_state']['label'] = 'County/State';
    $fields['billing_postcode']['label'] = 'Postcode';
    $fields['billing_email']['label'] = 'Email';
    $fields['billing_phone']['label'] = 'Phone';
	$fields['billing_house_number']['label'] = 'House number';

    return $fields;
}

// Shipping fields on my account edit-addresses and checkout
add_filter( 'woocommerce_shipping_fields' , 'custom_shipping_fields' );
function custom_shipping_fields( $fields ) {

    // Shipping Fields
    $fields['shipping_first_name']['label'] = 'First name';
    $fields['shipping_last_name']['label'] = 'Last name';
    $fields['shipping_company']['label'] = 'Company name';
    $fields['shipping_address_1']['label'] = 'Street address';
    $fields['shipping_address_2']['label'] = 'Apartment, unit, etc.';
    $fields['shipping_city']['label'] = 'City';
    $fields['shipping_country']['label'] = 'Country';
    $fields['shipping_state']['label'] = 'County/State';
    $fields['shipping_postcode']['label'] = 'Postcode';
    $fields['shipping_email']['label'] = 'Email';
    $fields['shipping_phone']['label'] = 'Phone';
	$fields['shipping_house_number']['label'] = 'House number';
	

    return $fields;
}

// All fields only on checkout
add_filter( 'woocommerce_checkout_fields' , 'other_custom_checkout_fields' );
function other_custom_checkout_fields( $fields ) {

    // Account Fields
    $fields['account']['account_username']['label'] = 'Username or email';
    $fields['account']['account_password']['label'] = 'Password';

    // Order Fields
    $fields['order']['order_comments']['label'] = 'Order notes';

    return $fields;
}







//Arnaud 
foreach(glob(get_template_directory() . "/functions/function_*.php") as $file){
	require $file;
}


