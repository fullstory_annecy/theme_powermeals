<?php


function wpse_226690_admin_menu() {
    add_menu_page(
        'Powermeals Plan',
        'Powermeals Plan',
        'read',
        'pm-plan-manager',
        '', // Callback, leave empty
        'dashicons-calendar',
        1 // Position
    );
}

add_action( 'admin_menu', 'wpse_226690_admin_menu' );


function create_posttype() {
 
    register_post_type( 'meals',
    // CPT Options
        array(
            'labels' => array(
                'name' => __( 'Meals' ),
                'singular_name' => __( 'Meals' )
            ),
            'public' => true,
            'has_archive' => true,
            'rewrite' => array('slug' => 'meals'),
            'show_in_rest' => true,
			'show_in_menu' => 'pm-plan-manager',
        )
    );
	
	register_post_type( 'variants',
    // CPT Options
        array(
            'labels' => array(
                'name' => __( 'Variants' ),
                'singular_name' => __( 'Variants' )
            ),
            'public' => true,
            'has_archive' => true,
            'rewrite' => array('slug' => 'variants'),
            'show_in_rest' => true,
			'show_in_menu' => 'pm-plan-manager',
        )
    );
	
	register_post_type( 'day_per_week',
    // CPT Options
        array(
            'labels' => array(
                'name' => __( 'Day per week' ),
                'singular_name' => __( 'Day per week' )
            ),
            'public' => true,
            'has_archive' => true,
            'rewrite' => array('slug' => 'day_per_week'),
            'show_in_rest' => true,
			'show_in_menu' => 'pm-plan-manager',
        )
    );
	
	register_post_type( 'meals_conbination',
    // CPT Options
        array(
            'labels' => array(
                'name' => __( 'Meals combination' ),
                'singular_name' => __( 'Meals combination' )
            ),
            'public' => true,
            'has_archive' => true,
            'rewrite' => array('slug' => 'meals_conbination'),
            'show_in_rest' => true,
			'show_in_menu' => 'pm-plan-manager',
        )
    );
	
	
	
	
}
// Hooking up our function to theme setup
add_action( 'init', 'create_posttype' );