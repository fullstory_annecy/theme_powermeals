<?php

/* plugins-wrap.twig */
class __TwigTemplate_8315c3b034dd54847b1c85fe96d1adcd5d1f1e05cec37efdcd3f8d709e0c5201 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        // line 1
        echo "<div class=\"wrap\">
    <h1>";
        // line 2
        echo twig_escape_filter($this->env, $this->getAttribute(($context["strings"] ?? null), "title", array()), "html", null, true);
        echo "</h1>

    <nav class=\"wcml-tabs wpml-tabs\" style=\"display:table;margin-top:30px;\">
        <a class=\"nav-tab nav-tab-active\" href=\"";
        // line 5
        echo twig_escape_filter($this->env, ($context["link_url"] ?? null));
        echo "\" >";
        echo twig_escape_filter($this->env, $this->getAttribute(($context["strings"] ?? null), "required", array()), "html", null, true);
        echo "</a>
    </nav>

    <div class=\"wcml-wrap\">
        <div class=\"wcml-section\">
            <div class=\"wcml-section-header\">
                <h3>
                    ";
        // line 12
        echo twig_escape_filter($this->env, $this->getAttribute(($context["strings"] ?? null), "plugins", array()), "html", null, true);
        echo "
                    <i class=\"otgs-ico-help wcml-tip\"
                       data-tip=\"";
        // line 14
        echo twig_escape_filter($this->env, $this->getAttribute(($context["strings"] ?? null), "depends", array()), "html", null, true);
        echo "\"></i>
                </h3>
            </div>
            <div class=\"wcml-section-content wcml-section-content-wide\">
                <ul>
                    ";
        // line 19
        if (($context["old_wpml"] ?? null)) {
            // line 20
            echo "                        <li>
                            <i class=\"otgs-ico-warning\"></i>
                            ";
            // line 22
            echo $this->getAttribute(($context["strings"] ?? null), "old_wpml_link", array());
            echo "
                            <a href=\"";
            // line 23
            echo twig_escape_filter($this->env, ($context["tracking_link"] ?? null), "html", null, true);
            echo "\"
                               target=\"_blank\">";
            // line 24
            echo twig_escape_filter($this->env, $this->getAttribute(($context["strings"] ?? null), "update_wpml", array()), "html", null, true);
            echo "</a>
                        </li>
                    ";
        } elseif (        // line 26
($context["icl_version"] ?? null)) {
            // line 27
            echo "                        <li>
                            <i class=\"otgs-ico-ok\"></i>
                            ";
            // line 29
            echo sprintf($this->getAttribute(($context["strings"] ?? null), "inst_active", array()), $this->getAttribute(($context["strings"] ?? null), "wpml", array()));
            echo "
                        </li>
                        ";
            // line 31
            if (($context["icl_setup"] ?? null)) {
                // line 32
                echo "                            <li>
                                <i class=\"otgs-ico-ok\"></i>
                                ";
                // line 34
                echo sprintf($this->getAttribute(($context["strings"] ?? null), "is_setup", array()), $this->getAttribute(($context["strings"] ?? null), "wpml", array()));
                echo "
                            </li>
                        ";
            } else {
                // line 37
                echo "                            <li>
                                <i class=\"otgs-ico-warning\"></i>
                                ";
                // line 39
                echo sprintf($this->getAttribute(($context["strings"] ?? null), "not_setup", array()), $this->getAttribute(($context["strings"] ?? null), "wpml", array()));
                echo "
                            </li>
                        ";
            }
            // line 42
            echo "                    ";
        } else {
            // line 43
            echo "                        <li>
                            <i class=\"otgs-ico-warning\"></i>
                            ";
            // line 45
            echo $this->getAttribute(($context["strings"] ?? null), "wpml_not_inst", array());
            echo "
                            <a href=\"";
            // line 46
            echo twig_escape_filter($this->env, ($context["install_wpml_link"] ?? null));
            echo "\" target=\"_blank\">";
            echo twig_escape_filter($this->env, $this->getAttribute(($context["strings"] ?? null), "get_wpml", array()), "html", null, true);
            echo "</a>
                        </li>
                    ";
        }
        // line 49
        echo "
                    ";
        // line 50
        if (($context["media_version"] ?? null)) {
            // line 51
            echo "                        <li>
                            <i class=\"otgs-ico-ok\"></i>
                            ";
            // line 53
            echo sprintf($this->getAttribute(($context["strings"] ?? null), "inst_active", array()), $this->getAttribute(($context["strings"] ?? null), "media", array()));
            echo "
                        </li>
                    ";
        } else {
            // line 56
            echo "                        <li>
                            <i class=\"otgs-ico-warning\"></i>
                            ";
            // line 58
            echo sprintf($this->getAttribute(($context["strings"] ?? null), "not_inst", array()), $this->getAttribute(($context["strings"] ?? null), "media", array()));
            echo "
                            <a href=\"";
            // line 59
            echo twig_escape_filter($this->env, ($context["install_wpml_link"] ?? null));
            echo "\" target=\"_blank\">";
            echo twig_escape_filter($this->env, $this->getAttribute(($context["strings"] ?? null), "get_wpml_media", array()), "html", null, true);
            echo "</a>
                        </li>
                    ";
        }
        // line 62
        echo "
                    ";
        // line 63
        if (($context["tm_version"] ?? null)) {
            // line 64
            echo "                        <li>
                            <i class=\"otgs-ico-ok\"></i>
                            ";
            // line 66
            echo sprintf($this->getAttribute(($context["strings"] ?? null), "inst_active", array()), $this->getAttribute(($context["strings"] ?? null), "tm", array()));
            echo "
                        </li>
                    ";
        } else {
            // line 69
            echo "                        <li>
                            <i class=\"otgs-ico-warning\"></i>
                            ";
            // line 71
            echo sprintf($this->getAttribute(($context["strings"] ?? null), "not_inst", array()), $this->getAttribute(($context["strings"] ?? null), "tm", array()));
            echo "
                            <a href=\"";
            // line 72
            echo twig_escape_filter($this->env, ($context["install_wpml_link"] ?? null));
            echo "\" target=\"_blank\">";
            echo twig_escape_filter($this->env, $this->getAttribute(($context["strings"] ?? null), "get_wpml_tm", array()), "html", null, true);
            echo "</a>
                        </li>
                    ";
        }
        // line 75
        echo "
                    ";
        // line 76
        if (($context["st_version"] ?? null)) {
            // line 77
            echo "                        <li>
                            <i class=\"otgs-ico-ok\"></i>
                            ";
            // line 79
            echo sprintf($this->getAttribute(($context["strings"] ?? null), "inst_active", array()), $this->getAttribute(($context["strings"] ?? null), "st", array()));
            echo "
                        </li>
                    ";
        } else {
            // line 82
            echo "                        <li>
                            <i class=\"otgs-ico-warning\"></i>
                            ";
            // line 84
            echo sprintf($this->getAttribute(($context["strings"] ?? null), "not_inst", array()), $this->getAttribute(($context["strings"] ?? null), "st", array()));
            echo "
                            <a href=\"";
            // line 85
            echo twig_escape_filter($this->env, ($context["install_wpml_link"] ?? null));
            echo "\" target=\"_blank\">";
            echo twig_escape_filter($this->env, $this->getAttribute(($context["strings"] ?? null), "get_wpml_st", array()), "html", null, true);
            echo "</a>
                        </li>
                    ";
        }
        // line 88
        echo "
                    ";
        // line 89
        if (($context["old_wc"] ?? null)) {
            // line 90
            echo "                        <li>
                            <i class=\"otgs-ico-warning\"></i>
                            ";
            // line 92
            echo $this->getAttribute(($context["strings"] ?? null), "old_wc", array());
            echo "
                            <a href=\"";
            // line 93
            echo twig_escape_filter($this->env, ($context["wc_link"] ?? null));
            echo "\" target=\"_blank\">";
            echo twig_escape_filter($this->env, $this->getAttribute(($context["strings"] ?? null), "download_wc", array()), "html", null, true);
            echo "</a>
                        </li>
                    ";
        } elseif (        // line 95
($context["wc"] ?? null)) {
            // line 96
            echo "                        <li>
                            <i class=\"otgs-ico-ok\"></i>
                            ";
            // line 98
            echo sprintf($this->getAttribute(($context["strings"] ?? null), "inst_active", array()), $this->getAttribute(($context["strings"] ?? null), "wc", array()));
            echo "
                        </li>
                    ";
        } else {
            // line 101
            echo "                        <li>
                            <i class=\"otgs-ico-warning\"></i>
                            ";
            // line 103
            echo sprintf($this->getAttribute(($context["strings"] ?? null), "not_inst", array()), $this->getAttribute(($context["strings"] ?? null), "wc", array()));
            echo "
                            <a href=\"";
            // line 104
            echo twig_escape_filter($this->env, ($context["wc_link"] ?? null));
            echo "\" target=\"_blank\">";
            echo twig_escape_filter($this->env, $this->getAttribute(($context["strings"] ?? null), "download_wc", array()), "html", null, true);
            echo "</a>
                        </li>
                    ";
        }
        // line 107
        echo "                </ul>
            </div>
        </div>
    </div>
</div>

";
    }

    public function getTemplateName()
    {
        return "plugins-wrap.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  265 => 107,  257 => 104,  253 => 103,  249 => 101,  243 => 98,  239 => 96,  237 => 95,  230 => 93,  226 => 92,  222 => 90,  220 => 89,  217 => 88,  209 => 85,  205 => 84,  201 => 82,  195 => 79,  191 => 77,  189 => 76,  186 => 75,  178 => 72,  174 => 71,  170 => 69,  164 => 66,  160 => 64,  158 => 63,  155 => 62,  147 => 59,  143 => 58,  139 => 56,  133 => 53,  129 => 51,  127 => 50,  124 => 49,  116 => 46,  112 => 45,  108 => 43,  105 => 42,  99 => 39,  95 => 37,  89 => 34,  85 => 32,  83 => 31,  78 => 29,  74 => 27,  72 => 26,  67 => 24,  63 => 23,  59 => 22,  55 => 20,  53 => 19,  45 => 14,  40 => 12,  28 => 5,  22 => 2,  19 => 1,);
    }

    /** @deprecated since 1.27 (to be removed in 2.0). Use getSourceContext() instead */
    public function getSource()
    {
        @trigger_error('The '.__METHOD__.' method is deprecated since version 1.27 and will be removed in 2.0. Use getSourceContext() instead.', E_USER_DEPRECATED);

        return $this->getSourceContext()->getCode();
    }

    public function getSourceContext()
    {
        return new Twig_Source("", "plugins-wrap.twig", "/home/clients/2bc3dd043387cca95b6bea439474a4c7/web/wp-content/plugins/woocommerce-multilingual/templates/plugins-wrap.twig");
    }
}
