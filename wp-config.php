<?php
/**
 * The base configuration for WordPress
 *
 * The wp-config.php creation script uses this file during the
 * installation. You don't have to use the web site, you can
 * copy this file to "wp-config.php" and fill in the values.
 *
 * This file contains the following configurations:
 *
 * * MySQL settings
 * * Secret keys
 * * Database table prefix
 * * ABSPATH
 *
 * @link https://codex.wordpress.org/Editing_wp-config.php
 *
 * @package WordPress
 */
// ** MySQL settings - You can get this info from your web host ** //
/** The name of the database for WordPress */
define( 'DB_NAME', "wp_powermeals" );
/** MySQL database username */
define( 'DB_USER', "root" );
/** MySQL database password */
define( 'DB_PASSWORD', "pHWdebIJDs" );
/** MySQL hostname */
define( 'DB_HOST', "localhost" );
/** Database Charset to use in creating database tables. */
define( 'DB_CHARSET', 'utf8mb4' );
/** The Database Collate type. Don't change this if in doubt. */
define( 'DB_COLLATE', '' );
define('WP_MEMORY_LIMIT', '256M');
/**#@+
 * Authentication Unique Keys and Salts.
 *
 * Change these to different unique phrases!
 * You can generate these using the {@link https://api.wordpress.org/secret-key/1.1/salt/ WordPress.org secret-key service}
 * You can change these at any point in time to invalidate all existing cookies. This will force all users to have to log in again.
 *
 * @since 2.6.0
 */
define( 'AUTH_KEY',         '_J9inm)fZ*D7 /RWJb+rz46hJde55Fho_5fhckBa@Z+T[xRq`3F>2`UV$zC{)_io' );
define( 'SECURE_AUTH_KEY',  'guXZ+S,q0P G%xdzXlS0[BP>Ip<<N`75|$M4k:KD =lFw6|zvD/_JJmgs+AydUqK' );
define( 'LOGGED_IN_KEY',    '<`q8PX=(AM:#_zUM8k1=J$?<#}%Bqw6pr0H%]=0h=C# 7W1azVSgpXk5[YCO1vO:' );
define( 'NONCE_KEY',        'qh)MRC#xGSPp&%%|KI*|IqjN]^a_P5wMIz$/uYodw.(|~G_-%TqkmEu-DQ@hIyij' );
define( 'AUTH_SALT',        'g(1!TVBKN]PYB$2R`V8O)Uo-Ceg;&bsp`>v_l<HZeJP+]G`7OodVa1A:>G^HQ+RV' );
define( 'SECURE_AUTH_SALT', 'T{~Fp5?rSGxC^FJP#Z+#@yQJ+l~KP_d>@k|xlWemoSO,=Hv(%!U})syDcb)HcR~?' );
define( 'LOGGED_IN_SALT',   'GiZ$jUgBf&>Z]n9>)zIaZd&0oSN~)(w{<fg{/g2RpoPFJn6$v8P54>F03R&M}w~C' );
define( 'NONCE_SALT',       'Q)t`La{(WCmK}dP5$P72FW8DZJlEWI.5H;+r:uPkHTgS9$0,tgu=]Mpz)llt&ZoP' );
/**#@-*/
/**
 * WordPress Database Table prefix.
 *
 * You can have multiple installations in one database if you give each
 * a unique prefix. Only numbers, letters, and underscores please!
 */
$table_prefix = 'mp_';
/**
 * For developers: WordPress debugging mode.
 *
 * Change this to true to enable the display of notices during development.
 * It is strongly recommended that plugin and theme developers use WP_DEBUG
 * in their development environments.
 *
 * For information on other constants that can be used for debugging,
 * visit the Codex.
 *
 * @link https://codex.wordpress.org/Debugging_in_WordPress
 */
//define( 'WP_DEBUG', true );
//ini_set('display_errors','Off');
//ini_set('error_reporting', E_ALL );
//define('WP_DEBUG', true);
//define('WP_DEBUG_DISPLAY', true);
/*define( 'WP_DEBUG_LOG', true );*/
/*define( 'WP_DEBUG_DISPLAY', true );*/
define( 'AUTOSAVE_INTERVAL', 300 );
define( 'WP_POST_REVISIONS', 5 );
define( 'EMPTY_TRASH_DAYS', 7 );
define( 'WP_CRON_LOCK_TIMEOUT', 120 );
/* That's all, stop editing! Happy publishing. */
/** Absolute path to the WordPress directory. */
if ( ! defined( 'ABSPATH' ) ) {
    define( 'ABSPATH', dirname( __FILE__ ) . '/' );
}
/** Sets up WordPress vars and included files. */
require_once( ABSPATH . 'wp-settings.php' );